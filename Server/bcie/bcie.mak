# Microsoft Developer Studio Generated NMAKE File, Based on bcie.dsp
!IF "$(CFG)" == ""
CFG=bcie - Win32 Debug
!MESSAGE No configuration specified. Defaulting to bcie - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "bcie - Win32 Release" && "$(CFG)" != "bcie - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "bcie.mak" CFG="bcie - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "bcie - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "bcie - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "bcie - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release
# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\bcie.exe"

!ELSE 

ALL : "$(OUTDIR)\bcie.exe"

!ENDIF 

CLEAN :
	-@erase "$(INTDIR)\auxiliar.obj"
	-@erase "$(INTDIR)\bcie.obj"
	-@erase "$(INTDIR)\busqueda.obj"
	-@erase "$(INTDIR)\insert.obj"
	-@erase "$(INTDIR)\saveload.obj"
	-@erase "$(INTDIR)\search.obj"
	-@erase "$(INTDIR)\vc50.idb"
	-@erase "$(OUTDIR)\bcie.exe"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP_PROJ=/nologo /ML /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D\
 "_MBCS" /Fp"$(INTDIR)\bcie.pch" /YX /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
CPP_OBJS=.\Release/
CPP_SBRS=.
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\bcie.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib\
 advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib\
 odbccp32.lib /nologo /subsystem:console /incremental:no\
 /pdb:"$(OUTDIR)\bcie.pdb" /machine:I386 /out:"$(OUTDIR)\bcie.exe" 
LINK32_OBJS= \
	"$(INTDIR)\auxiliar.obj" \
	"$(INTDIR)\bcie.obj" \
	"$(INTDIR)\busqueda.obj" \
	"$(INTDIR)\insert.obj" \
	"$(INTDIR)\saveload.obj" \
	"$(INTDIR)\search.obj"

"$(OUTDIR)\bcie.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "bcie - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug
# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\bcie.exe"

!ELSE 

ALL : "$(OUTDIR)\bcie.exe"

!ENDIF 

CLEAN :
	-@erase "$(INTDIR)\auxiliar.obj"
	-@erase "$(INTDIR)\bcie.obj"
	-@erase "$(INTDIR)\busqueda.obj"
	-@erase "$(INTDIR)\insert.obj"
	-@erase "$(INTDIR)\saveload.obj"
	-@erase "$(INTDIR)\search.obj"
	-@erase "$(INTDIR)\vc50.idb"
	-@erase "$(INTDIR)\vc50.pdb"
	-@erase "$(OUTDIR)\bcie.exe"
	-@erase "$(OUTDIR)\bcie.ilk"
	-@erase "$(OUTDIR)\bcie.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP_PROJ=/nologo /Zp1 /MLd /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D\
 "_CONSOLE" /D "_MBCS" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
CPP_OBJS=.\Debug/
CPP_SBRS=.
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\bcie.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib\
 advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib\
 odbccp32.lib /nologo /subsystem:console /incremental:yes\
 /pdb:"$(OUTDIR)\bcie.pdb" /debug /machine:I386 /out:"$(OUTDIR)\bcie.exe"\
 /pdbtype:sept 
LINK32_OBJS= \
	"$(INTDIR)\auxiliar.obj" \
	"$(INTDIR)\bcie.obj" \
	"$(INTDIR)\busqueda.obj" \
	"$(INTDIR)\insert.obj" \
	"$(INTDIR)\saveload.obj" \
	"$(INTDIR)\search.obj"

"$(OUTDIR)\bcie.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

.c{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<


!IF "$(CFG)" == "bcie - Win32 Release" || "$(CFG)" == "bcie - Win32 Debug"
SOURCE=..\auxiliar.cpp
DEP_CPP_AUXIL=\
	"..\cie9.h"\
	

"$(INTDIR)\auxiliar.obj" : $(SOURCE) $(DEP_CPP_AUXIL) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\bcie.cpp
DEP_CPP_BCIE_=\
	"..\cie9.h"\
	

"$(INTDIR)\bcie.obj" : $(SOURCE) $(DEP_CPP_BCIE_) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\busqueda.cpp
DEP_CPP_BUSQU=\
	"..\cie9.h"\
	

"$(INTDIR)\busqueda.obj" : $(SOURCE) $(DEP_CPP_BUSQU) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\insert.cpp
DEP_CPP_INSER=\
	"..\cie9.h"\
	

"$(INTDIR)\insert.obj" : $(SOURCE) $(DEP_CPP_INSER) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\saveload.cpp
DEP_CPP_SAVEL=\
	"..\cie9.h"\
	

"$(INTDIR)\saveload.obj" : $(SOURCE) $(DEP_CPP_SAVEL) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\search.cpp
DEP_CPP_SEARC=\
	"..\cie9.h"\
	

"$(INTDIR)\search.obj" : $(SOURCE) $(DEP_CPP_SEARC) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)



!ENDIF 

