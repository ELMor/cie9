
#ifndef __CIE9_H__
#define __CIE9_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <sys/stat.h>

#define NPALMX		25000		// M�ximo n�mero de Palabras
#define NREFMX		25000		// M�ximo n�mero de referencias
#define NGAPMN		   10		// Lote de reserva de memoria
#define NGAPSORT	  500		// Lote de items sin ordenar.
#define NPALCOM		 1000		// Numero de palabras comunes

#define FINDCIE			"cie.ndx"	// Nombre del fichero indexado
#define SHAREDMEMSEGID	1000		// Identificaci�n del segmento de memoria compartido

#define TokPalabras		0
#define TokCampos		1
#define TokTodo			2

#define	ModeAppend		0
#define ModeIntersect	1

#ifdef UNIX
	#define stricmp  strcasecmp
	#define strnicmp strncasecmp
#else
	#include <process.h>
#endif

typedef struct _dummyReferencia {
	char *Referencia;
	long *Palabras;
	long NPalabras;
	char *Diagnos;
} Ref;

typedef struct _dummyPalabra {
	char *Palabra;
	long *Referencias;
	char *enDiag;
	long NReferencias;
	long *Palabras;
	long NPalabras;
} Pal;

char *Translate			(char *in);
char *Tokenize  		(char *,int);
long  Level				(char *);
long  astrlen			(char *in);
long  anumfld			(char *in);

void *elmmalloc			(size_t size );
void *elmrealloc		(void *p, size_t size);
void  elmfree			(void *p );
char *elmfgets			(char *buf, size_t sz, FILE *fp);

int   cmpPal			(const void *p1, const void *p2 );
int   cmpRef			(const void *p1, const void *p2 );
int   cmpLong			(const void *p1, const void *p2);
int   cmpRootString		(const void *p1, const void *p2);

void *BusPalCom			(char *s);
long  BusPal			(char *p);
long  BusRef			(char *p);
int   isRangeRef		(char *s, char **l1,char **rp1,char **rs1,char **l2,char **rp2,char **rs2 );
char *isRef				(char *s, char **Letra, char **RPrin, char **RSec);

long  InsPal			(char *p);
long  InsRef			(char *p);
void  InsMatch			(long np,long nr, char enDiag=0);
void  InsLink			(long,long);

void  IndexAll   		(void);
void  IndexPalCom		(void);

void  Warn				(long, long);

void  ReloadData		(int);
void  SaveData			(char *fname);
void  LoadData			(char *fname);
void  LoadPalCom		(char *fname);
void  LoadNomCie		(char *fname);
void  LoadIndCie		(void);
void  FreeData			(void);
long  Parrafos			(char *Texto, char **frases);

void  WriteBinary		(FILE *fp,char*Padre,char*buf,char*s);

#endif
