�� infecciosa NCOC	720.9	
�� Marie-Str�mpell	720.0	
�� no piog�nica	721.90	
��� con mielopat�a	721.91	
�� piog�nica	720.9	
�� reumatoide	720.0	
�� traum�tica (antigua)	721.7	
�� tuberculosa	015.0 [720.81]	v�ase adem�s Tuberculosis
� estafiloc�cica	711.0	
� estreptoc�cica	711.0	
� forma especificada NCOC	716.8	
� gonoc�cica	098.50	
� gotosa (aguda)	274.0	
� hipertr�fica	715.9	v�ase adem�s Osteoartrosis
�� espina dorsal	721.90	
��� con mielopat�a	721.91	
"� idiop�tica, blenorreica"	099.3	
� infecciosa (aguda) (cr�nica) (subaguda) NCOC	711.9	
�� espina dorsal	720.9	
�� no pi�gena	711.9	
� inflamatoria NCOC	714.9	
� juvenil reumatoide (cr�nica) (poliarticular)	714.30	
�� aguda	714.31	
�� monoarticular	714.33	
�� pauciarticular	714.32	
� lumbar	721.3	"v�ase adem�s Espondilosis, lumbar"
� meningoc�cica	036.82	
� menop�usica NCOC	716.3	
� migratoria		"v�ase Fiebre, reum�tica"
� neumoc�cica	711.0	
� neurop�tica (de Charcot)	094.0 [713.5]	
�� diab�tica	250.6 [713.5]	
�� no sifil�tica NCOC	349.9 [713.5]	
�� siringomi�lica	336.0 [713.5]	
�� tab�tica	094.0 [713.5]	
� no piog�nica NCOC	716.9	
�� espina dorsal	721.90	
��� con mielopat�a	721.91	
� nudosa	715.9	v�ase adem�s Osteoartritis
�� espina dorsal	721.90	
��� con mielopat�a	721.91	
� ocron�tica	270.2 [713.0]	
� palindr�mica	719.3	"v�ase adem�s Reumatismo, palindr�mico"
� piog�nica o pi�mica	711.0	
� posdisent�rica	009.0 [711.3]	
"� posreum�tica, cr�nica (de Jacoud)"	714.4	
� progresiva primaria	714.0	
�� espina dorsal	720.9	
� proliferativa	714.0	
�� espina dorsal	720.0	
� psori�tica	696.0	
� purulenta	711.0	
� quilosa	125.9 [711.7]	v�ase adem�s Filariasis
� reum�tica	714.0	
�� aguda o subaguda		"v�ase Fiebre, reum�tica"
�� cr�nica	714.0	
�� espina dorsal	720.9	
� reumatoide (nodular)	714.0	
�� con		
��� esplenoadenomegalia y leucopenia	714.1	
��� implicaci�n visceral o sist�mica	714.2	
�� aortitis	714.89	
�� carditis	714.2	
�� enfermedad cardiaca	714.2	
�� espina dorsal	720.0	
�� juvenil (cr�nica) (poliarticular)	714.30	
��� aguda	714.31	
��� monoarticular	714.33	
��� pauciarticular	714.32	
� rub�ola	056.71	
"� sacra, sacroil�aca, sacrococc�gea"	721.3	"v�ase adem�s Espondilosis, sacra"
� senil o senescente	715.9	v�ase adem�s Osteoartritis
�� espina dorsal	721.90	
��� con mielopat�a	721.91	
� s�ptica	711.0	
� s�rica (no terap�utica) (terap�utica)	999.5 [713.6]	
� sifil�tica	094.0 [713.5]	
�� cong�nita	090.49 [713.5]	
� supurativa	711.0	
� syphil�tica deformans (de Charcot)	094.0 [713.5]	
� tor�cica	721.2	"v�ase adem�s Espondoilosis, tor�cica"
� t�xica de menopausia	716.3	
� tra�matica (cr�nica) (antigua) (pos)	716.1	
�� lesi�n corriente - naturaleza de lesi�n		
� tuberculosa	015.9 [711.4]	"v�ase adem�s Tuberculosis, artritis"
� uretr�tica	099.3 [711.1]	
"� �rica, ur�tica"	274.0	
� vellosa	716.8	
� ven�rea	099.3 [811.1]	
� vertebral	721.90	"v�ase adem�s Artritis, espina dorsal"
"� von Bechterew, de"	720.0	
Artrocatadisis	715.35	
Artrocele	719.0	"v�ase adem�s Efusi�n, articulaci�n"
Artrocondritis		v�ase Artritis
"Artrodesis, estado de"	V45.4	
Artrodinia	719.4	"v�ase adem�s Dolor, articulaci�n"
� psic�gena	307.89	
Artrodisplasia	755.9	
"Artrofibrosis, articulaci�n"	718.5	v�ase adem�s Anquilosis.
Artrofitis	719.80	
� cadera	719.85	
� codo	719.82	
� hombro (regi�n)	719.81	
� mano	719.84	
� mu�eca	719.83	
� pie	719.87	
� regi�n pelviana	719.85	
� rodilla	719.86	
� sitio especificado NCOC	719.88	
� sitios m�ltiples	719.89	
� tobillo	719.87	
Artrofitos	718.1	"v�ase adem�s Suelto, cuerpo, articulaci�n"
Artrogriposis	728.3	
"� m�ltiple, cong�nita"	754.89	
Artrolitiasis	274.0	
Artroonicodisplasia	756.89	
Artroosteoonicodisplasia	756.89	
Artropat�a	716.9	v�ase adem�s Artritis
� �cido �rico	274.0	
"� Beh�et, de"	711.2 [136.1]	
"� Charcot, de"	094.0 [713.5]	
�� diab�tica	250.6 [713.5]	
�� siringomi�lica	336.0 [713.5]	
�� tab�tica	094.0 [713.5]	
� cristales (inducida por)		"v�ase Artritis, debida a cristales"
� especificada NCOC	716.8	
� gotosa	274.0	
"� neur�gena, neurop�tica (de Charcot) (Tab�tica)"	094.0 [713.5]	
�� diab�tica	250.6 [713.5]	
�� no sifil�tica	349.9 [713.5]	
�� siringomi�lica	336.0 [713.5]	
� posdisent�rica NCOC	009.0 [711.3]	
"� posreum�tica, cr�nica (de Jacoud)"	714.4	
� psori�tica	696.0	
� pulmonar	731.2	
� siringomielia	336.0 [713.5]	
� tabes dorsalis	094.0 [713.5]	
� tab�tica	094.0 [713.5]	
� transitoria	716.4	
� traum�tica	716.1	
Artropiosis	711.0	"v�ase adem�s Artritis, piog�nica"
Artrosis (deformante) (degenerativa)	715.9	v�ase adem�s Osteoaratrosis
"� Charcot, de"	094.0 [713.5]	
� espina dorsal	721.90	v�ase adem�s Espondilosis
� poliarticular	715.09	
ARV (VIH) (enfermedad) (infecci�n)		v�ase Virus de inmunodeficiencia Humana (enfermedad) (infecci�n)
Asa 		
� intestino	560.2	v�ase adem�s V�lvulo
� nervio intrascler�tico	379.29	
� vascular en la papila (�ptica)	743.57	
"Asa de cubo, fractura en (cart�lago semilunar)"	836.2	"v�ase adem�s Desgarro, menisco"
"Asboe-Hansen, enfermedad de (incontinencia pigmentaria)"	757.33	
Ascariasis (intestinal) (pulm�n)	127.0	
Ascaridiasis	127.0	
Ascaridosis	127.0	
Ascaris	127.0	
� lumbricoides (infestaci�n)	127.0	
� neumon�a	127.0	
Ascendente		v�ase enfermedad espec�fica
"Aschoff, cuerpos de"	398.0	"v�ase adem�s Miocarditis, reum�tica"
Ascitis	789.5	
� abdominal	789.5	
� articulaci�n	719.0	"v�ase adem�s Efusi�n, articulaci�n"
� cancerosa	197.6(M8000/6) 	
� cardiaca	428.0	
� cong�nita	778.0	
� coraz�n	428.0	
� debida a S. japonicum	120.2	
"� feto, cuando causa desproporci�n fetopelviana"	653.7	
� maligna	197.6(M8000/6) 	
� quilosa (no fil�rica)	457.8	
�� fil�rica	125.9	"v�ase adem�s Infestaci�n, fil�rica"
� seudoquilosa	789.5	
� sifil�tica	095.2	
� tuberculosa	014.0	v�ase adem�s Tuberculosa
ASCVD (ECVAS) (enfermedad cardiovascular arterioscler�tica)	429.2	
As�ptico		v�ase enfermedad espec�fica
Asesoramiento NCOC	V65.9	
"� abuso, malos tratos o descuido de ni�os"	V61.21	
� anticonceptivo NCOC	V25.09	
�� anticonceptivo oral (p�ldora)	V25.01	
�� atenci�n NCOC	V25.9	
�� dispositivo (intrauterino)	V25.02	
�� mantenimiento	V25.40	
��� anticonceptivo oral (p�ldora)	V25.41	
��� dispositivo anticonceptivo intrauterino	V25.42	
��� tipo especificado NCOC	V25.49	
�� prescripci�n NCOC	V25.02	
��� anticonceptivo oral (p�ldora)	V25.01	
���� receta repetida	V25.41	
��� receta repetida	V25.40	
�� vigilancia	V25.40	
� conflicto entre padre(s) e hijo(s)	V61.20
�� problema especificado NCOC	V61.29
� diet�tico	V65.3
� explicaci�n de	
�� medicaci�n de	V65.4
�� resultados de investigaci�n	V65.4
� gen�tico	V26.3
� m�dico (para)	V65.9
�� de parte de otra persona	V65.1
�� enfermedad no demostrada	V65.5
�� internado en colegio	V60.6
�� internado en instituci�n	V60.1
�� persona que vive sola	V60.3
�� temor de enfermedad sin enfermedad detectada	V65.5
� motivo especificado NCOC	V65.4
� para tercera parte no asistente	V65.1
� planificaci�n familiar	V25.09	
� procreativo	V26.4	
� salud (consejo) (educaci�n) (instrucci�n) 	V65.4	"v�ase adem�s Asesoramiento, m�dico"
� sexual	V65.4	
"Asfixia, asfixiaci�n (por)"	799.0	
� ahogamiento	994.1	
� ahorcamiento	994.7	
� alimento o cuerpo extra�o (en laringe)	933.1	
�� bronquio (principal)	934.1	
�� bronquiolos	934.8	
�� faringe	933.0	
�� garganta	933.0	
"�� nariz, tubos nasales"	932	
�� nasofaringe	933.0	
�� pulm�n	934.8	
�� tracto respiratorio	934.9	
��� parte especificada NCOC	934.8	
�� tr�quea	934.0	
� aplastamiento		"v�ase Lesi�n, interna, �rganos intrator�cicos"
� babero	994.7	
� bolsa de pl�stico	994.7	
� cambios gravitacionales	994.7	
� constricci�n	994.7	
� estrangulaci�n	994.7	
� fetal		"v�ase Asfixia, intrauterina"
"� gas, humos o vapor NCOC"	987.9	
�� especificado(s)		"v�ase Tabla de drogas, f�rmacos y productos qu�micos"
� inhalaci�n		v�ase Inhalaci�n
� intrauterina		
�� muerte fetal (antes del inicio del parto)	768.0	
��� durante el parto	768.1	
�� nacido con vida		"v�ase Sufrimiento, fetal, ni�o nacido con vida"
� local	443.0	
� mec�nica	994.7	
�� durante el parto	768.4	"v�ase adem�s Sufrimiento, fetal"
� membrana amni�tica	768.9	"v�ase adem�s Asfixia, reci�n nacido"
� moco	933.1	
�� bronquio (principal)	934.1	
�� faringe	933.0	
�� garganta	933.0	
�� laringe	933.1	
�� pulm�n	934.8	
�� reci�n nacido	770.1	
�� tracto respiratorio	934.9	
��� parte especificada NCOC	934.8	
�� tr�quea	934.0	
�� tubos nasales	932	
�� vaginal (feto o reci�n nacido)	770.1	
� mon�xido de carbono	986	
� nacimiento	768.9	"v�ase adem�s Asfixia, reci�n nacido"
� patol�gica	799.0	
� posnatal	768.9	"v�ase adem�s Asfixia, reci�n nacido"
�� mec�nica	994.7	
� prenatal		"v�ase Sufrimiento, fetal"
� presi�n	994.7	
� reci�n nacido	768.9	
�� azul (puntuaci�n Apgar 4-7)	768.6	
�� blanca (puntuaci�n Apgar 0-3)	768.5	
�� grave (puntuaci�n Apgar 0-3)	768.5	
�� leve o moderada (puntuaci�n Apgar 4-7)	768.6	
�� l�vida (puntuaci�n Apgar 4-7)	786.6	
�� p�lida (puntuaci�n Apgar 0-3)	768.5	
� reticular	782.61	
� ropa de cama	994.7	
� soterramiento	994.7	
�� aplastamiento		"v�ase Lesi�n, interna, �rganos intrator�cicos"
� sumersi�n	994.1	
� traum�tica NCOC		"v�ase Lesi�n, interna, �rganos intrator�cicos"
� v�mitos		"v�ase Asfixia, alimento o cuerpo extra�o"
"Asherman, s�ndrome de"	621.5	
Asialia	527.7	
"Asi�tica, c�lera"	001.9	v�ase adem�s C�lera
Asimbolia	784.60	
Asimetr�a		v�ase adem�s Distorsi�n
� cara	754.0	
� mand�bula	524.1	
� pecho	786.9	
� pelvis con desproporci�n	653.0	
�� cuando obstruye el parto	660.1	
��� que afecta al feto o al reci�n nacido	763.1	
�� que afecta al feto o al reci�n nacido	763.1	
"Asim�trica, respiraci�n"	786.09	
"Asimilaci�n, pelvis"		
� con desproporci�n	653.2	
�� cuando obstruye el parto	660.1	
��� que afecta al feto o al reci�n nacido	763.1	
�� que afecta al feto o al reci�n nacido	763.1	
Asinergia	781.3	
� ventricular 	429.89	
 Asistolia (coraz�n)	427.5	"v�ase adem�s Paro, cardiaco"
"Asma, asm�tico (bronquial) (catarral) (espasm�dica)"	493.9	
� con		
�� enfermedad pulmonar obstructiva cr�nica (EPOC)	493.2	
�� fiebre del heno 	493.0	
"�� rinitis, al�rgica"	493.0	
� al�rgica	493.9	
�� causa declarada ( al�rgeno externo)	493.0	
� alfareros	502	
at�pica	493.0	
� carboneros	500	
� card�aca	428.1	"v�ase adem�s Fallo, ventricular, izquierda"
� cardiobronquial	428.1	"v�ase adem�s Fallo, ventricular, izquierda"
� cardiorrenal 	404.9	"v�ase adem�s Hipertensi�n, cardiorrenal"
� cedro rojo	495.8	
� coraz�n	428.1	"v�ase adem�s Fallo, ventricular, izquierda"
� crup	493.9	
� debida a 		
�� detergente 	507.8	
�� inhalaci�n de humos	506.3	
�� proceso inmunol�gico interno	493.0	
� detergente 	507.8	
� empaquetadores de carne	506.9	
� end�gena (intr�nseca)	493.1
� eosin�fila	518.3
� eosin�fila pulmonar	518.3
� ex�gena (cosm�ticos) (caspa o polvo) (drogas) (polvo) (plumas) (alimento) (heno) (platino) (polen)	493.0
� extr�nseca	493.0
� fabricantes de piedras de molino	502
� heno	493.0
� IgE	493.0
� infancia 	493.0
� infecciosa	493.1
� inicio tard�o	493.1
� intr�nseca	493.1
"� Kopp, de"	254.8
� limpiadores por chorro de arena	502
� lunes por la ma�ana	504
� madera	495.8
� marmolistas	502	
"� Millar, de (laringismo estriduloso)"	478.75	
� mineros (del carb�n)	500	
� molineros	502	
� neumoconi�tica (ocupacional) NCOC	505	
� Nueva Orleans (epid�mica)	493.0	
� obstructiva (cr�nica) (con enfermedad pulmonar obstructiva)	493.2	
� platino 	493.0	
� psic�gena	316 [493.9]	
"� Rostan, de"	428.1	"v�ase adem�s Fallo, ventricular, izquierda"
� secoyosis	495.8	
� t�mica	254.8	
� tuberculosa	011.9	"v�ase adem�s Tuberculosis, pulmonar"
"� Wichmann, de (laringismo estriduloso)"	478.75	
"Asocial(es), personalida o tendencias"	301.7	
Aspergillus (flavus) (fumigatius) (infecci�n) (terreus)	117.3	
Aspergilosis	117.3	
� con neumon�a	117.3 [484.6]	
� no sifil�tica NCOC	117.3	
Aspermatogenesia	606.0	
Aspermia (testicular)	606.0	
Aspiraci�n		
"� alimento, cuerpo extra�o, o gasolina (con asfixia)"		"v�ase Asfixia, alimento o cuerpo extra�o"
� bronquitis	507.0	
� contenido de conducto de parto	770.1	
� l�quido amni�tico	770.1	
� meconio 	770.1	
� moco	933.1	
�� dentro de		
��� bronquio (principal)	934.1	
��� pulm�n 	934.8	
��� tracto respiratorio	934.9	
���� parte especificada NCOC	934.8	
��� tr�quea	934.0	
�� reci�n nacido	770.1	
�� vaginal (feto o reci�n nacido)	770.1	
� neumon�a 	507.0	
� neumonitis	507.0	
�� feto o reci�n nacido	770.1	
�� obst�trica	668.0	
� reci�n nacido	770.1	
� s�ndrome de reci�n nacido (masiva) (meconio)	770.1	
� v�rnix caseosa	770.1	
Aspiraci�n por la nariz (esnifar) de		
� adhesivo o pegamento pl�stico (aeromodelismo) 	304.6	v�ase adem�s Dependencia
� coca�na 	304.2	v�ase adem�s Dependencia
� �ter	304.6	v�ase adem�s Dependencia
Asplenia	759.0	
� con mesocardia	746.87	
"Assam, fiebre de"	085.0	
"Assmann, foco de"	011.0	v�ase adem�s Tuberculosis
Astasia (-abasia)	307.9	
� hist�rica	300.11	
Asteatosis	706.8	
� cutis	706.8	
"Astenia, ast�nica"	780.7	
� anhidr�tica tr�pica	705.1	
� cardiaca 	428.9	"v�ase adem�s Fallo, cardiaco"
�� psic�gena	306.2	
� cardiovascular	428.9	"v�ase adem�s Fallo, cardiaco"
�� psic�gena	306.2	
� coraz�n	428.9	"v�ase adem�s Fallo, cardiaco"
�� psic�gena	306.2	
� hist�rica	300.11	
� miocardiaca	428.9	"v�ase adem�s Fallo, cardiaco"
�� psic�gena	306.2	
� nerviosa	300.5	
� neurocirculatoria	306.2	
� neur�tica	300.5	
� psicofisiol�gica	300.5	
� psic�gena	300.5	
� psiconeur�tica	300.5	
"� reacci�n, psiconeur�tica"	300.5	
� senil	797	
"� Stiller, de "	780.7	
Astenop�a	368.13	
� acomodativa	367.4	
� hist�rica (muscular)	300.11	
� psic�gena	306.7	
Astenospermia	792.2	
Astereognosis	780.9	
Asterixis	781.3	
� en enfermedad hep�tica	572.8	
Astigmatismo (compuesto) (cong�nito)	367.20	
� irregular	367.22	
� regular	367.21	
Astilla		"v�ase Traumatismo, superficial,por sitio"
"Astilleros, ojo o enfermedad de "	077.1	
Astroblastoma	(M9430/3)	
� nariz	748.1	
� sitio especificado		"v�ase Neoplasia, por sitio maligna"
� sitio no especificado	191.9	
Astrocitoma (qu�stico) 	(M9400/3)	
� fibrilar 	(M9420/3)	
�� sitio especificado		"v�ase Neoplasia, por sitio, maligna"
�� sitio no especificado	191.9	
� fibroso	(M9420/3)	
�� sitio especificado		"v�ase Neoplasia, por sitio, maligna"
�� sitio no especificado	191.9	
� gemistoc�tico	(M9411/3)	
�� sitio especificado		"v�ase Neoplasia, por sitio, maligna"
�� sitio no especificado 	191.9	
� juvenil	(M9421/3)	
�� sitio especificado		"v�ase Neoplasia, por sitio, maligna"
�� sitio no especificado	191.9	
� nariz	748.1	
� piloc�tico	(M9421/3)	
�� sitio especificado		"v�ase Neoplasia, por sitio, maligna"
�� sitio no especificado	191.9	
� piloide	(M9421/3)	
�� sitio especificado		"v�ase Neoplasia, por sitio, maligna"
�� sitio no especificado	191.9	
� protopl�smico	(M9410/3)	
�� sitio especificado		"v�ase Neoplasia, por sitio, maligna"
�� sitio no especificado	191.9	
� sitio especificado		"v�ase Neoplasia, por sitio, maligna"
� sitio no especificado	191.9	
� subependimario	237.5(M9383/1) 	
�� c�lulas gigantes	237.5(M9383/1) 	
� tipo anapl�sico	(M9401/3)	
�� sitio especificado		"v�ase Neoplasia, por sitio, maligna"
�� sitio no especificado	191.9	
Astroglioma	(M9400/3)	
� nariz	748.1	
�� sitio especificado		"v�ase Neoplasia, por sitio, maligna"
�� sitio no especificado	191.9	
Ataque		
� angina		v�ase Angina
"� apoplexia, apopl�ctico "	436	"v�ase adem�s Enfermedad, cerebrovascular, aguda"
� aquin�sico	345.0	v�ase adem�s Epilepsia
� bilioso	787.0	v�ase adem�s V�mitos
� catal�ptico	300.11	
"� cerebro, cerebral"	436	"v�ase adem�s Enfermedad, cerebrovascular, aguda"
"� cian�tico, reci�n nacido"	770.8	
� coraz�n 	410.9	"v�ase adem�s Infarto, miocardio"
� coronario	410.9	"v�ase adem�s Infarto, miocardio"
� epil�ptico	345.9	v�ase adem�s Epilepsia
"� epileptiforme, epileptoide"	780.3	
� esquizofreniforme	295.4	v�ase adem�s Esquizofrenia
� hemiplej�a	436	"v�ase adem�s Enfermedad, cerebrovascular, aguda"
� hist�rico	300.11	
� inconciencia	780.2	
�� hist�rica	300.11	
� jacksoniano	345.5	v�ase adem�s Epilepsia
"� miocardio, miocardiaco"	410.9	"v�ase adem�s Infarto, miocardio"
� miocl�nico	345.1	v�ase adem�s Epilepsia
� p�nico	300.01	
� par�lisis	436	"v�ase adem�s Enfermedad, cerebrovascular, aguda"
� paroxismal	780.3	
� psicomotor	345.4	v�ase adem�s Epilepsia
� salutatorio	345.6	v�ase adem�s Epilepsia
� sensorial y motor	780.3	
� s�ncope	780.2	
"� t�xico, cerebral"	780.3	
"� transitorio, isqu�mico (TIA)"	435.9	
� vasomotor	780.2	
� vasovagal (idiop�tico) (paroxismal)	780.2	
Ataque	436	"v�ase adem�s Enfermedad, cerebrovascular, aguda"
� apopl�tico	436	"v�ase adem�s Enfermedad, cerebrovascular, aguda"
� cerebro	436	"v�ase adem�s Enfermedad, cerebrovascular, aguda"
� cicatrizado o antiguo	438	v�ase adem�s categor�a
�� sin efectos residuales	V12.5	
� coraz�n		"v�ase Enfermedad, coraz�n"
� efecto tard�o	438	v�ase categor�a
� en evoluci�n	435.9	
� epil�ptico		v�ase Epilepsia
� fulgurante	994.0	
� paral�tico	436	"v�ase adem�s Enfermedad, cerebrovascular, aguda"
� progresivo	435.9	
Ataque (convulsi�n)	780.3	
� acin�sico (idiop�tico)	345.0	
�� psicomotor	345.4	
"� apoplej�a, apopl�ctico"	436	"v�ase adem�s Enfermedad, cerebrovascular, aguda"
� at�nico	345.0	v�ase adem�s Epilepsia
� aut�nomo	300.11	
� cerebro o cerebral	436	"v�ase adem�s Enfermedad, cerebrovascular, aguda"
� convulsivo	780.3	v�ase adem�s Convulsiones
� coraz�n		"v�ase Enfermedad, coraz�n"
� cortical (focal) (motor) 	345.5	v�ase adem�s Epilepsia
"� epilepsia, epil�ptico (cript�geno)"	345.9	v�ase adem�s Epilepsia
"� epileptiforme, epileptoide"	780.3	
�� focal 	345.5	v�ase adem�s Epilepsia
� febril	780.3	
� hist�rico	5	
� Jacksoniano (focal)	345.5	v�ase adem�s Epilepsia
�� tipo		
��� motor	345.5	
��� sensorial	345.5	
� par�lisis	436	"v�ase adem�s Enfermedad, cerebrovascular, aguda"
� reci�n nacido	779.0	
� recurrente		v�ase Epilepsia
� repetitiva		v�ase Epilepsia
� salutatorio	345.6	v�ase adem�s Epilepsia
� uncinado	345.4	v�ase adem�s Epilepsia
"Ataxia, at�xica"	781.3	
� aguda 	781.3	
� andar	781.2	
�� hist�rico	300.11	
� cerebelar	334.3	
�� en		
��� alcoholismo	303.9 [334.4]	
��� enfermedad neopl�sica NCOC	239.4 [334.4]	
��� mixedema	244.9 [334.4]	
�� hereditaria (de Marie)	334.2	
� cerebral	331.89	
� cerebro	331.89	
� esp�stica	094.0	
�� hereditaria	334.1	
�� sifil�tica	094.0
� espinal 	
��hereditaria	334.0
��progresiva	094.0
"�familia,familiar"	334.2
��cerebral (de Marie)	334.2
��espinal (de Friedreich)	334.0
"� Friedrich, de (heredofamiliar) (espinal)"	334.0
� general	781.3
� hereditaria NCOC	334.2
�� cerebelar	334.2
�� esp�stica	334.1
�� espinal	334.0
� heredofamiliar (de Marie)	334.2
� hist�rica	300.11
� l�bulo frontal	781.3
� locomotriz (progresiva)	094.0	
�� diab�tica	250.6 [337.1]	
"� Marie, de (cerebelar) (heredofamiliar)"	334.2	
� origen no org�nico	307.9	
�parcial	094.0	
�posvaricela	052.7	
�progresiva	094.0	
�psic�gena	307.9	
"� Sanger-Brown, de "	334.2	
� telangiectasia	334.8	
Ataxia-telangiectasia	334.8	
Atelectasia (colapso por absorci�n) (completa) (compresi�n) (masiva) (parcial) (posinfecciosa) (colapso por presi�n) (pulmonar) (relajaci�n)	518.0	
� primaria	770.4	
� reci�n nacido (cong�nita) (parcial)	770.5	
�� primaria	770.4	
� tuberculosa 	011.9	"v�ase adem�s Tuberculosis, pulmonar"
Atelia		v�ase Distorsi�n
� con ausencia de pez�n 	757.5	
Ateliosis	253.3	
Atelocardia	746.9	
Atelomielia	742.59	
Atenci�n		
� anticonceptiva	V25.9	
�� tipo especificado NCOC	V25.8	
� procreativa	V26.9	
�� tipo especificado NCOC	V26.8	
Atenci�n de		
� apertura artificial (de)	V55.9	
�� sitio especificado NCOC	V55.8	
�� tracto digestivo NCOC	V55.4	
�� tracto urinario NCOC	V55.6	
�� vagina	V55.7	
� cistotom�a	V55.5	
� colostom�a	V55.3	
� gastrostom�a	V55.1	
� ileostom�a	V55.2	
� nefrostom�a	V55.6	
� suturas	V58.3	
� Traqueostom�a	V55.0	
� ureterostom�a	V55.6	
� uretrostom�a	V55.6	
� vendajes quir�rgicos	V58.3	
� yeyunostom�a	V55.4	
Ateriectasia	447.8	
"Ateroma, ateromatoso "	440.9	v�ase adem�s Arteriosclerosis
"� aorta, a�rtica"	440.0	
�� v�lvula 	424.1	"v�ase adem� Endocarditis, a�rtica"
� arteria		v�ase Arteriosclerosis
� basilar (arteria)	433.0	"v�ase adem�s Oclusi�n, arteria, basilar"
"� carot�dea (arteria) (com�n) (interna) (v�ase adem�s Oclusi�n, arteria, carot�dea)"	433.1	
� cerebral (arterias)	437.0	
"� coraz�n, cardiaca"	414.0	
� coronaria (arterias)	414.0	
� degeneraci�n		v�ase Arteriosclerosis
"� miocardio, miocardiaco"	414.0	
� mitral (v�lvula)	424.0	
� piel	706.2	
� tric�spide (coraz�n) (v�lvula)	424.2	
� v�lvula pulmonar (coraz�n)	424.3	"v�ase adem�s Endocarditis, pulmonar"
"� v�lvula, valvular"		v�ase Endocarditis
� vertebral (arteria)	433.2	"v�ase adem�s Oclusi�n, arteria, vertebral"
Ateromatosis	272.8	"v�ase adem�s Arteriosclerosis arterial, cong�nita"
Aterosclerosis	414.0	v�ase adem�s Arteriosclerosis coronaria (arteria)
Atetosis (adquirida)	781.0	
� bilateral	333.7	
� cong�nita (bilateral)	333.7	
� doble 	333.7	
� unilateral	781.0	
Athelia	757.6	
At�pico		v�ase enfermedad espec�fica
"� distribuci�n, vaso"	747.6	
� endometrio	621.9	
� ri��n	593.89	
"Atipismo, cuello uterino"	622.1	
Atireosis (cong�nita)	243	
� adquirida		v�ase Hipotiroidismo
Atiria (adquirida)	244.9	v�ase adem�s Hipotiroidismo
� cong�nita	243	
Atiroidismo (adquirida)	244.9	v�ase adem�s Hipotiroidismo
� cong�nita	243	
"Atleta, de"	
� coraz�n	429.3
� pie	110.4
"Atmosf�rica, pirexia"	992.0
"Aton�a, at�nico"	
� capilar	448.9
� ciego	564.8
�� psic�gena	306.4
� colon	564.8
�� psic�gena	306.4
� cong�nita	779.8
� dispepsia	536.8
�� psic�gena	306.4
� est�mago	536.8
�� neur�tica o psic�gena	306.4
�� psic�gena	306.4
� intestino	564.8	
�� psic�gena	306.4	
� pared abdominal	728.2	
� �tero	661.2	
�� que afecta al feto o al reci�n nacido	763.7	
� vejiga (esf�nter)	596.4	
�� neur�gena NCOC	344.61	
� vesical	596.4	
Atop�a NCOC	V15.0	
Atrangantado		
� disco		v�ase Papiledema
� durante v�mitos NCOC	933.1	"v�ase adem�s Asfixia, alimento"
� flema	933.1	
"� por alimento, flema o v�mito NCOC"	933.1	"v�ase adem�s Asfixia, alimento"
"Atransferrinemia, cong�nita"	273.8	
Atrepsia	261	
"Atresia, atr�sico (cong�nita)"	759.8	
� acueducto de Silvio	742.3	
�� con espina b�fida 	741.0	v�ase adem�s Espina b�fida
"� ano, anal (canal)"	751.2	
� aorta	747.22	
�� con  hipoplasia de la aorta ascendente y desarrollo defectuoso del ventr�culo izquierdo (con atresia de la v�lvula mitral)	746.7	
�� anillo	747.21	
�� cayado	747.11	
� a�rtica (orificio) (v�lvula)	746.89	
�� cayado	747.11	
� arteria NCOC 	747.6	
�� cerebral	747.81	
�� coronaria	746.85	
�� ojo	743.58	
�� pulmonar	747.3	
�� umbilical	747.5	
"� bilis, conducto biliar o tubo (com�n)"	751.61	
�� adquirida 	576.2	"v�ase adem�s Obstrucci�n, biliar"
� bronquio	748.3	
� canal auditivo (externo)	744.02	
� cardiaca		
�� v�lvula	746.89	
��� a�rtica	746.89	
��� mitral	746.89	
��� pulmonar	746.01	
��� tric�spide	746.1	
� ciego	751.2	
� coana	748.0	
� colon	751.2	
� conducto c�stico	751.61	
�� adquirida	575.8	
��� con obstrucci�n 	575.2	"v�ase adem�s Obstrucci�n, ves�cula biliar"
� conducto deferente	752.8
� conducto eyaculatorio	752.8
� conducto nasolagrimal	743.65
� conducto o gl�ndula salival	750.23
�� adquirida	527.8
� conducto o gl�ndula submaxilar	750.23
�� adquirida	527.8
� conducto parot�deo	750.23
�� adquirida	527.8
� conducto sublingual	750.23
�� adquirida	527.8
� coraz�n	
�� v�lvula NCOC	746.89
��� a�rtica	746.89
��� mitral	746.89
��� pulmonar	746.01
��� tric�spide	746.1	
� cuello uterino (adquirida)	622.4	
�� cong�nita	752.49	
�� en el embarazo o alumbramiento	654.6	
��� cuando obstruye el parto	660.2	
���� que afecta al feto o al reci�n nacido	763.1	
��� que afecta al feto o al reci�n nacido	763.8	
� duodeno	751.1	
� epiglotis	748.3	
� es�fago	750.3	
� foramen de 		
�� Luschka	742.3	
��� con espina b�fida	741.0	v�ase adem�s Espina b�fida
�� Magendie	742.3	
��� con espina b�fida	741.0	v�ase adem�s Espina b�fida
� glotis	748.3	
� himen	752.42	
�� adquirida	623.3	
�� posinfecciosa	623.3	
� �leon	751.1	
� intestino (delgado)	751.1	
�� grueso	751.2	
"� iris, �ngulo de filtraci�n"	743.20	v�ase adem�s Buftalmia
"� lagrimal, aparato"	743.65	
�� adquirida		"v�ase Estenosis, lagrimal"
� laringe	748.3	
"� ligamento, ancho"	752.19	
� meato �seo (o�do)	744.03	
� meato urinario	753.6	
� nares (anterior) (posterior)	748.0	
"� nariz, ventanilla de la nariz"	748.0	
�� adquirida	738.0	
� nasofaringe	748.8	
"� o�do, canal"	744.02	
� �rgano o sitio NCOC		"v�ase Anomal�a, tipo especificado NCOC"
� �rgano o tracto urinario NCOC	751.8	
�� inferior	751.2	
�� superior	750.8	
� �rganos digestivos NCOC	751.8	
� �rganos genitales		
�� externos		
��� femeninos	752.49	
��� masculinos	752.8	
�� internos		
��� femeninos	752.8	
��� masculinos	752.8	
� orificio ureterovesical	753.2	
� orificio vesicouretral	753.6	
� oviducto (adquirida)	628.2
�� cong�nita	752.19
� pulm�n	748.5
� pulmonar (arteria)	747.3
�� v�lvula	746.01
�� vena	747.49
� pulm�nica	746.01
� pupila	743.46
� quiste folicular	620.0
� recto	751.2
� ri��n 	753.3
� tracto urinario NCOC	753.2
� tr�quea	748.3
� trompa de Eustaquio	744.24
� trompa de Falopio (adquirida)	628.2
�� cong�nita	752.19
� uni�n urteropelviana	753.2
� ur�ter	753.2
� uretra (valvular)	753.6
� �tero	752.3
�� adquirida	621.8
� vagina (adquirida)	623.2
�� cong�nita	752.49
�� posgonoc�cica (antigua)	098.2
�� posinfecciosa	623.2
�� senil	623.2
� v�lvula mitral	746.89
"�� con atresia o hipoplasia de orificio o v�lvula mitral, con   hipoplasia de la aorta ascendente y desarrollo defectuoso del ventr�culo izquierdo"	746.7
� v�lvula tric�spide	746.1
� vascular NCOC	747.6
�� cerebral	747.81
� vaso sangu�neo	747.6
�� arteria pulmonar	747.3
�� cerebral	747.81
� vaso sangu�neo	747.6
�� arteria pulmonar	747.3
�� cerebral	747.81
�� renal	747.6
� vejiga (cuello)	753.6
� vena cava (inferior) (superior)	747.49
� vena NCOC	747.6
�� cardiaca	746.89
�� grande 	747.49
�� porta	747.49
�� pulmonar	747.49
� ves�cula biliar	751.69
� vulva	752.49
�� adquirida	624.8
� yeyuno	751.1	
Atrici�n		
� dientes (excesiva) (Tejidos duros)	521.1	
� enc�a	523.2	
"Atrioventricular, conducto com�n"	745.69	
"Atriquia, atricosis"	704.00	
� cong�nita (universal)	757.4	
"Atrofia, atrophia"		
"� adiposa, timo (gl�ndula)"	254.8	
� alba	709.0	
� amarilla (aguda) (cong�nita) (h�gado) (subaguda) 	570	"v�ase adem�s Necrosis, h�gado"
"�� como resultado de administraci�n de sangre, plasma, suero, u otra sustancia biol�gica (dentro de los ocho meses siguientes a la administraci�n)"		"v�ase Hepatitis, viral"
�� cr�nica	571.8	
� aparato lagrimal (primaria)	375.13	
�� secundaria	375.14	
� ap�ndice	543.9	
"� Aran-Duchenne, de, muscular"	335.21	
� arterioscler�tica		v�ase Arteriosclerosis
� artritis	714.0	
�� espina dorsal	720.9	
� bazo (senil) 	289.59	
� blanca (de Mili�n)	701.3	
� brazo	728.2	
� card�aca (marr�n) (senil) 	429.1	"v�ase adem�s Degeneraci�n, miocardiaca"
� cart�lago (infecciosa) (articulaci�n)	733.99	
� cavidad bucal	528.9	
� cerebelar		"v�ase Atrofia, cerebro"
� cerebral		"v�ase Atrofia, cerebro"
� cerebro (corteza) (progresiva)	331.9	
�� con demencia	290.10	
"�� Alzheimer, de"	331.0	
��� con demencia		"v�ase Alzheimer, demencia"
�� circunscrita (de Pick)	331.1	
��� con demencia 	290.10	
�� cong�nita	742.4	
�� hereditaria	331.9	
�� senil	331.2	
"� Charcot-Marie-Tooth, de "	356.1	
� cicatriz NCOC	709.2	
"� coloide, degenerativa"	701.3	
� conducto biliar (cualquiera)	576.8	
� conducto c�stico	576.8	
� conducto deferente	608.89	
� conducto o gl�ndula salival	527.0	
� conjuntiva (senil)	372.8	
� coraz�n (marr�n) (senil) 	429.1	"v�ase adem�s Degeneraci�n, miocardiaca"
� cord�n esperm�tico	608.89	
� coroides	363.40	
�� hereditaria	363.50	"v�ase adem�s Distrofia, coroides"
��� anular		
���� central	363.54	
���� difusa	363.57	
���� generalizada	363.57	
��� senil	363.41	
�� secundaria difusa	363.42	
� cortical	331.9	"v�ase adem�s Atrofia, cerebro"
"� Cruveilhier, de "	335.21	
� cuello uterino (endometrio) (mucosa) (miometrio) ( senil)	622.8	
�� menop�usica	627.8	
� cuerpo cavernoso	607.89	
� cuerpo ciliar	364.57	
� cutis	701.8	
�� idiop�tica progresiva	701.8	
�� senil	701.8	
�dacriosialadenopat�a	710.2
�degenerativa	
��coloide	701.3
��senil	701.3
"� D�j�rine-Thomas, de "	333.0
"� dermatol�gica, difusa (idiop�tica)"	701.8
"� desdentada, reborde alveolar"	525.2
"� Duchenne-Aran, de "	335.21
� enc�a	523.2
� endometrio (senil)	621.8
�� cuello uterino	622.8
"� enfisema, pulm�n"	492.8
� ent�rica	569.89
� epid�dimo	608.3
� escayola	728.2
"� esclerosis, lobular (del cerebro)"	331.0
�� con demencia	290.10	
� escroto	608.89	
� espina dorsal (columna)	733.99	
� espinal (m�dula)	336.8	
�� aguda	336.8	
�� muscular (cr�nica)	335.10	
��� adulta	335.19	
��� familiar	335.11	
��� juvenil	335.10	
�� par�lisis	335.10	
��� aguda 	045.1	"v�ase adem�s Pliomielitis, con par�lisis"
� est�mago	537.89	
� estriada y macular	701.3	
�� sifil�tica	095.8	
� facial (piel)	701.9	
� falta de uso		
�� hueso	733.7	
�� m�sculo	728.2	
� faringe	478.29	
� faringitis	472.1	
"� fascia tarsoorbitaria, cong�nita"	743.66	
� fascioscapulohumeral (de Landouzy-D�j�rine)	359.1	
� flava hepatis (aguda) (subaguda)	570	"v�ase adem�s Necrosis, h�gado"
� garganta	478.29	
� g�strica	537.89	
� gastritis (cr�nica)	535.1	
� gastrointestinal	569.89	
� gl�ndula parot�dea	527.0	
� gl�ndula sublingual	527.0	
� gl�ndula submaxilar	527.0	
� gl�ndula suprarrenal (autoinmune) (c�psula) (gl�ndula)	255.4	
�� con hipofunci�n 	255.4	
� glandular 	289.3	
"� globo del ojo, cuasa desconocida"	360.41	
� gyrata de coroides y retina (central)	363.54	
�� generalizada	363.57	
� hemifacial	754.0	
"�� Romberg, de "	349.89	
�  hidronefrosis	591	
� h�gado ( aguda) (subaguda)	570	"v�ase adem�s Necrosis, h�gado"
�� amarilla (cong�nita)	570	
��� con		
���� aborto		"v�ase Aborto, por tipo, con complicaci�n especificada NCOC"
���� embarazo ect�pico	639.8	v�ase adem�s categor�as 633.0 - 633.9
���� embarazo molar	639.8	v�ase adem�s categor�as 630 - 632
��� cr�nica	571.8	
��� cuando complica el embarazo	646.7	
��� curada	571.5	
��� despu�s de		
���� aborto 	639.8	
���� embarazo ect�pico o molar	639.8	
��� obstr�tica	646.7	
"��� por inyecci�n, inoculaci�n o transfusi�n (dentro de los ocho meses siguientes a la administraci�n)"		"v�ase Hepatitis, viral"
��� posaborto	639.8	
��� posinmunizaci�n	070.3	
���� con coma hep�tico	070.2	
��� postransfusi�n	070.3	
���� con coma hep�tico	070.2	
"��� puerperal, posparto"	674.8	
�� cr�nica (amarilla)	571.8	
� hueso (senil)	733.99	
�� debida a		
��� falta de uso	733.7	
��� infecci�n	733.99	
��� tabes dorsalis (neur�gena)	094.0	
�� postraum�tica	733.99	
"� idop�tica difusa, dermatol�gica"	701.8	
� infantil	261	
"�� par�lisis, aguda"	045.1	"v�ase adem�s Poliomielitis, con par�lisis"
� intestino	569.89	
� iris (generalizada) (posinfecciosa) (en forma de sector)	364.59	
�� esencial	364.51	
�� esf�nter	364.54	
�� progresiva	364.51	
� labio	528.5	
"� Landouzy-D�j�rine, de"	359.1	
