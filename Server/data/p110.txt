�� tipo infantil	(M9071/3)	
�� tipo poliembrionario	(M9072/3)	
"�� y teratoma, mixto"	(M9081/3)	
� en		
�� adenoma pleom�rfico	(M8940/3)	
�� adenoma polipoide	(M8210/3)	
�� adenoma tubular	(M8210/3)	
�� adenoma velloso	(M8261/3)	
�� p�lipo adenomatoso	(M8210/3)	
�� poliposis adenomatosa del colon	153.9(M8220/3) 	
�� situ	(M8010/3)	"v�ase Carcinoma, in situ"
� endometrioide	(M8380/3)	
� eosin�filo	(M8280/3)	
�� sitio especificado		"v�ase Neoplasia, por sitio, maligna"
�� sitio no especificado	194.3	
� epidermoide	(M8070/3)	"v�ase adem�s Carcinoma, c�lulas escamosas"
"�� in situ, tipo de Bowen"	(M8081/2)	"v�ase Neoplasia, piel, in situ"
�� intrad�rmico		"v�ase Neoplasi, piel, in situ"
"�� y adenocarcinoma, mixto"	(M8560/3)	
� escamoso (c�lulas)	(M8070/3)	
"�� c�lulas peque�as, tipo no queratinizante"	(M8073/3)	
�� c�lulas tipo fusiforme	(M8074/3)	
"�� intraepid�rmico, tipo Bowen"		"v�ase Neoplasi, piel, in situ"
�� microinvasivo	(M8076/3)	
��� sitio especificado		"v�ase Neoplasia, por sitio, maligna"
��� sitio no especificado	180.9	
�� papilar	(M8052/3)	
�� seudoglandular	(M8075/3)	
�� tipo adenoide	(M8075/3)	
�� tipo no queratinizante	(M8072/3)	
�� tipo queratinizante (c�lulas grandes)	(M8071/3)	
�� verrugoso	(M8051/3)	
"�� y adenocarcinoma, mixto"	(M8560/3)	
� escirroso	(M8141/3)	
� esclerosante no encapsulado	193(M8350/3) 	
"� fibroepitelial, tipo c�lulas basales "	(M8093/3)	"v�ase Neoplasia, piel, maligna"
� folicular	(M8330/3)	
�� sitio especificado		"v�ase Neoplasia, por sitio, maligna"
�� sitio no especificado	193	
�� tipo bien diferenciado	(M8331/3) 193	
�� tipo folicular puro	(M8331/3) 193	
�� tipo moderadamente diferenciada	(M8332/3) 193	
�� tipo trabecular	193(M8332/3) 	
�� y papilar (mixto)	193(M8340/3) 	
� gelatinoso	(M8480/3)	
� gl�ndulas sudor�paras	(M8400/3)	"v�ase Neoplasia, piel, maligna"
� hepatocelular	155.0(M8170/3) 	
"�� y conducto biliar, mixto"	155.0(M8180/3) 	
� hepatocolangiol�tico	155.0(M8180/3) 	
� hipernefroide	(M8311/3)	
� in situ	(M8010/2)	"v�ase adem�s Neoplasia, por sitio, maligna"
�� c�lulas escamosas	(M8070/2)	"v�ase adem�s Neplasia, por sitio, in situ"
��� con invasi�n estrumosa dudosa	(M8076/2)	
���� sitio especificado		"v�ase Neoplasia, por sitio, in situ"
���� sitio no especificado	233.1	
�� c�lulas transitorias	(M8120/2)	"v�ase Neoplasia, por sitio, in situ"
�� epidermoide	(M8070/2)	"v�ase adem�s, Neoplasia, por sitio, in situ"
��� con invasi�n estrumosa dudosa	(M8076/2)	
���� sitio especificado		"v�ase Neoplasia, por sitio, in situ"
���� sitio no especificado	233.1	
��� tipo de Bowen	(M8081/2)	"v�ase Neoplasia, piel, in situ"
�� intracanalicular (intraductal)	(M8500/2)	
��� sitio especificado		"v�ase Neoplasia, por sitio, in situ"
��� sitio no especificado	233.0	
�� lobular	(M8520/2)	
��� sitio especificado		"v�ase Neoplasia, por sitio, in situ"
��� sitio no especificado	233.0	
�� papilar	(M8050/2)	"v�ase Neoplasia, por sitio, in situ"
� infiltrante canalicular	(M8500/3)	
�� con enfermedad de Paget	(M8541/3)	"v�ase Neoplasia, mama, maligna"
�� sitio especificado		"v�ase Neoplasia, por sitio, maligna"
�� sitio no especificado	174.9	
� inflamatorio	(M8530/3)	
�� sitio especificado		"v�ase Neoplasia, por sitio, maligna"
�� sitio no especificado	174.9	
� intracanicular (intraductal) (no infiltrante)	(M8500/2)	
�� papilar	(M8503/2)	
��� sitio especificado		"v�ase Neoplasia, por sitio, in situ"
��� sitio no especificado	233.0	
�� sitio especificado		"v�ase Neoplasia, por sitio, in situ"
�� sitio no especificado	233.0	
� intraepid�rmico 	(M8070/2)	"v�ase adem�s Neoplasia, piel, in situ"
"�� c�lulas escamosas, tipo de Bowen"	(M8081/2)	"v�ase Neoplasia, piel, in situ"
� intraepitelial 	(M8010/2)	"v�ase adem�s Neoplasia, in situ"
�� c�lulas escamosas	(M8072/2)	"v�ase Neoplasia, in situ"
� intra�seo	170.1(M9270/3) 	
�� maxilar superior (hueso)	170.0	
"� juvenil, mama"	(M8502/3)	"v�ase Neoplasia, mama, maligna"
"� Kulchitsky, c�lulas de (tumor carcinoide de intestino)"	259.2	
� linfoepitelial	(M8082/3)	
� lobular (infiltrante)	(M8520/3)	
�� no infiltrante	(M8520/3)	
��� sitio especificado		"v�ase Neoplasia, por sitio, in situ"
��� sitio no especificado	233.0	
�� sitio especificado		"v�ase Neoplasia, por sitio, maligna"
�� sitio no especificado	174.9	
� medular	(M8510/3)	
�� con		
��� estroma amiloide	(M8511/3)	
���� sitio especificado		"v�ase Neoplasia, por sitio, maligna"
���� sitio no especificado	193	
��� estroma linfoide	(M8512/3)	
���� sitio especificado		"v�ase Neoplasia, por sitio, maligna"
���� sitio no especificado	174.9	
� mesometafr�nico	(M9110/3)	
� meson�frico	(M9110/3)	
� metast�sico	(M8010/6)	v�ase Met�stasis
� metat�pico	(M8095/3)	"v�ase Neoplasia, piel, maligna"
� mucinoso	(M8480/3)	
� mucoepidermoide	(M8430/3)	
� mucoide	(M8480/3)	
�� c�lulas	(M8300/3)	
��� sitio especificado		"v�ase Neoplasia por sitio, maligna"
��� sitio no especificado	194.3	
� mucoso	(M84/80/3)	
� no infiltrante		
�� intracanalicular	(M8500/2)	
��� papilar	(M8503/2)	
���� sitio especificado		"v�ase Neoplasia, por sitio, in situ"
���� sitio no especificado	233.0	
��� sitio especificado		"v�ase Neoplasia, por sitio, in situ"
��� sitio no especificado	233.0	
�� intrac�stico (M8504/2)		"v�ase Neoplasia, por sitio, in situ"
�� lobular	(M8520/2)	
��� sitio especificado		"v�ase Neoplasia, por sitio, in situ"
��� sitio no especificado	233.0	
� odontog�nico	170.1(M9270/3) 	
�� maxila superior (hueso)	170.0	
� onoc�tico	(M8290/3)	
� oxif�lico	(M8290/3)	
� papilar	(M8050/3)	
�� c�lulas escamosas	(M8052/3)	
�� c�lulas transitorias	(M8130/3)	
�� epidermoide	(M8052/3)	
�� intracanalicular (no infiltrante)	(M8503/2)	
��� sitio especificado		"v�ase Neoplasia, por sitio, in situ"
��� sitio no especificado	233.0	
�� seroso	(M8460/3)	
��� sitio especificado		"v�ase Neoplasia, por sitio, maligna"
��� sitio no especificado	183.0	
��� superficial	(M8461/3)	
���� sitio especificado		"v�ase Neoplasia, por sitio, maligna"
���� sitio no especificado	183.0	
�� y folicular (mixto)	193(M8340/3) 	
� papiloc�stico	(M8450/3)	
�� sitio especificado		"v�ase Neoplasia, por sitio, maligna"
�� sitio no especificado	183.0	
� pleom�rfico	(M8022/3)	
� productor de mucina	(M8481/3)	
"� Schmincke, de "	(M8082/3)	"v�ase Neoplasia, nasofaringe, maligna"
"� Schneider, de"	(M8121/3)	
�� sitio especificado		"v�ase Neoplasia, por sitio, maligna"
�� sitio no especificado	160.0	
� seb�ceo	(M8410/3)	"v�ase Neoplasia, piel, maligna"
� secretorio de mucina	(M8481/3)	
"� secretorio, mama"	(M8502/3)	"v�ase Neoplasia, mama, maligna"
� secundario	(M8010/6)	"v�ase Neoplasia, por sitio, maligna, secundaria"
� seroso	(M8441/3)	
�� papilar	(M8460/3)	
��� sitio especificado		"v�ase Neoplasia, por sitio, maligna"
��� sitio no especificado	183.0	
"�� superficial, papilar"	(M8461/3)	
��� sitio especificado		"v�ase Neoplasia, por sitio, maligna"
��� sitio no especificado	183.0	
"� Sertoli, c�lulas de"	(M8640/3)	
�� sitio especificado		"v�ase Neoplasia, por sitio, maligna"
�� sitio no especificado	186.9	
"� seudoglandular, c�lulas escamosas"	(M8075/3)	
� seudomucinoso	(M8470/3)	
�� sitio especificado		"v�ase Neoplasia, por sitio, maligna"
�� sitio no especificado	183.0	
� seudosarcomatoso	(M8033/3)	
� simple	(M8231/3)	
� s�lido	(M8230/3)	
�� con estroma amiloide	(M8511/3)	
��� sitio especificado		"v�ase Neoplasia, por sitio, maligna"
��� sitio no especificado	193	
� t�mico	164.0(M8580/3) 	
� tipo anapl�sico	(M8021/3)	
� tipo cilindriforme	(M8200/3)	
� tipo conducto biliar	(M8160/3)	
�� h�gado	155.1	
�� sitio especificado NCOC		"v�ase Neoplasia, por sitio, maligna"
�� sitio no especificado	155.1	
"�� y hepatocelular, mixto"	155.0(M8180/3)	
� tipo difuso	(M8145/3)	
�� sitio especificado		"v�ase Neoplasia, por sitio, maligna"
�� sitio no especificado	151.9	
� tipo intestinal	(M8144/3)	
�� sitio especificado		"v�ase Neoplasia, por sitio, maligna"
�� sitio no especificado	151.9	
"� tipo morfea, c�lulas basales"	(M8092/3)	"v�ase Neoplasia, piel, maligna"
� tipo no diferenciado	(M8020/3)	
� tipo Regaud	(M8082/3)	"v�ase Neoplasia, nasofaringe, maligna"
� trabecular	(M8190/3)	
� transitorio (c�lulas)	(M8120/3)	
�� papilar	(M8130/3)	
�� tipo c�lulas fusiformes	(M8122/3)	
� tubular	(M8211/3)	
� urotelial	(M8120/3)	
� velloso	(M8262/3)	
� ventr�culi	151.9	
� verrugoso (epidermoide) (c�lulas escamosas)	(M8051/3)	
Carcinomafobia	300.29	
Carcinomatosis		
� peritoneal	197.6(M8010/6) 	
� sitio especificado NCOC	(M8010/3)	"v�ase Neoplasia, por sitio, maligna"
� sitio no especificado	199.0(M8010/6) 	
Carcinosarcoma	(M8980/3)	"v�ase adem�s Neoplasia, por sitio, maligna"
� tipo embrionario	(M8981/3)	"v�ase Neoplasia, por sitio, maligna"
Cardenal (superficie de piel intacta)		v�ase adem�s Contusi�n
� con		
�� fractura		"v�ase Fractura, por sitio"
�� herida abierta		"v�ase Herida, abierta, por sitio"
� cord�n umbilical	663.6	
�� que afecta al feto o al reci�n nacido	726.6	
"� �rgano interno (abdomen, pecho o pelvis)"		"v�ase Herida, interna, por sitio"
"Cardiaca, ectopia"	746.87	
Cardiaco		v�ase adem�s enfermedad espec�fica
� marcapasos		
�� colocaci�n o ajuste	V53.3	
�� in situ	V45.0	
� muerte		"v�ase Enfermedad, coraz�n"
� taponamiento	423.9	
Cardialgia	786.51	"v�ase adem�s Dolor, precordial"
"Cardias, cardial"		v�ase enfermedad espec�fica
Cardiectasia		"v�ase Hipertrofia, cardiaca"
Cardiocalasia	530.8	
Cardiofobia	300.29	
Cardiomalacia	429.1	"v�ase adem�s Degeneraci�n, miocardiaca"
Cardiomegalia	429.3	"v�ase adem�s Hipertrofia, cardiaca"
� cong�nita	746.89	
� glicog�nica difusa	271.0	
� glic�geno	271.0	
� hipertensiva	402.90	"v�ase adem�s Hipertensi�n, coraz�n"
� idiop�tica	429.3	
Cardiomioliposis	429.1	"v�ase adem�s Degeneraci�n, miocardiaca"
Cardiomiopat�a (congestiva) (constrictiva) (familiar) (infiltrante) (obstructiva) (restrictiva) (espor�dica)	425.4	
� alcoh�lica	425.5	
� almacenamiento de glic�geno	271.0 [425.7]	
� amiloide	277.3 [425.7]
� beriberi	265.0 [425.7]
� cobalto-cerveza	425.5
� cong�nita	425.3
� debida a	
�� amiloidosis	277.3 [425.7]
�� ataxia de Friedrich	334.0 [425.8]
�� beriberi	265.0 [425.7]
�� distrofia muscular progresiva	359.1 [425.8]
�� enfermedad de Chagas	086.0
�� glicogenosis cardiaca	271.0 [425.7]
�� mioton�a atr�fica	359.2 [425.8]
�� mucopolisacaridosis	277.5 [425.7]
 �� sarcoidosis	135 [425.8]
� en	
�� enfermedad de Chagas	086.0
�� sarcoidosis	135 [425.8]	
� hipertr�fica		
�� no obstructiva	425.4	
�� obstructiva	425.1	
��� cong�nita	746.84	
� idiop�tica (conc�ntrica)	425.4	
� isqu�mica	414.8	
� metab�lica NCOC	277.9 [425.7]	
�� amiloide	277.3 [425.7]	
�� tirot�xica	242.9 [425.7]	v�ase adem�s Tirotoxicosis
�� tirotoxicosis	242.9 [425.7]	v�ase adem�s Tirotoxicosis
� nutritiva	269.9 [425.7]	
�� beriberi	265.0 [425.7]	
� oscura de Africa	425.2	
� postparto	674.8	
� primaria	425.4	
� secundaria	425.9	
� tirot�xica	242.9 [425.7]	v�ase adem�s Tirotoxicosis
� t�xica NCOC	425.9	
� tuberculosa	017.9 [425.8]	v�ase adem�s Tuberculosis
Cardionefritis		"v�ase Hipertensi�n, cardiorrenal"
Cardionefropat�a		"v�ase Hipertensi�n, cardiorrenal"
Cardionefrosis		"v�ase Hipertensi�n, cardiorrenal"
Cardioneurosis	306.2	
Cardiopat�a	429.9	"v�ase adem�s Enfermedad, coraz�n"
� hipertensiva	402.90	"v�ase adem�s Hipertensi�n, coraz�n"
� idiop�tica	425.4	
� mucopolisacaridosis	277.5 [425.7]	
Cardiopat�a nigra	416.0	
Cardiopericarditis	423.9	v�ase adem�s Pericarditis
Cardioptosis	746.87	
Cardiorrenal		v�ase enfermedad espec�fica
Cardiorrexis	410.9	"v�ase adem�s Infarto, miocardio"
Cardiosclerosis	414.0	
Cardios�nfisis	423.1	
Cardiosis		"v�ase Enfermedad, coraz�n"
Cardiospasmo (es�fago) (est�mago) (reflejo)	530.0	
� cong�nito	750.7	
Cardiostenosis		"v�ase Enfermedad, coraz�n"
Cardiotirotoxicosis		v�ase Hipertiroidismo
Cardiovascular		"v�ase enfermedad, espec�fica"
Carditis (aguda) (bacteriana) (cr�nica) (subaguda)	429.89	
"� Coxsackie, de "	074.20	
� hipertensiva	402.90	"v�ase adem�s Hipertensi�n, coraz�n"
� meningoc�cica	036.40	
� reum�tica		"v�ase Enfermedad, coraz�n, reum�tica."
� reumatoide	714.2	
"Carencia, carente"		
� 11-beta-hidroxilasa	255.2
� 17-alfa-hidroxilasa	255.2
� 18-hidroxiesteroide deshidrogenasa	255.2
� 20-alfa-hidroxilasa	255.2
� 21-hidroxilasa	255.2
� 3-beta-hidroxiesteroide deshidrogenasa	255.2
� �cido asc�rbico (con escorbuto)	267
� �cido cevitam�nico ( con escorbuto)	267
� �cido f�lico (vitamina Bc)	266.2
�� anemia	281.2
� �cido nicot�nico (amida)	265.2
� �cido pantot�nico	266.2
� adenohipofisaria	253.2
� adenosino deaminasa	277.2
� aldolasa (hereditaria)	271.2
� aldolasa de fructosa-1-fosfato	271.2
� alfa-1-antitripsina	277.6	
� alfa-fucosidasa	271.8	
� alfa-lipoprote�na	272.5	
� alfa-mannosidasa	271.8	
� amino�cido	270.9	
� anemia		"v�ase Anemia, carencia"
� anemia por glucosa-6-fosfato deshidrogenasa	282.2	
� aneurina	265.1	
�� con beriberi	265.0	
� anticuerpos NCOC	279.00	
� antihemof�lico		
�� factor (A)	286.0	
��� B	286.1	
��� C	286.2	
�� globulina (AHG) NCOC	286.0	
� antitripsina	277.6	
� argininosuccinata de sintetasa o liasa	270.6	
� autoprotrombina		
�� C	286.3	"v�ase adem�s Defecto, coagulaci�n"
�� I 	286.3	"v�ase adem�s Defecto, coagulaci�n"
�� II	286.1	
� biotina	266.2	
� bradiquinasa-1	277.6	
� brote de diente	520.0	v�ase adem�s Anodoncia
� cabeza	V48.0	
� calciferol	268.9	
�� con		
��� osteomalacia	268.2	
��� raquitismo	268.0	v�ase adem�s Raquitismo
� calcio	275.4	
�� diet�tico	269.3	
"� calor�as, grave"	261	
� carbamilfosfato sintetasa	270.6	
� cardiaca	428.0	"v�ase adem�s Insuficiencia, mioc�rdica"
� carnitilpalmitil transferasa	791.3	
� caroteno	264.9	
"� Carr, factor de"	286.9	"v�ase adem�s Defecto, coagulaci�n"
"� c�lulas intersticiales, hormona estimuladora de (HECI) (ICSH)"	253.4	
� ceruloplasmina	275.1	
"� Christmas, factor de"	286.1	
� cianocobalamina (vitamina B12)	266.2	
� cinc	269.3	
� citrina	269.1	
"� coagulaci�n, factor de NCOC"	286.9	
�� con		
��� aborto		"v�ase Aborto, por tipo, con hemorragia"
��� embarazo ect�pico	639.1	v�ase adem�s categor�as 634-638
��� embarazo molar	639.1	v�ase adem�s categor�as 630-632
�� adquirida (cualquiera)	286.7	
�� anteparto o intraparto	641.3	
��� que afecta al feto o al reci�n nacido	762.1	
�� debida a		
��� carencia de vitamina K	286.7	
��� enfermedad hep�tica	286.7	
�� posparto	666.3	
"�� reci�n nacidos, transitoria"	776.3	
�� tipo especificado NCOC	286.3	
� co�gulos (sangre)	286.9	"v�ase adem�s Defecto, coagulaci�n"
� cobre NCOC	275.1	
� colina 	266.2	
"� combinada, dos o m�s factores de coagulaci�n"	286.9	"v�ase adem�s Defecto, coagulaci�n"
� corticoadrenal	255.4	
"� crecimiento humano, hormona de"	253.3	
� cromo	269.3	
� cuello	V48.1	
�� dedo de mano		"v�ase Ausencia, dedo de mano"
�� dedo de pie		"v�ase Ausencia, dedo de pie"
� derivaci�n de monofosfato de hexosa (MFH)	282.2	
� desmolasa	255.2	
� dieta	269.9	
� dihidrofolato reductasa	281.2	
� dihidropteridina reductasa	270.1	
� disacaridasa (intestinal)	271.3	
� edema	262	
� eje craneofacial	756.0	
� endocrina	259.9	
� enfermedad NCOC	269.9	
� enzima ramificante (amilopectinosis)	271.0	
� enzimas desramificantes (dextrinosis l�mite)	271.0	
"� enzimas, circulantes NCOC"	277.6	"v�ase adem�s Carencia, pro enzima espec�fica"
� ergosterol	268.9	
�� con		
��� osteomalacia	268.2	
��� raquitismo	268.0	v�ase adem�s Raquitismo
� especificada NCOC	269.8	
"� estimuladora de melanocitos, hormona (HEM) (MSH)"	253.4	
� factor 	286.9	"v�ase adem�s Defecto, coagulaci�n"
�� Hageman	286.3	
�� I (cong�nita) (fibrin�geno)	286.3	
��� anteparto o intraparto	641.3	
���� que afecta a feto o al reci�n nacido	762.1	
��� posparto	666.3	
"��� reci�n nacido, transitoria"	776.3	
�� II (cong�nita) (protrombina)	286.3	
�� IX (Christmas) (cong�nita) (funcional)	286.1	
�� m�ltiple (cong�nita)	286.9	
��� adquirida	286.7	
�� V (cong�nita) (labil)	286.3	
�� VII (cong�nita) (estable)	286.3	
�� VIII (cong�nita) (funcional)	286.0	
��� con		
���� defecto funcional	286.0	
���� defecto vascular	286.4	
�� X (cong�nita) (Stuart-Prower)	286.3	
�� XI (cong�nita) (antecedente de tromboplastina pl�smica)	286.2	
�� XII (cong�nita) (Hageman)	286.3	
�� XIII (cong�nita) (estabilizante de fibrina)	286.3	
� factor de activaci�n (sangre)	286.3	"v�ase adem�s Defecto, coagulaci�n"
� factor de complemento NCOC	279.8	
� factor de contacto	286.3	"v�ase adem�s Defecto, coagulaci�n"
� factor de cristal	286.3	
� factor estabilizante de fibrina (cong�nita)	286.3	"v�ase adem�s Defecto, coagulaci�n"
�� adquirida	286.7	
� factor estable (cong�nita) 	286.3	"v�ase adem�s Defecto, coagulaci�n"
�� adquirida	286.7	
� factor intr�nseco (de Castle) (cong�nita)	281.0	
� factor l�bil (cong�nita)	286.3	"v�ase adem�s Defecto, coagulaci�n"
�� adquirida	286.7	
� fibrinasa	286.3	"v�ase adem�s Defecto, coagulaci�n"
� fibrin�geno (cong�nita)	286.3	"v�ase adem�s Defecto, coagulaci�n"
�� adquirida	286.6	
"� Fletcher, factor de"	286.9	"v�ase adem�s Defecto, coagulaci�n"
� fluorina	269.3	
"� folato, anemia"	281.2	
� fosfofructoquinasa	271.2	
� fosfoglucomutasa	271.0	
�� fosfohexosisomerasa	271.0	
� fosforilasa de nucleosida de purina	277.2	
� fosforilasa del h�gado	271.0	
"� fosforilasa quinasa, h�gado"	271.0	
� fructoquinasa	271.2	
"� fructosa-1, 6-difosfato"	271.2	
� FSH (hormona estimuladora de fol�culos)	253.4	
� fucosidasa	271.8	
� galactoquinasa	271.1	
� galactosa 1-fosfato uridiltransferasa	271.1	
� gammaglobulina en la sangre	279.00	
� globulina AC (cong�nita)	286.3	"v�ase adem�s Defecto, coagulaci�n"
�� adquirida	286.7	
� globulina aceleradora (Ac G) (sangre) 	286.3	"v�ase adem�s Defecto, coagulaci�n"
� glucocorticoide	255.4	
� gluc�geno sintetasa	271.0	
� gluconiltransferasa	277.4	
� glucosa-6-fosfatasa	271.0	
� glucosa-6-fosfato deshidrogenasa (anemia)	282.2	
� glutati�n eritroc�tico (anemia)	282.2	
� glutati�n-reductasa (anemia)	282.2	
"� Hageman, factor de (cong�nita)"	286.3	"v�ase adem�s Defecto, coagulaci�n"
� HCH (hormona de crecimiento humano)	253.3	
� Hemoglobina	285.9	v�ase adem�s Anemia
� hepatofosforilasa	271.0	
� HG-PRT	277.2	
� hidroxilasa	255.2	
� hidroxilasa de fenilalina	270.1	
"� hierro, anemia"	280.9	
� hipoxantina-guanina fosforibosiltransferasa (HG-PRT)	277.2	
� hormona		"v�ase adem�s Carencia, por hormona espec�fica"
�� crecimiento (humano) (aislado)	253.3	
�� estimuladora de c�lulas intersticiales	253.4	
�� estimuladora de fol�culos	253.4	
�� estimuladora de melanocitos	253.4
"�� humana, de crecimiento"	253.3
�� luteinizante	253.4
�� pituitaria anterior (aislada) (parcial) NCOC	253.4
��� crecimiento (humano)	253.3
�� testicular	257.2
� hormona antidiur�tica	253.5
� hormona de crecimiento	253.3
� hormona estimuladora de fol�culos (HEF)	253.4
� humoral	279.00
�� con	
��� aumento de IgM	279.05
��� hiper-IgM	279.05
���� ligada al cromosoma X	279.05
���� recesiva autos�mica	279.05
�� especificada NCOC	279.09
�� hipogammaglobulinemia cong�nita	279.04
��� no ligada al sexo	279.06
�� inmunoglobulina selectiva NCOC	279.03
��� IgA	279.01
��� IgG	279.03
��� IgM	279.02
���� aumento de	279.05
� ICSH (HECI) (hormona estimuladora de c�lulas intersticiales)	253.4
"� inferior, miembro"	V49.0
�� cong�nita	755.30
��� con ausencia total de elementos distales	755.31
"��� longitudinal (completa) (parcial) (con carencias distales, incompleta)"	755.32
���� con ausencia total de elementos distales	755.31
���� falange(s)	755.39
����� cuando significa todos los d�gitos	755.31
���� femoral	755.34
"���� femoral, tibial, peroneal combinada (incompleta)"	755.33
���� metatarsiano(s)	755.38
���� peroneal	755.37
���� tarsiano(s)	755.38
���� tibia	755.36
���� tibioperoneal	755.35
��� transversal	755.31
� inhibidor de alfa-1-tripsina	277.6
� inmunidad NCOC	279.3
�� combinada (grave)	279.2
��� s�ndrome de	279.2
�� humoral NCOC	279.00
�� IgA (secretoria)	279.01
�� IgG	279.03
�� IgM	279.02
�� mediada por c�lulas	279.10
��� con		
���� hiperinmunoglobulinemia	279.2	
���� trombocitopenia y eczema	279.12	
��� especificada NCOC	279.19	
�� variable com�n	279.06	
"� inmunoglobulina, selectiva NCOC"	279.03	
�� IgA	279.01	
�� IgG	279.03	
�� IgM	279.02	
� inositol (complejo B)	266.2	
� interfer�n	279.4	
� invertasa	271.3	
� lactasa	271.3	
"� Laki-Lorand, factor de"	286.3	"v�ase adem�s Defecto, coagulaci�n"
� lectina-colesterol aciltransferasa	272.5	
� LH (HL) (hormona luteinizante)	253.4	
� lipocaico	577.8	
� lipoide (alta densidad)	272.5	
� lipoprote�na (familiar) (alta densidad)	272.5	
� l�quido lagrimal (adquirida)	375.15	
�� cong�nita	743.64	
"� lisomal alfa-1, 4-glucosidasa"	271.0	
"� luteinizante, hormona (HL) (LH)"	253.4	
� magnesio	275.2	
� mannosidasa	271.8	
� menadiona (vitamina K)	269.0	
�� reci�n nacidos	776.0	
� mental (familiar) (hereditaria)	319	"v�ase adem�s Retraso, mental"
� miembro	V49.0	
�� inferior	V49.0	
��� cong�nita	755.30	"v�ase adem�s Carencia, inferior, miembro, cong�nita"
�� superior	V49.0	
��� cong�nita	725.20	"v�ase adem�s Carencia, inferior, miembro, cong�nita"
� mineral NCOC	269.3	
� mioc�rdica	428.0	"v�ase adem�s Insuficiencia, mioc�rdica"
� miofosforilasa	271.0	
� molibdeno	269.3	
� moral	301.7	
� movimientos sac�dicos de los ojos	379.57	
� movimientos suaves de seguimiento (ojo)	379.58	
"� m�ltiple, s�ndrome de"	260	
� NADH (DPNH) -metemoblobina-reductasa (cong�nita)	289.7	
� NADH diaforasa o reductasa (cong�nita)	289.7	
� nariz	V48.8	
� niacina (amida-) (-tript�fano)	265.2	
� nicotinamida	265.2	
� n�mero de dientes	520.0	v�ase adem�s Anodoncia
"� nutrici�n, nutritiva"	269.9	
�� especificada NCOC	269.8	
� o�do(s)	269.9	
� �rgano interno	V47.0	
� ornitina transcarbamilasa	270.6	
� ov�rica	256.3	
� oxidasa de �cido homogent�stico	270.2	
� ox�geno	799.0	v�ase adem�s Anoxia
� paratiroides (gl�ndula)	252.1	
� p�rpado(s)	V48.8	
� piracina (alfa) (beta)	266.1	
� piridoxal	266.1	
� piridoxamina	266.1	
� piridoxina (derivados)	266.1	
� piruvato quinasa (PK)	282.3	
� pituitaria (anterior)	253.2	
�� posterior	253.5	
� placenta		"v�ase Placenta, insuficiencia"
� plaquetas NCOC	287.1	
�� constitucional	286.4	
� plasma		
�� celular	279.00	
�� prote�nico (paraproteinemia) (piroglobulinemia)	273.8	
��� gammaglobulina	279.00	
�� tromboplastina		
��� antecedente (PTA)	286.2	
��� componente (PTC)	286.1	
� poliglandular	258.9	
� potasio (K)	276.8	
� proacelerina (cong�nita)	286.3	"v�ase adem�s Defecto, cong�nito"
�� adquirida	286.7	
"� proconvertina, factor de (cong�nita)"	286.3	"v�ase adem�s Defecto, coagulaci�n"
�� adquirida	286.7	
"� proconvertina, factor de (cong�nita)"	286.3	"v�ase adem�s Defecto, coagulaci�n"
�� adquirida	286.7	
� prolactina	253.4	
� prote�na	260	
�� anemia	281.4	
�� plasma		"v�ase Carencia, plasma, prote�nas"
� protrombina (cong�nita)	286.3	"v�ase adem�s Defecto, coagulaci�n"
�� adquirida	286.7	
"� Prower, factor de"	286.3	"v�ase adem�s Defecto, coagulaci�n"
� PRT	277.2	
� psicobiol�gica	301.6	
� PTA	286.2	
� PTC	286.1	
� riboflavina (vitamina B2)	266.0	
� sal	276.1	
� sal biliar	579.8	
� salivaci�n	527.7	
� secreci�n		
�� gl�ndula salival (cualquiera)	527.7	
�� orina	788.5	
�� ovario	256.3	
� selenio	269.3	
� seudocolinesterasa	289.8	
� s�ndrome de m�sculo abdominal	756.7	
"� s�ndrome de, m�ltiple"	260	
� sistema nervioso central	349.9	
� sodio (Na)	276.1	
� SPCA	286.3	"v�ase adem�s Defecto, coagulaci�n"
"� Stuart (-Prower), factor de"	286.3	"v�ase adem�s Defecto, coagulaci�n"
� sucrasa	271.3	
� sucrasa-isomaltasa	271.3	
� suero		
"�� antitripsina, familiar"	277.6
�� prote�nas (cong�nita)	273.8
� sulfito oxidasa	270.0
"� superior, miembro"	V49.0
�� cong�nita	755.20
��� con ausencia total de elementos distales	755.21
"��� longitudinal (completa) (parcial) (con carencias distales, incompleta)"	755.22
���� carpiano(s)	755.28
���� cubital	755.27
���� falange(s)	755.29
����� cuando significa todos los d�gitos	755.21
���� humeral	755.24
"���� humeral, radial, cubital, combinada (incompleta)"	755.23
���� metacarpiano(s)	755.28
���� radial	755.26
���� radiocubital	755.25
��� transversal (completa) (parcial)	755.21	
"� tiamina, tiam�nica (cloruro)"	265.1	
� timolinf�tica	279.2	
� tiroidea (gl�ndula)	244.9	
� tocoferol	269.1	
� trombopoyeteno	287.3	
� tromboquinasa	286.3	"v�ase adem�s Defecto, coagulaci�n"
�� reci�n nacidos	776.0	
� tronco	V48.1	
� UDPG-gluc�geno transferasa	271.0	
� vascular	459.9	
� vasopresina	253.5	
� viosterol	268.9	"v�ase adem�s Carencia, calciferol"
� visi�n de color (cong�nita)	368.59	
�� adquirida	368.55	
� vitamina (m�ltiple) NCOC	269.2	
�� A 	264.9
��� con	
���� ceguera nocturna	264.5
"���� cicatriz de c�rnea, xeroft�lmica"	264.6
���� manchas de Bit�t	264.1
����� corneales	264.2
������ con ulceraci�n corneal	264.3
���� manifestaci�n especificada NCOC	264.8
����� ocular	264.7
���� queratomalacia	264.4
"���� queratosis, folicular"	264.8
���� xeroderma	264.8
���� xeroftalmia	264.7
���� xerosis	
����� conjuntival	264.0
������ con manchas de Bit�t	264.1
����� corneal	264.2
������ con ulceraci�n corneal	264.3
�� �cido f�lico	266.2
�� �cido nicot�nico	265.2
�� B (complejo) NCOC	266.9
��� con	
���� beriberi	265.0
���� pelagra	265.2
��� tipo especificado NCOC	266.2
�� B1 NCOC	265.1
��� beriberi	265.0
�� B12	266.2
�� B2	266.0
�� B6	266.1
�� Bc (�cido f�lico)	266.2
�� C (�cido asc�rbico) (con escorbuto)	267
�� D (calciferol) (ergosterol)	268.9	
��� con		
���� osteomalacia	268.2	
���� raquitismo	268.0	v�ase adem�s Raquitismo
�� E	269.1	
�� especificada NCOC 	269.1 	
�� G	266.0	
�� H	266.2	
�� K	269.0	
��� de los reci�n nacidos	776.0	
�� P	269.1	
�� PP	265.2	
� yodo	269.3	
"Cariados, dientes"	521.0	
Caries		
� dental	521.0	
� dientes	521.0	
� senil	797	
Caries (hueso)	015.9 [730.8]	"v�ase adem�s Tuberculosis, hueso"
� cadera	015.1 [730.85]	v�ase adem�s Tuberculosis
� cemento	521.0	
� cerebrospinal (tuberculosa)	015.0 [730.88]	
� dental (aguda) (cr�nica) (incipiente) (infectada) (con exposici�n de la pulpa)	521.0	
� dentina (aguda) (cr�nica)	521.0	
� detenida	521.0	
� dientes (interna)	521.0	
� esmalte (aguda) (cr�nica) (incipiente)	521.0	
"� espina, espinal (columna) (tuberculosa)"	015.0 [730.88]	
� huesecillo	385.24	
� laberinto	386.8	
� mastoidea (cr�nica) (ap�fisis)	383.1	
� meato externo	380.89	
� miembro NCOC	015.7 [730.88]	
� nariz	015.7 [730.88]	
� o�do medio	385.89	
� �rbita	015.7 [730.88]	
� pe�asco	383.20	
� rodilla	015.2 [730.86]	
� sacro (tuberculoso)	015.0 [730.88]	
� sifil�tica	095.5	
�� cong�nita	090.0 [730.88]	
� v�rtebra (columna) (tuberculosa)	015.0 [730.88]	
"Carini, s�ndrome de (ictiosis cong�nita)"	757.1	
Carne		
� asma de empaquetadores de	506.9	
� envenenamiento por		"v�ase Envenenamiento, alimento"
Carne viva	781.0	
"Carnosa, mola"	631	
Carnosinemia	270.5	
Carotidina	337.0	
Carotinemia (diet�tica)	278.3	
Carotinosis (cutis) (piel)	278.3	
"Carpinteros, s�ndrome de los"	759.8	
"Carpopedal, espasmo"	781.7	v�ase adem�s Tetania
Carpoptosis	736.05	
"Carri�n, enfermedad de (Bartonelosis)"	088.0	
Carter		
� fiebre recurrente de (asi�tica)	087.0	
Cart�lago		v�ase enfermedad espec�fica
Car�nculo (inflamado)		
"� absceso, lagrimal"	375.30	v�ase adem�s Dacriocistitis
� conjuntiva	372.00	
�� aguda	372.00	
� labio vulvar (mayor) (menor)	616.8	
� lagrimal	375.30	
� p�rpado	373.00	
� uretra (benigno)	599.3	
� vagina (pared)	616.8	
"Cascada, est�mago en"	537.6	
Caseificaci�n de gl�ndula linf�tica	017.2	v�ase adem�s Tuberculosis
Caseosa		
� bronquitis		"v�ase Tuberculosis, pulmonar"
� meningitis	013.0	
� neumon�a		"v�ase Tuberculosis, pulmonar"
Caspa	690	
"Cassidy (-Scholte), s�ndrome de (carcinoide maligno)"	259.2	
"Castleman, tumor o linfoma de (hiperplasia de n�dulo linf�tico mediast�nica)"	785.6	
Castraci�n traum�tica	878.2	
� complicada	878.3	
Catafasia	307.0	
Catalepsia	300.11	
� catat�nica (aguda)	295.2	v�ase adem�s Esquizofrenia
� esquizofr�nica	295.2	v�ase adem�s Esquizofrenia
� hist�rica	300.11	
Catalepsia	780.0	
� hist�rico	300.13	
Cataplexia (idiop�tica)	347	
Catarata (anterior cortical) (anterior polar) (capsular) (central) (cortical) (hipermadura) (incipiente) (inmadura) (madura) (negra) (nuclear)	366.9	
� anterior		
�� piramidal	743.31	
�� polar subcapsular		
"��� infantil, juvenil o presenil"	366.01	
��� senil	366.13	
�� y embrionaria axial posterior	743.33	
� asociada con		
�� calcinosis	275.4 [366.42]	
�� disostosis craneofacial	756.0 [366.44]
�� galactosemia	271.1 [366.44]
�� hiperparatiroidismo	252.1 [366.42]
�� neovascularizaci�n	366.33
�� trastornos miot�nicos	359.2 [366.43]
� azul punteada	743.39
� cer�lea	743.39
� complicada NCOC	366.30
� cong�nita	743.30
�� capsular o subcapsular	743.31
�� cortical	743.32
�� nuclear	743.33
�� tipo especificado NCOC	743.39
�� total o subtotal	743.34
�� zonular	743.32
"� copo de nieve, en forma de"	250.5 [366.41]
� coronaria (cong�nita)	743.39	
�� adquirida	366.12	
� cupuliforme	366.14	
� debida a		
�� calcosis	366.34 [360.24]	
�� coroiditis cr�nica	366.32 [363.20]	v�ase adem�s Coroiditis
�� distrofia retiniana pigmentaria	366.34 [362.74]	
�� glaucoma	366.31 [365.39]	
"�� infecci�n, intraocular NCOC"	366.32	
"�� iridociclitis, cr�nica"	366.33 [364.10]	
�� miop�a degenerativa	366.34 [360.21]	
�� trastorno ocular inflamatorio NCOC	366.32	
� diab�tica	250.5 [366.41]	
� el�ctrica	366.46	
� en enfermedad ocular NCOC	366.30	
� especificada NCOC	366.8	
"� girasol, en forma de"	360.24 [366.34]	
� heterocr�mica	366.33	
� inducida por droga	366.45	
� infantil	366.00	"v�ase adem�s Catarata, juvenil"
� intumescente	366.12	
� irradiaci�n	366.46	
� juvenil	366.00	
�� cortical	366.03	
�� especificada NCOC	366.09	
�� formas combinadas	366.09	
�� lamelar o laminar	366.03	
�� nuclear	366.04	
�� polar subcapsular anterior	366.01	
�� polar subcapsular posterior	366.02	
�� zonular	366.03	
� lamelar o laminar	743.32	
"�� infantil, juvenil o presenil"	366.03	
� miot�nica 	359.2 [366.43]	
� mixedema	244.9 [366.44]	
"� Morgagni, de"	366.18	
"� posterior, polar (capsular)"	743.31	
"�� infantil, juvenil o presenil"	366.02	
�� senil	366.14	
� presenil	366.00	"v�ase adem�s Catarata, juvenil"
� punteada		
�� adquirida	366.12	
�� cong�nita	743.39	
� radiaci�n	366.46	
� rayos t�rmicos	366.46	
� secundaria (membran�cea)	366.50	
�� cuando oscurece la vista	366.53	
"�� tipo especificado, que no oscurece la vista"	366.52	
� senil	366.10
�� cortical	366.15
�� especificada NCOC	366.19
�� formas combinadas	366.19
�� hipermadura	366.18
�� incipiente	366.12
�� inmadura	366.12
�� madura	366.17
�� nuclear	366.16
�� polar subcapsular anterior	366.13
�� polar subcapsular posterior	366.14
�� total o subtotal	366.17
"� sopladores de vidrio, de los"	366.46
� subtotal (senil)	366.17
�� cong�nita	743.34
� tet�nica NCOC	252.1 [366.42]
� total (madura) (senil)	366.17	
�� cong�nita	743.34	
�� localizada	366.21	
�� traum�tica	366.22	
� t�xica	366.45	
� traum�tica	366.20	
�� parcialmente resuelta	366.23	
�� total	366.22	
� zonular (perinuclear)	743.32	
"�� infantil, juvenil o presenil"	366.03	
Catarata	366.10	v�ase adem�s Catarata
� brunescens	366.16	
"� cerulea, cer�lea"	743.39	
"� complicata, complicada"	366.30	
"� congenita, cong�nita"	743.30	
"� coralliformis, coraliforme"	743.39	
� coronaria (cong�nita)	743.39	
�� adquirida	366.12	
� diab�tica	250.5 [366.41]	
"� floriformis, en forma de flor"	360.24 [366.34]	
� membran�cea		
�� accreta	366.50	
�� congenita	743.39	
"� nigra, negra"	366.16	
Catarrhus aestivus	477.9	"v�ase adem�s Fiebre, heno"
"Catarro, catarral (inflamaci�n)"	460	v�ase adem�s enfermedad espec�fica
� agudo	460	
"� asma, asm�tica"	493.9	v�ase adem�s Asma
� boca	528.0	
"� Bostock, de"	477.9	"v�ase adem�s Fiebre, heno"
� bronquial	490	
�� aguda	466.0	
�� cr�nica	491.0	
�� subaguda	466.0	
� congesti�n	472.0	
� conjuntivitis	372.03	
� cr�nico	472.0	
"� cuello uterino, cervical (canal)"		v�ase Cervicitis
� debido a s�filis	095.9	
�� cong�nito	090.0	
� ent�rico		v�ase Enteritis
� epid�rmico	487.1	
"� Eustaquio, de "	381.50	
� fauces	462	v�ase adem�s Faringitis
� febril	460	
� fibrinoso agudo	466.0	
� garganta	472.1	
� gastroent�rico		v�ase Enteritis
� gastrointestinal		v�ase Enteritis
� gingivitis	523.0	
� heno	477.9	"v�ase adem�s Fiebre, heno"
� h�gado	070.1	
�� con coma hep�tico	070.0	
� infeccioso	460	
� intestinal		v�ase Enteritis
� intestino		v�ase Enteritis
� laringe	476.0	"v�ase adem�s Laringitis, cr�nica"
� nariz		"v�ase Catarro, nasal"
� nasal (cr�nico)	472.0	v�ase adem�s Rinitis
�� agudo	460	
� nasobronquial	472.2	
� nasofar�ngeo (cr�nico)	472.2	
�� agudo	460	
"� neumoc�cico, agudo"	466.0	
� oftalmia	372.03	
� o�do medio (cr�nico)		"v�ase Otitis media, cr�nico"
� ojo (agudo) (verano)	372.03	
� pecho	490	v�ase adem�s Bronquitis
� primavera (ojo)	372.13	
� pulm�n	490	v�ase adem�s Bronquitis
�� agudo	466.0	
�� cr�nico	491.0	 
� pulmonar	490	v�ase adem�s Bronquitis
�� agudo	466.0	
�� cr�nico	491.0	
� sofocante	493.9	v�ase adem�s Asma
� traque�tis	464.10	
�� con obstrucci�n	464.11	
� tubotimp�nico	381.4	
�� agudo	381.00	"v�ase adem�s Otitis media, aguda, no supurativa"
�� cr�nico	381.10	
� vasomotor	477.9	"v�ase adem�s Fiebre, heno"
� verano (heno)	477.9	"v�ase adem�s Fiebre, heno"
