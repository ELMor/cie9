�� cavidad oral	935.0
�� ciego	936
�� colon 	936
�� conductos o gl�ndulas lagrimales	930.2
�� conjuntiva	930.1
�� c�rnea	930.0
�� cuello uterino (canal)	939.1
"�� delgado, intestino"	936
�� duodeno	936
�� enc�a	935.0
�� escler�tica	930.1
�� es�fago	935.1
�� est�mago (bola de pelo)	935.2
�� faringe	933.0
�� fosa(s) nasal(es)	932
"�� frontal, seno"	932
�� garganta	933.0	
�� globo del ojo	930.8	
��� intraocular		"v�ase Cuerpo extra�o, intraocular "
��� penetrante	871.6	
���� magn�tico	871.5	
����� retenido o antiguo	360.50	
���� retenido o antiguo	360.60	
"�� Highmore, antro de"	932	
�� hipofaringe	933.0	
"�� ileocecal, v�lvula"	936	
�� �leon	936	
�� ingerido	938	
��� lengua	933.0	
�� inspiraci�n (de)	933.1	
�� intestino (delgado) (grueso)	936	
�� laringe	933.1	
�� lengua	935.0	
��� tragada	933.0	
�� nariz (fosa(s))	932	
"�� nasal, seno"	932	
�� nasofaringe	933.0	
�� o�do (externo)	931	
�� ojo (externo)	930.9	
��� intraocular		"v�ase Cuerpo extra�o, por sitio "
��� sitio especificado NCOC	930.8	
��� sitios combinados	930.8	
�� �rgano o tracto digestivo NCOC	938	
�� pabell�n de la oreja	931	
�� paladar	935.0	
�� p�rpado	930.1	
��� retenido o antiguo	374.86	
�� pene	939.3	
"�� piriforme, seno"	933.0	
�� pulm�n	934.8	
�� reborde alveolar	935.0	
�� recto	937	
�� rectosigmoideo	937	
��� uni�n	937	
�� saco conjuntival	930.1	
�� seno	932	
��� accesorio	932	
��� frontal	932	
��� maxilar	932	
��� nasal	932	
��� piriforme	933.0	
�� seno accesorio	932	
�� seno maxilar	932	
�� sofocaci�n por	933.1	"v�ase adem�s Asfixia, alimento"
�� tracto 	
��� gastrointestinal	938
��� genitourinario	939.9
��� respiratorio	934.9
���� parte especificada NCOC	934.8
�� tr�quea	934.0
�� tubo digestivo	938
�� ur�ter	939.0
�� uretra	939.0
�� �tero (cualquier parte)	939.1
�� vagina	939.2
�� v�lvula ileocecal	936
�� vejiga	939.0
�� v�as respiratorias (superiores)	933.0
��� inferiores	934.8
�� vulva	939.2
� retenido (antiguo) (no magn�tico) (en) 	
�� c�mara anterior (ojo)	360.61
��� magn�tico	360.51
�� cristalino	360.63
��� magn�tico	360.53
�� cuerpo ciliar	360.62
��� magn�tico	360.52
�� globo del ojo	360.60
��� magn�tico	360.50
�� intraocular	360.60
��� magn�tico	360.50
��� sitio especificado NCOC	360.69
���� magn�tico	360.59
�� iris	360.62
��� magn�tico 	360.52 
�� m�sculo	729.6
�� �rbita	376.6	
�� pared posterior del globo del ojo	360.65	
��� magn�tico	360.55	
�� p�rpado	374.86	
�� retina	360.65	
��� magn�tico	360.55	
�� retrobulbar	376.6	
�� tejido blando	729.6	
�� v�treo	360.64	
��� magn�tico	360.54	
� retina	871.6	
�� magn�tico	871.5	
��� retenido o antiguo	360.55	
�� retenido o antiguo	360.65	
"� superficial, sin herida abierta importante"	919.6	"v�ase adem�s Traumatismo, superficial, por sitio"
� v�treo (humor)	871.6	
�� magn�tico	871.5	
��� retenido o antiguo	360.54	
�� retenido o antiguo	360.64	
"Cuerpo o seno carot�deo, s�ndrome de"	337.0	
"Cuerpo, cuerpos "		
� Aschoff	398.0	"v�ase adem�s Miocarditis, reum�tica"
"� asteroide, v�treo"	379.22	
� basculante	307.3	
� citoides (retina)	362.82	
"� coroides, coloides (degenerativos)"	362.57	
�� hereditarios	362.77	
� de arroz (articulaci�n)	718.1	"v�ase adem�s Suelto, cuerpo, articulaci6n"
�� rodilla	717.6	
� drusen (retina)	362.57	v�ase adem�s Drusen
�� disco �ptico	377.21	
� extra�o		v�ase Cuerpo extra�o 
"� fibrina, pleura"	511.0	
� Hassall-Henle	371.41	
"� Mallory, de"	034.1	
� Mooser	081.0	
� Negri	071	
� sueltos(as) 		
�� articulaci�n	718.1	"v�ase adem�s Suelto, cuerpo, articulaci6n"
��� rodilla	717.6	
�� rodilla	717.6	
"�� vaina, tend�n"	727.82	
Cuidado (de) 		
� alivio durante vacaciones	V60.5	
� beb� en buen estado de salud	V20.1	
� falta de (durante o despu�s del parto) (bebe) (ni�o)	995.5	
�� cuando afecta a los padres o familia	V61.21	
�� cuando hace que la familia busque asesoramiento	V61.21	
�� persona especificada que no sea ni�o	995.81
� inadecuado (durante o despu�s de parto) (beb�) (ni�o)	995.5
�� cuando afecta a los padres o familia	V61.21
�� cuando hace que la familia busque asesoramiento	V61.21
�� persona especificada que no sea ni�o	995.81
� lactancia de la madre	V24.1
� miembro de familia (minusv�lido) (enfermo) 	
�� no disponible debido a 	
��� ausencia (persona que proporciona cuidados) (paciente)	V60.4
��� incapacidad (cualquier motivo) de la persona que proporciona cuidados	V60.4
�� proporcionado fuera deI hogar para aliviar durante �poca de vacaciones	V60.5
�� que crea problemas para la familia	V61.49
� ni�o (rutinario)	V20.1
� no disponible debido a 	
�� ausencia de la persona que proporciona los cuidados 	V60.4
�� incapacidad (cualquier motivo) de la persona que proporciona los cuidados	V60.4
� paciente en convalecencia despu�s de	V66.9
�� cirug�a NCOC	V66.0
�� psicoterapia	V66.3
�� quimoterapia	V66.2
�� radioterapia	V66.1
�� tratamiento (para)	V66.5
��� combinado	V66.6
��� fractura	V66.4
��� tipo especificado NCOC	V66.5
��� trastorno mental NCOC	V66.3
�� tratamiento m�dico NCOC	V66.5
�� tratamiento quir�rgico NCOC	V66.0
� posparto 	
�� inmediatamente despu�s del parto	V24.0
�� seguimiento rutinario	V24.2
� prenatal	V22.1
�� embarazo de alto riesgo	V23.9	
��� problema especificado NCOC	V23.8	
�� primer embarazo	V22.0	
Cuidados posoperatorios	V58.9	
� aperturas artificiales		"v�ase Atenci�n de, artificial, apertura "
� despu�s de cirug�a NCOC	V58.4	
� di�lisis extracorp�rea (intermitente) preparaci�n) (tratamiento) 	V56.0 	
� di�lisis peritoneal (intermitente) (preparaci�n) (tratamiento)	V56.8	
� di�lisis renal (intermitente) (preparaci�n) (tratamiento)	V56.0	
� educaci�n en el andar	V57.1 	
�� para empleo de miembro(s) artifical(es) 	V57.81 	
� educaci�n ort�ptica	V57.4	
� educaci�n ort�tica	V57.81 	
� ejercicio (remedial) (terap�utico)	V57.1	
�� respiraci�n 	V57.0 	
� ejercicio de respiraci�n 	V57.0 	
� fractura	V54.9
�� cuidado especificado NCOC	V54.8
�� extracci�n de 	
��� dispositivo de fijaci�n externa	V54.8
��� dispositivo de fijaci�n interna	V54.0
� marcapasos 	
�� cardiaco	V53.3
�� cerebro 	V53.0  
�� m�dula espinal	V53.0
�� nervio perif�rico	V53.0
�� seno carot�deo	V53.3
� marcapasos cardiaco	V53.3
� marcapasos de seno carot�deo	V53.3
� marcapasos neural (cerebro) (nervio perif�rico) (m�dula espinal)	V53.0
� ortod�ntica 	V58.5
� ortop�dica  	V54.9
�� cuidado especificado NCOC	V54.8
�� extracci�n de dispositivo de fijaci�n 	
��� externa 	V54.8
��� interna	V54.0
�� recambio de dispositivo externo de fijaci�n o tracci�n	V54.8
� procedimiento de rehabilitaci�n 	V57.9
�� ejercicios de respiraci�n	V57.0
�� ejercicios remediales	V57.1
�� ejercicios terap�uticos	V57.1
�� habla 	V57.3
�� ocupacional 	V57.2
�� ort�ptica 	V57.4
�� ort�tica 	V57.81
�� profesional	V57.2
�� terapia f�sica NCOC 	V57.1
�� tipo especificado NCOC 	V57.89
� que implica 	
�� di�lisis (intermitente) (preparaci�n) (tratamiento) 	
��� extracorp�rea	V56.0
��� peritoneal 	V56.8
��� renal 	V56.0
�� educaci�n en el andar	V57.1
��� para el empleo de miembro(s) artificial(es)	V57.81
�� educaci�n ort�ptica	V57.4
�� educaci�n ort�tica 	V57.81
�� extracci�n de 	
��� clavos 	V54.0
��� dispositivo de fijaci�n 	
���� externa 	V54.8
���� interna	V54.0
"��� dispositivo de tracci�n, externo"	V54.8
��� escayola	V54.8
��� placa de fractura	V54.0
��� suturas 	V58.3
��� tornillos 	V54.0
��� varillas 	V54.0
��� vendajes quir�rgicos 	V58.3
��� vendajes 	V58.3
�� sesi�n de radioterapia	V58.0
� rehabilitaci�n profesional	V57.2
� sesi�n de quimioterapia (adjuntiva) (mantenimiento)  	V58.1
� sesi�n de radioterapia 	V58.0
� terapia del habla 	V57.3
� terapia f�sica NCOC	V57.1
�� ejercicios de respiraci�n	V57.0
� terapia ocupacional	V57.2
� tipo especificado NCOC	V58.8
� transfusi�n de sangre. sin diagn�stico declarado	V58.2
"Culis, picor de los"	126.9	
"Cuna, muerte en"	798.0	
"Cuneiforme, v�rtebra"	733.00	v�ase adem�s Osteoporosis
"Curling, �lcera de"		"v�ase Ulcera, duodeno "
" Curschmann (-Batten (-Steiner, enfermedad o s�ndrome de"	359.2	
Curva		
� espalda (hist�rica)	300.11	
� nariz	738.0	
�� cong�nita	754.0	
Curvatura		
� antebrazo	736.09	
�� desde la l�nea mediana (c�bito valgo) 	736.01	
�� hacia la l�nea mediana (c�bito varo)	736.02	
� f�mur	736.89	
�� cong�nita	754.42	
� peron�	736.89	
�� cong�nita	754.43	
"� pierna(s), huesos largos, cong�nita"	754.44	
� radio	736.09	
�� desde la l�nea mediana (c�bito valgo) 	736.01	
�� hacia la l�nea mediana (c�bito varo)	736.02	
� tibia	736.89	
�� cong�nita	754.43	
Curvatura		
"� c�bito, idiop�tica, progresiva (cong�nita) "	755.54	
� espina (adquirida) (angular) (idiop�tica) (incorrecta) (postural)	737.9	
�� cifoscoli�tica	737.30	v�ase adem�s Cifoscoliosis
�� cif�tica	737.10	v�ase adem�s Cifosis
�� cong�nita	754.2	
��� debida a o asociada con  		
���� enfermedad de Charcot-Marie-Tooth 	356.1 [737.40]	
���� mucopolisacaridosis	277.5 [737.40]	
���� neurofibromatosis	237.7 [737.40]	
���� oste�tis  		
����� deformante	731.0 [73740	
����� fibrosa c�stica	252.0 [737.401]	
���� osteoporosis	733.00 [737.40]	v�ase adem�s Osteoporosis
���� poliomielitis	138 [737.40]	v�ase adem�s Poliomielitis
���� tuberculosis (curvatura de Pott)	015.0 [737.43]	v�ase  adem�s Tuberculosis
�� efecto tard�o de raquitismo	268.1 [737.40]	
�� escoli�tica	737.30	v�ase adem�s Escoliosis
�� especificada NCOC	737.8	
"�� Pott, de"	015.0 [737.40]	
�� tuberculosa	015.0 [737.40]	
"� �rgano o sitio, cong�nito NCOC"		v�ase  Distorsi�n  
� pene (lateral)	752.8	
"� Pott, de (espinal)"	015.0 [737.43]	v�ase adem�s Tuberculosis
"Cushing,de"		
"� basofilismo, enfermedad, o s�ndrome (yatrog�nico) (idiop�tico) (basofilismo pituitario) (pituitario dependiente) "	255.0	
� �lcera		"v�ase Ulcera, p�ptica"
Cushingoide debido a terapia con esteroides 		
� sobredosis o ingesti�n o administraci�n de sustancia incorrecta	962.0	
� sustancia correcta administrada de forma correcta	255.0	
Cut�neo		v�ase adem�s enfermedad espec�fica 
� cuerno (mejilla) (p�rpado) (boca)	702.8	
� hemorragia	782.7	
� larva migrans	126.9	
Cutis		v�ase adem�s enfermedad espec�fica 
� hiperel�stico	756.83	
�� adquirido	701.8	
� laxo	756.83	
�� senil	701.8	
� marm�reo	782.61	
� osteosis	709.3	
� pendular	756.83	
�� adquirido	701.8	
� romboidal nuca	701.8	
� verticis gyrata	757.39	
�� adquirido	701.8	
"Cyriax, s�ndrome de (costilla deslizante)"	733.99	
"Czerny, enfermedad de (hidrartrosis peri�dica de la rodilla)"	719.06	
"Da Costa, s�ndrome de (astenia neurocirculatoria)"	306.2	 
Daae (-Finsen) enfermedad de (pleurodinia epid�mica)	074.1	
"Dabney, garra de"	074.1	
"Dacrioadenitis, dacriadenitis"	375.00	
� aguda	375.01	
� cr�nica	375.02	
Dacriocistitis	375.30	
� aguda	375.32	
� cr�nica	375.42	
� flemonosa	375.33	
� neonatal	771.6	
� sifil�tica	095.8	
�� cong�nita	090.0	
"� tracomatosa, activa"	076.1	
�� efecto tard�o	139.1	
� tuberculosa	017.3	v�ase adem�s Tuberculosis
Dacriocistoblenorrea	375.42	
Dacriocistocele	375.43	
"Dacriolito, dacriolitiasis"	375.57	
Dacrioma	375.43	
Dacriopericistitis (aguda) (subaguda)	375.32	
� cr�nica	375.42	
Dacriops	375.11	
"Dacriosialadenopat�a, atr�fica"	710.2	
Dacriostenosis	375.56	
� cong�nita	743.65	
Dactilitis	686.9	
� c�lulas falciformes	282.61	
� hueso	730.2	v�ase adem�s Osteomielitis
� sifil�tica	095.5	
� tuberculosa	015.5	v�ase adem�s Tuberculosis
Dactilolisis espont�nea	136.0	
Dactilos�nfisis	755.10	v�ase adem�s Sindactilismo
"Dameshek, s�ndrome de (anemia eritrobl�stica)"	282.4	
"Dana-Putman, s�ndrome de (esclerosis combinada subaguda con anemia perniciosa)"	281.0 [336.2]	
"Danbolt (-Closs), s�ndrome de (acrodermatitis enterop�tica)"	686.8	
"Dandy, fiebre de"	061	
"Dandy-Walker, deformidad o s�ndrome de (atresia, foramen de Magendie)"	742.3	
� con espina b�fida	741.0	v�ase adem�s Espina b�fida
"Danielssen, enfermedad de (lepra anest�sica)"	030.1	
"Danlos, s�ndrome de"	756.83	
"Darier, enfermedad de (cong�nita) (queratosis folicular)"	757.39	
� cuando significa eritema anular centr�fugo	695.0	
� debida a carencia de vitamina A	264.8	
"Darier-Roussy, sarcoide de"	135	
"Darling, de "		
� enfermedad	115.00	v�ase adem�s Histoplasmosis americana
� histoplasmosis	115.00	"v�ase adem�s Histoplasmosis, americana"
Dartre (herpes)	054.9	
"Darwin, tub�rculo de"	744.29	
"Davidson, anemia de (refractaria)"	284.9	
"Davies, enfermedad de"	425.0	
"Davies-Colley, s�ndrome de (costilla deslizante)"	733.99	
"Dawson, encefalitis"	046.2	
Da�o(s)		
� arterioscler�tico		v�ase Arteriosclerosis 
� card�aco		"v�ase adem�s Enfermedad, coraz�n "
� cardiorrenal (vascular)	404.90	v�ase adem�s Hipertensi6n cardiorrenal
� cerebral NCOC		"v�ase Da�o(s), cerebro "
� cerebro	348.9	
"�� an�xico, hip�xico"	348.1	
��� durante o como resultado de un procedimiento	997.0	
�� debido a traumatismo al nacer	767.0	
�� m�nimo (ni�o)	314.9	v�ase adem�s Hiperquinesia
�� ni�o NCOC	343.9	
�� reci�n nacido	767.0	
"� c�ccix, cuando complica el parto"	665.6	
� coraz�n		"v�ase adem�s Enfermedad, coraz�n "
�� v�lvula		v�ase Endocarditis 
� coronario	414.9	"v�ase adem�s Isquemia, coraz�n"
� h�gado	571.9	
�� alcoh�lico	571.3	
� hipot�lamo NCOC	348.9	
� miocardio	429.1	"v�ase adem�s Degeneraci�n, mioc�rdica"
"� ojo, lesi�n de parto"	767.8	
� pelviano 		
"�� articulaci�n o ligamento, durante el parto"	665.6	
�� �rgano NCOC 		
��� con 		
���� aborto		"v�ase Aborto por tipo, con da�os a �rganos p�lvicos "
���� embarazo ect�pico	639.2	(v�ase adem�s categor�as 633.0-633.9)
���� embarazo molar	639.2	(v�ase adem�s categor�as 630-632)
��� despu�s de 		
���� aborto	639.2	
���� embarazo ect�pico o molar	639.2	
��� durante el parto	665.5	
� renal	593.9	v�ase adem�s Enfermedad renal
� sistema nervioso central		"v�ase Da�o(s), cerebro "
"� subendocardio, subendoc�rdico"	429.1	v�ase adem�s Degeneraci�n rnioc�rdica
� vascular	459.9	
DCM o MBD (disfunci�n cerebral m�nima) ni�o 	314.9	(v�ase adem�s Hipercinesia)
"De Lange, s�ndrome de (enano de Amsterdam retraso mental, y braquicefalia)"	759.89	
"De Morgan, manchas de (angiomas seniles)"	448.1	
"De Quervain, de "		
� enfermedad (vaina de tend�n)	727.04	
� tiroiditis (tiroiditis granulomatosa subaguda)	245.1	
"De Toni-Fanconi, s�ndrome de (cistinosis)"	270.0	
D�bil mental	317	
"D�bil, pulso r�pido debido a choque despu�s de traumatismo"	958.4	
Debilidad (general) (infantil) (posinfecciosa)	799.3	
� con dificultad nutritiva	269.9	
� cong�nita o neonatal NCOC	779.9	
� nerviosa	300.5	
� senil	797	
� vejez	797	
Debilidad	799.8	
� senil	797	
"Debilidad de, d�bil (generalizada)"	780.7	
� arcos plantares (adquirida)	734	
�� cong�nita	754.61	
� cong�nita	779.8	
"� coraz�n, cardiaca"	428.9	v�ase adem�s Fallo coraz�n
�� cong�nita	746.9	
� esf�nter de la vejiga	788.3	
� fondo pelviano	618.8	
� mental 	317	
� miocardio	428.9	"v�ase adem�s Fallo, cardiaco"
� m�sculo	728.9	
�� ocular		v�ase Estrabismo 
� pie (doble)		"v�ase Debilidad, arcos plantares "
� pulso	785.9	
� reci�n nacido	779.8	
� senil	797	
� valvular		v�ase Endocarditis
Debilitaci�n 		
� enfermedad	799.4	
�� debida a desnutrici�n	261	
� extrema (debida a desnutrici�n) 	261	
� muscular NCOC	728.2	
� par�lisis	335.21	
"D�bove, enfermedad de (esplenomegalia)"	789.2	
Decapitaci�n	874.9	
� fetal (para facilitar el parto)	763.8	
Deceleraci�n del 		
� coraz�n	427.89	
� flujo urinario	788.6	
Deciduitis (aguda) 		
� con 		
�� aborto		"v�ase Aborto, por tipo, con septicemia "
�� embarazo ect�pico	639.0	(v�ase adem�s categor�as 633.0-633.9)
�� embarazo molar	639.0	(v�ase adem�s categor�as 630-632)
� despu�s de 		
�� aborto	639.0	
�� embarazo ect�pico o molar	639.0	
� en el embarazo	646.6	
"� puerperal, posparto"	670	
� que afecta al feto o al reci�n nacido	760.8	
Deciduoma maligno  	181 (M9100/33)	
"Deciduos, dientes (persistentes)"	520.6	
"Decubital, dec�bito, gangrena de"	707.0[785.4]	
Dec�bito (�lcera)	707.0	
� con gangrena	707.0 [785.4]	
Dec�bitos	707.0	v�ase adem�s Dec�bito
Dedo de mano		v�ase enfermedad espec�fica
Dedo de pie		v�ase enfermedad espec�fica
Defecto de llenamiento 		
� duodeno	793.4	
� est�mago	793.4	
� intestino	793.4	
� ri��n	793.5	
� tracto biliar	793.3	
� tracto gastrointestinal	793.4	
� ur�ter	793.5	
� vejiga	793.5	
� ves�cula biliar	793.3	
"Defecto, defectuoso"	759.9	
� 11 -hidroxilasa	255.2	
� 21-hidroxilasa	255.2	
� 3-beta-hidroxiesteroide deshidrogenasa	255.2	
� �cido hemogent�stico	270.2	
� almohadillas endoc�rdicas	745.60	
� alto grado	317	
� a�rtico septal	745.0	
"� aprendizaje, espec�fico"	315.2	
� atrial septal (tipo ostium secundum)	745.5	
�� adquirida	429.71	
�� tipo ostium primum	745.61	
� atrioventricular 		
�� canal	745.69	
�� tabique	745.4	
��� adquirido	429.71	
� atrium secundum	745.5	
�� adquirido	429.71	
� audici�n	389.9	v�ase adem�s Sordera
� auricular septal	745.5	
�� adquirido	429.71	
"� bios�ntesis, andr�geno testicular"	257.2	
� campo visual	368.40	
�� arqueado	368.43	
�� escal�n nasal	368.44	
"�� heter�nimo, bilateral"	368.47	
"�� hom�nimo, bilateral"	368.46	
�� localizado NCOC	368.44	
�� perif�rico	368.44	
�� sector	368.43	
� captaci�n de yoduro	246.1	
� circulaci�n (adquirida)	459.9	
�� cong�nita	747.9	
�� reci�n nacidos	747.9	
� coagulaci�n (factor)	286.9	"v�ase adem�s Carencia, factor de coagulaci6n"
�� con 		
��� aborto		"v�ase Aborto, por tipo, con hemorragia "
��� embarazo ect�pico	639.1	v�ase adem�s categor�as 634-638
��� embarazo molar	639.1	v�ase adem�s categor�as 630-632
�� adquirido (cualquiera)	286.7	
�� anteparto o intraparto	641.3	
��� que afecta al feto o al reci�n nacido	762.1	
�� cuando causa hemorragia de embarazo o parto	641.3	
�� debido a 		
��� carencia de vitamina K	286.7	
��� enfermedad hep�tica	286.7	
�� posparto	666.3	
"�� reci�n nacidos, transitorio"	776.3	
�� tipo especificado NCOC	286.3	
� co�gulo NCOC	286.9	"v�ase adem�s Defecto, coagulaci�n"
� conducci�n (cardiaca)	426.9	
�� hueso	389.00	"v�ase adem�s Sordera, conductiva"
"� cong�nito, �rgano o sitio NCOC"		v�ase adem�s Anomal�a 
�� circulaci�n	747.9	
�� c�spides pulmonares		"v�ase Anomal�a, v�lvula de coraz�n "
"�� Descemet, membrana de"	743.9	
��� tipo especificado NCOC	743.49	
�� diafragma	756.6	
�� ectod�rmico	757.9	
�� es�fago	750.9	
�� sistema respiratorio	748.9	
��� tipo especificado NCOC	748.8	
� cromosomas		"v�ase Anomal�a, cromosomas "
"� cuneiforrne, dientes (abrasi�n)"	521.2	
"� c�spides pulmonares, cong�nito"	746.00	
� del desarrollo		v�ase adem�s Anomal�a. por sitio 
�� cola de caballo	742.59	
�� test�culo	752.9	
�� vaso	747.9	
�� ventr�culo izquierdo	746.9	
"��� con atresia o hipoplasia de orificio o v�lvula a�rtica, con hipoplasia de la aorta ascendente"	746.7	
��� en s�ndrome hipopl�sico del coraz�n izquierdo	746.7	
� dentina (hereditario)	520.5	
"� Desmecet, membrana de (cong�nito)"	743.9	
�� adquirido	371.30	
�� tipo especificado NCOC	743.49	
� deut�nico	368.52	
� diafragma 		
"�� con elevaci�n, eventraci�n o hernia"		"v�ase Hernia, diafragma "
�� cong�nito	756.6	
"��� con elevaci�n, eventraci�n o hernia"	756.6	
��� de gran magnitud (con elevaci�n. eventraci�n o hernia)	756.6	
"� diente, cuneiforme"	521.2	
"� ectod�rmico, cong�nito"	757.9	
"� Eisenmenger, de (defecto ventricular septal)"	745.4	
"� endoc�rdica, almohadilla"	745.60	
�� tipo especificado NCOC	745.69	
"� es�fago, cong�nito"	750.9	
� excreci�n de bilirrubina	277.4	
� extensor retinaculum	728.9	
� fosa oval	745.5	
"� gen, portador (presunto) de"	V19.8	
� Gerbode	745.4	
"� glaucomatoso, sin tensi�n elevada"	365.89	
� Hageman (factor)	286.3	"v�ase adem�s Defecto, coagulaci�n"
� interatrial septal	745.5	
�� adquirido	429.71	
� interauricular septal	745.5	
�� adquirido	429.71	
� interventricular septal	745.4	
"�� con estenosis o atresia pulmonar, dextraposici�n de aorta, e hipertrofia del ventr�culo derecho"	745.2	
�� adquirido	429.71	
�� en tetralog�a de Fallot	745.2	
� lenguaje NCOC	784.5	
�� del desarrollo	315.39	
�� secundario a lesi�n org�nica	784.5	
� ligaci�n de peroxidasa	246.1	
� llenamiento 		
�� est�mago	793.4	
�� ri��n	793.5	
�� tracto biliar	793.3	
�� ur�ter	793.5	
�� vejiga	793.5	
�� ves�cula biliar	793.3	
� mental	319	"v�ase adem�s Retraso, mental"
� osteocondrial NCOC	738.8	
� Ostium 		
�� primum	745.61	
�� secundum	745.5	
"� pared abdominal, cong�nita"	756.7	
� pelvis renal	753.9	
�� obstructivo	753.2	
�� tipo especificado NCOC	753.3	
� pericardio	746.89	
� plaqueta (cualitativo)	287.1	
�� constitucional	286.4	
� polimerizaci�n de fibrina	286.3	"v�ase adem�s Defecto, coagulaci�n"
"� postural, de la espina dorsal"	737.9	
� prot�nico	368.51	
� quinureninasa	270.2	
"� retina, retinal"	361.30	
�� con desprendimiento	361.00	"v�ase adem�s Desprendimiento, retina, con defecto retinal"
�� haz de fibras nerviosas	362.85	
�� m�ltiple	361.33	
��� con desprendimiento	361.02	
�� sencillo	361.30	
��� con desprendimiento	361.01	
� septal (cierre) (coraz�n) NCOC	745.9	
�� adquirido	429.71	
�� atrial	745.5	
�� tipo especificado NCOC	745.8	
� s�ntesis de hormona tiroidea	246.1	
"� sistema respiratorio, cong�nito"	748.9	
�� tipo especificado NCOC	748.8	
� suministro placentario de sangre		"v�ase Placenta, insuficiencia "
� tabique aorticopulmonar	745.0	
� tabique bulbar	745.0	
"� Taussig-Bing (transposici�n, aorta y arteria pulmonar cabalgada)"	745.11	
� trit�nico	368.53
� ur�ter	753.9
�� obstructivo	753.2
� vascular (adquirido) (local)	459.9
�� cong�nito	747.6
� ventricular septal	745.5
"�� con estenosis o atresia pulmonar, dextraposici�n de aorta y hipertrofia de ventr�culo derecho"	745.2
�� adquirido	429.71
�� anterior aislado	745.4
�� en tetralog�a de Fallot	745.2
�� entre infund�bulo y porci�n anterior	745.4
�� tipo canal atrioventricular	745.69
� vista NCOC	369.9
� voz	784.40
� yodotirosina deshalogenasa	246.1
� yoduro insoluble en butanol	246.1
Defectuoso		v�ase enfermedad espec�fica 
� posici�n de dientes	524.3	
Deferentitis	608.4	
� gonorreica (aguda)	098.14	
�� cr�nica o de duraci�n de 2 meses o m�s	098.34	
Deferentitis	608.4	
Deficiencia		v�ase Carencia
Deficiente		v�ase adem�s Carencia 
� acci�n refleja de parpadeo	374.45	
� eje craneofacial	756.0	
� n�mero de dientes	520.0	v�ase adem�s Anodoncia
� secreci�n de orina	788.5	
D�ficit 		
� neurol�gico NCOC	781.9	
�� debido a 		
��� ataque isqu�mico transitorio	435.9	
��� lesi�n cerebrovascular	436	"v�ase adem�s Enfermedad, cerebrovascular, aguda"
���� efecto tard�o	438	v�ase categor�a
� ox�geno	799.0	
Deflexi�n 		
� espina		"v�ase Curvatura, espina "
� radio	736.09	
� tabique (adquirida) (nasal) (nariz)	470	
� turbinados (nariz)	470	
Defluvium		
� capillorum	704.00	v�ase adem�s Alopecia
� ciliorum	374.55	
� unguium	703.8	
Deformidad	759.9	
"� abdomen, cong�nita"	759.9	
�� adquirida	738.8	
�� cong�nita	756.7	
�� s�ndrome de deficiencia muscular	756.7
� adquirida (sitio no especificado)	738.9
�� sitio especificado NCOC	738.8
� ano (cong�nita)	751.5
�� adquirida	569.49
� antebrazo (adquirida)	736.00
�� cong�nita	755.50
� aorta (cong�nita)	747.20
�� adquirida	447.8
�� cayado	747.21
��� adquirida	447.8
�� coarctai�n	747.10
� a�rtico 	
�� anillo	747.21
�� cayado	747.21
��� adquirida	447.8
�� c�spide o v�lvula (cong�nita)	746.9	
��� adquirida	424.1	"v�ase adem�s Endocarditis, a�rtica"
� aparato o conducto lagrimal (cong�nita)	743.9	
�� adquirida	375.69	
� ap�ndice	751.5	
� arteria (cong�nita) (perif�rica) NCOC	747.6	
�� adquirida	447.8	
�� cerebral	747.81	
�� coronaria (cong�nita)	746.85	
��� adquirida	414.9	"v�ase adem�s Isquemia, coraz�n"
�� retinal	743.9	
�� umbilical	747.5	
� arteria coronaria (cong�nita)	746.85	
�� adquirida	414.9	"v�ase adem�s Isquemia, coraz�n"
� arteriovenosa (cong�nita)	747.6	
� articulaci�n (adquirida) NCOC	738.8	
�� cong�nita	755.9	
�� contracci�n (abducci�n) (aducci�n) (extensi�n) (flexi�n)		"v�ase Contracci�n, articulaci�n "
� articulaci�n sacroiliaca (cong�nita)	755.69	
�� adquirida	738.5	
� atrial septal (cong�nita) (coraz�n)	745.5	
� aur�cula del coraz�n (cong�nita)	746.9	
"� Bartholin, conducto de (cong�nita)"	750.9	
� bazo 		
�� adquirida	289.59	
�� cong�nita	759.0	
"� biliar, conducto (cong�nita)"	751.60	
�� adquirida	576.8	
"��� con c�lculo, coledocolitiasis o piedras "		v�ase Coledocolitiasis
� boca (adquirida)	528.9	
�� cong�nita NCOC	750.9	
��� tipo especificado NCOC	750.26	
"� bolsa sinovial, cong�nita "	756.9	
� botonera (dedo de mano)	736.21	
� brazo (adquirida)	736.89	
�� cong�nita	755.50	
� bronquio (cong�nita)	748.3	
�� adquirida	519.1	
� bulbo duodenal	537.89	
� cabeza (adquirida)	738.1	
�� cong�nita	756.0	"v�ase adem�s Deformidad, cr�neo, cong�nita"
� cadera (articulaci�n) (adquirida)	736.30	
�� cong�nita NCOC	755.63	
�� flexi�n	718.45	
��� cong�nita	754.32	"v�ase adem�s Subluxaci�n, cong�nita, cadera"
� campo visual (contracci�n)	368.45	
� canal auditivo (cong�nita) (externa)	744.3	"v�ase adem�s Deformidad, o�do"
�� adquirida	380.50	
� canal de Nuck	752.9	
� canto (cong�nita)	743.9	
�� adquirida	374.89	
� capilar (adquirida)	448.9	
�� cong�nita	747.6	
� cara (adquirida)	738.1	
�� cong�nita (cualquier parte)	744.9	
��� debida a mala posici�n y presi�n intrauterina	754.0	
� cardiaca		"v�ase Deformidad, coraz�n "
� cart�lago cricoides (cong�nita)	748.3	
�� adquirida	478.79	
� cart�lago o disco intervertebral (adquirida)		"v�ase adem�s Desplazamiento, disco intervertebral "
�� cong�nita	756.10	
"� car�nculo, lagrimal (cong�nita)"	743.9	
�� adquirida	375.69	
"� cascada, est�mago en"	537.6	
� cayado a�rtico transversal (cong�nita)	747.21	
� ceja (cong�nita)	744.89	
� cerebral (cong�nita)	742.9	
�� adquirida	348.8	
� cerebro (cong�nita)	742.9	
�� adquirida	348.8	
�� m�ltiple	742.4	
�� reducci�n	742.2	
�� vaso (cong�nita)	747.81	
� cicatricial		v�ase Cicatriz 
� ciego (cong�nita)	751.5	
�� adquirida	569.89	
� cilios (cong�nita)	743.9	
�� adquirida	374.89	
� clav�cula (adquirida)	738.8	
�� cong�nita	755.51	
� cl�toris (cong�nita)	752.40	
�� adquirida	624.8	
� c�ccix (adquirida)	738.6	
�� cong�nita	756.10	
� codo (articulaci�n) (adquirida)	736.00	
�� cong�nita	755.50	
�� contracci�n	718.42	
� colon (cong�nita)	751.5	
�� adquirida	569.89	
� concha (o�do) (cong�nita)	744.3	"v�ase adem�s Deformidad, o�do"
�� adquirida	380.32	
� conducto arterioso	747.0	
� conducto biliar (cong�nita)	751.60	
�� adquirida	576.8	
"��� con c�lculo, coledocolitiasis o piedras "		 v�ase Coledocolitiasis
� conducto c�stico (cong�nita)	751.60	
�� adquirida	575.8	
� conducto deferente (cong�nita)	752.9	
�� adquirida	608.89	
� conducto eyaculatorio (cong�nita)	752.9	
�� adquirida	608.89	
� conducto hep�tico (cong�nita)	751.60	
�� adquirida	576.8	
"��� con c�lculo, coledocolitiasis o piedras "		 v�ase Coledocolitiasis
"� cong�nita, �rgano o sitio no listado"	759.9	v�ase adem�s Anomal�a
� coraz�n (cong�nita)	746.9	
�� aur�cula (cong�nita)	746.9	
�� tabique	745.9	
��� auricular	745.5	
��� tipo especificado NCOC	745.8	
��� ventricular	745.4	
�� v�lvula (cong�nita) NCOC	746.9	
��� adquirida 		 v�ase Endocarditis 
��� pulmonar (cong�nita)	746.00	
��� tipo especificado NCOC	746.89	
�� ventr�culo (cong�nita)	746.9	
� cord�n esperm�tico (cong�nita)	752.9	
�� adquirida	608.89	
��� torsi�n	608.2	
� c�rnea (cong�nita)	743.9	
�� adquirida	371.70	
� coroides (cong�nita)	743.9	
�� adquirida	363.8	
�� plexo (cong�nita)	742.9	
��� adquirida	349.2	
� costilla (adquirida)	738.3	
�� cong�nita	756.3	
��� cervical	756.2	
� cr�neo (adquirida)	738.1	
�� cong�nita 		"(v�ase adem�s Deformidad, cr�neo, cong�nita) "
� cr�neo (adquirida)	738.1	
�� cong�nita	756.0	
��� con 		
���� anencefalia	740.0	
���� encefalocele	742.0	
���� hidrocefalia	742.3	
����� con espina b�fida	741.0	(v�ase adem�s Espina b�fida)
���� microcefalia	742.1	
��� debida a mala posici�n y presi�n intrauterina	754.0	
� cristalino (cong�nita)	743.9	
�� adquirida	379.39	
� c�bito (adquirida)	736.00	
�� cong�nita	755.50	
� cuello (adquirida) NCOC	738.2	
�� cong�nita (cualquier parte)	744.9	
��� esternocleidomastoideo	754.1	
"� cuello de cisne, en (adquirida) "		
�� dedo de mano	736.22	
�� mano	736.09	
� cuello uterino (adquirida)	622.8	
�� cong�nita	752.40	
� culata	736.02	
� Dandy-Walker	742.3	
�� con espina b�fida	741.0	v�ase adem�s Espina b�fida
� dedo de mano (adquirida)	736.20	
�� cong�nita	755.50	
�� contractura por flexl�n	718.44	
�� cuello de cisne	736.22	
�� tipo botonera	736.21	
� dedo de pie (adquirida)	735.9	
�� cong�nita	755.66
�� especificada NCOC	735.8
� dedo pulgar (adquirida)	736.20
�� cong�nita	755.50
� diafragma (cong�nita)	736.20
�� adquirida	738.8
"� diente, dientes NCOC"	520.9
� duodeno (cong�nita)	751.5
�� adquirida	537.89
� duramadre (cong�nita)	742.9
�� cerebro	742.4
��� adquirida	349.2
�� raqu�dea	742.59
��� adquirida	349.2
� ectod�rmico (cong�nita) NCOC	757.9
�� tipo especificado NCOC	757.8
� enc�a (cong�nita)	750.9	
�� adquirida	523.9	
� epid�dimo (cong�nita)	752.9	
�� adquirida	608.89	
�� torsi�n	608.2	
� epiglotis (cong�nita)	748.3	
�� adquirida	478.79	
� esc�pula (adquirida)	736.89	
�� cong�nita	755.50	
� escroto (cong�nita)	752.9	
�� adquirida	608.89	
� es�fago (cong�nita)	750.9	
�� adquirida	530.8	
� espalda (adquirida)		"v�ase Deformidad, espina"
� espina (adquirida) NCOC	738.5	
�� cifoscoli�tica	737.30	(v�ase adem�s Cifoscoliosis)
�� cif�tica	737.10	(v�ase adem�s Cifosis)
�� cong�nita	756.10	
��� debida a mala posici�n y presi�n intrauterina	754.2	
�� escoli�tica	737.30	(v�ase adem�s Escoliosis)
�� lord�tica	737.20	(v�ase adem�s Lordosis)
�� raqu�tica	268.1	
� espinal		
�� columna		"v�ase Deformidad, espina"
�� m�dula (cong�nita)	742.9	
��� adquirida	336.8	
��� vaso (cong�nita)	747.6	
�� ra�z de nervio (cong�nita)	742.9	
��� adquirida	724.9	
� estern�n (adquirida)	738.3	
�� cong�nita	756.3	
� est�mago (cong�nita)	750.9	
�� adquirida	537.89
� extremidad (adquirida)	736.9
"�� cong�nita, salvo deformidad por reducci�n"	755.9
��� inferior	755.60
��� superior	755.50
