� despu�s de infusi�n, perfusi�n o transfusi�n	999.2
� durante o como resultado de procedimiento	997.2
� embarazo	671.9
�� profunda	671.3
�� superficial	671.2
�� tipo especificado NCOC	671.5
� extremidad inferior	451.2
�� profunda (vasos)	451.19
��� vaso especificado NCOC	451.19
��� vena femoral	451.11
�� superficial (vasos)	451.0
� femoropopl�tea	451.0
� gotosa	274.89 [451.9]
� ileofemoral	451.11
"� mama, superficial"	451.89
� migratoria (superficial)	453.1
� ombligo	451.89	
"� pared tor�cica, superficial"	451.89	
� pelviana 		
�� con 		
��� aborto 		"v�ase Aborto, por tipo, con septicemia "
��� embarazo ect�pico	639.0	v�ase adem�s categor�as 633.0-633.9
��� embarazo molar	639.0	v�ase adem�s categor�as 630-632
�� despu�s de 		
��� aborto	639.0	
��� embarazo ect�pico o molar	639.0	
"�� puerperal, posparto"	671.4	
� pierna	451.2	
�� profunda (vasos)	451.19	
��� vaso especificado NCOC	451.19	
��� vena femoral	451.11	
�� superficial (vasos)	451.0	
� posoperatoria	997.2	
� profunda (vasos)	451.19	
�� vaso especificado NCOC	451.19	
�� vena femoral	451.11	
"� puerperal, posparto, parto"	671.9	
�� extremidades inferiores	671.2	
�� pelvis	671.4	
�� profunda	671.4	
�� sitio especificado NCOC	671.5 	
�� superficial	671.2	
� retina	362.18	
� safena (grande) (larga)	451.0	
�� accesoria o peque�a	451.0	
� seno		
� cavernoso (venoso) 		"v�ase Flebitis, seno intracraneal "
� cerebral (venoso) 		"v�ase Flebitis, seno intracraneal "
� craneal (venoso) 		"v�ase Flebitis, seno intracraneal "
�� intracraneal (cualquiera) (venoso)	325	
��� efecto tard�o 	326	v�ase categor�a 326
��� no pi�gena	437.6	
���� en el embarazo o puerperio	671.5	
�� lateral (venoso) 		"v�ase Flebitis, seno intracraneal "
�� longitudinal 		"v�ase Flebitis, seno intracraneal "
�� meninges 		"v�ase Flebitis, seno intracraneal "
� sifil�tica	093.89	
� sitio especificado NCOC	451.89	
"� ulcera, ulcerativa"	451.9	
�� extremidad inferior	451.2	
��� profunda (vasos)	451.2	
���� vaso especificado NCOC	451.19	
���� vena femoral	451.11	
��� superficial (vasos)	451.0	
�� pierna	451.2	
��� profunda (vasos)	451.19	
���� vaso especificado NCOC	451.19	
���� vena femoral	451.11	
��� superficial (vasos)	451.0	
� �tero (s�ptica)	615.9	v�ase adem�s Endometritis   
� varicosa (pierna) (extremidad inferior) 	454.1	"v�ase adem�s Varicosa, vena"
� vena(s) 		
�� femoral (profunda)	451.11	
�� hep�ticas	451.89	
�� il�aca	451.81	
�� popl�tea	451.19	
�� porta	572.1	
�� tibial	451.19	
Flebofibrosis	459.89	
Flebolitos	459.89	
Flebosclerosis	459.89	
"Fleb�tomo, fiebre"	066.0	
Flebotrombosis 		v�ase Trombosis
Flegmasia 		
� alba dolens (vasos profundos)	671.4	
�� cuando complica el embarazo	671.3	
�� no puerperal	451.19	
"�� puerperal, posparto, alumbramiento"	671.4	
� cer�lea dolens	451.19	
"Fleischer (-Kayser), anillo de (pigmentaci�n c�rnea)"	275.1[371.14]	
"Fleischner, enfermedad de"	732.3	
"Flemas, ahogado por"	933.1	
Flem�n	682.9	v�ase adem�s Absceso
� erisipelatoso	035	v�ase adem�s Erisipelas   
� garganta	478.29	
� il�aco	682.2	
�� fosa	540.1	
Flem�n	522.7	
Flemonoso 		v�ase enfermedad espec�fica
Flexibilidad c�rea	300.11	v�ase adem�s Catalepsia   
Flexi�n 		
"� contractura, articulaci�n"	718.4	"v�ase adem�s Contracci�n, articulaci�n"
� cuello uterino	621.6	"v�ase adem�s Mala posici�n, �tero"
"� deformidad, articulaci�n"	718.4	"v�ase adem�s Contracci�n, articulaci�n"
"�� cadera, cong�nita"	754.32	"v�ase adem�s Subluxaci�n, cong�nita, cadera"
� �tero	621.6	"v�ase adem�s Mala posici�n, �tero    "
"Flexner, de "		
� bacilo	004.1	
� diarrea (ulcerativa)	004.1	
� disenter�a	004.1	
"Flexner-Boyd, disenter�a"	004.2	
Flexura 		v�ase enfermedad espec�fica
Flictenulosis (al�rgica) (no tuberculosa) (queratoconjuntivitis)	370.31	
� c�rnea	370.31	
�� con ulcera	370.00	"v�ase adem�s Ulcera, c�rnea     "
� tuberculosa	017.3 [370.31]	v�ase adem�s Tuberculosis    
Flojo 		
� ni�o NCOC	781.9	
"� v�lvula (mitral), s�ndrome de"	424.0	
"Flotadores (c�lulas), v�treo"	379.24	
Flotante 		
� bazo	289.59	
� cart�lago (articulaci�n)	718.0	"v�ase adem�s Trastorno, cart�lago, articular"
�� rodilla	717.6	
� costilla	756.3	
� h�gado (cong�nito)	751.69	
� ri��n	593.0	
�� cong�nito	753.3	
� ves�cula biliar (cong�nita)	751.69	
Flotante 		v�ase adem�s enfermedad espec�fica 
� cart�lago (articulaci�n)	718.1	"v�ase adem�s Flotante, cuerpo, articulaci�n"
�� rodilla	717.6	
� cuerpo 		
�� articulaci�n	718.10	
��� cadera	718.15	
��� codo	718.12	
��� hombro (regi�n)	718.11	
��� implante prot�sico 		"v�ase Complicaciones, mec�nicas "
��� mano	718.14	
��� m�ltiples sitios	718.19	
��� mu�eca	718.13	
��� pie	718.17	
��� regi�n peIviana	718.15	
��� rodilla	717.16	
��� sitio especificado NCOC	718.18	
��� tobillo	718.17	
�� en vaina de tend�n	727.82	
� diente(s)	525.8	
� faceta (vertebral)	724.9	
� implante prot�sico 		"v�ase Complicaciones, mec�nicas "
"� sesamoideo, articulaci�n"	718.1	"v�ase adem�s Flotante, cuerpo, articulaci�n"
"Fluctuante, tensi�n sangu�nea"	796.4	
Flujo (albus) (vaginal)	623.5	
� sangriento (serosangu�neo)	009.0	
� tricomoni�sico (Trichomonas vaginalis)	131.00	
Fluorosis (dental) (cr�nica)	520.3	
"Fobia, f�bico (reacci�n)"	300.20	
� aislada NCOC	300.29	
� animal	300.29	
� estado	300.20	
� obsesiva	300.3	
� simple NCOC	300.29	
� social	300.23	
Focal 		v�ase enfermedad espec�fica
"Fochier, absceso de "		"v�ase Absceso, por sitio"
Foco de Assmann	011.0	v�ase adem�s Tuberculosis
Focomelia	755.4	
� miembro inferior	755.32	
�� completa	755.33	
�� distal	755.35	
�� proximal	755.34	
� miembro superior	755.22	
�� completa	755.23	
�� distal	755.25	
�� proximal	755.24	
Fogo selvagem	694.4	
"Foix-Alajouanine, s�ndrome de"	336.1	
Folicular 		v�ase adem�s enfermedad espec�fica 
� quiste (atr�sico)	620.0	
Foliculitis	704.8	
� abscedens et suffodiens	704.8	
� decalvante	704.09	
� gonorreica (aguda)	098.0	
�� cr�nica o de duraci�n de 2 meses o mas	098.2	
� pustular	704.8	
"� queloide, queloidal"	706.1	
� uleritematosa reticulada	701.8	
Fol�culo 		
� cuello uterino (de Naboth) (rotura)	616.0	
"� graafiano, rotura, con hemorragia"	620.0	
� Naboth	616.0	
"Foliculosis, conjuntival"	372.02	
Folie a deux	297.3	
Folliclis (primaria)	017.0	v�ase adem�s Tuberculosis
"Folling, enfermedad de (fenilcetonuria)"	270.1	
Fondo de retina en mosaico (tigroide)	362.89	
Fondo de saco 		v�ase adem�s enfermedad espec�fica 
� flavimaculato	362.76	
"Fong, s�ndrome de (osteoonicodisplasia hereditaria)"	756.89	
Fontanela abultada (cong�nita)	756.0	
Foramen 		
� Botal sin cerrarse 	745.5	"v�ase adem�s Imperfecto, cierre"
� oval sin cerrarse 	745.5	"v�ase adem�s Imperfecto, cierre"
� v�lvula de Eustaquio	746.89	
Foramen oval (abierto) (no obturado) (persistente)	745.5	
"Forbes, enfermedad de (almacenamiento de gluc�geno)"	271.0	
"Forbes-Albright, s�ndrome de (amenorrea y lactaci�n no puerperales asociadas con tumor pituitario)"	253.1	
F�rceps 		
� parto NCOC	669.5	
�� que afecta al feto o al reci�n nacido  	763.2	
"Fordyce, enfermedad de (gl�ndulas seb�ceas ect�picas) (boca)"	750.26	
"Fordyce-Fox, enfermedad de (miliaria apocrina)"	705.82	
Foria	378.40	v�ase adem�s Heteroforia
Formaci�n 		
� hialina en c�rnea	371.49	
� hueso en tejido de cicatriz (piel)	709.3	
� perlas de Elschnig (extracci�n poscatarata)	366.51	
� secuestro �seo (debido a infecci�n)	730.1	v�ase adem�s Osteomielitis
� tejido conjuntivo en v�treo	379.25	
� v�lvula 		
"�� colon, cong�nita"	751.5	
�� ur�ter (cong�nita)	753.2	
"Fort Bragg, fiebre de"	100.89	
"Forzado, nacimiento o parto NCOC"	669.8	
� que afecta al feto o al reci�n nacido NCOC	763.8	
Fosa 		v�ase adem�s enfermedad espec�fica 
� piriforme 		v�ase enfermedad espec�fica
Fosfatemia	275.3	
Fosfaturia	275.3	
"Fosfat�rico, trastorno tubular"	588.0	
"Foster-Kennedy, s�ndrome de"	377.04	
"Fothergill, de "		
"� enfermedad, cuando significa escarlatina anginosa"	034.1	
� neuralgia	350.1	"v�ase adem�s Neuralgia, trig�mino"
Fotocoproporfiria	277.1	
Fotodermatitis (solar)	692.79	
� luz diferente a la solar	692.89	
Fotofobia	368.13	
Fotoftalmia	370.24	
Fotopsia	368.15	
Fotoqueratitis	370.24	
Fotorretinitis	363.31	
Fotorretinopat�a	363.31	
Fotosensibilidad (solar)	692.79	
� luz diferente a la solar	692.89	
"Fotosensibilizaci�n (solar), de la piel"	692.79	
� luz diferente a la solar	692.89	
"Fournier, enfermedad de (gangrena idiop�tica)"	608.83	
"Foville, s�ndrome de"	344.8	
"Fox, de "		
� enfermedad (miliaria apocrina)	705.82	
� imp�tigo (contagiosa) 	684	
"Fox-Fordyce, enfermedad de (miliaria apocrina)"	705.82	
Fractura (abducci�n) (aducci�n) (aplastamiento) (avulsi�n) (cerrada)(compresi�n) (dislocaci�n) (oblicua) (separaci�n)	829.0	
� con		
�� traumatismos internos en la misma regi�n (estados clasificables bajo 860-869) 		"v�ase adem�s Traumatismo, interno, por sitio "
��� regi�n pelviana 		"v�ase Fractura, pelvis "
� abierta	829.1	
� acet�bulo (con traumatismo visceral) (cerrada)	808.0	
�� abierta	808.1	
� acromi�n (ap�fisis) (cerrada)	811.01	
�� abierta	811.11	
� alv�olo (cerrada)	802.8	
�� abierta	802.9	
� antebrazo (cerrada) NCOC	813.80	
�� abierta	813.90	
�� ca�a	813.20	
��� abierta	813.30	
�� extremo inferior (extremo distal) (ep�fisis inferior)	813.40	
��� abierta	813.50	
�� extremo superior (extremo proximal) (ep�fisis superior)	813.00	
��� abierta	813.10	
� antro 		"v�ase Fractura, cr�neo, base "
� ap�fisis transversal 		"v�ase Fractura, v�rtebra, por sitio "
� arco neural 		"v�ase Fractura, v�rtebra, por sitio "
� asa de cubo (cart�lago semilunar) 		"v�ase Desgarre, menisco "
� astr�galo (cerrada)	825.21	
�� abierta	825.31	
� atlas 		"v�ase Fractura, v�rtebra, cervical, primera "
� axis 		"v�ase Fractura, v�rtebra, cervical, segunda "
"� Barton, de "		"v�ase Fractura, radio, extremo inferior "
� basal (cr�neo) 		"v�ase Fractura, cr�neo, base "
"� Bennet, de "		"v�ase adem�s Fractura, metacarpo, huesos metacarpianos "
�� espina 		"v�ase Fractura, tibia, extremo superior "
"� Bennett, de (cerrada)"	815.01	
�� abierta	815.11	
� bimaleolar (cerrada)	824.4	
�� abierta	824.5	
� b�veda craneal 		"v�ase Fractura, cr�neo, b�veda "
"� boxeadores, de los "		"v�ase Fractura, hueso(s)metacarpianos "
� brazo (cerrada)	818.0	
�� abierta	818.1	
�� ambos (cualquier combinaci�n de huesos) (con costilla(s)) (con estern�n)	819.0	
��� abierta	819.1	
�� inferior	813.80	
��� abierta	813.90	
�� superior 		"v�ase Fractura, h�mero "
�� y pierna(s) (cualquier combinaci�n de huesos)	828.0	
��� abierta	828.1	
� cadera (cerrada)	820.8	"v�ase adem�s Fractura, f�mur, cuello"
�� abierta	820.9	
� calc�neo (cerrada)	825.0	
�� abierta	825.1	
� capitellum (humero) (cerrada)	812.49	
�� abierta	812.59	
"� carpiano(s), hueso(s) (mu�eca NCOC)    "	814.00	
�� abierta	814.10	
�� sitio especificado NCOC	814.09	
��� abierta	814.19	
� cart�lago tiroideo (cerrada)	807.5	
�� abierta	807.6	
"� cart�lago, rodilla (semilunar) "		"v�ase Desgarro, menisco "
� cavadores de arcilla		"v�ase Fractura, v�rtebra, cervical "
� ca�a verde 		"v�ase Fractura, por sitio "
� cervical 		"v�ase Fractura, v�rtebra, cervical "
� ch�feres 		"v�ase Fractura, c�bito, extremo inferior "
� cicatriz�ndose o antigua 		
�� cambio de escayola	V54.8	
�� complicaciones 		v�ase enfermedad espec�fica 
�� cuidados posteriores o convalescencia	V54.9	
�� eliminaci�n de 		
��� dispositivo de fijaci�n 		
���� externo	V54.8	
���� interno	V54.0	
��� escayola	V54.8	
� cigoma (arco cigom�tico) (cerrada)	802.4	
�� abierta	802.5	
� cincel 		"v�ase Fractura, radio, extremo superior "
� clav�cula (parte interligamentosa) (cerrada)    	810.00	
�� abierta	810.10	
�� ca�a (tercio mediano)	810.02	
��� abierta	810.12	
�� debida a traumatismo al nacer	767.2	
�� extremo acromial	810.03	
��� abierta	810.13	
�� extremo esternal	810.01	
��� abierta	810.11	
� c�ccix 		"v�ase adem�s Fractura, v�rtebra, c�ccix "
�� cuando complica el parto	665.6	
� codo 		"v�ase adem�s Fractura, h�mero, extremo inferior "
�� ol�cranon (ap�fisis) (cerrada)	813.01	
��� abierta	813.11	
�� supracondilar (cerrada)	812.41	
��� abierta	812.51	
"� Colles, de (con reversi�n) (cerrada)    "	813.41	
�� abierta	813.51	
"� Colles, de "		"v�ase Fractura, h�mero, extremo inferior "
� compresi�n 		"v�ase adem�s Fractura, por sitio "
�� no traum�tica	733.1	
� cong�nita	756.9	
� conminuta		"v�ase Fractura, por sitio "
"� coracoides, ap�fisis (cerrada)"	811.02	
�� abierta	811.12	
"� coronoides, ap�fisis (c�bito) (cerrada)    "	813.02	
�� abierta	813.12	
�� mand�bula (cerrada)	802.23	
��� abierta	802.33	
"� corredores de velocidad, de los "		"v�ase Fractura, ilion "
� costilla(s) (cerrada)	807.0	
�� con t�rax hundido (abierta)	807.4	
� abierta	807.1	
"� costocondral, uni�n "		"v�ase Fractura, costilla "
"� costoesternal, uni�n"		"v�ase Fractura, costilla "
� cr�neo (m�ltiple NCOC) (con huesos faciales) (cerrada)	803.0	
�� con 		
"��� contusi�n, cerebral"	803.1	
��� hemorragia 		
���� epidural	803.2	
���� extradural (intracraneal) NCOC	803.2 	
���� subaracnoidea	803.2	
���� subdural	803.2	
"��� laceraci�n, cerebral"	803.1	
��� otros huesos 		"v�ase Fractura, m�ltiple, cr�neo "
�� traumatismo intracraneal NCOC	803.4	
�� abierta	803.5	
��� con 		
"���� contusi�n, cerebral"	803.6	
���� hemorragia 		
����� epidural	803.7	
����� extradural  	803.7	
����� intracraneal	803.8 	
����� subaracnoidea	803.7	
����� subdural	803.7	
"���� laceraci�n, cerebral"	803.6	
���� traumatismo intracraneal NCOC   	803.9
�� base (antro) (hueso etmoides) (fosa) (o�do interno) (seno nasal) (occipucio) (esfenoides) (hueso temporal) (cerrada)	801.0
��� con 	
"���� contusi�n, cerebral"	801.1
���� hemorragia 	
����� epidural	801.2
����� extradural  	801.2
����� intracraneal	801.3 
����� subaracnoidea	801.2
����� subdural	801.2
"���� laceraci�n, cerebral"	801.1
���� traumatismo intracraneal NCOC	801.4
��� abierta	801.5
���� con 	
"����� contusi�n, cerebral"	801.6
����� hemorragia 	
������ epidural	801.7
������ extradural  	801.7 
������ intracraneal	801.8
������ subaracnoidea	801.7
������ subdural	801.7
"����� laceraci�n, cerebral"	801.6
����� traumatismo intracraneal NCOC        	801.9
�� b�veda (hueso frontal) (hueso parietal) (v�rtice) (cerrada)	800.0
��� con 	
"���� contusi�n, cerebral"	800.1
���� hemorragia (intracraneal) NCOC	800.3
����� epidural	800.2
����� extradural	800.2
����� subaracnoidea	800.2
����� subdural	800.2
"���� laceraci�n, cerebral"	800.1
���� traumatismo intracraneal NCOC	800.4	
��� abierta	800.5	
���� con 		
"����� contusi�n, cerebral"	800.6	
����� hemorragia (intracraneal)	800.8	
������ epidural	800.7	
������ extradural	800.7	
������ subaracnoidea	800.7	
������ subdural	800.7	
"����� laceraci�n, cerebral"	800.6	
����� traumatismo intracraneal NCOC	800.9	
�� huesos faciales 		"v�ase Fractura, huesos faciales "
�� traumatismo al nacer	767.3	
� cr�neo 		"v�ase Fractura, cr�neo, por sitio "
"� cricoides, cart�lago (cerrada)"	807.5	
�� abierta	807.6	
� c�bito (s�lo) (cerrada)	813.82	
�� con radio NCOC	813.83	
��� abierta	813.93	
�� abierta NCOC	813.92	
�� cabeza 		"v�ase Fractura, c�bito, extremo inferior "
�� ca�a	813.22	
��� con radio (ca�a)	813.23	
���� abierta	813.33	
��� abierta	813.32	
"�� coronoides, ap�fisis (cerrada)"	813.02	
��� abierta	813.12	
�� ep�fisis 		
��� inferior		"v�ase Fractura, c�bito, extremo inferior "
��� superior		"v�ase Fractura, c�bito, extremo superior "
"�� estiloides, ap�fisis "		"v�ase Fractura, c�bito, extremo inferior "
�� extremo distal 		"v�ase Fractura, c�bito extremo inferior "
�� extremo inferior (extremo distal) (cabeza) (ep�fisis inferior) (ap�fisis estiloides)	813.43	
��� con radio (extremo inferior)	813.44	
���� abierta	813.54	
��� abierta	813.53	
�� extremo proximal 		"v�ase Fractura, c�bito, extremo superior "
�� extremo superior (ep�fisis)	813.04	
��� con radio (extremo superior)	813.08	
���� abierta	813.18	
��� abierta	813.14	
��� m�ltiples sitios	813.04	
���� abierta	813.14	
��� sitio especificado NCOC	813.04	
���� abierta	813.14	
"�� ol�cranon, ap�fisis (cerrada)"	813.01	
��� abierta	813.11	
�� transversal 		"v�ase Fractura, c�bito, extremo inferior "
� cuboides (tobillo) (cerrada)	825.23	
�� abierta	825.33	
� cuello		"v�ase Fractura, v�rtebra, cervical "
� cuneiforme 		
�� mu�eca (cerrada)	814.03	
��� abierta	814.13	
�� pie (cerrada)	825.24	
��� abierta	825.34	
� cuneta 		"v�ase Fractura, cr�neo, b�veda "
� debida a 		
�� disparo de arma de fuego 		"v�ase adem�s Fractura, por sitio, abierta "
�� neoplasia	733.1	
�� osteoporosis	733.1	
�� traumatismo al nacer 		"v�ase Nacimiento, traumatismo, fractura "
� dedo(s) de un pie (cerrada)	826.0	
�� con hueso(s) del mismo miembro inferior     	827.0	
��� abierta	827.1	
�� abierta	826.1	
� dedo(s) de una mano (cerrada)	816.00	"v�ase adem�s Fractura, falange, mano     "
�� con 		
"��� hueso(s) metacarpiano(s), de la misma mano"	817.0	
���� abierta	817.1	
��� pulgar de la misma mano	816.03	
���� abierta	816.13	
�� abierta	816.10	
� diente (ra�z)	873.63	
�� complicada	873.73	
"� dispositivo prot�sico, interno "		"v�ase Complicaciones, mec�nicas "
"� Dupuytren, de (tobillo) (peron�) (cerrada)"	824.4	
�� abierta	824.5	
�� radio	813.42	
��� abierta	813.52	
"� Duverney, de "		"v�ase Fractura, ilion "
� efecto tard�o 		"v�ase Tard�o, efectos (de, fractura)"
"� empeine, de un pie (cerrada)"	825.20	
�� con dedo(s) del mismo pie	827.0	
��� abierta	827.1	
�� abierta	825.30	
� escafoides (navicular) 		
�� carpiano (mu�eca) (cerrada)	814.01	
��� abierta	814.11	
�� tarsiano (tobillo) (cerrada)	825.22	
��� abierta	825.32	
� esc�pula (cerrada)	811.00	
�� abierta	811.10	
"�� acromial, acromion (ap�fisis)"	811.01	
��� abierta	811.11	
"�� coracoides, ap�fisis"	811.02	
��� abierta	811.12	
�� cuello	811.03	
��� abierta	811.13	
�� cuerpo	811.09	
��� abierta	811.19	
�� glenoidea (cavidad) (fosa)	811.03	
��� abierta	811.13	
� esfenoides (hueso) (seno) 		"v�ase Fractura, cr�neo, base "
� espalda 		"v�ase Fractura, v�rtebra, por sitio "
� espina 		"v�ase adem�s Fractura, v�rtebra, por sitio "
�� debida a traumatismo al nacer	767.4	
"� espinosa, ap�fisis "		"v�ase Fractura, v�rtebra, por sitio "
� espontanea (causa desconocida)	733.1	
� estern�n (cerrada)	807.2	
�� con t�rax hundido	807.4	
�� abierta	807.3	
"� estiloidea, ap�fisis "		
�� c�bito 		"v�ase Fractura, c�bito, extremo inferior "
�� hueso temporal 		"v�ase Fractura, cr�neo, base "
�� metacarpiana (cerrada)	815.02	
��� abierta	815.12	
�� radio 		"v�ase Fractura, radio, extremo inferior "
� estribo 		"v�ase Fractura, cr�neo, base "
� etmoides (hueso) (seno) 		"v�ase Fractura, cr�neo, base "
"� falange(s), de un solo pie (cerrada)"	826.0	
�� con hueso(s) del mismo miembro inferior	827.0	
��� abierta	827.1	
�� abierta	826.1	
�� mano (cerrada)	816.00	
��� con hueso(s) metacarpiano(s) de la misma mano	817.0	
���� abierta	817.1	
��� abierta	816.10	
��� distal	816.02	
���� abierta	816.12	
��� medio	816.01	
���� abierta	816.11	
��� m�ltiples sitios NCOC	816.03	
���� abierta	816.13	
��� proximal	816.01	
���� abierta	816.11	
� fatiga 		"v�ase Fractura, de la marcha "
"� f�mur, femoral (cerrada)"	821.00	
�� abierta	821.10	
�� cabeza	820.09	
��� abierta	820.19	
�� ca�a (tercio inferior) (tercio medio) (tercio superior)	821.01	
��� abierta	821.11	
�� cervicotrocanteriana	820.03	
��� abierta	820.13
"�� c�ndilos, epic�ndilos"	821.21
��� abierta	821.31
�� cuello (cerrada)	820.8
��� abierta	820.9
��� base (cervicotrocanteriana)	820.03
���� abierta	820.13
��� cervical media	820.02
���� abierta	820.12
��� extracapsular	820.20
���� abierta	820.30
��� intertrocanteriana (secci�n)	820.21
���� abierta	820.31
��� intracapsular	820.00
���� abierta	820.10
��� intratrocanteriana	820.21
���� abierta	820.31
��� sitio especificado NCOC	820.09
���� abierta	820.19
��� transcervical	820.02
���� abierta	820.12
��� transtrocanteriana	820.20
���� abierta	820.30
"�� en forma de T, dentro de la articulaci�n de la rodilla"	821.21
��� abierta	821.31
�� ep�fisis (separaci�n) 	
��� cabeza	820.01
���� abierta	820.11
��� capital	820.01
���� abierta	820.11
��� inferior	821.22
���� abierta	821.32
��� superior	820.01	
���� abierta	820.11	
��� trocanteriana	820.01	
���� abierta	820.11	
�� extremo distal 		"v�ase Fractura, f�mur, extremo inferior "
�� extremo o extremidad inferior (extremos distal) (cerrada)	821.20	
��� abierta	821.30	
"��� c�ndilos, epic�ndilos"	821.21	
���� abierta	821.31	
��� en forma de T	821.21	
���� abierta	821.31	
��� ep�fisis (separaci�n)	821.22	
���� abierta	821.32	
��� m�ltiples sitios	821.29	
���� abierta	821.39	
��� sitio especificado NCOC	821.29	
���� abierta	821.39	
��� supracondilar	821.23	
���� abierta	821.33	
�� extremo superior	820.8	
��� abierta	820.9	
�� peritrocanteriana (secci�n)	820.20	
��� abierta	820.30	
�� subcapital	820.09	
��� abierta	820.19	
�� subtrocanteriana (regi�n) (secci�n)	820.22	
��� abierta	820.32	
�� supracondilar	821.23	
��� abierta	821.33	
�� transepifisaria	820.01	
��� abierta	820.11	
�� troc�nter (mayor) (menor)	820.20	"v�ase adem�s Fractura, f�mur, cuello, por sitio"
��� abierta	820.30	
"� fosa, anterior, media o posterior "		"v�ase Fractura, cr�neo, base "
� frontal (hueso) 		"v�ase adem�s Fractura, cr�neo, b�veda "
�� seno 		"v�ase Fractura, cr�neo, base "
"� Galeazzi, de "		"v�ase Fractura, radio, extremo inferior "
"� ganchoso, hueso (cerrada)"	814.08	
�� abierta	814.18	
� glenoidea (cavidad) (esc�pula) (fosa) (cerrada)	811.03	
�� abierta	811.13	
"� Gosselin, de "		"v�ase Fractura, tobillo "
"� hioides, hueso (cerrada)"	807.5	
�� abierta	807.6	
� hiperextensi�n 		"v�ase Fractura, radio, extremo inferior "
� hombro 		"v�ase adem�s Fractura, humero, extremo superior "
�� om�plato 		"v�ase Fractura, esc�pula "
"� huesecillos, auditivos (yunque) (martillo) (estribo) "		"v�ase Fractura, cr�neo, base "
� hueso (cerrada) NCOC	829.0	
�� abierta	829.1	
�� patol�gica (causa desconocida)	733.1	
�� traumatismo al nacer NCOC	767.3	
� hueso grande del carpo (cerrada)	814.07	
�� abierta	814.17	
� hueso pectoral 		"v�ase Fractura, estern�n "
� hueso(s) craneal(es) NCOC	802.8	
�� con 		
��� cr�neo 		"v�ase adem�s Fractura, cr�neo, "
���� con implicaci�n de otros huesos		"v�ase Fractura, m�ltiple, cr�neo "
��� otro(s) hueso(s) 		"v�ase Fractura, m�ltiple, cr�neo "
�� abierta	802.9	
"� huesos largos, debida a traumatismo al nacer "		"v�ase Nacimiento, traumatismo, fractura "
� humero (cerrada)	812.20	
�� abierta	812.30	
�� ap�fisis articular	812.44	"v�ase adem�s Fractura, h�mero, c�ndilo(s)"
��� abierta	812.54	
�� cabeza	812.09	
��� abierta	812.19	
�� capitellum	812.49	
��� abierta	812.59	
�� ca�a	812.21	
��� abierta	812.31	
�� c�ndilo(s)	812.44	
��� abierta	812.54	
��� lateral (externo)	812.42	
���� abierta	812.52	
��� medio (epic�ndilo interno)	812.43	
���� abierta	812.53	
�� cuello	812.01	
��� abierta	812.11	
�� cuello anat�mico	812.02	
��� abierta	812.12	
�� cuello quir�rgico	812.01	
��� abierta	812.11	
�� en forma de T	812.44	
��� abierta	812.54	
�� epic�ndilo interno	812.43	
��� abierta	812.53	
�� ep�fisis 		
��� inferior	812.44	"v�ase adem�s Fractura, h�mero, c�ndilo(s)"
���� abierta	812.54	
��� superior	812.09	
���� abierta	812.19	
"�� externo, c�ndilo"	812.42	
��� abierta	812.52	
�� extremo distal 		"v�ase Fractura, h�mero, extremo inferior "
�� extremo o extremidad inferior (extremo distal)	812.40	"v�ase adem�s Fractura, h�mero, por sitio"
��� abierta	812.50	
��� m�ltiples sitios NCOC	812.49	
���� abierta	812.59	
��� sitio especificado NCOC	812.49	
���� abierta	812.59	
�� extremo o extremidad superior (extremo proximal)	812.00	"v�ase adem�s Fractura, h�mero, por sitio"
��� abierta	812.10	
��� sitio especificado NCOC	812.09	
���� abierta	812.19	
�� extremo proximal 		"v�ase Fractura, humero, extremo superior "
�� supracondilar	812.41	
��� abierta	812.51	
�� tr�clea	812.49	
��� abierta	812.59	
�� tuberosidad 		"v�ase Fractura, h�mero, extremo superior "
��� mayor	812.03	
���� abierta	812.13	
��� menor	812.09	
���� abierta	812.19	
�� tuberosidad 		"v�ase Fractura, h�mero, extremo superior "
�� tuberosidad menor	812.09	
��� abierta	812.19	
� ilion (con traumatismo visceral) (cerrada)    	808.41	
�� abierta	808.51	
"� impacci�n, impactada"		"v�ase Fractura, por sitio "
"� innominado, hueso (con traumatismo visceral) (cerrada)"	808.49	
�� abierta	808.59	
� interno 		
"�� cart�lago semilunar, rodilla "		"v�ase Desgarro, menisco, medio "
�� o�do 		"v�ase Fractura, cr�neo, base "
� intertrocanteriana 		"v�ase Fractura, f�mur, cuello, intertrocanteriana "
� isquion (con traumatismo visceral) (cerrada)    	808.42	
�� abierta	808.52	
� isquion 		v�ase Fractura isquion 
�� mand�bula	802.24	
��� abierta	802.34	
� laberinto (�seo) 		"v�ase Fractura, cr�neo, base "
� lanzadores de granadas 		"v�ase Fractura, h�mero, cana "
� laringe (cerrada)	807.5	
�� abierta	807.6	
"� Le Fort, de "		"v�ase Fractura, maxila "
� lumbar 		"v�ase Fractura, v�rtebra, lumbar "
"� lunado, hueso (cerrada)"	814.02	
�� abierta	814.12	
� madera verde 		"v�ase Fractura, por sitio "
"� Malaigne, de (cerrada)"	808.43	
�� abierta	808.53	
"� malar, hueso (cerrada)"	802.4	
�� abierta	802.5	
� malauni�n	733.81	
� mal�olo (cerrada)	824.8	
�� abierta	824.9	
�� bimaleolar	824.4	
��� abierta	824.5	
�� lateral	824.2	
��� abierta	824.3	
��� y media 		"v�ase adem�s Fractura, maI�olo, bimaleolar "
���� con labio de tibia		"v�ase Fractura, mal�olo, trimaleolar "
�� media (cerrada)	824.0	
��� abierta	824.1	
��� y lateral 		"v�ase adem�s Fractura, maI�olo, bimaleolar "
���� con labio de tibia 		"v�ase Fractura, mal�olo, trimaleolar "
�� trimaleolar (cerrada)	824.6	
��� abierta	824.7
� mand�bula (cerrada)	802.20
�� abierta	802.30
�� �ngulo	802.25
��� abierta	802.35
�� ap�fisis 	
��� condilar	802.21
���� abierta	802.31
��� coronoides	802.23
���� abierta	802.33
�� cuerpo	802.28
��� abierta	802.38
��� reborde alveolar	802.27
���� abierta	802.37
��� s�nfisis	802.26
���� abierta	802.36
�� m�ltiples sitios	802.29	
��� abierta	802.39	
�� ramo NCOC	802.24	
��� abierta	802.34	
�� subcondilar	802.22	
��� abierta	802.32	
� mandibular (hueso) (inferior) (cerrada)	802.20	"v�ase adem�s Fractura, mand�bula"
�� abierta	802.30	
�� �ngulo	802.25	
��� abierta	802.35	
�� superior		"v�ase Fractura, maxila "
"� mano, una (cerrada)"	815.00	
�� abierta	815.10	
�� carpianos	814.00	
��� abierta	814.10	
��� sitio especificado NCOC	814.09	
���� abierta	814.19	
�� falanges	816.00	"v�ase adem�s Fractura, falange, mano"
��� abierta	816.10	
�� metacarpianos	815.00	
��� abierta	815.10	
"�� m�ltiple, huesos de una mano"	817.0	
��� abierta	817.1	
� manubrio 		"v�ase Fractura, estern�n "
� marcha (cerrada)	825.20	
�� abierta	825.30	
� martillo 		"v�ase Fractura, cr�neo, base "
"� maxila, maxilar (superior) (cerrada)"	802.4	
�� abierta	802.5	
�� inferior 		"v�ase Fractura, mand�bula "
"� menisco, rodilla "		"v�ase Desgarro, menisco "
"� metacarpo, metacarpiano (hueso(s)), de una sola mano (cerrada)"	815.00	
�� con falange(s) de la mano (dedo(s)) (pulgar) de la misma mano	817.0
��� abierta	817.1
�� abierta	815.10
�� base	815.02
��� abierta	815.12
��� primer metacarpiano	815.01
���� abierta	815.11
��� pulgar	815.01
���� abierta	815.11
�� ca�a	815.03
��� abierta	815.13
�� cuello	815.04
��� abierta	815.14
�� m�ltiples sitios	815.09
��� abierta	815.19
"� metatarso, metatarsianos (hueso(s)), de un solo pie (cerrada)"	825.25
�� con hueso(s) tarsiano(s)	825.29	
��� abierta	825.39	
�� abierta	825.35	
� miembro 		
�� inferior (m�ltiple) (cerrada) NCOC     	827.0	
�� abierta	827.1	
�� superior (m�ltiple) (cerrada) NCOC	818.0	
��� abierta	818.1	
"� Monteggia, de (cerrada)"	813.03	
�� abierta	813.13	
"� Moore, de "		"v�ase Fractura, radio, extremo inferior "
"� multiangular, hueso (cerrada) "		
�� mayor (trapecio)	814.05	
��� abierta	814.15	
�� menor (trapezoides)	814.06	
��� abierta	814.16	
� m�ltiple (cerrada)	829.0
�� abierta	829.1
"�� brazo (huesos m�ltiples en el mismo brazo, salvo los de la mano s�lo) (sitios clasificables bajo 810-817 junto con sitios  clasificables bajo otra categor�a de tres d�gitos entre 810-817 en el mismo brazo) (cerrada)"	818.0
��� abierta	818.1
"�� brazos, ambos o brazo(s) con costilla(s) o estern�n (sitios clasificables bajo 810-818 junto con sitios clasificables dentro de las mismas categor�as en otro miembro o dentro de 807) (cerrada)"	819.0
��� abierta	819.1
"�� columna vertebral con otros huesos, salvo huesos craneales o faciales (sitios clasificables bajo 805 u 806 junto con sitios clasificables bajo 807-808 u 810-829) (cerrada)"	809.0
��� abierta	809.1
"�� cr�neo, huesos especificados o no especificados junto con cualquier hueso o combinaci�n de huesos (sitios clasificables bajo 800-803 junto con sitios clasificables bajo 805-829) (cerrada)"	804.0
	
