��� con 	
"���� contusi�n, cerebral"	804.1
���� hemorragia (intracraneal) NCOC   	804.8
����� epidural	804.2
����� extradural	804.2
����� subaracnoide	804.2
����� subdural	804.2
"���� laceraci�n, cerebral"	804.1
���� traumatismo intracraneal NCOC   	804.4
��� abierta	804.5
���� con 	
����� contusi�n cerebral	804.6
����� hemorragia (intracraneal) NCOC    	804.8
������ epidural	804.7
������ extradural	804.7
������ subaracnoide	804.7
������ subdural	804.7	
����� laceraci�n cerebral	804.6	
����� traumatismo intracraneal NCOC    	804.9	
�� huesos del tronco NCOC (cerrada)	809.0	
��� abierta	809.1	
"�� mano, hueso(s) metacarpiano(s) con falange o falanges de la misma mano (sitios clasificables bajo 815 junto con sitios clasificables bajo 816 en la misma mano) (cerrada)"	817.0	
�� pelvis junto con otros huesos salvo los craneales o faciales (sitios clasificables bajo 808 junto con sitios clasificables bajo 805-807 o bajo 810-829) (cerrada)	809.0	
��� abierta	809.1	
�� pierna (huesos m�ltiples en la misma pierna) (sitios clasificables bajo otra categor�a de tres d�gitos en la mismas categor�as para la misma pierna) (cerrada)	827.0	
��� abierta	827.1	
"�� piernas, ambas o pierna(s) con brazo(s), costilla(s) o estern�n (sitios clasificables bajo 820-827 junto con sitios clasificables bajo las mismas categor�as en otra pierna o bajo 807 o bajo 810-819) (cerrada)"	828.0	
��� abierta	828.1	
� muslo 		"v�ase Fractura, f�mur, ca�a "
� mu�eca (cerrada)	814.00	
�� abierta	814.10	
"� nariz, nasal (hueso) (tabique) (cerrada)"	802.0	
�� abierta	802.1	
� nasal (hueso(s)) (cerrada)	802.0	
�� abierta	802.1	
�� seno 		"v�ase Fractura, cr�neo, base "
� navicular (escafoides) 		
�� mu�eca (cerrada)	814.01	
��� abierta	814.11	
�� tobillo (cerrada)	825.22	
��� abierta	825.32	
� occipucio 		"v�ase Fractura, cr�neo, base "
"� odontoides, ap�fisis "		"v�ase Fractura, v�rtebra cervical "
� olecranon (ap�fisis) (c�bito) (cerrada)	813.01	
�� abierta	813.11	
"� �rbita, orbital (hueso) (regi�n) (cerrada)"	802.8	
�� abierta	802.9	
�� parte especificada NCOC	802.8	
��� abierta	802.9	
�� suelo (revent�n)	802.6	
��� abierta	802.7	
�� techo 		"v�ase Fractura, cr�neo, base "
� orificio 		
�� calcis (cerrada)	825.0	
��� abierta	825.1	
�� magnum (cerrada)	814.07	
��� abierta	814.17	
�� pubis (con traumatismo visceral) (cerrada)	808.2	
��� abierta	808.3	
�� triquetrum (triquetro) (cerrada)	814.03	
��� abierta	814.13	
� �seo		
�� laberinto 		"v�ase Fractura, cr�neo, base "
�� meato auditivo 		"v�ase Fractura, cr�neo, base "
� osteopor�tica (cualquier hueso)	733.1	
� paladar (cerrada)	802.8	
�� abierta	802.9	
� paracaidistas 		"v�ase Fractura, tibia, extremo inferior "
� parada de golpe 		"v�ase Fractura, de Monteggia "
"� parietal, hueso "		"v�ase Fractura, cr�neo, b�veda "
� patelar (cerrada)	822.0	
�� abierta	822.1	
� patol�gica (causa desconocida)	733.1	
� ped�culo (de arco vertebral) 		"v�ase Fractura, v�rtebra, por sitio "
"� pelvis, pelviano (hueso(s)) (con traumatismo visceral) (cerrada)"	808.8	
�� abierta	808.9	
�� m�ltiple (con ruptura del c�rculo pelviano)	808.43	
��� abierta	808.53	
�� reborde (cerrada)	808.49	
��� abierta	808.59	
� peritrocanteriana (cerrada)	820.20	
�� abierta	820.30	
� peron� (cerrada)	823.81	
�� con implicaci�n del tobillo	824.2	
��� abierta	824.3	
�� con tibia	823.82	
��� abierta	823.92	
�� abierta NCOC	823.91	
�� cabeza 		"v�ase Fractura, peron�, extremo superior "
�� ca�a	823.21	
��� con tibia	823.22	
���� abierta	823.32	
��� abierta	823.31	
�� ep�fisis 		
��� inferior	824.8	
���� abierta	824.9	
��� superior 		"v�ase Fractura, peron�, extremo superior "
�� extremo distal	824.8	
��� abierta	824.9	
�� extremo o extremidad inferior	824.8	
��� abierta	824.9	
�� extremo o extremidad superior (ep�fisis) (cabeza) (extremo proximal) (estiloides)	823.01	
��� con tibia	823.02	
���� abierta	823.12	
��� abierta	823.11	
�� extremo proximal 		"v�ase Fractura, peron�, extremo superior "
�� mal�olo (externo) (lateral)	824.2	
��� abierta	824.3	
"� pie, salvo dedo(s) s�lo (cerrada)"	825.20	
�� abierta	825.30	
� pierna (cerrada)	827.0	
�� con costilla(s) o estern�n	828.0	
��� abierta	828.1	
�� abierta	827.1	
�� ambas (cualquier combinaci�n de huesos)	828.0	
��� abierta	828.1	
�� inferior 		"v�ase Fractura, tibia "
�� superior 		"v�ase Fractura, f�mur "
� pisciforme (cerrada)	814.04	
�� abierta	814.14	
"� Pott, de (cerrada)"	824.4	
�� abierta	824.5	
� pubis (con traumatismo visceral) (cerrada)    	808.2	
�� abierta	808.3	
� pulgar (y dedo(s)) de una sola mano (cerrada)	816.00	"v�ase adem�s Fractura, falange, mano"
�� con hueso(s) metacarpiano(s) de la misma mano	817.0	
��� abierta	817.1	
�� abierta	816.10	
�� metacarpiano(s) 		"v�ase Fractura, metacarpo "
"� Quervain, de (cerrada)"	814.01	
�� abierta	814.11	
� radio (solo) (cerrada)	813.81	
�� con c�bito NCOC	813.83	
��� abierta	813.93	
�� abierta NCOC	813.91	
�� cabeza 		"v�ase Fractura, radio, extremo superior "
�� ca�a (cerrada)	813.21	
��� con c�bito (ca�a)	813.23	
���� abierta	813.33	
��� abierta	813.31	
�� cuello 		"v�ase Fractura, radio, extremo inferior "
�� ep�fisis 		
��� inferior 		"v�ase Fractura, radio, extremo inferior "
��� superior 		"v�ase Fractura, radio, extremo superior "
�� extremo distal 		"v�ase Fractura, radio, extremo inferior "
�� extremo o extremidad inferior (extremo distal) (ep�fisis inferior)	813.42	
��� con c�bito (extremo inferior)	813.44	
���� abierta	813.54	
��� abierta	813.52	
�� extremo proximal 		"v�ase Fractura, radio, extremo superior "
�� extremo superior	813.07	
��� con c�bito (extremo superior)	813.08	
���� abierta	813.18	
��� abierta	813.17	
��� cabeza	813.05	
���� abierta	813.15	
��� cuello	813.06	
���� abierta	813.16	
��� ep�fisis	813.05	
���� abierta	813.15	
��� m�ltiples sitios	813.07	
���� abierta	813.17	
��� sitio especificado NCOC	813.07	
���� abierta	813.17	
"� ra�z, diente"	873.63	
�� complicada	873.73	
� ramo 		
�� inferior o superior (con traumatismo visceral) (cerrada)	808.2	
��� abierta	808.3	
�� isquion 		"v�ase adem�s Fractura, isquion "
� reborde de bota 		"v�ase Fractura, peron� "
� revent�n 		"v�ase Fractura, falange, mano "
� rodilla 		
�� cart�lago (semilunar) 		"v�ase Desgarre, menisco "
�� r�tula (cerrada)	822.0	
��� abierta	822.1	
� sacro 		"v�ase Fractura, v�rtebra, sacro "
� semilunar 		
�� cart�lago (interior) (rodilla) 		"v�ase Desgarre, menisco "
"�� hueso, mu�eca (cerrada)"	814.02	
��� abierta	814.12	
� seno (etmoides) (frontal) (nasal) (esfenoidal) 		"v�ase Fractura, cr�neo, base "
�� maxilar 		"v�ase Fractura, maxila "
"� sesamoideo, hueso "		"v�ase Fractura, por sitio "
"� Shepherd, de (cerrada)"	825.21	
�� abierta	825.31	
"� s�nfisis, pubiana (con traumatismo visceral) (cerrada)"	808.2	
�� abierta	808.3	
"� Skillern, de "		"v�ase Fractura, radio, cana "
"� Smith, de"	813.41	
�� abierta	813.51	
"� Stieda, de "		"v�ase Fractura, f�mur, extremo inferior "
"� supracondilar, codo"	812.41	
�� abierta	812.51	
� tallo verde 		"v�ase Fractura, por sitio "
� talo (astr�galo) (hueso de tobillo) (cerrada)	825..21	
�� abierta	825.31	
"� tal�n, hueso de (cerrada)"	825.0	
�� abierta	825.1	
"� tarso, hueso(s) tarsiano(s) (con metatarso) de un solo pie (cerrada) NCOC"	825.29	
�� abierta	825.39	
"� temporal, hueso (estiloides) "		"v�ase Fractura, cr�neo, base "
� tend�n 		"v�ase Esguince, por sitio "
� tensi�n	733.1	
� tibia (cerrada)	823.80	
�� con implicaci�n del tobillo	824.0	
��� abierta	824.1	
�� con peron�	823.82	
��� abierta	823.92	
�� abierta NCOC	823.90	
�� cabeza (con implicaci�n de la articulaci�n de la rodilla) 		"v�ase Fractura, tibia, extremo superior "
�� ca�a	823.20	
��� con peron�	823.22	
���� abierta	823.32	
��� abierta	823.30	
�� c�ndilos 		"v�ase Fractura, tibia, extremo superior "
�� eminencia intercondiloidea 		"v�ase Fractura, tibia, extremo superior "
�� ep�fisis 		
��� inferior	824.8	
���� abierta	824.9	
��� superior 		"v�ase Fractura, tibia, extremo superior "
�� espina 		"v�ase Fractura, tibia, extremo superior "
�� extremo distal	824.8	
��� abierta	824.9	
�� extremo o extremidad inferior (labio anterior)	824.8	
��� abierta	824.9	
�� extremo o extremidad superior (c�ndilo) (ep�fisis) (cabeza) (espina) (extremo proximal) (tuberosidad)	823.00	
��� con peron�	823.02	
���� abierta	823.12	
��� abierta	823.10	
�� extremo proximal 		"v�ase Fractura, tibia, extremo superior "
�� mal�olo (interno) (medio)	824.0	
��� abierta	824.1	
�� tuberosidad 		"v�ase Fractura, tibia, extremo superior "
� tobillo (mal�olo) (cerrada)	824.8	
�� abierta	824.9	
�� bimaleolar (de Dupuytren) (de Pott) 	824.4	
��� abierta	824.5	
�� hueso	825.21	
��� abierta	825.31	
�� mal�olo lateral (externo) s�lo (peroneal)	824.2
��� abierta	824.3
�� mal�olo mediano (interno) s�lo (tibial)  	824.0
��� abierta	824.1
�� talo (astr�galo)	825.21
��� abierta	825.31
�� trimaleolar	824.6
��� abierta	824.7
� trapecio (cerrada)	814.05
�� abierta	814.15
"� trapezoides, hueso abierta"	814.06
�� abierta	814.16
� tr�quea (cerrada)	807.5
�� abierta	807.6
� trimaleolar (cerrada)	824.6
�� abierta	824.7
� triquetral (hueso) (cerrada)	814.03	
�� abierta	814.13	
� troc�nter (mayor) (menor) (cerrada)	820.20	"v�ase adem�s Fractura, f�mur, cuello, por sitio"
�� abierta	820.30	
� tronco (huesos) (cerrada)	809.0	
�� abierta	809.1	
� tuberosidad (externa) 		"v�ase Fractura, por sitio "
� unciforme (cerrada)	814.08	
�� abierta	814.18	
"� v�rtebra, vertebral (espalda) (cuerpo) (columna) (arco neural) (ped�culo) (espina) (ap�fisis espinosa) (ap�fisis transversal) (cerrada)"	805.8	
�� con 		
��� contusi�n espinal 		"v�ase Fractura, v�rtebra, por sitio, con traumatismo de la m�dula espinal "
��� cuadriplejia 		"v�ase Fractura, v�rtebra, por sitio, con traumatismo de la m�dula espinal "
��� hematomielia 		"v�ase Fractura, v�rtebra, por sitio, con traumatismo de la m�dula espinal "
��� par�lisis 		"v�ase Fractura, v�rtebra, por sitio, con traumatismo de la m�dula espinal "
��� paraplejia 		"v�ase Fractura, v�rtebra, por sitio, con traumatismo de la m�dula espinal "
��� traumatismo de 		
���� cauda equina (cola de caballo) 		"v�ase Fractura, v�rtebra, sacro, con traumatismo de la m�dula espinal "
���� nervio 		"v�ase Fractura, v�rtebra, por sitio, con traumatismo de la m�dula espinal "
��� traumatismo de m�dula espinal (cerrada) NCOC	806.8	
���� cervical	806.0	
����� abierta	806.1	
"���� dorsal, dorsolumbar"	806.2	
����� abierta	806.9	
"���� tor�cica, toracolumbar"	806.2	
����� abierta	806.3	
�� abierta NCOC	805.9	
�� atlantoaxoidea 		"v�ase, Fractura, v�rtebra, cervical "
�� cervical (verdugos) (l�grima) (cerrada)	805.00	
��� con traumatismo de m�dula espinal		"v�ase, Fractura, v�rtebra, con traumatismo de m�dula espinal, cervical "
��� abierta	805.10	
��� cuarta	805.04	
���� abierta	805.14
��� m�ltiples sitios	805.08
���� abierta	805.18
��� primera (atlas)	805.01
���� abierta	805.11
��� quinta	805.05
���� abierta	805.15
��� segunda (axis)	805.02
���� abierta	805.12
��� s�ptima	805.07
���� abierta	805.17
��� sexta	805.06
���� abierta	805.16
��� tercera	805.03
���� abierta	805.13
�� c�ccix (cerrada)	805.6
��� con traumatismo de la m�dula espinal (cerrada)	806.60	
���� abierta	806.70	
���� tipo especificado NCOC	806.69	
����� abierta	806.79	
���� traumatismo de la cauda equina   	806.62	
����� abierta	806.72	
����� lesi�n completa	806.61	
���� abierta	806.71	
"�� compresi�n, no debida a traumatismo"	733.1	
�� debida a osteoporosis	733.1	
�� dorsal (cerrada)	805.2	
��� con traumatismo de m�dula espinal		"v�ase, Fractura, v�rtebra, con traumatismo de m�dula espinal, dorsal "
��� abierta	805.3	
�� dorsolumbar (cerrada)	805.2	
��� con traumatismo de m�dula espinal		"v�ase, Fractura, v�rtebra, con traumatismo de m�dula espinal, dorsal "
��� abierta	805.3	
�� feto o reci�n nacido	767.4
�� lumbar (cerrada)	805.4
��� con traumatismo de m�dula espinal (cerrada)	806.4
���� abierta	806.5
��� abierta	805.5
�� no traum�tica	733.1
�� patol�gica (cualquier sitio)	733.1
�� sacro (cerrada)	805.6
��� con traumatismo de m�dula espinal	806.60
���� abierta	806.70
���� tipo especificado NCOC	806.69
����� abierta	806.79
���� traumatismo de cauda equina	806.62
����� abierta	806.72
����� lesi�n completa	806.61
������ abierta	806.71
��� abierta	805.7	
�� sitio no especificado (cerrada)	805.8	
��� con traumatismo de m�dula espinal (cerrada)	806.8	
���� abierta	806.9	
��� abierta	805.9	
�� tor�cica (cerrada)	805.2	
��� con traumatismo de m�dula espinal		"v�ase Fractura, v�rtebra, con traumatismo de m�dula espinal, tor�cica "
��� abierta	805.3	
� v�rtice 		"v�ase Fractura, cr�neo, b�veda "
� v�mer (hueso)	802.0	
�� abierta	802.1	
"� Wagstaffe, de "		"v�ase Fractura, tobillo "
� xifoides (ap�fisis) 		"v�ase Fractura, estern�n"
� yunque 		"v�ase Fractura, cr�neo, base"
Fractura de los estanques 		"v�ase Fractura, cr�neo, b�veda"
Fragilidad 		
� capilar (hereditaria)	287.8	
� hueso (cong�nita)	756.51	
�� con sordera y escler�tica azul	756.51	
� pelo	704.2	
� u�as	703.8	
�� cong�nita	757.5	
Fragilitas 		
� crinium	704.2	
� ossium	756.51	
�� con escler�tica azul	756.51	
� pelo	704.2	
� unguium	703.8	
�� cong�nita	757.5	
Fragmentaci�n 		"v�ase Fractura, por sitio"
"Frambesia, framb�sico (tr6pica) (yaws)   "	102.9	
� chancro	102.0	
� cuando causa paso de cangrejo (�wet crab�)   	102.1
"� cut�nea, menos de cincos a�os despu�s de producirse la infecci�n"	102.2
� ganglio	102.6
"� gangosis, gangosa"	102.5
� goma	102.4
�� hueso	102.6
� gomosa 	
�� framb�side	102.4
�� oste�tis	102.6
�� periostitis	102.6
� hidrartrosis	102.6
� hiperqueratosis (precoz) (tard�a) (palmar)    (plantar)	102.3
� latente (con serolog�a positiva) (sin manifestaciones cl�nicas)	102.8
� lesi�n o �lcera inicial	102.0
� lesiones 	
�� articulaciones	102.6
�� hueso o articulaci�n	102.6
�� iniciales	102.0
� madre 	102.0
� mantecosa	102.1
� mucosa	102.7
� nodular tard�a (ulcerada)	102.4
� n�dulos yuxtaarticulares	102.7
� oste�tis	102.6
"� papiloma, papilomatosa (palmar) (plantar)   "	102.1
� papilomatosa m�ltiple	102.1
� p�rpado	102.9 [373.4]
� periostitis (hipertr�fica)	102.6
� precoz (cut�nea) (macular) (maculopapular) (micropapular) (papular)	102.2
�� framb�side	102.2
�� lesiones cut�neas NCOC	102.2
� primaria	102.0
"� tard�a, nodular (ulcerada)"	102.4	
� �lceras	102.4	
Framb�side 		
� gomosa	102.4	
� precoz de frambesia	102.2	
Frambesioma	102.1	
"Franceschetti, s�ndrome de (diostosis mandibulofacial)"	756.0	
"Francis, enfermedad de"	021.9	v�ase adem�s Tularemia
"Frank, trombocitopenia esencial de"	287.3	"v�ase adem�s P�rpura, trombocitop�nica"
"Franklin, enfermedad de (cadena pesada)"	273.2	
"Fraser, s�ndrome de"	759.89	
Frecuencia (urinaria)	788.4	
� micci�n (nocturna)	788.4	
� psic�gena	306.53	
"Freeman-Sheldon, s�ndrome de"	759.89	
"Frei, enfermedad de (bub�n clim�tico)"	099.1	
"Freiberg, de "		
"� enfermedad (osteocondrosis, segundo metatarsiano)"	732.5	
� infracci�n de cabeza de metatarsiano	732.5	
� osteocondrosis	732.5	
"Fr�mito, fricci�n, cardiaco"	785.3	
Frenillo 		
� lingual	750.0	
� orificio externo (os externo) del cuello uterino	752.49	
Frenitis 	323.9	
Fresa 		
� lengua (roja) (blanca)	529.3	
� mancha	757.32	
� ves�cula biliar en	575.6	"v�ase adem�s Enfermedad, ves�cula biliar"
"Frey, s�ndrome de (s�ndrome auriculotemporal)"	350.8	
Fricci�n 		
"� fr�mito, cardiaco"	785.3	
� precordial	785.3	
� quemadura por	919.0	"v�ase adem�s Traumatismo, superficial, por sitio"
� sonidos pectorales	786.7	
"Friderichsen-Waterhouse, s�ndrome o enfermedad de"	036.3	
"Friedlfinder, de "		
� B (bacilo) NCOC	041.3	v�ase adem�s enfermedad espec�fica
�� septicemia	038.49	
� enfermedad (endarteritis obliterante) 		v�ase Arteriosclerosis
"Friedreich, de "		
� ataxia	334.0	
� enfermedad	333.2	
�� sist�mica combinada	334.0	
� esclerosis (m�dula espinal)	334.0	
� mioclon�a	333.2	
"Friedrich-Erb-Arnold, s�ndrome de (acropaquiderma)"	757.39	
Frigidez	302.72	
� ps�quica o psic�gena	302.72	
Frinoderma	264.8	
Fr�o	460	
� absceso 		"v�ase adem�s Tuberculosis, absceso "
�� articular 		"v�ase Tuberculosis, articula"
� aglutinina 		
�� enfermedad (cr�nica) o s�ndrome	283.0	
�� hemoglobinuria	283.0	
��� parox�stica (fr�a) (nocturno)	283.2	
� agotamiento por	991.8	
� efectos de	991.9	
�� efecto especificado NCOC	991.8	
� excesivo	991.9	
�� efecto especificado NCOC	991.8	
� exposici�n a	991.9	
�� efecto especificado NCOC	991.8	
� profundo	464.10	
� s�ndrome de lesi�n (reci�n nacido)	778.2	
"Fr�hlich, enfermedad o s�ndrome de (distrofia adiposogenital)"	253.8	
"Froin, s�ndrome de"	336.8	
"Frommel, enfermedad de"	676.6	
"Frommel-Chiari, s�ndrome de"	676.6	
Frontal 		v�ase adem�s enfermedad espec�fica 
� s�ndrome de l�bulo	310.0	
Fructosemia	271.2	
Fructosuria (benigna) (esencial)	271.2	
Frustrado 		
� aborto	632	
"� parto (a, o casi a t�rmino)"	656.4	
"� trabajo del parto (a, o casi a t�rmino)"	656.4	
"Fuch, de "		
� ciclitis heterocrom�tica	364.21	
� distrofia c�rnea (endotelial)	371.57	
� mancha negra (mi�pica)	360.21	
Fucosidosis	271.8	
Fuego de San Antonio	035	v�ase adem�s Erisipela
Fuga	780.9	
� hist�rica	300.13	
� reacci�n a tensi�n emocional extrema (transitoria)	308.1	
"Fuller Albright, s�ndrome de (oste�tis fibrosa diseminada)"	756.59	
Fulminante 		v�ase enfermedad espec�fica
"Fumadores, de los "		
� bronquitis	491.0	
� garganta	472.1	
� lengua	528.6	
� s�ndrome	305.1	"v�ase adem�s Abuso, drogas, sin dependencia"
� tos	491.0	
Funcional 		v�ase enfermedad espec�fica
"Fundidores de lat�n, dolor de"	985.8	
Funguemia	117.9	
"Fungus, fungoso "		
� cerebral	348.8	
� enfermedad NCOC	117.9	
� infecci�n 		"v�ase Infecci�n, hongos "
� test�culo	016.5 [608.81]	v�ase adem�s Tuberculosis
Funiculitis (aguda)	608.4	
� cr�nica	608.4	
� end�mica	608.4	
� gonoc�cica (aguda)	098.14	
�� cr�nica o de duraci�n de 2 meses o m�s  	098.34	
� tuberculosa	016.5	v�ase adem�s Tuberculosis
Furfur	690	
� microsporon	111.0	
Furia	312.0	"v�ase adem�s Perturbaci�n, conducta"
� cuando significa rabia 	071	
Furor paroxismal (idiop�tica)	345.8	v�ase adem�s Epilepsia
Fur�nculo	680.9	
� ano	680.5	
� antebrazo	680.3	
� axila	680.3	
� brazo (cualquier parte encima de la mu�eca)    	680.3	
� brazo superior	680.3	
"� cabeza (cualquier parte, salvo cara)"	680.8	
� cadera	680.6	
"� canal auditivo, externo"	680.0	
"� cara (cualquier parte, salvo ojo)"	680.0	
� conducto deferente	608.4	
� cord�n esperm�tico	608.4	
� costado	680.2	
� cuello	680.1	
� cuero cabelludo (cualquier parte)	680.8	
� cuerpo cavernoso	607.2	
� dedo 		
�� de mano (cualquiera)	680.4	
�� de pie (cualquiera)	680.7	
� escroto	608.4	
� espalda (cualquier parte)	680.2	
� gl�teo (regi�n)	680.5	
� hombro	680.3	
� ingle	680.2	
� labio vulvar (mayor) (menor)	616.4	
� lagrimal 		
�� canal�culos (conducto) (saco)	375.30	v�ase adem�s Dacrioadenitis
�� gl�ndula	375.00	v�ase adem�s Dacrioadenitis  
� maligno	022.0	
� mama	680.2	
� mano (cualquier parte)	680.4
� m�ltiples sitios	680.9
� muslo	680.6
� mu�eca	680.4
� nalga	680.5
� nariz (externa) (tabique)	680.0
� o�do (cualquier parte)	680.0
� ombligo	680.2
� �rbita	376.01
� pabell�n de la oreja	680.0
� pared 	
�� abdominal	680.2
�� tor�cica	680.2
� p�rpado	373.13
� partes posteriores	680.5
� pene	607.2
� perineo	680.2	
� pie (cualquier parte)	680.7	
� piel NCOC	680.9	
"� pierna, cualquier parte, salvo pie"	680.6	
� pulgar	680.4	
� regi�n pectoral	680.2	
� ri��n	590.2	"v�ase adem�s Absceso, ri��n"
� rodilla	680.6	
� sien (regi�n temporal)	680.0	
� sitio especificado NCOC	680.8	
� tal�n	680.7	
� test�culo	604.90	
� tobillo	680.6	
� tronco	680.2	
� t�nica vaginal	608.4	
� ves�cula seminal	608.0	
� vulva	616.4	
Furunculosis	680.9	v�ase adem�s Fur�nculo
� meato auditivo externo	680.0 [380.13]	
Fusarium (infecci�n por) 	118	
"Fusi�n, fusionado (cong�nita) "		
� anal (con canal urogenital)	751.5	
� aorta y arteria pulmonar	745.0	
� articulaci�n (adquirida) 		v�ase adem�s Anquilosis 
�� cong�nita	755.8	
� astragaloscafoidea	755.67	
� atrial	745.5	
� atrioventricular	745.69	
"� aur�culas, coraz�n"	745.5	
"� binocular, con estereopsis defectuosa"	368.33	
� canal auditivo	744.02	
"� cervical, espina "		"v�ase Fusi�n, espina "
� coanas	748.0	
"� comisura, v�lvula mitral"	746.5	
� conducto sublingual con conducto submaxilar en su abertura en la boca	750.26	
� costillas	756.3	
"� cr�neo, imperfecta"	756.0	
"� c�spides, v�lvula del coraz�n NCOC"	746.89	
�� mitral	746.5	
�� tric�spide	746.89	
� dedos 		
�� de mano	755.11	"v�ase adem�s Sindactilia, dedos de mano"
�� de pie	755.13	"v�ase adem�s Sindactilia, dedos de pie"
� diente(s)	520.2	
� espina (adquirida)	724.9	
�� cong�nita (v�rtebra)	756.15	
�� estado de 		
��� artrodesis	V45.4	
��� posoperatorio	V45.4
� gemelos	759.4
� himen	752.42
� himeno-uretral	599.8
�� cuando obstruye el parto	660.1
��� que afecta al feto o al reci�n nacido   	763.1
� huesecillos	756.9
�� auditivos	744.04
� huesecillos del o�do	744.04
� hueso	756.9
� labio vulvar (mayor) (menor)	752.49
� laringe y tr�quea	748.3
"� l�bulo, pulm�n"	748.5
� lumbosacra (adquirida)	724.6
�� cong�nita	756.15
�� quir�rgica	V45.4
� miembro	755.8	
�� inferior	755.69	
�� superior	755.59	
� narices (anterior) (posterior)	748.0	
"� nariz, nasal"	748.0	
� �rgano o sitio NCOC 		"v�ase Anomal�a, tipo especificado NCOC "
"� pulmonar, c�spide"	746.02	
� ri�ones (incompleta)	753.3	
� sacroil�aca (adquirida) (articulaci�n)	724.6	
�� cong�nita	755.69	
�� quir�rgica	V45.4	
� segmento de v�lvula pulmonar	746.02	
"� suturas craneales, prematura"	756.0	
� talonavicular (barra)	755.67	
� test�culos	752.8	
� tr�quea y es�fago	750.3	
� uretral-himenal	599.8	
� vagina	752.49	
"� v�lvula, c�spides de "		"v�ase Fusi�n, c�spides, v�lvula del coraz�n "
"� ventr�culos, coraz�n"	745.4	
� v�rtebra (arco) 		"v�ase Fusi�n, espina "
� vulva	752.49	
"G, trisom�a"	758.0	
"Gafsa, bot�n o fur�nculo de"	085.1	
"Gaisbock, enfermedad o s�ndrome de (policitemia hipert�nica)"	289.0	
Galactocele (mama) (infectado)	611.5	
"� puerperal, posparto"	676.8	
Galactoforitis	611.0	
"� puerperal, posparto"	675.2	
Galactorrea	676.6	
� no asociado con el alumbramiento	611.6	
Galactosemia (cl�sica) (cong�nita)	271.1	
Galactosuria	271.1	
Galacturia	791.1	
� bilharziasis	120.0	
"Galeno, vena de "		v�ase enfermedad espec�fica
"Galope, ritmo de"	427.89	
Gammaloidosis	277.3	
Gammopat�a	273.9	
� macroglobulinemia	273.3	
� monocl�nica (benigna) (esencial) (idiop�tica) (con discrasia linfoplasmac�tica)  	273.1	
"Gamna, enfermedad de (esplenomegalia sider�tica)"	289.51	
Gampsodactilia (cong�nita)	754.71	
"Gampstorp, enfermedad de (adinamia epis�dica hereditaria)"	359.3	
Ganancia de peso (anormal) (excesiva)	783.1	"v�ase adem�s Peso, ganancia"
"Gandy-Nanta, enfermedad de (esplenomegalia sider�tica)"	289.51	
Ganglio	727.43	
� articulaci�n	727.41	
� de yaws (frambesia) (precoz) (tard�o)  	102.6	
� periostio	730.3	v�ase adem�s Periostitis
� tuberculoso	015.9	v�ase adem�s Tuberculosis  
� vaina de tend�n (compuesto) (difuso)  	727.42	
Gangliocitoma 	(M9490/0)	"v�ase Neoplasia, tejido conjuntivo, benigna"
Ganglioglioma 	(M9505/1)	"v�ase Neoplasia, por sitio, comportamiento incierto"
Ganglioneuroblastoma 	(M9490/3)	"v�ase Neoplasia, tejido conjuntivo, maligna"
Ganglioneuroma 	(M9490/0)	"v�ase adem�s Neoplasia, tejido conjuntivo, benigna "
� maligna 	(M9490/3)	"v�ase Neoplasia, tejido conjuntivo, maligna"
Ganglioneuromatosis 	(M9491/0)	"v�ase Neoplasia, tejido conjuntivo, benigna  "
Ganglionitis		
� ganglio de Gasser	350.1	
� geniculada	351.1	
�� herp�tica	053.11	
�� reci�n nacido	767.5	
� herpes zoster	053.11	
� herp�tica geniculada (s�ndrome de Hunt)	053.11	
� quinto nervio	350.1	"v�ase adem�s Neuralgia, trig�mino"
Gangliosidosis	330.1	
Gangosa	102.5	
"Gangrena, gangrenoso (anemia) (arteria) (celulitis) (cut�nea) (dermatitis) (estasis) (h�meda) (infecciosa) (p�nfigo) (seca) (s�ptica) (�lcera)"	785.4	
� con 		
�� arteriosclerosis	440.2 [785.4]	
�� diabetes (mellitus)	250.7 [785.4]	
� abdomen (pared)	785.4	
�� arterioscler�tica	440.2 [785.4]	
� adenitis	683	
� alveolar	526.5	
� amigdalitis (aguda)	463	
� angina	462	
�� dift�rica	032.0	
� ano	569.49	
� ap�ndice	540.9	
"�� con perforaci�n, peritonitis (generalizada)"		
� rotura	540.0	
� ap�ndices epiploicos 		"v�ase Gangrena, mesenterio "
� arterioscler�tica (general)(senil)	440.2 [785.4]	
� aur�cula	785.4	
� Bacillus welchii	040.0	"v�ase adem�s Gangrena, gaseosa"
� boca	528.1	
� ciego 		"v�ase Gangrena, intestino "
� Clostridium perfringens o welchii	040.0	"v�ase adem�s Gangrena, gaseosa"
� colon 		"v�ase Gangrena, intestino "
� conducto 		
�� biliar	576.8	v�ase adem�s Colangitis
�� deferente	608.4	
��� no infecciosa	608.89	
� conductos deferentes	608.4	
�� no infeccioso	608.89	
� cord�n esperm�tico	608.4	
�� no infecciosa	608.89	
� c�rnea	371.40	
� cuerpos cavernosos (infecciosa)	607.2	
�� no infecciosa	607.89	
"� cut�nea, progresiva"	785.4	
� de dec�bito	707.0 [785.4]	
� diab�tica (cualquier sitio)	250.7 [785.4]	
� diente (pulpa)	522.1	
� dolor de garganta	462	
� enc�a	523.8	
� enfisematosa	040.0	"v�ase adem�s Gangrena, gaseosa"
� epid�mica (grano ergotizado)	988.2	
� epid�dimo (infecciosa)	604.99	v�ase adem�s Epididimitis
� erisipela	035	v�ase adem�s Erisipela
� escroto	608.4	
�� no infecciosa	608.83	
� espina	785.4	
"� espiroquet�sica, espiroqu�tica NCOC"	104.8	
� est�mago	537.89	
� estomatitis	528.1	
� extremidad (inferior) (superior)	785.4	
� faringe	462	
�� s�ptica	034.0	
� garganta	462	
�� dift�rica	032.0	
� gaseosa (bacilo)	040.0	
�� con 		
��� aborto 		"v�ase Aborto, por tipo, con septicemia "
��� embarazo ect�pico	639.0	v�ase adem�s categor�as 633.0-633.9
��� embarazo molar	639.0	v�ase adem�s categor�as 630-632
�� despu�s de 		
��� aborto	639.0	
��� embarazo ect�pico o molar	639.0	
"�� puerperal, posparto, alumbramiento"	670	
� glositis	529.0	
� hernia 		"v�ase Hernia, por sitio, con gangrena "
� hidropes�a	785.4	
� h�gado	573.8	
"� hospitalario, noma"	528.1	
"� intestino, intestinal (aguda) (hemorr�gica) (masiva)"	557.0	
�� con 		
��� embolia o infarto mesent�rico	557.0	
��� hernia		"v�ase Hernia, por sitio, con gangrena "
��� obstrucci�n	560.9	"v�ase adem�s Obstrucci�n, intestino"
� laringitis	464.0	
� linfangitis	457.2	
"� Meleney, de (cut�nea)"	686.0	
� mesenterio	557.0	
�� con 		
��� embolia o infarto	557.0	
��� obstrucci�n intestinal	560.9	"v�ase adem�s Obstrucci�n, intestino"
� neumon�a	513.0	
� noma	528.1	
� ombligo	785.4	
� orquitis	604.90	
� ovario	614.2	v�ase adem�s Salpingooforitis
� p�ncreas	577.0	
� pene (infecciosa)	607.2	
�� no infecciosa	607.89	
� perineo	785.4	
"� Pott, de"	440.2 [785.4]	
� presenil	443.1	
� progresiva cut�nea	785.4	
� pulm�n	513.0	
"�� espiroquet�sica, espiroqu�tica"	104.8	
� pulmonar	513.0	
"� pulpa, diente"	522.1	
"� Raynaud, de (gangrena sim�trica)"	443.0 [785.4]	
� recto	569.49	
� retrofar�ngea	478.24	
� rotura 		"v�ase Hernia, por sitio, con gangrena "
� senil	440.2 [785.4]	
� sim�trica	443.0 [785.4]	
� tejido conjuntivo	785.4	
� test�culo (infecciosa)	604.99	v�ase adem�s Orquitis
�� no infecciosa	608.89	
� tiroidea (gl�ndula)	246.8	
� tuberculosa NCOC	011.9	v�ase adem�s Tuberculosis
� t�nica vaginal	608.4	
�� no infecciosa	608.89	
� �tero	615.9	v�ase adem�s Endometritis
� uvulitis	528.3	
� vejiga	595.89	
� ves�cula o conducto biliar	575.0	"v�ase adem�s Colecistitis, aguda"
� vulva	616.10	v�ase adem�s Vulvitis
"Ganister, enfermedad de (ocupacional)"	502	
� con tuberculosis 		"v�ase Tuberculosis, pulmonar"
"Ganser, s�ndrome de, hist�rico"	300.16	
"Gardner-Diamond, s�ndrome de (sensibilizaci�n autoeritroc�tica)"	287.2	
Garganta 		v�ase enfermedad espec�fica
Gargolismo	277.5	
"Garra de, del "		
� Dabney	074.1	
� diablo	074.1	
"Garra, dedo de pie en (cong�nito)"	754.71
� adquirido	735.5
"Garra, mano en (adquirida)"	736.06
� cong�nita	755.59
"Garra, pie en (cong�nito)"	754.71
� adquirido	736.74
Garrapatas 	
� fiebre (transmitida por) NCOC	066.1
"�� americana, de las monta�as"	066.1
�� Colorado	066.1
�� de las monta�as	066.1
