� nefr�tica	581.9	
� neonatal 		"v�ase Hidropes�a, fetal "
� nutritiva	269.9	
� ovario	620.8	
� pal�dica	084.9	v�ase adem�s Paludismo
� pecho	511.9	
� pericardio	423.9	v�ase adem�s Pericarditis
� pulm�n	514	
� reci�n nacido 		"v�ase Hidropes�a, fetal "
� renal	581.9	v�ase adem�s Nefrosis
� ri��n	581.9	v�ase adem�s Nefrosis
� ur�mica 		v�ase Uremia 
Hidropionefrosis	590.80	v�ase adem�s Pielitis
� cr�nica	590.00	
Hidrorraquis	742.53	
Hidrorrea (nasal)	478.1	
� embarazo	658.1	
� grav�dica	658.1	
Hidrosadenitis	705.83	
Hidrosalpinx (trompa de Falopio) (folicular)	614.1	
Hidrot�rax (doble) (pleural)	511.8	
� estafiloc�cico	511.1	
� estreptoc�cico	511.1	
� neumoc�cico	511.1	
� no tuberculoso	511.8	
�� bacteriano	511.1	
� quiloso (no fil�rico)	457.8	
�� fil�rico	125.9	"v�ase adem�s Infestaci�n, fil�rica"
� traum�tico	862.29	
�� con herida penetrante del t�rax	862.39	
� tuberculoso	012.0	"v�ase adem�s Tuberculosis, pleura"
Hidrour�ter	593.5	
� cong�nito	753.2	
Hidroureteronefrosis 	591	v�ase adem�s Hidronefrosis
Hidrouretra	599.8	
Hidroxiprolinemia	270.8	
Hidroxiprolinuria	270.8	
Hidroxiquinorreninuria	270.2	
Hierro 		
� anemia por carencia de	280.9	
� enfermedad de almacenamiento de	275.0	
� enfermedad del metabolismo	275.0	
Hifema 		v�ase Hipema  
H�gado		v�ase adem�s enfermedad espec�fica 
� donante	V59.8	
Higroma (cong�nito) (qu�stico) 	228.1 (M9173/0)	
� prepatelar (r�tula)	727.3	
� subdural 	081.9	"v�ase Hematoma, subdural "
"Hildenbrand, enfermedad de (tifus)"		
"Hilger, s�ndrome de"	337.0	
Hilio 	579.1	v�ase enfermedad espec�fica 
"Hill, diarrea de"		
"Hilliard, lupus de"	017.0	v�ase adem�s Tuberculosis
"Hilo de cobre, arterias en, retina"	362.13	
Himen 		v�ase enfermedad espec�fica
Himen cribiforme	752.49	
Himenolepiasis (diminuta) (infecci�n) (infestaci�n) (nana)	123.6	
Himenolepsis (diminuta) (infecci�n) (infestaci�n) (nana)	123.6	
Hinchamiento	787.3	
Hinchaz�n 		v�ase Tumefacci�n
"Hinchaz�n, azul"	491.2	
Hipalgesia	782.0	"v�ase adem�s Perturbaci�n, sensaci�n"
Hipema (c�mara anterior) (cuerpo ciliar) (iris)	364.41	
� traumatico	921.3	
Hiper-TBG-nemia	246.8
"Hiperabducci�n, s�ndrome de"	447.8
"Hiperacidez, g�strica"	536.8
� psic�gena	306.4
"Hiperactivo, hiperactividad "	
"� c�lulas basales, cuello uterino"	622.1
� colon	564.1
"� epitelial, cuello uterino (basal)"	622.1
� est�mago	536.8
� gastrointestinal	536.8
�� psic�gena	306.4
� intestino (s�ndrome de)	564.1
�� ruidos	787.5
� intestino	564.1
� laberinto (unilateral)	386.51
�� con p�rdida de actividad laber�ntica	386.58
�� bilateral	386.52	
� membrana mucosa nasal	478.1	
� ni�o	314.01	
� tiroides (gl�ndula) 	242.9	v�ase adem�s Tirotoxicosis
Hiperacusia	388.42	
Hiperadrenalismo (cortical)	255.3	
� medular	255.6	
Hiperadrenocorticismo	255.3	
� cong�nito	255.2	
� yatr�geno 		
�� sobredosis o ingesti�n o administraci�n de sustancia incorrecta	962.0	
�� sustancia correcta administrada de forma correcta	255.3	
Hiperafectividad	301.11	
Hiperaldosteronismo (at�pico) (hiperpl�sico) (normoaldosteronal) (normotensivo) (primario) (secundario)	255.1	
Hiperalgesia	782.0	"v�ase adem�s Perturbaci�n, sensaci�n"
Hiperalimentaci�n	783.6	
� caroteno	278.3
� especificada NCOC	278.8
� vitamina A	278.2
� vitamina D	278.4
Hiperamilasemia	790.5
Hiperaminoaciduria	270.9
� arginina	270.6
� cistina	270.0
� citrulina	270.6
� glicina	270.0
� lisina	270.7
� ornitina	270.6
"� renal (tipos I, II, III)"	270.0
Hiperamnesia	780.9
Hiperamonemia (cong�nita)	270.6
Hiperazotemia	791.9
Hiperbetalipoproteinemia (adquirida) (esencial) (familiar) (hereditaria) (primaria) (secundaria)	272.0	
� con prebetalipoproteinemia	272.2	
Hiperbilirrubinemia	782.4	
� cong�nita	277.4	
� constitucional	277.4	
� neonatal (transitoria)	774.6	"v�ase adem�s Ictericia, feto o reci�n nacido"
�� de prematuridad	774.2	
"Hiperbilirrubin�mica, encefalopat�a, reci�n nacido"	774.7	
� debida a isoinmunizaci�n	773.4	
"Hipercalcemia, hipercalc�mico (idiop�tica)"	275.4	
� nefropat�a	588.8	
Hipercalcinuria	275.4	
Hipercalemia	276.7	
Hipercapnia	786.09	
� con trastorno acidob�sico mixto	276.4	
Hipercarotinemia	278.3	
Hipercementosis	521.5	
"Hipercinesia, hipercin�tico (enfermedad) (reacci�n) (s�ndrome)"	314.9	
� con 		
�� manifestaci�n especificada NCOC	314.8	
�� perturbaci�n simple de la actividad y atenci�n	314.01	
�� retardo del desarrollo	314.1	
�� trastorno de conducta	314.2	
� coraz�n (enfermedad)	429.82	
� de la infancia o adolescencia NCOC	314.9	
Hipercloremia	276.9	
Hiperclorhidria	536.8	
� neur�tica	306.4	
� psic�gena	306.4	
"Hipercoagulaci�n, s�ndrome de"	289.8	
Hipercolesterinemia 		v�ase Hipercolesterolemia
Hipercolesterolemia	272.0	
"� con hipergliceridemia, end�gena"	272.2
� esencial	272.0
� familiar	272.0
� hereditaria	272.0
� primaria	272.0
� pura	272.0
Hipercolesterolosis	272.0
Hipercorticosteronismo	
� sobredosis o ingesti�n o administraci�n de sustancia incorrecta	962.0
� sustancia correcta administrada de forma correcta	255.3
Hipercortisonismo 	
� sobredosis o ingesti�n o administraci�n de sustancia incorrecta	962.0
� sustancia correcta administrada de forma correcta	255.3
"Hiperdin�mico, estado o s�ndrome betaadren�rgico (circulatorio)"	429.82
Hiperelectrolitemia	276.9
Hiperemesis	536.2
� grav�dica (leve) (antes de completarse 22 meses de gestaci�n)	643.0	
�� con 		
��� depleci�n de carbohidratos	643.1	
��� desequilibrio de electrolitos	643.1	
��� deshidrataci�n	643.1	
��� perturbaci�n metab�lica	643.1	
�� grave (con perturbaci�n metab�lica)	643.1	
�� que afecta al feto o al reci�n nacido	761.8	
� psic�gena	306.4	
� que surge durante el embarazo 		"v�ase Hiperemesis, grav�dica"
Hiperemia (aguda)	780.9	
� bazo	289.59	
� cerebral	437.8	
� conjuntiva	372.71	
� ent�rica	564.8	
� est�mago	537.89	
� h�gado (activa) (pasiva)	573.8
� intestino	564.8
� iris	364.41
� laberinto	386.30
� mucosa anal	569.49
"� o�do, interno, aguda"	386.30
� ojo	372.71
� ovario	620.8
� p�rpado (activa) (pasiva)	374.82
� pasiva	780.9
� pulm�n	514
� pulmonar	514
� renal	593.81
� retina	362.89
� ri��n	593.81
� vejiga	596.7
Hiperestesia (superficie del cuerpo)	782.0	"v�ase adem�s Perturbaci�n, sensaci�n"
� faringe (reflejo)	478.29	
� laringe (reflejo)	478.79	
�� hist�rica	300.11	
Hiperestrinismo	256.0	
Hiperestrogenismo	256.0	
Hiperestrogenosis	256.0	
"Hiperextensi�n, articulaci�n"	718.80	
� cadera	718.85	
� codo	718.82	
� hombro (regi�n)	718.81	
� mano	718.84	
� m�ltiples sitios	718.89	
� mu�eca	718.83	
� pie	718.87	
� regi�n pelviana	718.85	
� rodilla	718.86	
� sitio especificado NCOC	718.88	
� tobillo	718.87	
Hiperfagia	783.6	
Hiperfenilalaninemia	270.1	
Hiperfibrin�lisis 		v�ase Fibrin�lisis
Hiperfoliculinismo	256.0	
Hiperforia	378.40	
� alternante	378.45	
Hiperfosfatemia	275.3	
Hiperfructosemia	271.2	
Hiperfunci�n 		
� corticoadrenal NCOC	255.3	
� laberinto 		"v�ase Hiperactivo, laberinto "
� meduloadrenal	255.6	
� ovario	256.1	
�� estr�geno	256.0
� p�ncreas	577.8
� paratiroidea (gl�ndula)	252.0
� pituitaria (anterior) (gl�ndula) (I�bulo)	253.1
� suprarrenal (corteza)	255.3
"�� andr�gena, benigna adquirida"	255.3
�� medula	255.6
�� virilismo	255.2
� testicular	257.0
Hipergammaglobulinemia	289.8
"� monoclonal, benigna (HMB)"	273.1
� policlonal	273.0
"� Waldenstr�m, de"	273.0
Hipergliceridemia	272.1
� end�gena	272.1
� esencial	272.1
� familiar	272.1	
� hereditaria	272.1	
� mixta	272.3	
� pura	272.1	
Hiperglicinemia	270.7	
Hiperglobulinemia	273.8	
Hiperglucemia	790.6	
� materna 		
�� diabetes manifiesta en infante	775.1	
�� que afecta al feto o al reci�n nacido	775.0	
� pospancreatectom�a (completa) (parcial)	251.3	
Hipergonadismo 		
� ov�rico	256.1	
� testicular (infantil) (primario)	257.0	
Hiperheparinemia	286.5	"v�ase adem�s Circulante, anticoagulantes"
Hiperhidrosis	780.8	
� psic�gena	306.3	
Hiperhistidinemia	270.5	
Hiperinsulinismo (ect�pico) (funcional) (org�nico) NCOC	251.1	
� espont�neo	251.2	
� infortunio terap�utico (por administraci�n de insulina)	962.3	
� reactivo	251.2	
� yatr�geno	251.0	
"Hiperirritabilidad (cerebral), en el reci�n nacido"	779.1	
Hiperlagrimaci�n	375.20	v�ase adem�s Epifora
Hiperlipemia	272.4	v�ase adem�s Hiperlipidemia
Hiperlipidemia	272.4	
� combinada	272.4	
� end�gena	272.1	
� ex�gena	272.3	
� grupo 		
�� A	272.0	
�� B	272.1
�� C	272.2
�� D	272.3
� inducida por 	
�� carbohidratos	272.1
�� grasas	272.3
� mixta	272.2
� tipo especificado NCOC	272.4
Hiperlipidosis	272.7
� hereditaria	272.7
Hiperlipoproteinemia (adquirida) (esencial) (familiar) (hereditaria) (primaria) (secundaria)	272.4
� tipo Fredrickson 	
�� I	272.3
�� IIa	272.0
�� IIb	272.2
�� III	272.2
�� IV	272.1
�� V	272.3
� tipo lipoide 	
�� de baja densidad (LBD)	272.0
�� de muy baja densidad (LMBD)	272.1
Hiperlisinemia	270.7
Hiperluteinizaci�n	256.1
Hipermagnesemia	275.2
� neonatal	775.5
Hipermaturidad (feto o reci�n nacido)	766.2
Hipermenorrea	626.2
Hipermetabolismo	794.7
Hipermetioninemia	270.4
Hiperm�trope	367.0
Hipermetrop�a (cong�nita)	367.0
Hipermotilidad 	
� est�mago	536.8
� gastrointestinal	536.8
� intestino	564.1
�� psic�gena	306.4
Hipermovilidad 	
� articulaci�n (adquirida)	718.80
�� cadera	718.85
�� codo	718.82
�� hombro (regi�n)	718.81
�� mano	718.84
�� m�ltiples sitios	718.89
�� mu�eca	718.83
�� pie	718.87
�� regi�n pelviana	718.85
�� rodilla	718.86
�� sitio especificado NCOC	718.88
�� tobillo	718.87
� ciego	564.1
� c�ccix	724.71
� colon	564.1
�� psic�gena	306.4
� esc�pula	718.81
� est�mago	536.8
�� psic�gena	306.4
� �leon	564.8
� menisco (rodilla)	717.5
"� ri��n, cong�nita"	753.3
� s�ndrome	728.5
"� test�culo, cong�nita"	752.5
Hipernasalismo	784.49
Hipernatremia	276.0
� con depleci�n de agua	276.0
Hipernefroma 	189.0 (M8312/3)	
Hiperop�a	367.0	
Hiperorexia	783.6	
Hiperornitinemia	270.6	
Hiperosmia 	781.1	"v�ase adem�s Perturbaci�n, sensaci�n"
Hiperosmolaridad	276.0	
Hiperosteog�nesis	733.99	
Hiperostosis	733.99	
� calv�rica	733.3	
� cortical	733.3	
�� infantil	756.59	
� cr�neo	733.3	
�� cong�nita	756.0	
� frontal interna del cr�neo	733.3	
� interna frontal	733.3	
� monom�lica	733.99	
"� vertebral, anquilosante"	721.6	
Hiperovarianismo	256.1	
"Hiperovarismo, hiperovaria"	256.1	
Hiperoxaluria (primaria)	271.8	
Hiperoxia	987.8	
Hiperparatiroidismo	252.0	
� ect�pico	259.3	
"� secundaria, de origen renal"	588.8	
Hiperpat�a	782.0	"v�ase adem�s Perturbaci�n, sensaci�n"
� psic�gena	307.80	
Hiperperistaltismo	787.4	
� psic�geno	306.4	
Hiperpermeabilidad capilar	448.9	
Hiperpiesia	401.9	v�ase adem�s Hipertensi�n
Hiperpiesis	401.9	v�ase adem�s Hipertensi�n
Hiperpigmentaci�n		v�ase Pigmentaci�n
Hiperpinealismo	259.8	
Hiperpipecolatemia	270.7	
Hiperpirexia	780.6	
� calor (efectos de)	992.0	
"� maligna, debida a anestesia"	995.89	
� origen desconocido	780.6	v�ase adem�s Pirexia
� pal�dica	084.6	v�ase adem�s Paludismo
� reum�tica 		"v�ase Fiebre, reum�tica"
Hiperpituitarismo	253.1	
"Hiperplasia, hiperpl�sico "		
� adenoides (tejido linfoideo)	474.12	
�� y am�gdalas	474.10	
� adrenal 		"v�ase Hiperplasia, suprarrenal "
� am�gdala (tejido linfoide)	474.11	
�� y adenoides	474.10	
� ap�ndice (linfoidea)	543.0	
"� arteria, fibromuscular NCOC"	447.8
�� car�tida	447.8
�� renal	447.3
"� car�tida, arteria"	447.8
� c�lulas 	
�� alfa (p�ncreas) 	
��� con exceso de 	
���� gastrina	251.5
���� glucag�n	251.4
�� islotes (p�ncreas)	251.9
��� alfa 	
���� con exceso de 	
����� gastrina	251.5
����� glucag�n	251.4
��� beta	251.1
�� p�ncreas (islotes)	251.9
"��� alfa, con exceso de "	
���� gastrina	251.5
���� glucag�n	251.4
��� beta	251.5
"� cementaci�n, cemento (diente(s))"	521.5
"� cl�toris, cong�nita"	752.49
� cuello uterino	622.1
�� c�lulas basales	622.1
�� cong�nita	752.49
�� endometrio	622.1
�� polipoide	622.1
� dentina	521.5
� enc�a	523.8
� endocervicitis	616.0
"� endometrio, endom�trica (adenomatosa) (at�pica) (glandular) (polipoide) (qu�stica) (�tero)"	621.3
�� cuello uterino	622.1
� epitelial	709.8
�� boca (focal)	528.7
"�� focal, oral, incluso la lengua"	528.7
�� lengua (focal)	528.7
�� pared vaginal	623.0
�� pez�n	611.8
�� piel	709.8
� eritroide	289.9
� faringe (linfoide)	478.29
� fascial osificante (progresiva)	728.11
"� fibromuscular, arteria NCOC"	447.8
�� car�tida	447.8
�� renal	447.3
� genital 	
�� femenina	629.8
�� masculina	608.89
� gingiva	523.8
� gl�ndula cervical	785.6
� gl�ndula salival (cualquiera)	527.1
� glandular 	
�� endometrio (�tero)	621.3
�� intersticial del �tero	621.3
�� qu�stica del �tero	621.3
� granuloc�tica	288.8
� h�gado (cong�nita)	751.69
"� himen, cong�nita"	752.49
� hueso	733.99
�� medula	289.9
� islotes de Langerhans	251.1
� linfoide (difusa) (nodular)	785.6
�� ap�ndice	543.0
�� intestino	569.89
� mama	611.1	"v�ase adem�s Hipertrofia, mama"
� mandibular	524.0	
�� condilar unilateral	526.89	
"� Marchand, de, m�ltiple nodular (h�gado)   "		"v�ase Cirrosis, posnecr�tica "
� maxilar	524.0	
"� medula, suprarrenal"	255.8	
"� miometrio, miom�trica"	621.2	
� nariz (linfoide) (polipoide)	478.1	
� n�dulo linf�tico (gl�ndula)	785.6	
"� �rgano o sitio, cong�nita NCOC "		"v�ase Anomal�a, tipo especificado NCOC "
� ovario	620.8	
"� paladar, papilar"	528.9	
� paratiroidea (gl�ndula)	252.0	
� persistente del v�treo (primaria)	743.51	
� pr�stata (adenofibromatosa) (nodular)	600	
"� renal, arteria (fibromuscular)"	447.3	
� reticuloendotelial (c�lulas)	289.9	
� ri��n (cong�nita)	753.3	
� Schimmelbusch	610.1	
� suprarrenal (c�psula) (corteza) (gl�ndula)	255.8	
�� con 		
��� precocidad sexual (masculina)	255.2	
��� virilismo suprarrenal	255.2	
��� virilizaci�n (femenina)	255.2	
�� cong�nita	255.2	
�� debida a exceso de ACTH (ect�pica) (pituitaria)	255.0	
�� medula	255.8	
� tejido blando oral (inflamatoria) (irritante) (mucosa) NCOC	528.9	
�� gingiva	523.8	
�� lengua	529.8	
� timo (gl�ndula) (persistente)	254.0	
� tiroidea	240.9	v�ase adem�s Bocio
�� primaria	242.0	
�� secundaria	242.2	
� uretrovaginal	599.8	
"� �tero, uterina (miometrio)"	621.2	
�� endometrio	621.3	
"� v�treo (humor), primaria persistente"	743.51	
� vulva	624.3	
� yuxtoglomerular (compleja) (ri��n)	593.89	
Hiperpnea	786.01	v�ase adem�s Hiperventilaci�n
Hiperpotasemia	276.7	
Hiperprebetalipoproteinemia	272.1	
� con quilomicronemia	272.3	
� familiar	272.1	
Hiperprolinemia	270.8	
Hiperproteinemia	273.8	
Hiperprotrombinemia	289.8	
Hiperpselafesia	782.0	
Hiperqueratosis	701.1	v�ase adem�s Queratosis   
� cong�nita	757.39	
� c�rnea	371.89	
� cuello uterino	622.1	
� cuerdas vocales	478.5	
� debida a frambesia (precoz) (tard�a) (palmar o plantar)	102.3	
� exc�ntrica	757.39	
� figurada centr�fuga atr�fica	757.39	
� folicular	757.39	
�� penetrante del cutis	701.1	
� lengua	528.7	
� l�mbica (c�rnea)	371.89	
� palmoplantar climat�rica	701.1	
� pinta (carate)	103.1	
� senil (con pruritis)	702	
� universal cong�nita	757.1	
� vagina	623.1	
� vulva	624.0	
Hiperquilia g�strica	536.8	
� psic�gena	306.4	
Hiperquilomicronemia (familiar) (con hiperbetalipoproteinemia)	272.3	
Hiperrafia	782.0	
"Hiperreactor, vascular"	780.2	
Hiperreflexia	796.1	
� m�sculo detrusor	344.61	
"� vejiga, aut�noma"	344.61	
Hipersalivaci�n	527.7	v�ase adem�s Pitalismo
Hipersarcosinemia	270.8	
Hipersecreci�n 		
� ACTH	255.3	
� andr�genos (ov�ricos)	256.1	
� calcitonina	246.0	
� corticoadrenal	255.3	
� cortisol	255.0	
� estr�geno	256.0	
� g�strico	536.8	
�� psic�geno	306.4	
� gastrina	251.5	
� gl�ndula salival (cualquiera)	527.7	
� gl�ndulas lagrimales	375.20	v�ase adem�s Epifora
� glucag�n	251.4	
� hormona 		
�� ACTH	255.3	
�� andr�genos ov�ricos	256.1	
�� crecimiento NCOC	253.0	
�� estimuladora de tiroides	242.8	
�� pituitaria anterior	253.1	
�� testicular	257.0	
� hormonas testiculares	257.0	
� insulina 		v�ase Hiperinsulinismo 
� leche	676.6	
� meduloadrenal	255.6	
"� ov�rica, andr�genos"	256.1	
� pituitaria (anterior)	253.1	
� respiratoria superior	478.9	
� tirocalcitonina	246.0	
Hipersegmentaci�n hereditaria	288.2	
� eosin�filos	288.2	
� n�cleos neutr�filos	288.2	
"Hipersensible, hipersensibilidad "		v�ase adem�s Alergia 
� ADN (�cido desoxirribonucleico) NCOC	287.2	
� angi�tis	446.20	
�� especificada NCOC	446.29	
� colon	564.1	
�� psic�geno	306.4	
� dolor	782.0	"v�ase adem�s Perturbaci�n, sensaci�n"
� droga	995.2	"v�ase adem�s Alergia, droga"
� es�fago	530.8	
� laberinto	386.58	
� neumonitis NCOC	495.9	
� picaduras de insectos 		"v�ase Traumatismo, superficial, por sitio "
� reacci�n	995.3	v�ase adem�s Alergia
�� est�mago (al�rgica) (no al�rgica)	536.8	
��� psic�gena	306.4	
�� tracto respiratorio superior	478.8	
� seno carot�deo	337.0	
Hipersomatropismo (cl�sico)	253.0	
Hipersomnia	780.54	
� con apnea del sue�o	780.53	
� origen no org�nico	307.43	
�� persistente	307.44	
�� transitorio	307.43	
Hipersplenismo	289.4	
Hipersteatosis	706.3	
Hipersuprarrenalismo	255.3	
Hipersusceptibilidad 		v�ase Alergia
"Hipertecosis, ovario"	256.8	
Hipertelorismo	756.0	
"� �rbita, orbital"	376.41	
 Hipertermia (de origen desconocido)	780.6	v�ase adem�s Pirexia
� reci�n nacido	778.4	
Hipertimergasia	296.0	"v�ase adem�s Psicosis, afectiva"
� episodio recurrente	296.1	
� episodio �nico	296.0	
"� reactiva (por tensi�n emocional, trauma psicol�gica)"	298.1	
Hipertimismo	254.8	
Hipertiroideo (recurrente) 		v�ase Hipertiroidismo
Hipertiroidismo (latente) (preadulto) (recurrente) (sin bocio)	242.9	
� con 		
�� bocio (difuso)	242.0	
��� adenomatoso	242.3	
���� multinodular	242.2	
���� uninodular	242.1	
��� nodular	242.3	
���� multinodular	242.2	
���� uninodular	242.1	
�� n�dulo tiroideo	242.1	
"� cuando complica el embarazo, parto o puerperio"	648.1	
�� neonatal (transitorio)	775.3	
Hipertirosinemia	270.2	
Hiperton�a 		v�ase Hipertonicidad
Hipertonicidad 	
� est�mago	536.8
�� psic�gena	306.4
� feto o reci�n nacido	779.8
� gastrointestinal (tracto)	536.8
� infancia	779.8
�� debida a desequilibrio de electrolitos  	779.8
� m�sculo	728.85
"� �tero, uterina (contracciones)"	661.4
�� que afecta al feto o al reci�n nacido	763.7
� vejiga	596.5
Hipertransaminemia	790.4
"Hipertransparente, pulm�n, unilateral  "	492.8
Hipertricosis	704.1
� cong�nita	757.4
� lanuginosa	757.4
�� adquirida	704.1
� p�rpado	374.54
"Hipertrigliceridemia, esencial"	272.1
"Hipertrofia, hipertr�fico "	
� adenoides (infecciosa)	474.12
�� y am�gdalas (faucial) (infecciosa) (lingual) (linfoide)	474.10
� adrenal (gl�ndula)	255.8
� amigdalar (faucial) (infecciosa) (lingual) (linfoidea)	474.11
�� y adenoides	474.10
� apertura normal del diafragma (cong�nita)	756.6
"� apocrina, gl�ndula"	705.82
� ap�fisis o reborde alveolar	525.8
� aritenoidea	478.79
� arteria NCOC	447.8
�� car�tida	447.8
�� cong�nita NCOC	747.6
�� renal	447.3	
� artritis (cr�nica)	715.9	v�ase adem�s Osteoartritis
�� espina dorsal	721.90	v�ase adem�s Espondilosis
� asim�trica (coraz�n)	429.9	
� auricular 		"v�ase Hipertrofia, cardiaca "
� barra media	600	
"� Bartholin, gl�ndula de"	624.8	
� bazo 		v�ase Esplenomegalia 
� boca	528.9	
� cabeza de metatarso	733.99	
� cardiaca (cr�nica) (idiop�tica)	429.3	
�� con 		
��� fiebre reum�tica (estados clasificables bajo 390) 	390	
���� activa	391.8	
����� con corea	392.0	
���� inactiva o quiescente (con corea)	398.99	
�� cong�nita NCOC	746.89	
�� grasosa	429.1	"v�ase adem�s Degeneraci�n, mioc�rdica"
�� hipertensiva	402.90	"v�ase adem�s Hipertensi�n, coraz�n"
�� idiop�tica	425.4	
�� reum�tica (con corea)	398.99	
��� activa o aguda	391.8	
���� con corea	392.0	
�� v�lvula	424.90	v�ase adem�s Endocarditis
��� cong�nita NCOC	746.89	
� cart�lago	733.99	
� cerebro	348.8	
� cicatriz	701.4	
� ciego	569.89	
� cl�toris (cirr�tica)	624.2	
�� cong�nita	752.49	
� colon	569.89	
�� cong�nita	751.3	
� conducto biliar	576.8	
� conducto deferente	608.89	
� conducto o gl�ndula salival	527.1	
�� cong�nita	750.26	
"� conjuntiva, linfoidea"	372.73	
� coraz�n (idiop�tica) 		"v�ase adem�s Hipertrofia, cardiaca "
�� v�lvula 		v�ase adem�s Endocarditis 
��� cong�nita NCOC	746.89	
� cord�n esperm�tico	608.89	
� c�rnea	371.89	
� cuello uterino	622.6	
�� alargamiento	622.6	
�� cong�nita	752.49	
� cuerdas vocales	478.5	
� cuerpos cavernosos	607.89	
� dedo de pie (cong�nita)	755.65	
�� adquirida	735.8	
� duodeno	537.89	
� enc�a (membrana mucosa)	523.8	
� endometrio (�tero)	621.3	
�� cuello uterino	622.6	
� epid�dimo	608.89	
� escafoides (tarsiana)	733.99	
� escroto	608.89	
� esf�nter rectal	569.49	
"� esof�gica, del hiato"	756.6	
� espondilitis (espina dorsal)	721.90	v�ase adem�s Espondilosis
� estado patol�gico de la piel NCOC	701.9	
� estenosis suba�rtica (idiop�tica)	425.1	
� est�mago	537.89	
� falo	607.89	
�� femenino (cl�toris)	624.2
� faringe	478.29
�� linfoidea (infecciosa) (tejido) pared  	478.29
"� far�ngea, am�gdala"	474.12
� faringitis	472.1
� frenillo (lengua)	529.8
�� labio	528.5
� gingiva	523.8
"� gl�ndula, glandular (general) NCOC"	785.6
"� grasa, masa de"	729.30
"�� infrapatelar, infrarrotuliana"	729.31
�� orbital	374.34
�� popl�tea	729.31
"�� prepatelar, prerrotuliana"	729.31
"�� retropatelar, retrorrotuliana"	729.31
�� rodilla	729.31
�� sitio especificado NCOC	729.39	
� hemifacial	754.0	
� hep�tica 		"v�ase Hipertrofia, h�gado "
� hiato esof�gico (cong�nita)	756.6	
�� con hernia 		"v�ase Hernia, diafragma "
� h�gado	789.1	
�� aguda	573.8	
�� cirr�tica 		"v�ase Cirrosis, h�gado "
�� cong�nita	751.69	
�� grasosa 		"v�ase Grasoso, h�gado "
"� hilo, gl�ndula"	785.6	
"� himen, cong�nita"	752.49	
� hipod�rmica seudoedematosa	757.0	
"� hoz, cr�neo"	733.99	
� hueso	733.99	
� �leon	569.89	
"� infrapatelar, infrarrotuliana, masa de grasa"	729.31
� intestino	569.89
"� labial, frenillo"	528.5
� labio 	
�� de la boca (frenillo)	528.5
��� cong�nita	744.81
�� vulvar (mayor) (menor)	624.3
"� lagrimal, gl�ndula, cr�nica"	375.03
� lengua	529.8
�� cong�nita	750.15
�� frenillo	529.8
�� papilas (foliaceas)	529.3
"� lengua, frenillo"	529.8
� ligamento	728.9
�� espinal	724.8
� ligamento espinal	728.9
"� linf�tica, gl�ndula"	785.6	
�� tuberculosa 		"v�ase Tuberculosis, gl�ndula linf�tica "
"� lingual, am�gdala (infecciosa)"	474.11	
� mama	611.1	
�� feto o reci�n nacido	778.7	
�� fibroqu�stica	610.1	
�� masiva de la pubertad	611.1	
"�� puerperal, posparto"	676.3	
�� qu�stica	610.1	
�� senil (parenquimatosa)	611.1	
"� mamaria, gl�ndula "		"v�ase Hipertrofia, mama "
"� maxilar, frenillo"	528.5	
"� Meckel, divert�culo de (cong�nita)"	751.0	
� mediastino	519.3	
"� Meibomio, gl�ndula de"	373.2	
� membrana mucosa 		
�� ap�fisis alveolar	523.8	
�� nariz	478.1	
�� turbinado (nasal)	478.0	
� menisco 		
"�� medio, adquirida"	717.3	
"�� rodilla, cong�nita"	755.64	
"� metatarso,"	733.99	
� miocardio	429.3	"v�ase adem�s Hipertrofia, cardiaca"
�� idiop�tica	425.4	
� miometrio	621.2	
� mucosa g�strica	535.2	
� m�sculo	728.9	
� nariz	478.1	"v�ase adem�s Hipertrofia, nasal"
� nasal	478.1	
�� aleta	478.1	
�� cart�lago	478.1	
