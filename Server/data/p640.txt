�� activa o aguda	391.2	
��� con corea (aguda) (reum�tica) (de Sydenham)	392.0	
� miositis	729.1	
� muscular	729.0	
� neumon�a	390.[517.1]	
� neumonitis	390.[517.1]	
� neumopericarditis 		"v�ase Reumatismo, reum�tico, pericarditis "
� neur�lgico	729.0	
� neuritis (aguda) (cr�nica)	729.2	
� neuromuscular	729.0	
� no articular	729.0	
� nudosa 		"v�ase Artritis, nudosa "
� palindr�mico	719.30	
�� cadera	719.35	
�� codo	719.32	
�� hombro (regi�n)	719.31	
�� mano	719.34	
�� m�ltiples sitios	719.39	
�� mu�eca	719.33	
�� pie	719.37	
�� regi�n pelviana	719.35	
�� rodilla	719.36	
�� sitio especificado NCOC	719.38	
�� tobillo	719.37	
"� pancarditis, aguda"	391.8	
�� con corea (aguda) (reum�tica) (de Sydenham)	392.0	
�� cr�nica o inactiva	398.99	
� pericardio 		"v�ase Reumatismo, reum�tico, pericarditis "
� pericardio adherente	393	
� pericarditis (activa) (aguda) (con derrame) (con neumon�a)	391.0	
�� con corea (aguda) (reum�tica) (de Sydenham)	392.0	
�� cr�nica o inactiva	393	
� pleuropericarditis 		"v�ase Reumatismo, reum�tico, pericarditis "
� poliarticular NCOC	716.9	v�ase adem�s Artritis
� poliartritis 		
�� aguda o subaguda 		"v�ase Fiebre, reum�tica "
�� cr�nica	714.0	
� psic�geno	306.0	
� radiculitis	729.2	
� s�ptico 		"v�ase Fiebre, reum�tica "
� subaguda NCOC	729.0	
� tort�colis	723.5	
� tuberculoso NCOC	015.9	v�ase adem�s Tuberculosis
Reumatoide 		v�ase adem�s enfermedad espec�fica 
� pulmones	714.81	
Revent�n de suturas o puntos (complicaci�n de cirug�a)	998.3	
"Reye, s�ndrome de"	331.81	
"Reye-Sheehan, s�ndrome de (necrosis pituitaria posparto)"	253.2	
Rh (factor) 		
� enfermedad hemol�tica	773.0	
"� incompatibilidad, inmunizaci�n o sensibilizaci�n "		
�� cuando afecta la atenci�n del embarazo  	656.1	
�� feto o reci�n nacido	773.0	
�� reacci�n a transfusi�n	999.7	
"� madre negativa, que afecta al feto o al reci�n nacido"	773.0	
� reacci�n a transfusi�n	999.7	
� t�tulo elevado	999.7	
"Rhesus (factor) (Rh), incompatibilidad "		"v�ase Rh, incompatibilidad"
"Rhoads y Bomford, anemia (refractaria) de"	284.9	
Rhus 		
"� diversiloba, dermatitis por"	692.6	
� radicans. dermatitis por	692.6	
"� toxicodendron, dermatitis por"	692.6	
"� venenata, dermatitis por"	692.6	
"� verniciflua, dermatitis por"	692.6	
"Riboflavina, carencia de"	266.0	
"Richter, hernia de "		"v�ase Hernia, de Richter"
Ricinismo	988.2	
"Rickettsia, enfermedad"	083.9	
� tipo especificado NCOC	083.8	
Rickettsiosis NCOC	083.9	
� pustulosa	083.2	
� tipo especificado NCOC	083.8	
� transmitida por garrapatas	082.9	
�� tipo especificado NCOC	082.8	
� vesicular	083.2	
"Ricord, chancro de"	091.0	
"Riddoch, s�ndrome de (desorientaci�n visual)"	368.16	
"Riedel, de "		
� enfermedad (tiroiditis le�osa)	245.3	
� estruma (tiroiditis le�osa)	245.3	
"� I�bulo, h�gado"	751.69	
� tiroiditis (le�osa)	245.3	
"Rieger, anomal�a o s�ndrome de (disgenesia mesod�rmica, segmento ocular anterior)"	743.44	
"Riehl, melanosis de"	709.0	
Riesgo de suicidio	300.9	
"Rietti-Greppi-Micheli, anemia o s�ndrome de"	282.4	
"Rieux, hernia de "		"v�ase Hernia, de Rieux"
"Riga, enfermedad de (aftas caqu�cticas)"	529.0	
"Riga-Fede, enfermedad de (aftas caqu�cticas)"	529.0	
"Riggs, enfermedad de (periodontitis compuesta)"	523.4	
"Rigidez, r�gido "		v�ase adem�s enfermedad espec�fica 
� abdominal	789.4	
"� articular, m�ltiple, cong�nita"	754.89	
� cuello uterino 		
�� en el embarazo o parto	654.6	
��� cuando obstruye el trabajo del parto	660.2
���� que afecta al feto o al reci�n nacido	763.1
��� que afecta al feto o al reci�n nacido	763.8
� espalda	724.8
� espina dorsal	724.8
� himen (adquirida) (cong�nita)	623.3
� nuca	781.6
� perineo o vulva 	
�� en el embarazo o parto	654.8
��� cuando obstruye el trabajo del parto	660.2
���� que afecta al feto o al reci�n nacido	763.1
��� que afecta al feto o al reci�n nacido	763.8
� suelo de la pelvis 	
�� en el embarazo o parto	654.4
��� cuando obstruye el trabajo del parto	660.2
���� que afecta al feto o al reci�n nacido	763.1
��� que afecta al feto o al reci�n nacido	763.8	
� vagina 		
�� en el embarazo o parto	654.7	
�� cuando obstruye el trabajo del parto	660.2	
���� que afecta al feto o al reci�n nacido	763.1	
��� que afecta al feto o al reci�n nacido	763.8	
R�gido 		
� cuello	723.5	v�ase adem�s Tort�colis
� espalda	724.8	
"Riley-Day, s�ndrome de (disautonom�a familiar)"	742.8	
Rinitis (atr�fica) (catarral) (cr�nica) (cruposa) (fibrinosa) (hiperpl�sica) (hipertr�fica) (membranosa) (purulenta) (supurativa) (ulcerativa)	472.0	
� con 		
�� dolor de garganta 		v�ase Nasofaringitis 
�� fiebre de heno	477.9	"v�ase adem�s Fiebre, heno"
��� con asma (bronquial)	493.0	
� aguda	460	
� al�rgica (estacional) (no estacional)	477.9	"v�ase adem�s Fiebre, heno"
�� con asma	493.0	v�ase adem�s Asma
� granulomatosa	472.0	
� infecciosa	460	
� neumoc�cica	460	
� obstructiva	472.0	
� sifil�tica	095.8	
�� cong�nita	090.0	
� tuberculosa	012.8	v�ase adem�s Tuberculosis   
� vasomotora	477.9	"v�ase adem�s Fiebre, heno"
Rinoantritis (cr�nica)	473.0	
� aguda	461.0	
Rinodacriolito	375.57	
Rinofaringitis (aguda) (subaguda)	460	v�ase adem�s Nasofaringitis
� cr�nica	472.2	
� destructiva ulcerosa	102.5	
� mutilante	102.5	
Rinofima	695.3	
Rinolalia (abierta) (cerrada)	784.49	
Rinolito	478.1	
� seno nasal	473.9	v�ase adem�s Sinusitis
Rinomegalia	478.1	
Rinorrea	478.1	
� cerebrospinal (I�quido)	349.81	
� espasm�dica	477.9	"v�ase adem�s Fiebre, heno   "
� paroxismal	477.9	"v�ase adem�s Fiebre, heno   "
Rinosalpingitis	381.50	
� aguda	381.51	
� cr�nica	381.52	
Rinoscleroma	040.1	
Rinosporidiosis	117.0	
"Rinovirus, infecci�n"	079.3	
"R�o Ross, fiebre de"	066.3
"R�os, ceguera de los"	125.3[36013]
Ritidosis facial	701.8
"R�tmicas, descargas motoras idiop�ticas   "	345.5
Ritmo 	
� anormal del coraz�n	427.9
"� atrioventricular, nodal"	427.89
� escape	427.89
� idioventricular	426.89
�� acelerado	427.89
� nodal	427.89
"� sue�o, inversi�n"	780.55
�� origen no org�nico	307.45
� trastorno	427.9
�� ect�pico	427.89
�� nodal	427.89
�� seno coronario	427.89	
"Ritter, enfermedad de (dermatitis exfoliativa    de los reci�n nacidos)"	695.81	
Rivalidad entre hermanos	313.3	
"Rivalta, enfermedad de (actinomicosis cervicofacial)"	039.3	
"Rizado, cord�n umbilical "		"v�ase Complicaciones, cord�n umbilical"
"Rizado, es�fago"	530.5	
Rizomelique seudopoliartr�tico	446.5	
Ri��n 		v�ase enfermedad espec�fica
"Robert, pelvis de"	755.69	
� con desproporci�n (fetopelviana)	653.0	
�� cuando obstruye el trabajo del parto	660.1	
��� que afecta al feto o al reci�n nacido   	763.1	
�� que afecta al feto o al reci�n nacido	763.1	
"Robin, s�ndrome de"	756.0	
"Robinson, displasia ectod�rmica (hidr�tica) de"	757.31	
"Robles, enfermedad de (oncocerciasis)"	125.3[360.13]	
Robo de 		
� arteria 		
�� subclavia	435.2	
�� vertebral	435.1	
Robo		
"solitario, problema infantil"	312.1	"v�ase adem�s Perturbaci�n, conducta"
"Rodens, ulcus (�lcera corrosiva) "	(M8090/3)	"v�ase adem�s Neoplasia, piel maligna "
� c�rnea	370.07	
Rodilla 		v�ase enfermedad espec�fica
Roetheln	056.9	
"Roger, enfermedad de (defecto septal interventricular cong�nito)"	745.4	
"Roger, enfermedad de"	754.4	
"Rokitansky, de "		
� enfermedad de	570	"v�ase adem�s Necrosis, h�gado"
� tumor	620.2	
"Rokitansky-Aschoff, senos de (evaginaci�n mucosa de la ves�cula biliar)"	575.8	
"Rokitansky-Kuster-Hauser, s�ndrome de (ausencia cong�nita de la vagina)"	752.49	
"Rollet, chancro de (sifil�tico)"	091.0	
"Romano-Ward, s�ndrome de (intervalo Q-T prolongado)"	794.31	
"Romanus, lesi�n de"	720.1	
"Romberg, enfermedad o s�ndrome de"	349.89	
"Rompehuesos, fiebre "	061	
Roncha (intr�pida)	708.9	v�ase adem�s Urticaria
Ronquera	784.49	
Ronquidos	786.09	
"Ropa de cama, asfixia o ahogo por"	994.7	
"Rosa de madera de Hawaii, dependencia de"	304.5	
Rosa(s) 		
� fiebre	477.0	
� resfriado	477.0	
� sarpullido	782.1	
�� epid�mico	056.9	
�� ni�os	057.8
Ros�cea	695.3
� acn�	695.3
� queratitis	695.3[370.49]
Rosado 	
� enfermedad	985.0
� soplador	492.8
"Rosario, raqu�tico"	268.0
"Rosen-Castleman-Liebow, s�ndrome de (proteinosis pulmonar)"	516.0
"Rosenbach, erisipelato�de o erisipeloide de"	027.1
"Rosenthal, enfermedad de (deficiencia de factor XI)"	286.2
Roseola	056.9
� complicada	056.8
� cong�nita	771.0
� ni�os	057.8
"Rossbach, enfermedad de (hiperclorhidria)"	536.8
� psic�gena 	306.4	
"R�ssle-Urbach-Wiethe, lipoproteinosis de"	272.8	
"Rostan, asma de (cardiaca)"	428.1	"v�ase adem�s Fallo, ventricular, izquierdo"
"Rot-Bernhard, enfermedad de"	355.1	
Rotaci�n 		
"� an�mala, incompleta o insuficiente "		v�ase  Mala rotaci�n 
� ciego (cong�nita)	751.4	
� colon (cong�nita)	751.4	
� diente(s)	524.3	
"� espina dorsal, incompleta o insuficiente  "	737.8	
"� manual, que afecta al feto o al reci�n nacido"	763.8	
"� v�rtebra, incompleta o insuficiente"	737.8	
R�teln	056.9	
"Roth, enfermedad o meralgia de"	355.1	
"Roth-Bernhardt, enfermedad o s�ndrome de"	355.1	
"Rothmund (-Thomson), s�ndrome de"	757.33	
"Rotor, enfermedad o s�ndrome de (hiperbilirubinemia idiop�tica)"	277.4	
"Rotundum, ulcus "		"v�ase Ulcera, est�mago"
Rotura 		
� cardiorrenal 		"v�ase Hipertensi�n, cardiorrenal "
� retina	361.30	"v�ase adem�s Defecto, retina"
Rotura 		
� Arco plantar	734	
�� cong�nita	755.67	
� compensaci�n 		"v�ase Enfermedad, coraz�n "
� cuello 		"v�ase Fractura, v�rtebra, cervical "
"� diente, dientes"	873.63	
�� complicada	873.73	
� espalda 		"v�ase Fractura, v�rtebra, por sitio "
� hueso 		"v�ase Fractura, por sitio "
� implante o dispositivo interno 		"v�ase listado bajo Complicaciones, mec�nicas "
� nariz	802.0	
�� abierta	802.1	
Rotura		v�ase adem�s Ruptura 
� anastomosis gastrointestinal	997.4	
"� cambio de fases, ciclo sue�o-vigilia de 24 horas"	780.55	
�� origen no org�nico	307.45	
� ciclo sue�o-vigilia (24 horas)	780.55	
�� origen no org�nico	307.45	
� familia	V61.0	
� herida 		
"�� ces�rea, operaci�n de"	674.1	
�� episiotom�a	674.2	
�� operaci�n	998.3	
��� ces�rea	674.1	
�� perineal (obst�trica)	674.2	
�� uterina	674.1	
� herida de ces�rea	674.1	
� herida operatoria	998.3	
"� huesecillos, cadena osicular"	385.23	
�� traum�tica 		"v�ase Fractura, cr�neo, base "
� ligamento(s) 		v�ase adem�s Esguince 
�� rodilla 		
��� antigua	717.89	
���� capsular	717.85	
���� colateral (mediano)	717.82	
����� lateral	717.81	
���� cruzado (posterior)	717.84	
����� anterior	717.83	
���� sitio especificado NCOC	717.85	
��� lesi�n actual 		"v�ase adem�s Dislocaci�n, rodilla "
� I�nea de suturas (externa)	998.3	
�� interna	998.3	
� marital	V61.1	
�� que implica divorcio o separaci�n	V61.0	
"� �rgano trasplantado, sitio anastomosis "		"v�ase Complicaciones, trasplante, �rgano, por sitio "
� par�nquima 		
�� bazo 		"v�ase Laceraci�n, bazo, par�nquima, masiva "
�� h�gado (hep�tica) 		"v�ase Laceraci�n, h�gado, importante"
"Roussy-L�vy, s�ndrome de"	334.3	
"Roy (-Jutras), s�ndrome de (acropaquidermia)"	757.39	
Rozadura	709.8	
Rub�ola	056.9	
� complicaci�n	056.8	
�� neurol�gica	056.00	
��� encefalomielitis	056.01	
��� tipo especificado NCOC	056.09	
�� tipo especificado NCOC	056.79	
� complicaciones especificadas NCOC	056.79	
� cong�nita	771.0	
� contacto	V01.4	
"� cuando complica el embarazo, parto o puerperio"	647.5	
� exposici�n a	V01.4	
� materna 		
�� con sospecha de da�os fetales que afectan la atenci�n del embarazo	655.3	
�� que afecta al feto o al reci�n nacido	760.2	
��� rub�ola manifiesta en el ni�o	771.0	
� vacunaci�n profil�ctica (contra)	V04.3	
Rubeosis del iris	364.42	
� diab�tica	250.5[364.42]	
"Rubinstein-Taybi, s�ndrome de (braquidactilia, estatura baja y retraso mental)"	759.89	
"Rud, s�ndrome de (deficiencia mental, epilepsia e infantilismo)"	759.89	
Rudimentario (cong�nito) 		v�ase adem�s Ag�nesis 
� brazo	755.22	
� bronquio traqueal	748.3	
� cuello uterino	752.49	
� cuerno uterino	752.3	
� hueso	756.9	
� I�bulo de la oreja	744.21	
� ojo	743.10	v�ase adem�s Microftalmos
� �rganos respiratorios en toracofago	759.4	
� pierna	755.32	
� r�tula	755.64	
� trompa de Falopio	752.19	
� �tero	752.3	
�� en el var�n	752.7	
�� s�lido o con cavidad	752.3	
� vagina	752.49	
Ruido	785.9	
� arterial (abdominal) (carot�deo)	785.9	
� supraclavicular	785.9	
"Ruiter-Pompen (-Wyers), s�ndrome de (angioqueratoma corp�reo difuso)"	272.7	
Rumiaci�n	787.0	
� neur�tica	300.3	
� obsesiva	300.3	
� psic�gena	307.53	
"Runeberg, enfermedad de (anemia perniciosa progresiva)"	281.0	
"Runge, s�ndrome de (posmaturidad)"	766.2	
Rupia	091.3	
� cong�nita	090.0	
� terciaria	095.9	
"Ruptura, roto"	553.9	
� absceso (espont�neo) 		"v�ase Absceso, por sitio "
� am�gdala	474.8	
� amnios 		"v�ase Ruptura, membranas "
� aneurisma 		v�ase adem�s Aneurisma 
� ano (esf�nter) 		"v�ase Laceraci�n, ano "
"� aorta, a�rtica"	441.5	
�� abdominal	441.3	
�� ascendente	441.1	
�� cayado	441.1	
�� descendente	441.5	
��� abdominal	441.3	
��� tor�cica	441.1	
�� sifil�tica	093.0	
"�� t�rax, tor�cica"	441.1	
�� transversal	441.1	
�� traum�tica (tor�cica)	901.0	
��� abdominal	902.0	
�� v�lvula o c�spide	424.1	"v�ase adem�s Endocarditis, a�rtica"
� aparato lagrimal (traum�tica)	870.2	
� ap�ndice (con peritonitis)	540.0	
�� traum�tica 		"v�ase Traumatismo, interno, tracto gastrointestinal "
� arteria	447.2	
�� cerebro	431	"v�ase adem�s Hemorragia, cerebro"
�� coraz�n	410.9	"v�ase adem�s Infarto, miocardio"
�� coronaria	410.9	"v�ase adem�s Infarto, miocardio"
�� men�ngea	430	"v�ase adem�s Hemorragia, subaracnoidea"
��� efecto tard�o 	438	v�ase categor�a 438
�� pulmonar	417.8	
�� traum�tica (complicaci�n)	904.9	"v�ase adem�s Traumatismo, vaso sangu�neo, por sitio"
� bazo	289.59	
�� cong�nita	767.8	
�� debida a traumatismo al nacer	767.8	
�� espont�nea	289.59	
�� no traum�tica	289.59	
�� pal�dica 	084.9	
�� traum�tica	865.04	
��� con herida penetrante de la cavidad   	865.14	
"� Bowman, membrana de"	371.31	
� capilares	448.9	
� c�psula de articulaci�n 		"v�ase Esguince, por sitio "
� cardiaca	410.9	"v�ase adem�s Infarto, miocardio"
� cart�lago (articular) (actual) 		"v�ase adem�s Esguince, por sitio "
�� rodilla		"v�ase Desgarre, menisco "
�� semilunar		"v�ase Desgarre, menisco "
"� cerebral, aneurisma (cong�nito)"	430	"v�ase adem�s Hemorragia, subaracnoidea"
�� efecto tard�o 	438	v�ase categor�a 438
� cerebro 		
�� aneurisma (cong�nito)	430	"v�ase adem�s Hemorragia, subaracnoidea"
��� efecto tard�o 	438	v�ase categor�a 438
��� sifil�tica	094.87	
�� hemorr�gica	431	"v�ase adem�s Hemorragia, cerebro"
�� sifil�tica	094.89	
�� traumatismo al nacer	767.0	
ciego (con peritonitis)	540.0	
�� traum�tica	863.89	
��� con herida penetrante de la cavidad   	863.99	
� c�rculo de Willis	430	"v�ase adem�s Hemorragia, subaracnoidea"
�� efecto tard�o 	438	v�ase categor�a
"� c�stico, conducto"	575.4	"v�ase adem�s Enfermedad, ves�cula biliar"
� colon	569.89	
�� traum�tica 		"v�ase Traumatismo, interno, colon "
"� conducto biliar, salvo el c�stico"	576.3	"v�ase adem�s Enfermedad, biliar"
�� c�stico	575.4	
�� traum�tica 		"v�ase Traumatismo, interno, intraabdominal "
� conducto tor�cico	457.8	
� coraz�n (aur�cula) (ventr�culo)	410.9	"v�ase adem�s Infarto, miocardio"
�� infecciosa	422.90	
�� traum�tica 		"v�ase Ruptura, miocardio, traum�tica "
� cord�n umbilical	663.8	
�� feto o reci�n nacido	772.0	
� c�rnea (traum�tica) 		"v�ase adem�s Ruptura, ojo "
�� debida a �lcera	370.00	
� coroides (directa) (indirecta) (traum�tica)	363.63	
� coronaria (arteria) (tromb�tica)	410.9	"v�ase adem�s Infarto, miocardio"
� cristalino (traum�tica)	366.20	
� cuando significa hernia		v�ase Hernia 
� cuello uterino 		
�� con 		
��� aborto 		"v�ase Aborto, por tipo, con da�o a �rganos pelvianos "
��� embarazo ect�pico	639.2	v�ase adem�s categor�as 633.0-633.9
��� embarazo molar	639.2	v�ase adem�s categor�a 630-632
�� despu�s de 		
��� aborto	639.2	
��� embarazo ect�pico o molar	639.2	
�� traum�tica 		"v�ase Traumatismo, interno, cuello uterino "
�� traumatismo obst�trico	665.3	
� cuerda tendinosa del coraz�n	429.5	
� cuerpo l�teo (infectado) (ovario)	620.1	
"� Descemet, membrana de"	371.33	
�� traum�tica 		"v�ase Ruptura, ojo "
� diafragma 		"v�ase adem�s Hernia, diafragma traum�tica v�ase Traumatismo, interno, diafragma "
� disco intervertebral 		"v�ase Desplazamiento, disco intervertebral "
�� traum�tica (actual) 		"v�ase Luxaci�n, v�rtebra "
� divert�culo 		
�� intestino (grueso)	562.10	v�ase adem�s Divert�culos
��� delgado	562.00	
�� vejiga	596.3	
� duodeno (�lcera) 		"v�ase Ulcera, duodeno, con perforaci�n "
� escler�tica	871.0	
� es�fago	530.4	
�� traum�tica	862.22	
��� con herida penetrante de la cavidad	862.32	
��� regi�n cervical 		"v�ase Herida, abierta, es�fago "
� est�mago	537.89	
�� debida a traumatismo al nacer	767.8	
�� traum�tica 		"v�ase Traumatismo, interno, est�mago "
�� �lcera 		"v�ase Ulcera, est�mago, con perforaci�n "
� faringe (no traum�tica) (espont�nea)	478.29	
"� f�stula arteriovenosa, cerebro (cong�nita)  "	430	
� fol�culo de Graaf (hematoma)	620.0	
� fontanela	767.3	
� g�strica	537.89	"v�ase adem�s Ruptura, est�mago  "
�� vaso	459.0	
� globo (ojo) (traum�tico) 		"v�ase Ruptura, ojo "
� herida operatoria	998.3	
� h�gado (traum�tica)	864.04	
�� con herida penetrante de la cavidad	864.14	
�� debida a traumatismo al nacer	767.8	
�� no traum�tica	573.8	
� himen	623.8	
� hueso 		"v�ase Fractura, por sitio "
� interna 		
�� cart�lago semilunar 		"v�ase Desgarre, menisco "
"�� �rgano, traum�tica "		"v�ase adem�s Traumatismo, interno, por sitio "
��� bazo 		"v�ase Ruptura, bazo, traum�tico "
��� coraz�n 		"v�ase Ruptura, miocardio, traum�tica "
��� h�gado 		"v�ase Ruptura, h�gado "
��� ri��n 		"v�ase Ruptura, ri��n "
� intestino	569.89	
�� traum�tica 		"v�ase Traumatismo, interno, intestino "
� intestino	569.89	
�� traum�tica 		"v�ase Traumatismo, interno, intestino "
"� intracraneal, traumatismo al nacer"	767.0	
� iris	364.76	
�� traum�tica		"v�ase Ruptura, ojo "
� ligamento 		"v�ase adem�s Esguince, por sitio"
�� con herida abierta 		"v�ase Herida, abierta, por sitio "
�� antigua	718.0	"v�ase adem�s Desarreglo, cart�lago, articular"
�� ureterosacro	620.8	
� linf�tica (n�dulo) (vaso)	457.8	
� manguito musculotendinoso (no traum�tico) (hombro)	840.4	
� manguito rotatorio (c�psula) (traum�tica)	840.4	
"�� no traum�tica, completa"	727.61	
� m�dula espinal 		"v�ase adem�s Traumatismo, espinal, por sitio "
�� debida a traumatismo al nacer	767.4	
�� feto o reci�n nacido	767.4	
�� sifil�tica	094.89	
�� traum�tica 		"v�ase adem�s Traumatismo, espinal, por sitio "
��� con fractura 		"v�ase Fractura, v�rtebra, por sitio, con traumatismo de m�dula espinal "
� membrana timp�nica	384.20	"v�ase adem�s Perforaci�n, t�mpano"
�� con otitis media 		v�ase Otitis media 
�� traum�tica 		"v�ase Herida, abierta, o�do "
� membranas (espont�nea) 		
�� artificial 		
��� seguida de parto retardado	658.3	
���� feto o reci�n nacido	761.1	
���� que afecta al feto o al reci�n nacido   	761.1	
�� prematura (menos de 24 horas antes del inicio del trabajo del parto)	658.1	
��� que afecta al feto o al reci�n nacido	761.1	
��� seguida de parto retardado	658.2	
���� que afecta al feto o al reci�n nacido   	761.1	
�� seguida de parto retardado	658.2	
��� que afecta al feto o al reci�n nacido	761.1	
� menisco (rodilla) 		"v�ase adem�s Desgarre, menisco "
�� antigua	717.5	"v�ase adem�s Desarreglo, menisco"
��� sitio que no sea la rodilla 		"v�ase Desarreglo, cart�lago, articular "
�� sitio que no sea la rodilla 		v�ase Esguince por sitio 
� mesenterio	568.89	
�� traum�tica 		"v�ase Traumatismo, interno, mesenterio "
"� miocardio, mioc�rdica"	410.9	"v�ase adem�s Infarto, miocardio"
�� traum�tica	861.03	
��� con herida penetrante del t�rax	861.13	
� mitral 		"v�ase Insuficiencia, mitral "
� m�sculo (traum�tica) NCOC 		"v�ase adem�s Esguince, por sitio "
�� con herida abierta 		"v�ase Herida, abierta, por sitio "
�� no traum�tica	728.83	
�� papilar (ventricular)	429.6	
� mu��n duodenal	537.89	
� no traum�tica (cuando significa hernia)	553.9	"v�ase adem�s Hernia, por sitio"
� obstruida	552.9	"v�ase adem�s Hernia, por sitio, con obstrucci�n"
�� gangrenosa	551.9	"v�ase adem�s Hernia, por sitio, con gangrena"
� ojo (sin prolapso de tejido intraocular)	871.0	
�� con 		
��� exposici�n de tejido intraocular	871.1	
��� p�rdida parcial de tejido intraocular  	871.2	
��� prolapso de tejido intracraneal	871.1	
�� debida a quemadura	940.5	
"� ovario, ov�rica"	620.8	
�� cuerpo l�teo	620.1	
�� fol�culo (de Graaf)	620.0	
� oviducto	620.8	
�� debida a embarazo 		"v�ase Embarazo, tub�rico "
� p�ncreas	577.8	
�� traum�tica 		"v�ase Traumatismo, interno, p�ncreas "
� pared libre (ventr�culo)	410.9	"v�ase adem�s Infarto, miocardio"
� pelviana 		
�� �rgano NCOC 		"v�ase Traumatismo, pelviano, �rganos "
"�� suelo, cuando complica el parto"	664.1	
� pene (traum�tica) 		"v�ase Herida, abierta, pene "
� perineo	624.8	
�� durante el parto	664.4	"v�ase adem�s Laceraci�n, perineo, cuando complica el parto"
� piosalpinx	614.2	v�ase adem�s Salpingooforitis
� posoperatoria	998.3	
� pr�stata (traum�tica) 		"v�ase Traumatismo, interno, pr�stata "
� pulmonar 		
�� arteria	417.8	
�� v�lvula (coraz�n)	424.3	"v�ase adem�s Endocarditis, pulmonar"
�� vaso	417.8	
�� vena	417.8	
"� pupila, esf�nter"	364.75	
� pus tub�rico	614.2	v�ase adem�s Salpingooforitis
� quiste 		v�ase Quiste 
� recto	569.49	
�� traum�tica 		"v�ase Traumatismo, interno, recto "
"� retina, retiniana (traum�tica) (sin desprendimiento)"	361.30	
�� con desprendimiento	361.00	"v�ase adem�s Desprendimiento, retina, con defecto retiniano"
� ri��n (traum�tica)	866.03	
�� con herida penetrante de la cavidad	866.13	
�� debida a traumatismo al nacer	767.8	
�� no traum�tica	593.89	
"� semilunar, cart�lago, rodilla"	836.2	"v�ase adem�s Desgarre, menisco"
�� antigua	717.5	"v�ase adem�s Desarreglo, menisco"
� seno de Valsalva	747.29	
� seno marginal (placentario) (con hemorragia)	641.2	
�� que afecta al feto o al reci�n nacido	762.1	
� sigmoide	569.89	
�� traum�tica 		"v�ase Traumatismo, interno colon, sigmoide "
� sinovio	727.50	
�� sitio especificado NCOC	727.59	
� tabique (cardiaco)	410.8	
� tend�n (traum�tica) 		"v�ase adem�s Esguince, por sitio "
�� con herida abierta 		"v�ase Herida, abierta, por sitio "
�� Aquiles	845.09	
��� no traum�tica	727.67	
�� b�ceps (perlado largo)	840.8	
��� no traum�tica	727.62	
�� cuadr�ceps	843.8	
��� no traum�tica	727.65	
�� manguito rotatorio	840.4	
"��� no traum�tica, completa"	727.61	
�� mano	842.10	
��� carpometacarpiana (articulaci�n)	842.11	
��� interfalangiana (articulaci�n)	842.13	
��� metacarpofalangiana (articulaci�n)   	842.12	
��� no traum�tica	727.63	
���� extensores	727.63	
���� flexores	727.64
��� sitio especificado NCOC	842.19
�� mu�eca	842.00
��� carpiana (articulaci�n)	842.01
��� no traum�tica	727.63
���� extensores	727.63
���� flexores	727.64
��� radiocarpiana (articulaci�n) (ligamento)   	842.02
"��� radiocubital (articulaci�n), distal"	842.09
��� sitio especificado NCOC	842.09
�� no traum�tica	727.60
��� sitio especificado NCOC	727.69
�� patelar (r�tula)	844.8
��� no traum�tica	727.66
�� pie	845.10
��� interfalangiana (articulaci�n)	845.13
��� metatarsofalangiana (articulaci�n)	845.12	
��� no traum�tica	727.68	
��� sitio especificado NCOC	845.19	
��� tarsometatarsiana (articulaci�n)	845.11	
�� tobillo	845.09	
��� no traum�tica	727.68	
� test�culo (traum�tica)	878.2	
�� complicada	878.3	
�� debida a s�filis	095.8	
"� t�mpano, membrana timp�nica"	384.20	"v�ase adem�s Perforaci�n, t�mpano"
�� con otitis media 		v�ase Otitis media 
�� traum�tica 		"v�ase Herida, abierta, o�do "
"� t�mpano, timp�nico (membrana)"	384.20	"v�ase adem�s Perforaci�n, t�mpano"
�� con otitis media		v�ase Otitis media 
�� traum�tica 		"v�ase Herida, abierta, o�do, t�mpano "
� traum�tica 		
�� con herida abierta		"v�ase Herida, abierta, por sitio "
�� aorta 		"v�ase Ruptura, aorta, traum�tica "
�� cuando significa hernia 		v�ase Hernia 
�� globo (del ojo) 		"v�ase Herida, abierta, globo del ojo "
"�� ligamento, m�sculo o tend�n "		"v�ase adem�s Esguince, por sitio "
��� con herida abierta 		"v�ase Herida, abierta, por sitio "
�� ojo	871.2	
"�� �rgano interno (abdomen, t�rax o pelvis)"		"v�ase adem�s Traumatismo, interno, por sitio "
��� bazo 		"v�ase Ruptura, bazo, traum�tica "
��� coraz�n		"v�ase Ruptura, miocardio, traum�tica "
��� h�gado 		"v�ase Ruptura, h�gado "
��� ri��n 		"v�ase Ruptura, ri��n "
�� sitio externo 		"v�ase Herida, abierta, por sitio "
�� t�mpano 		"v�ase Herida, abierta, o�do, t�mpano "
� tric�spide (coraz�n) (v�lvula) 		"v�ase Endocarditis, tric�spide "
� trompa de Falopio	620.8	
�� debida a embarazo 		"v�ase Embarazo, tub�rico "
�� traum�tica 		"v�ase Traumatismo, interno, trompa de Falopio "
"� tub�rica, trompa"	620.8	
�� absceso	614.2	v�ase adem�s Salpingooforitis
�� debida a embarazo 		"v�ase Embarazo, tub�rico "
� ur�ter (traum�tica)	867.2	"v�ase adem�s Traumatismo, interno, ur�ter"
�� no traum�tica	593.89	
� uretra	599.8	
�� con 		
��� aborto 		"v�ase Aborto, por tipo, con da�o a �rganos peIvianos "
��� embarazo ect�pico	639.2	v�ase adem�s categor�as 633.0-633.9
��� embarazo molar	639.2	v�ase adem�s categor�a 630-632
�� despu�s de 		
��� aborto	639.2	
��� embarazo ect�pico o molar	639.2	
�� traum�tica 		"v�ase Traumatismo, uretra "
�� traumatismo obst�trico	665.3	
� �tero (traum�tica) 		"v�ase adem�s Traumatismo, interno, �tero "
�� despu�s del parto	665.1	
�� durante el parto	665.1	
�� gr�vido (durante el parto)	665.1	
��� antes del parto	665.0	
"�� no puerperal, no traum�tica"	621.8	
�� no traum�tica	621.8	
�� que afecta al feto o al reci�n nacido	763.8	
� �tero gr�vido (antes del inicio del trabajo del parto)	665.0	
� vagina	878.6	
�� complicada	878.7	
�� cuando complica el parto 		"v�ase Laceraci�n, vagina, cuando complica el parto "
"� v�lvula, valvular (coraz�n) "		v�ase Endocarditis 
� varices 		v�ase Varices 
� vaso (sangu�neo)	459.0	
�� pulmonar	417.8	
� vaso sangu�neo 	459.0	(v�ase adem�s Hemorragia)
�� cerebro	431	"v�ase adem�s Hemorragia, cerebro"
�� coraz�n	410.9	"v�ase adem�s Infarto, miocardio"
�� traum�tica (complicaci�n)	904.9	"v�ase adem�s Traumatismo, vaso sangu�neo, por sitio"
� vejiga (esf�nter)	596.6	
�� con 		
��� aborto 		"v�ase Aborto, por tipo, con da�o a �rganos peIvianos "
��� embarazo ect�pico	639.2	v�ase adem�s categor�as 633.0-633.9
��� embarazo molar	639.2	v�ase adem�s categor�a 630-632
�� despu�s de 		
��� aborto	639.2	
��� embarazo ect�pico o molar	639.2	
�� espont�nea	596.6	
�� no traum�tica	596.6	
�� traum�tica 		"v�ase Traumatismo, interno, vejiga "
�� traumatismo obst�trico	665.5	
vena 		
�� cava	459.0	
�� espl�nica	459.0	
�� varicosa 		"v�ase Varicosa, vena "
� ventr�culo (pared libre) (izquierdo)	410.9	"v�ase adem�s Infarto, miocardio"
� vesical (urinaria)	596.6	
�� traum�tica 		"v�ase Traumatismo, interno, vejiga "
� ves�cula biliar o conducto	575.4	"v�ase adem�s  Enfermedad, ves�cula biliar"
�� traum�tica 		"v�ase Traumatismo, interno, ves�cula biliar "
� v�scera	799.8	
� v�scera abdominal NCOC	799.8	
�� traumatismo obst�trico	665.5	
� vulva	878.4	
�� complicada	878.5	
�� cuando complica el parto	664.0	
"Rusa, encefalitis tipo primaveroestival  "	063.0	
"Russell (-Silver), s�ndrome de (hemihipertrofia cong�nita y baja estatura)"	759.89	
"Russell, disenter�a de"	004.8	
"Russell, enano de (enanismo uterino y disostosis craneofacial)"	759.89	
"Rust, enfermedad de (espondilitis tuberculosa)"	015.0[720.81]	
"Rustitskii, enfermedad de (mieloma m�ltiple) "	203.0(M9730/3)	
"Ruysch, enfermedad de (enfermedad de Hirschsprung)"	751.3	
"Rytand-Lipsitch, s�ndrome de (bloqueo atrioventricular completo)"	426.0	
" Saburral, lengua"	529.3	
"Sacacorchos, es�fago en"	530.5	
"Sacaromices, infecci�n por"	112.9	v�ase adem�s Candidiasis
Sacaropinuria	270.7	
"Sachs (-Tay), enfermedad de (idiotez familiar amaur�tica)"		
"Sacks-Libman, enfermedad de"	710.0[424.91]	
Saco		
� bronquio	748.3	
"� Douglas, de "		v�ase enfermedad espec�fica 
"� es�fago, esof�gico (cong�nito)"	750.4	
�� adquirido	530.6	
"� faringe, far�ngeo (cong�nito)"	750.27	
� g�strico	537.1	
"� Hartmann, de (saculaci�n anormal del cuello de la ves�cula biliar)"	575.8	
Saco lagrimal 		v�ase enfermedad espec�fica
Sacralgia	724.6	
Sacralizaci�n 		
� incompleta (v�rtebra)	756.15	
� quinta v�rtebra lumbar	756.15	
Sacro 		v�ase enfermedad espec�fica
Sacrodinia	724.6	
"Sacroil�aca, articulaci�n "		v�ase enfermedad espec�fica 
Sacroili�tis NCOC	720.2	
Sacudida 		
� cabeza (temblor)	781.0	
� par�lisis	332.0	v�ase adem�s Parkinsonismo
Saculaci�n 		
� aorta (no sifil�tica)	441.9	"v�ase adem�s Aneurisma, aorta"
�� rota	441.5	
�� sifil�tica	093.0	
� colon	569.89	
� intralar�ngea (cong�nita) (ventricular)	748.3	
� laringe (cong�nita) (ventricular)	748.3	
� �rgano o sitio (cong�nita) 		v�ase Distorsi�n 
� rectosigmoide	569.89	
� sigmoide	569.89	
� ur�ter	593.89	
� uretra	599.2	
"� �tero gr�vido, cuando complica el parto"	654.4	
�� que afecta al feto o al reci�n nacido	763.8	
� vejiga	596.3	
� vesical	596.3	
Sacular 		v�ase enfermedad espec�fica
Sadismo (sexual)	302.84	
"Saemisch, �lcera de"	370.04	
"Saenger, s�ndrome de"	379.46	
"Saint, tr�ada de"	553.3	"v�ase adem�s Hernia, diafragma"
"Sajada, herida"		
� externa 		"v�ase Herida, abierta, por sitio "
"� �rganos internos (abdomen, t�rax o pelvis)"		"v�ase Traumatismo, interno, por sitio, con herida abierta Salicilismo "
� sobredosis o administraci�n o ingesti�n de sustancia incorrecta	965.1	
� sustancia correcta administrada de forma correcta	535.4	
Salida (orificio de) 		v�ase adem�s enfermedad espec�fica 
� s�ndrome (tor�cico) de	353.0	
Salivaci�n (excesiva)	527.7	v�ase adem�s Ptialismo
"Salival, conducto o gl�ndula "		v�ase adem�s enfermedad espec�fica 
� enfermedad v�rica de	078.5	
Salmonella (aertrycke) (choleraesuis) (enteriditis) (gallinarum) (suipestifer) (typhimurium)	003.9	"v�ase adem�s Infecci�n, Salmonella"
� artritis	003.23	
� meningitis	003.21	
� neumon�a	003.22	
� osteomielitis	003.24	
� portador (presunto) de	V02.3	
� septicemia	003.1	
� tifosa	002.0	
�� portador (presunto) de	V02.1	
Salmonelosis	003.0	
� con neumon�a	003.22	
Salpingitis (catarral) (nodular) (purulenta) (s�ptica) (seudofolicular) (trompa de Falopio)	614.2	v�ase adem�s Salpingooforitis
� antigua		"v�ase Salpingooforitis, cr�nica "
� espec�fica (cr�nica)	098.37	
�� aguda	098.17	
� folicular	614.1	
� gonoc�cica (cr�nica)	098.37	
�� aguda	098.17	
� intersticial (cr�nica)	614.1	
� �stmica nudosa	614.1	
� o�do	381.50	
�� aguda	381.51	
�� cr�nica	381.52	
"� puerperal, posparto, parto"	670.	
� trompa de Eustaquio	381.50	
�� aguda	381.51	
�� cr�nica	381.52	
� tuberculosa (aguda) (cr�nica)	016.6	v�ase adem�s Tuberculosis
� ven�rea (cr�nica)	098.37	
�� aguda	098.17	
