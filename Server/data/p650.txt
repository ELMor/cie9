Salpingocele	620.4	
Salpingooforitis (catarral) (purulenta) (rota) (s�ptica) (supurativa)	614.2	
� aguda	614.0	
�� con 		
��� aborto 		"v�ase Aborto, por tipo, con septicemia "
��� embarazo ect�pico	639.0	v�ase adem�s categor�as 633.0-633.9
��� embarazo molar	639.0	v�ase adem�s categor�as 630-632
�� despu�s de 		
��� aborto	639.0	
��� embarazo ect�pico o molar	639.0	
�� gonoc�cica	098.17	
"�� puerperal, posparto, parto"	670	
�� tuberculosa	016.6	v�ase adem�s Tuberculosis      
� antigua 		"v�ase Salpingooforitis, cr�nica "
� cr�nica	614.1	
�� gonoc�cica	098.37	
�� tuberculosa  	016.6	v�ase adem�s Tuberculosis
� cuando complica el embarazo	646.6	
�� que afecta al feto o al reci�n nacido	760.8	
� espec�fica 		"v�ase Salpingooforitis, gonoc�cica "
� gonoc�cica (cr�nica)	098.37	
�� aguda	098.17	
� puerperal	670	
� subaguda	614.0	"v�ase adem�s Salpingooforitis, aguda"
� tuberculosa (aguda) (cr�nica)	016.6	v�ase adem�s Tuberculosis
� ven�rea 		"v�ase Salpingooforitis, gonoc�cica"
Salpingoovaritis	614.2	v�ase adem�s Salpingooforitis
Salpingoperitonitis	614.2	v�ase adem�s Salpingooforitis
"Saltadores, rodilla de"	727.2	
Salud 		
� asesoramiento	V65.4	
� auditor�a	V70.0	
� chequeo	V70.0	
� educaci�n	V65.4	
� instrucci�n	V65.4	
� peligro	V15.9	v�ase adem�s Historia de 
�� causa especificada 	V15.89	
� servicios proporcionados a causa de 		
�� alojamiento inadecuado	V60.1	
�� causa especificada NCOC	V60.8	
�� falta de alojamiento	V60.0	
�� no disponibilidad de cuidados en el hogar	V60.4	
�� persona que vive sola	V60.3	
�� pobreza	V60.3	
�� recursos inadecuados	V60.2	
�� relevo por vacaciones para persona que proporciona cuidados en el hogar	V60.5	
�� residencia de colegio de internados	V60.6	
�� residencia en instituci�n	V60.6	
Salur�tica 		
� nefritis	593.9	"v�ase adem�s Enfermedad, renal"
� s�ndrome	593.9	"v�ase adem�s Enfermedad, renal"
"Salzmann, distrofia nodular de"	371.46	
"Sampson, quiste o tumor de"	617.1	
"San, Santo "		
"� Antonio, fuego de"	035	v�ase adem�s Erisipelas
"� Guido, baile de "		v�ase Corea 
"� Luis, encefalitis, tipo de"	062.3	
"� Vito, baile de "		v�ase Corea
"Sander, enfermedad de (paranoia)"	297.1	
"Sandhoff, enfermedad de"	330.1	
"Sanfilippo, s�ndrome de (mucopolisacaridosis III)"	277.5	
"Sanger-Brown, ataxia de"	334.2	
Sangre 		
"� constituyentes, anormales NCOC"	790.6	
"� c�rnea, dep�sito"	371.12	
� discrasia	289.9	
�� con 		
��� aborto 		"v�ase Aborto, por tipo, con hemorragia, retardada o excesiva "
��� embarazo ect�pico	639.1	v�ase adem�s categor�as 633.0-633.9
��� embarazo molar	639.1	v�ase adem�s categor�as 630-632
�� despu�s de 		
��� aborto	639.1	
��� embarazo ect�pico o molar	639.1	
�� feto o reci�n nacido NCOC	776.9	
"�� puerperal, postparto"	666.3	
� donante	V59.0	
� en 		
�� heces	578.1	v�ase adem�s Melena
��� ocultas	792.1	
�� orina	599.7	v�ase adem�s Hematuria
� enfermedad	289.9	
�� especificada NCOC	289.8	
� envenenamiento	038.9	v�ase adem�s Septicemia
� esputo	786.3	v�ase adem�s Hemoptisis
� mola	631	
� oculta	792.1	
� ruptura de vaso sangu�neo 		v�ase Hemorragia 
� tensi�n 		
�� alta	401.9	v�ase adem�s Hipertensi�n
"��� lectura incidental (aislada) (no espec�fica), sin diagn�stico de hipertensi�n"	796.2	
�� baja	458.9	v�ase adem�s Hipotensi�n
"��� lectura incidental (aislada) (no espec�fica), sin diagn�stico de hipotensi�n"	796.3	
"�� disminuida, debido a choque despu�s de lesi�n"	958.4	
�� fluctuante	796.4	
� transfusi�n 		
�� donante	V59.0	
�� reacci�n o complicaci�n 		"v�ase Complicaciones, transfusi�n "
�� sin diagn�stico declarado	V58.2	
� trematodos NCOC	120.9	"v�ase adem�s Infestaci�n, Schistosoma"
� tumor		v�ase Hematoma 
� v�mitos 	578.0	v�ase adem�s Hematemesis
Sangr�a 	459.0	v�ase adem�s Hemorragia
� anal	569.3	
� anovulatoria	628.0	
"� at�nica, despu�s de parto"	666.1	
� boca	528.9	
� capilar	448.9	
� debida a subinvoluci�n	621.1	
�� puerperal	666.2	
� despu�s de coito	626.7	
� enc�as	523.8	
"� excesiva, asociada con inicio de menopausia"	627.0	
� familiar	286.9	"v�ase adem�s Defecto, coagulaci�n"
� garganta	784.8	
� gastrointestinal	578.9	
� hemorroides 		"v�ase Hemorroides, hemorragia"
� intermenstrual 		
�� irregular	626.6	
�� regular	626.5	
� intraoperatoria	998.1	
� irregular NCOC	626.4	
� menop�usica	627.0	
� mu��n umbilical	772.3	
� nariz	784.7	
� no relacionado con el ciclo menstrual	626.6	
� o�do	388.69	
� ombligo	789.9	
� ovulaci�n	626.5	
� pez�n	611.79	
� posclimat�rica	627.1	
� poscoito	626.7	
� posmenop�usica	627.1	
�� despu�s de menopausia inducida	627.4	
� posoperatoria	998.1	
� preclimat�rica	627.0	
� pubertad	626.3	
"�� excesiva, con inicio de per�odo menstrual"	626.3	
"� recto, rectal"	569.3	
� sustituci�n	625.8	
� tendencias	286.9	"v�ase adem�s Defecto, coagulaci�n"
"� �tero, uterina"	626.9	
�� climat�rica	627.0	
�� disfuncional	626.8	
�� funcional	626.8	
�� no relacionada con el ciclo menstrual	626.6	
"� vagina, vaginal"	623.8	
�� funcional	626.8	
Sanguijuelas (acu�ticas) (terrestres)	134.2	
Sano 		
� donante	V59.9	v�ase adem�s Donante
� infante o ni�o 		
�� que acompa�a a madre enferma	V65.0	
�� que recibe cuidados	V20.1	
� persona 		
�� admitida para esterilizaci�n	V25.2	
�� que acompa�a a pariente enfermo	V65.0	
�� que recibe inoculaci�n o vacunaci�n profiI�ctica	V05.9	"v�ase adem�s Vacunaci�n, profil�ctica"
Sano preocupado	V65.5	
"Sao Paolo, fiebre o tifus de"	082.0	
Saponificaci�n mesent�rica	567.8	
Sapremia 		v�ase Septicemia
Sarampi�n (hemorr�gico) (negro) (sin brotar)	055.9	
� con 		
�� encefalitis	055.0	
�� neumon�a	055.1	
�� otitis media	055.2	
�� queratitis	055.71	
�� queratoconjuntivitis	055.71	
� alem�n	056.9	
� complicaci�n	055.8	
�� tipo especificado NCOC	055.79	
� complicaciones especificadas NCOC	055.79	
� encefalitis	055.0	
� franc�s	056.9	
� libre	056.9	
� neumon�a	055.1	
� otitis media	055.2	
� queratitis	055.71	
� queratoconjuntivitis	055.71	
"� vacunaci�n, profil�ctica (contra)"	V04.2	
Sarampi�n	055.9	
� complicada	055.8	
� cuando significa rub�ola	056.9	v�ase adem�s Rub�ola
� escarlatinosis	057.8	
Sarcocele (benigno) 		
� sifil�tico	095.8	
�� cong�nito	090.5	
Sarcoepiplocele	553.9	v�ase adem�s Hernia
Sarcoepiplonfalocele	553.1	"v�ase adem�s Hernia, ombligo"
Sarcoide (cualquier sitio)	135	
� con implicaci�n pulmonar	135 [517.8]	
"� Boeck, de"	135	
� Darier-Roussy	135	
� Spiegler-Fendt	686.8	
Sarcoidosis	135	
� cardiaca	135 [425.8]	
� pulm�n	135.[517.8]	
 Sarcoma	(M8800/3)	"v�ase adem�s Neoplasia, tejido conjuntivo, maligna "
� amelobl�stico 	170.1 (M9330/3)	
�� maxilar superior (hueso)	170.0	
� botrioide 	(M8910/3)	"v�ase Neoplasia, tejido conjuntivo, maligna "
� c�lulas 		
�� cebadas 	(M9740/3)202.6	
"�� claras, de tendones y aponeurosis "	(M9044/3)	"v�ase Neoplasia, tejido conjuntivo, maligna "
�� epitelioides 	(M8804/3)	"v�ase Neoplasia, tejido conjuntivo, maligna "
�� fusiformes 	(M8801/3)	"v�ase Neoplasia, tejido conjuntivo, maligna "
�� gigantes 	(M8802/3)	"v�ase adem�s Neoplasia, tejido conjuntivo, maligna "
��� hueso 	(M9250/3)	"v�ase Neoplasia, hueso, maligna "
"�� Kupffer, de "	(M9124/3)155.0	
�� peque�as 	(M8803/3)	"v�ase Neoplasia, tejido conjuntivo, maligna "
�� plasm�ticas 	(M9731/3)203.8	
�� pleom�rficas 	(M8802/3)	"v�ase Neoplasia, tejido conjuntivo, maligna "
�� redondas 	(M8803/3)	"v�ase Neoplasia, tejido conjuntivo, maligna "
�� reticulares 	(M9640/3)200.0	
��� nodular 	(M9642/3)200.0	
��� tipo c�lulas pleom�rficas 	(M9641/3)200.0	
� cerebelo 	(M9480/3)191.6	
�� circunscrito (aracnoideo) 	(M9471/3)191.6	
� circunscrito (aracnoideo) cerebelar 	191.6(M9471/3)	
� embrionario 	(M8991/3)	"v�ase Neoplasia, tejido conjuntivo, maligna"
� endometrio (estromal) 	(M8930/3)182.0	
�� istmo	182.1	
� endotelial 	(M9130/3)	"v�ase adem�s Neoplasia, tejido conjuntivo, maligna"
�� hueso 	(M9260/3)	"v�ase Neoplasia, hueso, maligna "
� estromal (endometrio) 	(M8930/3)182.0	
�� istmo	182.1	
"� Ewing, de "	(M9260/3)	"v�ase Neoplasia, hueso, maligna "
� germinobl�stico (difuso) 	202.8 (M9632/3)	
�� folicular 	(M9697/3)202.0	
� glomoide 	(M8710/3)	"v�ase Neoplasia, tejido conjuntivo, maligna "
� granuloc�tica 	205.3(M9930/3)	
� hemangioendotelial 	(M9130/3)	"v�ase Neoplasia, tejido conjuntivo, maligna "
"� hemorr�gico, m�ltiple "	(M9140/3)	"v�ase Kaposi, sarcoma de"
"� Hodgkin, de "	201.2(M9662/3)	
� inmunobl�stico 	200.8(M9612/3)	
"� Kaposi, de "	(M9140/3)	"v�ase Kaposi, sarcoma de "
� leptomen�ngeo 	(M9530/3)	"v�ase Neoplasia, meninges, maligna "
� linfangioendotelial 	(M9170/3)	"v�ase Neoplasia, tejido conjuntivo, maligna "
� linfobl�stica 	200.1(M9630/3)	
� linfoc�tica 	200.1(M9620/3)	
� melan�tico 	(M8720/3)	v�ase Melanoma 
� men�ngeo 	(M9530/3)	"v�ase Neoplasia, meninges, maligna "
� meningotelial 	(M9530/3)	"v�ase Neoplasia, meninges, maligna "
� mesenquimatoso 	(M8800/3)	"v�ase adem�s Neoplasia, tejido conjuntivo, maligna "
�� mixta 	(M8990/3)	"v�ase Neoplasia, tejido conjuntivo, maligna "
� mesotelial 	(M9050/3)	"v�ase Neoplasia, por sitio, maligna "
� mieloide 	205.3(M9930/3)	
� monstrocelular 	(M9481/3)	
�� sitio especificado 		"v�ase Neoplasia, por sitio, maligna "
�� sitio no especificado	191.9	
� neur�geno 	(M9540/3)	"v�ase Neoplasia, tejido conjuntivo, maligna "
� odontog�nico 	170.1(M9270/3)	
�� maxilar superior (hueso)	170.0	
� osteobl�stico 	(M9180/3)	"v�ase Neoplasia, hueso, maligna "
� osteog�nico 	(M9180/3)	"v�ase adem�s Neoplasia, hueso, maligna "
�� peri�stico 	(M9190/3)	"v�ase Neoplasia, hueso, maligna "
�� yuxtacortical 	(M9190/3)	"v�ase Neoplasia, hueso, maligna "
� parte blanda del alv�olo 	(M9581/3)	"v�ase Neoplasia, tejido conjuntivo, maligna "
� peri�stico 	(M8812/3)	"v�ase adem�s Neoplasia, hueso, maligna "
�� osteog�nico 	(M9190/3)	"v�ase Neoplasia, hueso, maligna "
� reticuloendotelial 	202.3(M9720/3)	
� sinovial 	(M9040/3)	"v�ase adem�s tejido conjuntivo, maligna "
�� tipo bif�sico 	(M9043/3)	"v�ase Neoplasia, tejido conjuntivo, maligna "
�� tipo c�lulas epitelioides 	(M9042/3)	"v�ase Neoplasia, tejido conjuntivo, maligna "
�� tipo c�lulas fusiformes 	(M9041/3)	"v�ase Neoplasia, tejido conjuntivo, maligna"
Sarcomatosis 		
� men�ngea 	(M9539/3)	"v�ase Neoplasia, meninges, maligna "
� sitio especificado NCOC 	(M8800/3)	"v�ase Neoplasia, tejido conjuntivo, maligna "
� sitio no especificado 	171.9(M8800/6)	
 Sarcosinemia	270.8	
Sarcosporidiosis	136.5	
Sarna (cualquier sitio)	133.0	
Sarpullido	782.1	
� alimento	693.1	"v�ase adem�s Alergia, alimento"
� calor	705.1	
� chancro	034.1	
� droga o f�rmaco (uso interno)	693.0	
�� contacto	692.3	
� enema	692.89	
� escarlata	034.1	
"� gl�teo, de los pa�ales"	691.0	
� lengua migratoria	529.1	
� ortigas (urticaria)	708.8	
"� pa�ales, de los"	691.0	
� pustular	782.1	
� rosas	782.1	
�� epid�mico	056.9	
�� ni�os	057.8	
� suero (profil�ctico) (terap�utico)	999.5	
� t�xico	782.1	
� virus ECHO 9	078.89	
Sarpullido por exceso de calor	705.1	
"Sarro, t�rtaro (dientes)"	523.6	
Satiriasis	302.89	
Saturninismo	984.9	
� tipo de plomo especificado 		"v�ase Tabla de drogas, f�rmacos y, productos qu�micos"
Saturnino 		v�ase enfermedad espec�fica 
Sauriasis 		v�ase Ictisosis
"Sauriderma, sauridermia"	757.39	
Sauriosis 		v�ase Ictiosis
"Savill, enfermedad de (dermatitis exfoliativa epid�mica)"	695.89	
SBE (endocarditis bacteriana subaguda)	421.0	
"Scaglietti-Dagnini, s�ndrome de (macroespondilitis acromeg�lica)"	253.0	
"Schamberg, enfermedad, dermatitis o dermatosis de (dermatosis pigmentaria progresiva)"	709.0	
"Schatzki, anillo de (es�fago) (inferior) (cong�nito)"	750.3	
� adquirido	530.3	
Schaufenster krankheit	413.9	
"Schaumann, de "		
� enfermedad (sarcoidosis)	135	
� linfogranulomatosis benigna	135	
� s�ndrome (sarcoidosis) 	135	
"Scheie, s�ndrome de (mucopolisacaridosis IS)"	277.5	
"Schenck, enfermedad de (esporotricosis)"	117.1	
"Scheuermann, enfermedad o osteocondrosis de"	732.0	
"Scheuthauer-Marie-Sainton, s�ndrome de (disostosis cleidocraneal)"	755.59	
"Schilder (-Flatau), enfermedad de"	341.1	 
"Schilling, leucemia monoc�tica tipo de"	206.9 (M9890/3) 	
"Schimmelbusch, enfermedad, mastitis qu�stica o hiperplasia de"	610.1	
"Schirmer, s�ndrome de (angiomatosis encefalocut�nea)"	759.6	
Schlafkrankheit (enfermedad del sue�o)	086.5	
"Schlatter, tibia de (osteocondrosis)"	732.4	
"Schlatter-Osgood, enfermedad de (osteocondrosis, tub�rculo tibial)"	732.4	
"Schloffer, tumor de"	567.2	v�ase adem�s Peritonitis
"Schmidt, s�ndrome de "		
� hemiplej�a esfalofaringolar�ngea	352.6	
� insuficiencia tiroidea-adrenocortical	258.1	
� vagoaccesorio	352.6	
Schminke 		
� carcinoma 	(M8082/3)	"v�ase Neoplasia, nasofaringe, maligna "
� tumor 	(M8082/3)	"v�ase Neoplasia, nasofaringe, maligna"
"Schmitz (-Stutzer), disenter�a de"	004.0	
"Schmorl, enfermedad o n�dulos de"	722.30	
"� lumbar, lumbosacra"	722.32	
� regi�n especificada NCOC	722.39	
"� tor�cica, toracolumbar"	722.31	
"Schneider, s�ndrome de"	047.9	
Schneiderian 		
� carcinoma 	(M8121/3)	
�� sitio especificado 		"v�ase Neoplasia, por sitio, maligna "
�� sitio no especificado	160.0	
� papiloma 	(M8121/0)	
�� sitio especificado 		"v�ase Neoplasia, por sitio, benigna "
�� sitio no especificado	212.0	
"Schoffer, tumor de"	567.2	v�ase adem�s Peritonitis
"Scholte, s�ndrome de (carcinoide maligno)"	259.2	
"Scholz (-Bielschowsky-Henneberg), s�ndrome de"	330.0	
"Scholz, enfermedad de"	330.0	
"Sch�nlein (-Henoch), enfermedad de (primaria) (p�rpura) (reum�tica)"	287.0	
"Schottm�ller, enfermedad de"	002.9	"v�ase adem�s Fiebre, paratifoidea"
"Schroeder, s�ndrome de (endocrino-hipertensivo)"	255.3	
"Schuller-Christian, enfermedad o s�ndrome de (histiocitosis X cr�nica)"	277.8	
"Schultz, enfermedad o s�ndrome de (agranulocitosis)"	288.0	
"Schultze, acroparestesia simple de"	443.89	
"Schwalbe-Ziehen-Oppenheimer, enfermedad de"	333.6	
Schwannoma 	(M9560/0)	"v�ase adem�s Neoplasia, tejido conjuntivo, benigna "
� maligna 	(M9560/3)	"v�ase Neoplasia, tejido conjuntivo, maligna "
"Schwartz (-Jampel), s�ndrome de"	756.89	
"Schwartz-Bartter, s�ndrome de (secreci�n inapropiada de hormona antidiur�tica)"	253.6	
"Schweninger-Buzzi, enfermedad de (atrofia macular)"	701.3	
"Seabright-Bantam, s�ndrome de (seudohipoparatiroidismo)"	275.4	
Seb�ceo 		
� enfermedad glandular NCOC	706.9	
� quiste	706.2	"v�ase adem�s Quiste, seb�ceo"
Sebocistomatosis	706.2	
"Seborrea, seborreico"	706.3	
� adiposa	706.3	
"� capitis, cabeza"	704.8	
� congestiva	695.4	
"� corporis, cuerpo"	706.3
� dermatitis	690
�� infantil	691.8
� di�tesis en ni�os	695.89
� eczema	690
�� infantil	691.8
� nigricans	705.89
� queratosis	702
� seca	690
� verruga 	702
"Secaderos para el l�pulo,"	
enfermedad de	270.2
Secci�n 	
� ces�rea 	
"�� posmortem, que afecta al feto o al reci�n nacido"	761.6
"�� previa, en el embarazo o parto"	654.2
��� que afecta al feto o al reci�n nacido	763.8	
�� que afecta al feto o al reci�n nacido	763.4	
"� nervio, traum�tica "		"v�ase Traumatismo, nervio, por sitio"
"Seckel, s�ndrome de"	759.89	
Seclusi�n de la pupila	364.74	
"Seco, sequedad "		v�ase adem�s enfermedad espec�fica 
� alveolo (diente)	526.5	
� boca	527.7	
� garganta	478.29	
� laringe	478.79	
� nariz	478.1	
� ojo	375.15	
�� s�ndrome	375.15	
� s�ndrome de piel seca	701.1	
"Secoyosis, asma"	495.8	
Secreci�n 		
"� catecolamina, por feocromocitoma"	255.6
� hormona 	
"�� antidiur�tica, inapropiada (s�ndrome)"	253.6
�� ect�pica NCOC	259.3
�� por 	
��� feocromocitoma	255.6
��� tumor carcinoide	259.2
� hormona antidiur�tica inapropiada (s�ndrome)	253.6
� urinaria 	
�� excesiva	788.4
�� supresi�n	788.5
Secreci�n impropia 	
� ACTH	255.0
� hormona 	
�� antidiur�tica (ADH) (excesiva)	253.6
��� carencia	253.5
�� ect�pica NCOC	259.3	
� pituitaria (posterior)	253.6	
"Secreci�n por acumulaci�n, pr�stata"	602.8	
"Secretan, enfermedad o s�ndrome de (edema postraum�tico)"	782.3	
Secuestro 		
� arteria pulmonar (cong�nita)	747.3	
� dental	525.8	
� hueso	730.1	v�ase adem�s Osteomielitis
"�� mand�bula, maxila"	526.4	
"� mand�bula, maxila (hueso)"	526.4	
� �rbita	376.10	
� pulm�n (cong�nita) (extralobular) (intralobular)	748.5	
� seno (accesorio) (nasal)	473.9	v�ase adem�s Sinusitis
�� maxilar	473.0	
Secundario 		v�ase adem�s enfermedad espec�fica 
� neoplasia 		"v�ase Neoplasia, por sitio, maligna, secundaria"
Sed excesiva	757.33	
� debida a privaci�n de agua	994.3	
"Seeligmann, s�ndrome de (ictiosis cong�nita)"	757.1	
Segmentaci�n incompleta (cong�nita) 		v�ase adem�s Fusi�n 
� hueso NCOC	756.9	
� lumbosacra (articulaci�n)	756.15	
� v�rtebra	756.15	
�� lumbosacra	756.15	
Seguimiento (examen) (rutina) (despu�s de)	V67.9	
"� c�ncer, quimioterapia para"	V67.2	
� cirug�a	V67.0	
� enfermedad especificada NCOC	V67.59	
� fractura	V67.4	
� medicaci�n de alto riesgo	V67.51	
� posparto 		
�� inmediatamente despu�s del parto	V24.0	
�� rutinario	V24.2
� psicoterapia	V67.3
� psiqui�trico	V67.3
� quimioterapia	V67.2
� radioterapia	V67.1
� tratamiento	V67.9
�� combinado NCOC	V67.6
�� cuando implica medicaci�n de alto riesgo NCOC	V67.51
�� especificado NCOC	V67.59
�� fractura	V67.4
�� trastorno mental	V67.3
� traumatismo NCOC	V67.59
Seguimiento posparto rutinario	V24.2
"Selv�tica, fiebre amarilla"	060.0
Semicoma	780.0
Semiconciencia	780.0
Seminal 		
� ves�cula 		v�ase enfermedad espec�fica 
� vesiculitis	608.0	v�ase adem�s Vesiculitis 
Seminoma 	(M9061/3)	
� espermatoc�tico 	(M9063/3)	
�� sitio especificado 		"v�ase Neoplasia, por sitio, maligna "
�� sitio no especificado	186.9	
� sitio especificado 		"v�ase Neoplasia. por sitio, maligna "
� sitio no especificado	186.9	
� tipo anapl�sico 	(M9062/3)	
�� sitio especificado 		"v�ase Neoplasia, por sitio, maligna "
�� sitio no especificado	186.9	
"Semlike Forest, encefalitis de"	062.8	
"Senear-Usher, enfermedad o s�ndrome de (p�nfigo eritematoso)"	694.4	
"Senecio jacobae, dermatitis por"	692.6	
Senectud	797	
Senil	797	v�ase adem�s enfermedad espec�fica
"� atrofia degenerativa, piel"	701.3	
� coraz�n (fallo)	797	
� cuello uterino (atr�fico)	622.8	
� endometrio (atr�fico)	621.8	
� ovario (atr�fico)	620.3	
� pulm�n	492.8	
� s�ndrome	259.8	
� trompa de Falopio (atr�fica)	620.3	
"� vagina, vaginitis (atr�fica)"	627.3	
� verruga 	702	
Senilidad	797	
� con 		
�� cambios mentales	290.9	
�� delirio	290.3	
�� estado confusional agudo	290.3	
�� psicosis NCOC 	290.20	"(v�ase adem�s Psicosis, senil)"
� prematura (s�ndrome)	259.8	
Seno 		v�ase adem�s F�stula 
� abdominal	569.81	
� cocc�geo (infectado)	685.1	
�� con absceso	685.0	
� dental	522.7	
� d�rmico (cong�nito)	685.1	
�� con absceso	685.0	
� drenaje 		v�ase F�stula 
� hendidura branquial (externo) (interno)	744.41	
"� infectado, piel NCOC"	686.9	
"� marginal, roto o hemorr�gico"	641.2	
�� que afecta al feto o al reci�n nacido	762.1	
� pericraneal	742.0	
� pilonidal (infectado) (recto)	685.1	
�� con absceso	685.0	
� rectovaginal	619.1	
"� Rokitansky-Aschoff, senos de"	575.8	"v�ase adem�s Enfermedad, ves�cula biliar"
� sacrococc�geo (dermoide) (infectado)	685.1	
�� con absceso	685.0	
"� tarsiano, s�ndrome de"	355.5	
� test�culo	608.89	
� tracto (posinfeccioso) 		v�ase F�stula 
� uraco	753.7	
"Seno enfermo, s�ndrome de"	427.81	
Sensaci�n 		
� ahogamiento	784.9	
� hormigueo 	782.0	"v�ase adem�s Perturbaci�n, sensaci�n"
� p�rdida de	782.0	"v�ase adem�s Perturbaci�n, sensaci�n"
"� picaz�n, picor"	782.0	"v�ase adem�s Perturbaci�n, sensaci�n"
� quemadura	782.0	"v�ase adem�s Perturbaci�n, sensaci�n"
�� lengua	529.6	
Sensibilidad dolorosa 		
� abdominal (generalizada) (localizada)	789.0	
� piel	782.0	
"Sensibilidad, perturbaci�n de NCOC (cortical) (profunda) (vibratoria)"	782.0	"v�ase adem�s Perturbaci�n, sensaci�n"
"Sensibilidad, sensibilizaci�n "		v�ase adem�s Alergia 
� autoeritroc�tica	287.2	
"� fr�a, autoinmune"	283.0	
� metemoglobina	289.7	
� ni�o (excesiva)	313.21	
� seno carot�deo	337.0	
� suxametonio	289.8	
"� tuberculina, sin s�ntomas cl�nicos ni radioI�gicos"	795.5	
"Sensible, dentina"	521.8	
Sensitiver Beziehungswahn	297.8	
Sentido o sensaci�n disminuida (fr�o) (calor) (t�ctil) (vibratorio)	782.0	"v�ase adem�s Perturbaci�n, sensaci�n"
"Sentido, p�rdida de (tacto)"	782.0	"v�ase adem�s Perturbaci�n, sensaci�n"
� gusto	781.1	
� olfato	781.1	
Separaci�n 		
� acromioclavicular 		"v�ase Luxaci�n, hombro "
"� anillo traqueal, incompleto (cong�nito)"	748.3	
� ansiedad anormal por	309.21	
"� ap�fisis, traum�tica "		"v�ase Fractura, por sitio "
� articulaci�n (actual) (traum�tica) 		"v�ase Luxaci�n, por sitio "
� coroides	363.70	
�� hemorr�gica	363.72	
�� serosa	363.71	
� costocondral (simple) (traum�tico) 		"v�ase Luxaci�n, costocondral "
"� ep�fisis, epifisario "		
�� no traum�tica	732.9	
��� femoral superior	732.2	
�� traum�tica		"v�ase Fractura, por sitio "
� esternoclavicular (traum�tica) 		"v�ase Luxaci�n, esternoclavicular "
� fractura 		"v�ase Fractura, por sitio "
"� hueso pubiano, traumatismo obst�trico"	665.6	
"� infund�bulo cardiaco, del ventr�culo derecho por divisi�n o tabique"	746.83	
� placenta (normalmente implantada) 		"v�ase Placenta, separaci�n "
"� retina, retiniana"	361.9	"v�ase adem�s Desprendimiento, retina"
�� capas	362.40	
��� sensoriales	361.10	v�ase adem�s Retinosquisis
�� epitelio pigmentario (exudativa)	362.42	
��� hemorr�gica	362.43	
"� s�nfisis del pubis, traumatismo obst�trico"	665.6	
Separaci�n conyugal	V61.0	
"Septicemia, septic�mico (generalizada) (supurativa)"	038.9	
� con 		
�� aborto 		"v�ase Aborto, por tipo, con septicemia "
�� embarazo ect�pico	639.0	v�ase adem�s categor�as 633.0-633.9
�� embarazo molar	639.0	v�ase adem�s categor�as 630-632
� Aerobacter aerogenes	038.49	
� anaer�bica	038.3	
� �ntrax	022.3	
� Bacillus coli	038.42	
� bacilos ent�ricos gramnegativos	038.40	
� Bacterioides	038.3	
� bucal	528.3	
� Clostridium	038.3	
"� como resultado de infusi�n, inyecci�n, transfusi�n o vacunaci�n"	999.3	
� cript�gena	038.9	
� cuando complica el parto	659.3	
� de estoma de traqueostom�a	519.0	
� dental (origen pulpal)	522.4	
� despu�s de 		
�� aborto	639.0
�� embarazo ect�pico o molar	639.0
"�� infusi�n, inyecci�n, perfusi�n, transfusi�n"	
o vacunaci�n	999.3
� Erysipelothrix (insidiosa) (rhusiopathiae)	027.1
� Escherichia coli	038.42
� estafiloc�cica	038.1
� estreptoc�cica (anaer�bica)	038.0
� feto (intrauterina)	771.8
� Friedlander (bacilo)	038.49
� gangrenosa	038.9
� gonoc�cica	098.89
� gramnegativa (organismo)	028.40
�� anaer�bica	038.3
� Hemophilus influenzae	038.41
� herpes (simple)	054.5
� herp�tica	054.5	
� intraocular	360.00	
� Listeria monocytogenes	027.0	
� localizada 		
�� en herida operatoria	998.5	
�� piel	682.9	v�ase adem�s Absceso
� Malleus	024.	
� meningoc�cica (cr�nica) (fulminante)	036.2	
� neumoc�cica	038.2	
"� ombligo, umbilical, reci�n nacido (organismo no especificado)"	771.8	
�� t�tanos	771.3	
� oral	528.3	
� organismo especificado NCOC	038.8	
� �rgano genital femenino NCOC	614.9	
� peste	020.0	
"� piel, localizada"	682.9	v�ase adem�s Absceso
� posaborto	639.0	
� posoperatoria	998.5	
� Proteus vulgaris	038.49	
� Pseudomonas (aeruginosa)	038.43	
"� puerperal, posparto, parto (peIviana)"	670	
� reci�n nacido (umbilical) (organismo no especificado)	771.8	
� Salmonella (aertrycke) (callinarum) (choleraesuis) (enteriditis) (suipestifer)	003.1	
� Serratia	038.44	
� Shigella	004.9	"v�ase adem�s Disenter�a, bacilar"
� suipestifer	003.1	
� urinaria	599.0	
� v�rica	079.9	
� Yersinia enterocol�tica	038.49	
S�ptico 		v�ase adem�s enfermedad espec�fica 
� am�gdalas	474.0	
� articulaci�n	711.0	"v�ase adem�s Artritis, s�ptica"
� bazo (aguda)	289.59	
� boca	528.3	
� brazo (con linfangitis)	682.3	
� cord�n umbilical (reci�n nacido) (organismo no especificado)	771.8	
� dedo de 		
�� mano (con linfangitis)	681.00	
�� pie (con linfangitis)	681.10	
� diente (origen pulpal)	522.4	
� embolia		v�ase Embolia 
� garganta	034.0	
� mano (con linfangitis)	682.4	
� pie (con linfangitis)	682.7	
� pierna (con linfangitis)	682.6	
� ri��n	590.9	"v�ase adem�s Infecci�n, ri��n"
� shock (endot�xico)	785.59	
� trombo 		v�ase Trombosis 
� �lcera	682.9	v�ase adem�s Absceso
�� garganta	034.0	
��� estreptoc�cica	034.0	
��� transmitida por leche	034.0	
� �tero	615.9	v�ase adem�s Endometritis
� u�a	681.9	
�� dedo de 		
��� mano	681.02	
��� pie	681.11	
� ves�cula biliar	575.8	v�ase Colecistitis
Serolog�a para s�filis 		
� dudosa 		
�� con signos y s�ntomas 		"v�ase S�filis, por sitio y fase "
�� seguimiento de s�filis latente 		"v�ase S�filis, latente "
� falsa positiva	795.6	
"� negativa, con signos y s�ntomas "		"v�ase S�filis, por sitio y fase "
� positiva	097.1	
�� con signos y s�ntomas 		"v�ase S�filis, por sitio y fase "
� falsa	795.6	
� resultado �nico 		"v�ase S�filis, latente "
� seguimiento de s�filis latente 		"v�ase S�filis, latente "
� reactivada	097.1	
Seroma 		v�ase Hematoma
Seropurulenta 		v�ase enfermedad espec�fica
"Serositis, m�ltiple"	569.89	
� peric�rdica	423.2	
� peritoneal	568.82	
� pleural 		v�ase Pleures�a
Seroso 		v�ase enfermedad espec�fica
"Sertoli, c�lulas de "		
� adenoma 	(M8640/0)	
�� sitio especificado 		"v�ase Neoplasia, por sitio, benigna "
�� sitio no especificado 		
���� femenino	220	
���� masculino	222.0	
� carcinoma 	(M8640/3)	
�� sitio especificado 		"v�ase Neoplasia, por sitio, maligna "
�� sitio no especificado	186.9	
� s�ndrome de (aplasia germinal)	606.0	
� tumor 	(M8640/0)	
�� con almacenamiento l�pido 	(M8641/0)	
��� sitio especificado 		"v�ase Neoplasia, por sitio, benigna "
��� sitio no especificado 		
���� femenino	220	
���� masculino	222.0	
�� sitio especificado 		"v�ase Neoplasia, por sitio, benigna "
�� sitio no especificado 		
��� femenino	220	
��� masculino	222.0	
"Sertoli-Leydig, tumor de c�lulas de "	(M8631/0)	
� sitio especificado 		"v�ase Neoplasia, por sitio, benigna "
� sitio no especificado 		
�� femenino	220	
�� masculino	222.0	
Servicios m�dicos proporcionados por 		v�ase Servicios sanitarios proporcionados a causa de
Sesamoiditis	733.99	
"Seudartrosis, seudoartrosis (hueso)"	733.82	
� articulaci�n despu�s de fusi�n	V45.4	
Seudoacantosis nigricans	701.8	
"Seudoagujero, m�cula"	362.54	
Seudoalucinaci�n	780.1	
Seudoaneurisma 		v�ase Aneurisma 
Seudoangina (pectoris)		v�ase Angina 
Seudoangioma	452	
Seudoarterioso	747.89	
Seudoartrosis 		v�ase Seudartrosis 
Seudoataxia	799.8	
Seudobolsa sinovial	727.89	
Seudociesis	300.11	
"Seudocirrosis del h�gado, peric�rdica"	423.2	
Seudocoartaci�n	747.21	
Seudoc�lera	025	
Seudocoxalgia	732.1	
Seudocromidrosis	705.89	
Seudodemencia	300.16	
Seudoelefantiasis neuroartr�tica	757.0	
Seudoencefalitis superior (aguda) hemorr�gica	265.1	
Seudoenfermedad de Hurler (mucopolidosis III)	272.7	
Seudoenfisema	518.89	
"Seudoerosi�n, cuello uterino, cong�nita"	752.49	
"Seudoexfoliaci�n, c�psula del cristalino"	366.11	
Seudofaquia	V43.1	
Seudofractura (espont�nea) (idiop�tica) (m�ltiple) (sim�trica)	268.2	
"Seudogarrotillo, seudocrup"	478.75	
Seudoglioma	360.44	
Seudogota 		v�ase Condrocalcinosis 
Seudohemianestesia	782.0	
"Seudohemofilia (Bernuth, de) (hereditaria) (tipo B)"	286.4	
� tipo A	287.8	
� vascular	287.8	
Seudohermafroditismo	752.7	
� con anomal�a cromos�mica 		v�ase Anomal�a cromos�mica 
� femenino (sin trastorno adrenocortical)	752.7	
�� con trastorno adrenocortical	255.2	
�� suprarrenal	255.2	
� masculino (sin trastorno gonadal)	752.7	
�� con 	
��� escroto hendido	752.7
��� test�culo femininizante	257.8
��� trastorno 	
���� adrenocortical	255.2
���� gonadal	257.9
�� suprarrenal	255.2
� suprarrenal	255.2
Seudohidroc�falo	348.2
Seudohipertrofia muscular	359.1
"Seudohipertr�fica, distrofia muscular (de Erb)"	359.1
Seudohipoparatiroidismo	275.4
Seudoinfluenza	487.1
Seudoinsomnio	307.49
Seudoleucemia	288.8
� infantil	285.8
Seudomembranoso 		v�ase enfermedad espec�fica 
Seudomeningocele (cerebral)  (infeccioso) (quir�rgico)	349.81	
� espinal	349.81	
Seudomenstruaci�n	626.8	
Seudomieloma	273.1	
Seudomixoma peritoneal 	197.6(M8480/6)	
Seudomucinoso 		
� peritoneo	568.89	
� quiste (ovario) 	220 (M8470/1)	
Seudomuermo	025	
Seudoneuritis �ptica (nervio)	377.24	
� papila	377.24	
�� cong�nita	743.57	
Seudoneuroma 		"v�ase Traumatismo, nervio, por sitio "
Seudoobstrucci�n intestinal	564.8	
Seudopapiledema	377.24	
Seudopar�lisis 	
"� at�nica, cong�nita"	358.8
� brazo o pierna	781.4
Seudopelada	704.09
Seudopolicitemia	289.0
"Seudopoliposis, colon"	556
Seudoporencefalia	348.0
Seudopsicosis	300.16
Seudopterigi�n	372.52
Seudoptosis (p�rpado)	374.34
Seudopupila de Argyll Robertson	379.45
Seudoquiste 	
� p�ncreas	577.2
� pulm�n	518.9
� retina	361.19
Seudorrabia	078.89
Seudorraquitismo	588.0	
� senil (de Pozzi)	731.0	
Seudorretinitis pigmentosa	362.65	
Seudorrub�ola	057.8	
Seudoscarlatina	057.8	
Seudosclerema	778.1	
Seudosclerosis (cerebro) 		
� esp�stica	046.1	
�� con demencia	290.10	v�ase adem�s Demencia presenil
"� Jakob, de"	046.1	
"� Westphal (-Strumpell), de (degeneraci�n hepatolenticular)"	275.1	
Seudohipoparatiroidismo	275.4	
Seudos�ndrome de Turner	759.89	
Seudotabes	799.8	
� diab�tica	250.6[337.1]	
Seudotalasemia	285.0	
Seudotetania	781.7	
� hist�rica	300.11	
Seudot�tanos	780.3	v�ase adem�s Convulsiones
Seudotriquinosis	710.3	
Seudotronco arterioso	747.29	
"Seudotuberculosis, pasteurella (infecci�n)"	027.2	
Seudotumor 		
� cerebral	348.2	
� �rbita (inflamatorio)	376.11	
Seudovacuna	051.1	
Seudoxantoma el�stico	757.39	
"Sever, enfermedad u osteocondritis de (calc�neo)"	732.5	
Sexta enfermedad	057.8	
Sextillizo 		
� afectado por complicaciones maternas del embarazo	761.5	
"� embarazo, (cuando complica el parto) NCOC"	651.8	
�� con p�rdida fetal y retenci�n de uno o m�s fetos	651.6	
� nacido sano 		"v�ase Reci�n nacido, m�ltiple"
Sexual 		
� anestesia	302.72	
� desviaci�n	302.9	"v�ase adem�s Desviaci�n, sexual "
� frigidez (femenina)	302.72	
"� funci�n, trastorno de (psic�geno)"	302.70	
�� tipo especificado NCOC	302.79	
� impotencia (psic�gena)	302.72	
�� origen org�nico NCOC	607.84	
� inmadurez (femenina) (masculina)	259.0	
� precocidad (constitucional) (cript�gena) (femenina) (idiop�tica) (masculina) NCOC	259.1	
�� con hiperplasia suprarrenal	255.2	
� sadismo	302.84	
� trastorno	302.9	"v�ase adem�s Desviaci�n, sexual"
Sexualidad patol�gica	302.9	"v�ase adem�s Desviaci�n, sexual"
"S�zary, enfermedad, reticulosis o s�ndrome de "	202.2(M9701/3)	
"Shaver, enfermedad de (neumoconiosis por bauxita) "	503	
"Sheehan, enfermedad o s�ndrome de (necrosis pituitaria posparto)"	253.2	
"Shiga, de "		
� bacilo	004.0	
� disenter�a	004.0	
Shigella (disenter�a)	004.9	"v�ase adem�s Disenter�a, bacilar"
� portador (presunto) de	V02.3	
Shigelosis	004.9	"v�ase adem�s Disenter�a, bacilar"
"Shirodkar, sutura de, en el embarazo"	654.5	
Shock	785.50	
� con 		
� aborto 		"v�ase Aborto, por tipo con choque "
�� embarazo ect�pico	639.5	v�ase adem�s categor�as 633.0-633.9
�� embarazo molar	639.5	v�ase adem�s categor�as 630-632
� al�rgico 		"v�ase Choque, anafil�ctico "
� anacl�tico	309.21	
� anafil�ctico	995.0	
�� despu�s de picadura(s)	989.5	
�� droga o sustancia medicamentosa 		
��� sobredosis o ingesti�n o administraci�n de sustancia incorrecta	977.9	
���� droga o f�rmaco especificado 		"v�ase Tabla de drogas, f�rmacos y productos qu�micos "
��� sustancia correcta administrada de forma correcta	995.0	
�� inmunizaci�n	999.4	
�� qu�mico 		"v�ase Tabla de drogas, f�rmacos y productos qu�micos "
�� suero	999.4	
�� sustancia medicamentosa correcta administrada de forma correcta	995.0	
� anafilactoideo 		"v�ase Choque, anafil�ctico "
� anest�sico 		
�� sobredosis o administraci�n de sustancia incorrecta	968.4	
��� anest�sico especificado 		"v�ase Tabla de drogas, f�rmacos y productos qu�micos "
�� sustancia correcta administrada de forma correcta	995.4	
� cardiog�nico	785.51	
� circulatorio	785.59	
� cuando complica 		
�� aborto 		"v�ase Aborto, por tipo con shock "
�� embarazo ect�pico	639.5	v�ase adem�s categor�as 633.0-633.9
�� embarazo molar	639.5	v�ase adem�s categor�as 630-632
�� parto y trabajo del parto	669.1	
� cultural	309.29	
� debido a droga o f�rmaco	995.0	
�� sobredosis o ingesti�n o administraci�n de sustancia incorrecta	977.9	
��� droga o f�rmaco especificado 		v�ase Tabla de drogas f�rmacos y productos qu�micos 
�� sustancia correcta administrada de forma correcta	995.0	
� despu�s de 		
�� aborto	639.5	
�� embarazo ect�pico o molar	639.5	
�� parto y trabajo del parto	669.1	
�� traumatismo (inmediato) (retardado)	958.4	
� durante el parto y trabajo del parto	669.1	
� el�ctrico	994.8	
� endot�xico	785.59	
�� debido a procedimiento quir�rgico	998.0	
� espinal 		"v�ase adem�s Traumatismo, espinal, por sitio "
�� con traumatismo de hueso espinal 		"v�ase Fractura, v�rtebra, por sitio, con traumatismo de m�dula espinal "
� gramnegativo	785.59	
� hematog�nico	785.59	
� hemorr�gico 		
�� debido a 		
��� cirug�a (intraoperatorio) (posoperatorio)	998.0	
��� enfermedad	785.59	
��� traumatismo	958.4	
� hipovol�mico NCOC	785.59	
�� quir�rgico	998.0	
�� traum�tico	958.4	
� historia (de)	V15.4	
� infortunio terap�utico NCOC	998.8	v�ase adem�s Complicaciones
� insulina	251.0	
�� infortunio terap�utico	962.3	
"� nacimiento, feto o reci�n nacido NCOC"	779.8	
� nervioso	308.9	"v�ase adem�s Reacci�n, tensi�n, aguda"
� obst�trico	669.1	
�� con 		
��� aborto 		"v�ase Aborto, por tipo con choque "
��� embarazo ect�pico	639.5	v�ase adem�s categor�as 633.0-633.9
��� embarazo molar	639.5	v�ase adem�s categor�as 630-632
�� despu�s de 		
��� aborto	639.5	
��� embarazo ect�pico o molar	639.5	
