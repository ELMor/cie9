���� embarazo ect�pico	639.2	v�ase adem�s categor�as 633.0-633.9
���� embarazo molar	639.2	v�ase adem�s categor�as 630-632
���� herida abierta penetrante de la cavidad	867.5	
��� despu�s de 		
���� aborto	639.2	
���� embarazo ect�pico o molar	639.2	
��� traumatismo obst�trico NCOC	665.5	
���� que afecta al feto o al reci�n nacido	763.8	
�� vaso sangu�neo 		"v�ase Traumatismo, vaso sangu�neo, por sitio "
�� vejiga (esf�nter)	867.0	
�� con 		
���� aborto 		"v�ase Aborto, por tipo, con, da�o a �rganos pelvianos "
���� embarazo ect�pico	639.2	v�ase adem�s categor�as 633.0-633.9
��� embarazo molar	639.2	v�ase adem�s categor�as 630-632
���� herida abierta penetrante de la cavidad	867.1	
��� despu�s de 		
���� aborto	639.2	
���� embarazo ect�pico o molar	639.2	
��� traumatismo obst�trico	665.5	
��� que afecta al feto o al reci�n nacido	763.8	
�� vesical (esf�nter)	867.0	
��� con herida abierta penetrante de la cavidad	867.1	
�� ves�cula biliar	868.02	
��� con herida abierta penetrante de la cavidad	868.12	
�� ves�cula seminal	867.6	
��� con herida abierta penetrante de la cavidad	867.7	
�� v�scera (abdominal)	868.00	"v�ase adem�s Traumatismo, interno, m�ltiple"
� con 		
"���� fractura, pelvis "		"v�ase Fractura, pelvis "
���� herida abierta penetrante de la cavidad	868.10	
��� tor�cica NCOC	862.8	"v�ase adem�s Traumatismo, interno, �rganos intrator�cicos"
���� con herida abierta penetrante de la cavidad	862.9	
�� yeyuno	863.29	
��� con herida abierta penetrante de la cavidad	863.39	
� intestino 		"v�ase Traumatismo, interno, intestino "
� intraabdominal (�rganos) NCOC 		"v�ase Traumatismo, interno, intraabdominal "
� intracraneal	854.0	
�� con 		
�� fractura de cr�neo 		"v�ase Fractura, cr�neo, por sitio "
��� herida intracraneal abierta	854.1	
�� contusi�n	851.8	
��� con herida intracraneal abierta	851.9	
��� cerebelo	851.4	
���� con herida intracraneal abierta	851.5	
��� corteza (cerebral)	851.0	
���� con herida intracraneal abierta	851.2	
��� ped�nculo cerebral	851.4	
���� con herida intracraneal abierta	851.5	
�� hematoma 		"v�ase Traumatismo, intracraneal, hemorragia "
�� hemorragia	853.0	
�� con 		
���� herida intracraneal abierta	853.1	
���� laceraci�n 		"v�ase Traumatismo, intracraneal, laceraci�n "
��� extradural	852.4	
���� con herida intracraneal abierta	852.5	
��� subaracnoidea	852.0	
���� con herida intracraneal abierta	852.1	
��� subdural	852.2	
���� con herida intracraneal abierta	852.3	
�� laceraci�n	851.8	
��� con herida intracraneal abierta	851.9	
��� cerebelo	851.6	
���� con herida intracraneal abierta	851.7	
��� corteza (cerebral)	851.2	
���� con herida intracraneal abierta	851.3	
��� ped�nculo cerebral	851.6	
���� con herida intracraneal abierta	851.7	
� intraocular 		"v�ase Traumatismo, globo del ojo, penetrante "
"� intrator�cico, �rganos (m�ltiple) "		"v�ase Traumatismo, interno, �rganos intrator�cicos "
� intrauterino 		"v�ase Traumatismo, interno, Intrauterino "
� iris	921.3	
�� penetrante 		"v�ase Traumatismo, globo del ojo, penetrante "
"� laberinto, o�do"	959.0	
� labio (boca)	959.0	
� labio vulvar (mayor) (menor)	959.1	
� laringe	959.0	
� latigazo (espina cervical)	847.0	
� lengua	959.0	
� ligamento 		
�� ancho 		"v�ase Traumatismo, interno ligamento, ancho "
�� redondo 		"v�ase Traumatismo, interno, ligamento, redondo "
�� sacroil�aco NCOC	959.1	
"� I�bulo, parietal "		"v�ase Traumatismo, intracraneal "
� lumbar (regi�n)	959.1	
�� plexo	953.5	
� lumbosacro (regi�n)	959.1	
�� plexo	953.5	
� mama	959.1	
� mand�bula	959.0	
"� mano(s), salvo dedos"	959.4	
"� materno, durante el embarazo, que afecta al feto o reci�n nacido"	760.5	
� maxila	959.0	
� mediast�nica (y otra(s) parte(s) del tronco)	911	
��� escapular (y brazo superior)	912	
��� esternal (y otra(s) parte(s) del tronco)	911	
�� pubiana (y otra(s) parte(s) del tronco)	911	
��� sacra (y otra(s) parte(s) del tronco)	911
"��� submaxilar (y otra(s) parte(s) de la cara, cuello y cuero cabelludo, salvo el ojo)"	910
"��� submentoniana (y otra(s) parte(s) de la cara, cuello y cuero cabelludo, salvo el ojo)"	910
"��� temporal (y otra(s) parte(s) de la cara, cuello y cuero cabelludo, salvo el ojo)"	910
�� antebrazo (y codo) (y mu�eca)	913
�� �rea periocular	918.0
"�� boca (y otra(s) parte(s) de la cara, cuello y cuero cabelludo, salvo el ojo)"	910
"�� cabeza (y otra(s) parte(s) de la cara, cuello y cuero cabelludo, salvo el ojo)"	910
"�� cadera (y tobillo, rodilla, pierna o muslo)"	916
"�� cara (cualquier parte(s), salvo ojo) (y cuello o cuero cabelludo)"	910
�� codo (y antebrazo) (y mu�eca)	913
�� costado (y otra(s) parte(s) del tronco)	911
"�� cuello (y cara o cuero cabelludo, cualquier parte(s), salvo el ojo)"	910
"�� cuero cabelludo (y otras parte(s) de la cara o cuello, salvo ojo)"	910
�� dedo(s) de 	
��� mano (u�a) (cualquiera)	915
��� pie (subungual) (u�a) (y pie)	917
�� diente(s)	521.2
"�� enc�as (y otra(s) parte(s) de la cara, cuello y cuero cabelludo, salvo el ojo)"	910
�� epid�dimo (y otra(s) parte(s) del tronco)	911
"�� epiglotis (y otra(s) parte(s) de la cara, cuello y cuero cabelludo, salvo el ojo)"	910
�� escler�tica	918.2
�� escroto (y otra(s) parte(s) del tronco)	911
"�� espacio popl�teo (y tobillo, cadera, pierna o muslo)"	916
�� extremidad 	
��� inferior (salvo el pie)	916
��� superior NCOC	913
"�� faringe (y otra(s) parte(s) de la cara, cuello y cuero cabelludo, salvo el ojo)"	910
"�� fosa supraclavicular (y otras partes de la cara, cuello, o cuero cabelludo, salvo ojo)"	910
"�� garganta (y otra(s) parte(s) de la cara, cuello y cuero cabelludo, salvo el ojo)"	910
�� globo (ojo)	918.9
�� hombro (y brazo superior)	912
�� ingle (y otra(s) parte(s) del tronco)	911
�� iris	918.9
�� labio vulvar (mayor) (menor) (y otra(s) parte(s) del tronco)	911
"�� labio(s) (boca) (y otra(s) parte(s) de la cara, cuello y cuero cabelludo, salvo el ojo)"	910
�� lagrimal (aparato) (gl�ndula) (saco)	918.0
"�� lengua (y otra(s) parte(s) de la cara, cuello y cuero cabelludo, salvo el ojo)"	910
�� mano(s) (salvo dedos s�lo)	914
�� m�ltiples sitios (no clasificables bajo la misma categor�a de tres d�gitos)	919
"�� muslo (y tobillo, cadera, rodilla, o pierna)"	916
�� mu�eca (y codo) (y antebrazo)	913
"�� nariz (tabique) (y otra(s) parte(s) de la cara, cuello y cuero cabelludo, salvo el ojo)"	910
"�� nasal (tabique) (y otra(s) parte(s) de la cara, cuello y cuero cabelludo, salvo el ojo)"	910
"�� o�do(s) (canal) externo) (pabell�n) (t�mpano) (y otra(s) parte(s) de la cara, cuello y cuero cabelludo, salvo el ojo)"	910
�� ojo(s) (y anexos) NCOC	918.9
"�� paladar (blando) (y otra(s) parte(s) de la cara, cuello y cuero cabelludo, salvo el ojo)"	910
�� p�rpado(s) (y �rea periocular)	918.0
�� pene (y otra(s) parte(s) del tronco)	911
�� perineo (y otra(s) parte(s) del tronco)	911
�� pie (falanges) (y dedos(s) del pie)	917
�� piel NCOC	919
"�� pierna (inferior) (superior) (y tobillo, cadera, rodilla o muslo)"	916
�� prepucio (y otra(s) parte(s) del tronco)	911
� pudendo (y otra(s) parte(s) del tronco)	911
�� pulgar(es) (u�a)	915
"�� rodilla (y tobillo, cadera, pierna o muslo)"	916
"�� salival (conductos) (gl�ndulas) (y otra(s) parte(s) de la cara, cuello y cuero cabelludo, salvo el ojo)"	910
"�� sien (y otra(s) parte(s) de la cara, cuello y cuero cabelludo, salvo el ojo)"	910
�� sitio(s) especificado(s) NCOC	919
�� subconjuntival	918.2
�� subcut�neo NCOC	919
�� supraorbital	918.0
�� tal�n (y pie o dedo de pie)	917
�� test�culo (y otra(s) parte(s) del tronco)	911	
"�� t�mpano, membrana timp�nica (y otra(s) parte(s) de la cara, cuello y cuero cabelludo, salvo el ojo)"	910	
"�� t�rax, tor�cico (externo) (y otra(s) parte(s) del tronco)"	911	
�� tronco (cualquier parte(s))	911	
�� t�nica vaginal (y otra(s) parte(s) del tronco)	911	
"�� �vula (y otra(s) parte(s) de la cara, cuello y cuero cabelludo, salvo el ojo)"	910	
�� vagina (y otra(s) parte(s) del tronco)	911	
�� vulva (y otra(s) parte(s) del tronco)	911	
� mediastino 		"v�ase Traumatismo, interno, mediastino "
� m�dula espinal 		"v�ase Traumatismo, espinal, por sitio "
� mejilla	959.0	
� membrana 		
�� cerebro	854.0	"v�ase adem�s Traumatismo, intracraneal"
�� timp�nica	959.0	
� Membrana de Descemet 		"v�ase Traumatismo, globo del ojo, penetrante "
� meninges (cerebrales) 		"v�ase Traumatismo, intracraneal "
� ment�n	959.0	
� mesent�rica 		
�� arteria 		"v�ase Traumatismo, vaso sangu�neo, mesent�rico, arteria "
�� plexo inferior	954.1	
�� vena 		"v�ase Traumatismo, vaso sangu�neo, mesent�rico, vena "
� mesenterio 		"v�ase Traumatismo, interno, mesenterio "
� mesosalpinx 		"v�ase Traumatismo, interno, mesosalpinx "
� m�ltiple (sitios no clasificables bajo la misma categor�a de cuatro d�gitos dentro de 959.0-959.7)	959.8	
�� extremo NCOC	869.1	
"� musculocut�neo, nervio"	955.4	
� muslo (y cadera)	959.6	
� mu�eca (y codo) (y antebrazo)	959.3	
� Nacido	665.9	"v�ase adem�s Nacimiento, traumatismo materno NCOC"
� nacimiento 		"v�ase adem�s Nacimiento, traumatismo "
"�� canal NCOC, cuando complica el parto"	665.9	
� nalga	959.1	
� nariz (tabique)	959.0	
� nasal (seno) (tabique)	959.0	
� nasofaringe	959.0	
� nervio	957.9	
� con implicaci�n de varias partes del cuerpo	957.8	
�� abductor	951.3	
�� accesorio	951.6	
�� ac�stico	951.5	
�� antebrazo	955.9	
�� auditivo	951.5	
�� axilar	955.0	
"�� braquial, plexo"	953.4	
�� brazo	955.9	"v�ase adem�s Traumatismo, nervio, miembro superior"
"�� cabeza y cuello, superficial"	957.0	
"�� cervical, simp�tico"	954.0	
�� ci�tico	956.0	
��� muslo	956.0
�� cintura 	
��� escapular	955.9
���� m�ltiple	955.8
���� sitio especificado NCOC	955.7
��� pelviana	956.9
���� m�ltiples sitios	956.8
���� sitio especificado NCOC	956.5
�� cintura escapular	955.9
��� m�ltiple 	
��� sitio especificado NCOC	955.7
�� craneal	951.9
��� cuarto o troclear	951.1
"��� d�cimo, neumog�strico o vago"	951.8
��� duod�cimo o hipogloso	951.7
��� noveno o glosofar�ngeo	951.8
"��� octavo, ac�stico o auditivo"	951.5
��� primero u olfatorio	951.8
��� quinto o trig�mino	951.2
��� reci�n nacido	767.7
��� segundo u �ptico	950.0
��� s�ptimo o facial	951.4
��� sexto o abductor	951.3
��� tercero u oculomotor	951.0
��� und�cimo o espinal	951.6
"�� cnural anterior, femoral"	956.1
�� cubital	955.2
��� antebrazo	955.2
��� mu�eca (y brazo)	955.2
�� cut�neo sensorial 	
��� miembro 	
���� inferior	956.4
���� superior	955.5	
�� dedo de 		
��� mano	955.9	
��� pie	956.9	
�� digital (dedo de mano)	955.6	
��� dedo de pie	956.5	
�� espinal	953.9	
�� plexo 		"v�ase Traumatismo, plexo, espinal "
��� ra�z	953.9	
���� cervical	953.0	
���� dorsal	953.1	
���� lumbar	953.2	
���� m�ltiples sitios	953.8	
���� sacra	953.3	
�� espl�cnico	954.1	
�� facial	951.4	
��� reci�n nacido	767.5
�� femoral	956.1
�� glosofar�ngeo	951.8
�� hipogloso	951.7
"�� lumbar, plexo"	953.5
"�� lumbosacro, plexo"	953.5
�� mano y mu�eca	955.9
�� mediano	955.1
��� antebrazo	955.1
��� mu�eca y mano	955.1
�� miembro 	
��� inferior	956.9
���� m�ltiple	956.8
���� sitio especificado NCOC	956.5
��� superior	955.9
���� m�ltiple	955.8
����� sitio especificado NCOC	955.7
�� m�ltiple (en varias partes del cuerpo) (sitios no clasificables bajo la misma categor�a de tres d�gitos)	957.8
�� musculocut�neo	955.4
�� musculospiral	955.3
��� brazo superior	955.3
�� muslo	956.9
�� mu�eca y mano	955.9
�� neumog�strico	951.8
�� oculomotor	951.0
�� olfatorio	951.8
�� �ptico	950.0
�� perif�rica	957.9
��� m�ltiple (en varias regiones) (sitios no clasificables bajo la misma categor�a de tres d�gitos)	957.8
��� sitio especificado NCOC	957.1
�� peroneal	956.3
��� pierna inferior	956.3
��� tobillo y pie	956.3	
�� pie y tobillo	956.9	
�� pierna	956.9	"v�ase adem�s Traumatismo, nervio, miembro inferior"
�� plantar	956.5	
�� plexo	957.9	
��� cel�aco	954.1	
��� espinal	953.9	
���� braquial	953.4	
��� lumbar	953.5	
���� lumbosacro	953.5	
���� m�ltiples sitios	953.8	
"��� mesent�rico, inferior"	954.1	
��� simp�tico NCOC	954.1	
�� radial	955.3	
��� mu�eca y mano	955.3	
"�� sacro, plexo"	953.5	
�� simp�tico NCOC	954.1
��� cervical	954.0
�� sitio especificado NCOC	957.1
�� tibial	956.5
�� pierna inferior	956.5
��� posterior	956.2
��� tobillo y pie	956.2
�� tobillo y pie	956.9
�� trig�mino	951.2
�� troclear	951.1
"� tronco, salvo cinturas peIviana y escapular"	954.9
��� simp�tico NCOC	954.8
��� sitio especificado NCOC	954.8
�� vago	951.8
� obst�trico NCOC	665.9
�� que afecta al feto o al reci�n nacido	763.8
� occipital (regi�n) (cuero cabelludo)	959.0	
�� I�bulo	854.0	"v�ase adem�s Traumatismo, intracraneal"
"� ocular, globo"	921.3	"v�ase adem�s Traumatismo, globo del ojo"
� o�do (canal) (externo) (pabell�n) (t�mpano)	959.0	
� o�do medio	959.0	
� ojo	921.9	
�� penetrante del globo del ojo 		"v�ase Traumatismo, globo del ojo, penetrante "
�� superficial	918.9	
� �ptico	950.9	
�� corteza	950.3	
�� nervio	950.0	
�� quiasma	950.1	
�� v�as �pticas	950.2	
"� �rbita, orbital (regi�n)"	921.2	
�� penetrante	870.3	
��� con cuerpo extra�o	870.4	
� orbital	918.0	
� �rgano(s) genital(es) 		
��� aborto 		"v�ase Aborto, por tipo, con, da�o a �rganos pelvianos "
��� embarazo ect�pico	639.2	v�ase adem�s categor�as 633.0-633.9
��� embarazo molar	639.2	v�ase adem�s categor�as 630-632
�� despu�s de 		
��� aborto	639.2	
��� embarazo ect�pico o molar	639.2	
�� externo	959.1	
�� interno 		"v�ase Traumatismo, interno, �rganos genitales "
�� traumatismo obst�trico NCOC	665.9	
��� que afecta al feto o al reci�n nacido	763.8	
� ovario 		"v�ase Traumatismo, interno, ovario "
� pabell�n de la oreja	959.0	
� paladar (blando)	959.0	
� p�ncreas 		"v�ase Traumatismo, interno, p�ncreas "
� paratiroides (gl�ndula)	959.0	
� parietal (regi�n) (cuero cabelludo)	959.0	
�� I�bulo 		"v�ase Traumatismo, intracraneal "
� p�rpado(s)	921.1	
�� laceraci�n 		"v�ase Laceraci�n, p�rpado "
�� superficial	918.0	
� parto 	665.9	"v�ase adem�s Nacimiento, traumatismo materno NCOC"
� pelviano 		
"�� articulaci�n o ligamento, cuando complica el parto"	665.6	
�� que afecta al feto o al reci�n nacido	763.8	
�� �rganos 		"v�ase adem�s Traumatismo, interno, pelvis "
�� con 		
���� aborto 		"v�ase Aborto, por tipo, con da�o a �rganos pelvianos "
���� embarazo ect�pico 	639.2	v�ase adem�s categor�as 633.0-633.9
���� embarazo molar	639.2	v�ase adem�s categor�as 630-632
��� despu�s de 		
���� aborto	639.2	
���� embarazo ect�pico o molar 	639.2	
��� traumatismo obst�trico	665.5	
���� que afecta al feto o al reci�n nacido      	763.8	
�� suelo	959.1	
��� cuando complica el parto	664.1	
���� que afecta al feto o al reci�n nacido      	763.8	
� pelvis	959.1	
� pene	959.1	
� perineo	959.1	
� peritoneo 		"v�ase Traumatismo, interno, peritoneo "
"� pie (y tobillo) (y rodilla) (y pierna, salvo muslo)"	959.7	
� piel NCOC	959.9	
� pierna salvo muslo (y tobillo) (y pie) (y rodilla)	959.7	
�� superior o muslo	959.6	
� pistola 		
�� de pintar 		"v�ase Herida, abierta, por sitio, complicada "
�� engrasadora 		"v�ase Herida, abierta, por sitio, complicada "
� pleura 		"v�ase Traumatismo, interno pleura "
� plexo braquial	953.4	
�� reci�n nacido	767.6	
� prepucio	959.1	
� pr�stata 		"v�ase Traumatismo, interno, pr�stata "
� pudendo	959.1	
� pulgar(es) (u�a)	959.5	
� pulm�n 		"v�ase Traumatismo, interno, pulm�n "
� radiaci�n NCOC	990	
� rayos 		
�� cat�dicos	990	
�� infrarrojos NCOC	990	
�� roentgen NCOC	990	
�� ultravioletas NCOC	990	
�� X NCOC	990	
� rayos infrarrojos NCOC	990	
� rayos roentgen NCOC	990	
� rayos ultravioletas NCOC	990	
� recto 		"v�ase Traumatismo, interno, recto "
� regi�n 		
�� costal	959.1	
�� epig�strica	959.1	
�� escapular	959.2	
�� esternal	959.1	
�� il�aca	959.1	
�� interscapular	959.1	
�� malar	959.0	
�� mastoidea	959.0	
�� mesotor�cica	959.1	
�� pubiana	959.1	
�� submaxilar	959.0	
�� submental	959.0	
�� temporal	959.0	
� regi�n escapular	959.2	
� regi�n esternal	959.1	
� regi�n il�aca	959.1	
� regi�n intercapsular	959.1	
� regi�n mastoidea	959.0	
� regi�n mediastina	959.1	
� regi�n molar	959.0	
� regi�n p�bica	959.1	
� regi�n submentoniana	959.0	
� retina	921.3	
�� penetrante 		"v�ase Traumatismo, globo del ojo, penetrante "
� retroperitoneal 		"v�ase Traumatismo, interno, retroperitoneo "
� ri��n 		"v�ase Traumatismo, interno, ri��n "
"� rodilla (y tobillo) (y pie) (y pierna, salvo muslo)"	959.7	
� sacro (regi�n)	959.1	
�� plexo	953.5	
� seno 		
�� cavernoso	854.0	"v�ase adem�s Traumatismo, intracraneal"
�� nasal	959.0	
� seno cavernoso	854.0	"v�ase, adem�s, Traumatismo, intracraneal"
�� nasal	959.0	
� sien	959.0	
� s�nfisis del pubis	959.1	
�� cuando complica el parto	665.6	
��� que afecta al feto o al reci�n nacido   	763.8	
"� sistema nervioso, difuso"	957.8	
� sitio especificado NCOC	959.8	
� subconjuntival	921.1	
� subcut�neo	959.9	
� subdural 		"v�ase Traumatismo, intracraneal "
"� subungual, dedos de "		
�� mano	959.5	
�� pie	959.7	
� superficial	919	
"�� abdomen, abdominal (m�sculo) (pared) (cualquier otra(s) parte(s) del tronco)"	911	
�� ano (y otra(s) parte(s) del tronco)	911	
�� axila (y brazo superior)	912	
�� brazo	913	
��� superior (y hombro)	912	
"�� canto, ojo"	918.0	
�� cl�toris (y otra(s) parte(s) del tronco)	911	
"�� conducto auditivo (externo) (meato) (y otra(s) parte(s) de la cara, cuello o cuero cabelludo, salvo el ojo)"	910	
�� conjuntiva	918.2	
�� c�rnea	918.1	
�� espalda (y otra(s) parte(s) del tronco)	911	
"�� frente (y otra(s) parte(s) de la cara, cuello o cuero cabelludo, salvo el ojo)"	910
�� mama (y otra(s) parte(s) del tronco)	911
"�� mejilla(s) (y otra(s) parte(s) de la cara, cuello y cuero cabelludo, salvo el ojo)"	910
"�� ment�n (y otra(s) parte(s) de la cara, cuello y cuero cabelludo, salvo el ojo)"	910
�� nalga (y otra(s) parte(s) del tronco)	911
�� pared tor�cica (y otra(s) parte(s) del tronco)	911
�� regi�n 	
��� costal (y otra(s) parte(s) del tronco)	911
��� epig�strica (y otra(s) parte(s) del tronco)	911
��� il�aca (y otra(s) parte(s) del tronco)	911
��� interescapular (y otra(s) parte(s) del tronco)	911
��� lumbar (y otra(s) parte(s) del tronco)	911
"��� malar (y otra(s) parte(s) de la cara, cuello y cuero cabelludo, salvo el ojo)"	910
"��� mastoidea (y otra(s) parte(s) de la cara, cuello y cuero cabelludo, salvo el ojo)"	910
��� mesotor�cica (y otra(s) parte(s) del tronco)	911
"��� occipital (y otra(s) parte(s) de la cara, cuello y cuero cabelludo, salvo el ojo)"	910
� orbital	918.0	
"� parietal (y otra(s) parte(s) de la cara, cuello y cuero cabelludo, salvo el ojo)"	910	
"�� tobillo (y cadera, rodilla, pierna o muslo) "	916	
� supraorbital	959.0	
� suprarrenal (gl�ndula) 		"v�ase Traumatismo, interno, suprarrenal "
� sustancia radiactiva o radio NCOC	990	
� tabique rectovaginal	959.1	
� tal�n	959.7	
� tejido 		
�� periuretral 		
��� con 		
���� aborto 		"v�ase Aborto, por tipo, con da�o a �rganos pelvianos "
���� embarazo ect�pico	639.2	v�ase adem�s categor�as 633.0-633.9
���� embarazo molar	639.2	v�ase adem�s categor�as 630-632
��� cuando complica el parto 	665.5	
���� que afecta al feto o al reci�n nacido	763.8	
��� despu�s de 		
���� aborto	639.2	
���� embarazo ect�pico o molar	639.2	
� test�culo	959.1	
� timo 		"v�ase Traumatismo, interno, timo "
"� t�mpano, membrana timp�nica"	959.0	
� tiroides (gl�ndula)	959.0	
"� tobillo (y pie) (y rodilla) (y pierna, salvo muslo)"	959.7	
� t�rax 	959.1	"v�ase adem�s Traumatismo, interno, pared tor�cica"
"� t�rax, tor�cico (externo)"	959.1	
�� cavidad 		"v�ase Traumatismo, interno, t�rax "
�� interno 		"v�ase Traumatismo, interno, �rganos intrator�cicos "
� torcedor 		"v�ase Aplastamiento, por tipo "
� tracto gastrointestinal 		"v�ase Traumatismo, interno, tracto, gastrointestinal "
� tr�quea 		"v�ase Traumatismo, interno, tr�quea "
� trompa de 		
�� Eustaquio	959.0	
�� Falopio 		"v�ase Traumatismo, interno, trompa de Falopio "
� tronco	959.1	
� t�nica vaginal	959.1	
� ur�ter 		"v�ase Traumatismo, interno, ur�ter "
� uretra (esf�nter) 		"v�ase Traumatismo, interno, uretra "
� �tero 		"v�ase Traumatismo, interno, �tero "
� �vula	959.0	
� u�a de 		
�� dedo de 		
��� mano	959.5	
��� pie	959.7	
� vagina	959.1	
� vascular 		"v�ase Traumatismo, vaso sangu�neo "
� vaso sangu�neo NCOC	904.9	
�� abdomen	902.9	
��� especificado NCOC	902.89
��� m�ltiple	902.87
�� aorta (tor�cica)	901.0
��� abdominal	902.0
�� arteria 	
��� car�tida	900.00
���� com�n	900.01
���� externa	900.02
���� interna	900.03
��� cel�aca	902.20
���� rama especificada NCOC	902.24
��� palmar	903.4
�� axilar	903.00
��� arteria	903.01
��� vena	903.02
�� braquial (arteria) (vena)	903.1
�� brazo NCOC	903.9	
�� bronquial	901.89	
�� cabeza	900.9	
��� especificado NCOC	900.89	
��� intracraneal 		"v�ase Traumatismo, intracraneal "
��� m�ltiple	900.82	
�� c�stico 		
��� arteria	902.24	
��� vena	902.39	
�� c�lica derecha	902.26	
�� cubital (arteria) (vena)	903.3	
�� cuello	900.9	
��� especificado NCOC	900.89	
��� m�ltiple	900.82	
�� debido a punci�n o laceraci�n accidental durante procedimiento	998.2	
�� digital (arteria) (vena)	903.5	
�� espl�nico 	
��� arteria	902.23
��� vena	902.34
�� extremidad 	
��� inferior	904.8
�� especificado NCOC	904.7
���� m�ltiple	904.7
��� superior	903.9
���� especificado NCOC	903.8
���� m�ltiple	903.8
�� femoral 	
��� arteria (superficial)	904.1
���� com�n	904.0
���� encima de origen profundo	904.0
��� vena	904.2
�� g�strico 	
��� arteria	902.21
��� vena	902.39
�� hep�tico 	
��� arteria	902.22
��� vena	902.11
�� hipog�strico	902.59
��� arteria	902.51
��� vena	902.52
�� ileoc�lico 	
��� arteria	902.26
��� vena	902.31
�� il�aco	902.50
��� arteria	902.53
��� rama especificada NCOC	902.59
��� vena	902.54
�� innominado 	
��� arteria	901.1
��� vena	901.3
�� intercostal (arteria) (vena)	901.81
�� mamario (arteria) (vena)	901.82
�� mesent�rico 	
��� arteria	902.20
���� inferior	902.27
���� rama especificada NCOC	902.29
���� superior (tronco)	902.25
"����� ramas, primaria"	902.26
��� vena	902.39
���� inferior	902.32
���� superior (y subdivisiones primarias)   	902.31
�� ov�rico	902.89
��� arteria	902.81
��� vena	902.82
�� pelvis 	
��� especificado NCOC	902.89
��� m�ltiple	902.87
�� pierna NCOC	904.8
� plantar (profundo) (arteria) (vena)	904.6
� plantar profundo	904.6
�� popl�teo	904.40
��� arteria	904.41
��� vena	904.42
�� porta	902.33
�� pulmonar	901.40
��� arteria	901.41
��� vena	901.42
�� radial (arteria) (vena)	903.2
�� renal	902.40
��� arteria	902.41
��� especificado NCOC	902.49
��� vena	902.42
�� safeno 	
��� arteria	904.7
��� vena (mayor) (menor)	904.3
�� subclavio 	
��� arteria	901.1
��� vena	901.3
�� suprarrenal	902.49
�� tibial	904.50
��� arteria	904.50
���� anterior	904.51
���� posterior	904.53
��� vena	904.50
���� anterior	904.52
���� posterior	904.54
�� tor�cico	901.9
� especificado NCOC	901.89
��� m�ltiple	901.83
�� uterino	902.39
��� arteria	902.55
��� vena	902.56
�� vena 	
��� �cigos	901.89
��� bas�lica	903.1
��� cava 	
���� inferior	902.10
����� ramas especificadas NCOC	902.19
���� superior	901.2
��� cef�lica (brazo)	903.1
��� hemi�cigos	901.89
��� yugular (externa)	900.81
���� interna	900.1	
� vejiga (esf�nter) 		"v�ase Traumatismo, interno, vejiga "
� vena	904.9	"v�ase adem�s Traumatismo, vaso sangu�neo, por sitio"
�� cava 		
��� inferior	902.10	
��� superior	901.2	
� vesical (esf�nter)		"v�ase Traumatismo, interno, vesical "
� ves�cula biliar 		"v�ase Traumatismo, interno, ves�cula biliar "
� ves�cula seminal 		"v�ase Traumatismo, interno, ves�cula seminal "
� v�scera (abdominal) 		"v�ase adem�s Traumatismo, interno, v�scera "
"�� con fractura, peIvis "		"v�ase Fractura, pelvis "
� visual	950.9	
�� corteza	950.3	
� v�treo (humor)	871.2	
� vulva	959.1	
� yeyuno 		"v�ase Traumatismo, interno, yeyuno"
"Travesura, maliciosa, de ni�o"	312.0	"v�ase adem�s Perturbaci�n, conducta"
"Treacher Collins, s�ndrome de (disostosis facial incompleta)"	756.0	
"Treitz, hernia de "		"v�ase Hernia, de Treitz"
Trematodiasis NCOC	121.9	
Trematodos o duelas NCOC	121.9	"v�ase adem�s Infestaci�n, trematodos o duelas"
� h�gado	121.3	
� sangre NCOC	120.9	"v�ase adem�s Infestaci�n, esquistosoma"
"Trematodos, infestaci�n por NCOC"	121.9	
"Trenes, mareo de los"	994.6	
"Treponema pallidum, infecci�n por"	097.9	v�ase adem�s S�filis
Treponematosis	102.9	
� debida a 		
�� T. pallidum 		v�ase S�filis 
�� T. pertenue (frambesia)	102.9	v�ase adem�s Frambesia
"Tres d�as, fiebre de"	066.0	
Tr�ada de 		
� Kartagener	759.3	
� Reiter (completa) (incompleta)	099.3	
� Saint	553.3	"v�ase adem�s Hernia, diafragma"
Trichinella spiralis (infecci�n) (infestaci�n) 	124	
"Trichocephalus, infestaci�n por"	127.3	
"Trichostrongylus (instabilis), infecci�n por"	127.6	
Trichuris trichiuria (cualquier sitio) (infecci�n) (infestaci�n)	127.3	
Trichuris trichuriura	127.3	
Tricobezoar	938	
� est�mago	935.2	
� intestino 	936	
Tricocefaliasis	127.3	
"Tricoc�falo, infestaci�n por"	127.3	
Tricocefalosis	127.3	
Tricoclasis	704.2	
Tricoepitelioma 	(M8100/0)	"v�ase adem�s Neoplasia, piel, benigna "
� maligno 	(M8100/3)	"v�ase Neoplasia, piel, maligna "
� mama	217	
� �rgano genital NCOC 		"v�ase Neoplasia, por sitio, benigna  "
Tricof�tide		v�ase Dermatofitosis 
Tricofitobezoar	938	
� est�mago	935.2	
� intestino	936	
Tricofitosis 		v�ase Dermatofitosis  
Tricofoliculoma	(M8101/0)	"v�ase Neoplasia, piel, benigna "
Tricolemoma 	(M8102/0)	"v�ase Neoplasia, piel, benigna"
Tricomatosis	704.2	
Tricomicosis	039.0	
� axilar	039.0	
� nodular	111.2	
� nudosa	111.2	
� rubra	039.0	
Tricomoniasis	131.9	
� cuello uterino	131.09	
� intestinal	007.3	
� pr�stata	131.03	
� sitio especificado NCOC	131.8	
� uretra	131.02	
� urogenital	131.00	
� vagina	131.01	
� vejiga	131.09	
� ves�cula seminal	131.09	
� vulva	131.01	
� vulvogenital	131.01	
Triconocardiosis (axilar) (palmellina)	039.0	
Triconodosis	704.2	
"Tricophytid, Tricophyton, infecci�n por"	110.9	v�ase adem�s Dermatofitosis
Tricoptilosis	704.2	
Tricorrexis (nudosa)	704.2	
Tricosporosis nudosa	111.2	
Tricostasis espinulosa (cong�nita)	757.4	
Tricostrongiliasis (intestino delgado)	127.6	
Tricostrongilosis	127.6	
Tricotiloman�a	300.3	
Tricromato an�malo (cong�nito)	368.59	
Tricromatopsia an�mala (cong�nita)	368.59	
Tricuriasis	127.3	
Tric�spide (v�lvula) 		v�ase enfermedad espec�fica  
Tr�fido		v�ase adem�s Accesorio 
� lengua	750.13	
� ri��n (pelvis)	753.3	
Trigeminia	427.89	
� posoperatoria	997.1	
"Trig�mino, neuralgia de"	350.1	"v�ase adem�s Neuralgia, trig�mino"
Trigeminoencefaloangiomatosis	759.6	
Trigonitis (vejiga) (cr�nica) (seudomembranosa)	595.3	
� tuberculosa 	016.1	v�ase adem�s Tuberculosis
Trigonocefalia	756.0	
Trihexosidosis	272.7	
"Trilladoras, pulm�n de"	495.0	
Trillizo(s) 		
� afectados por complicaciones maternas del embarazo	761.5	
� embarazo (cuando complica el parto)	651.1	
�� con p�rdida fetal y retenci�n de uno o m�s fetos	651.4	
� nacido sano 		"v�ase Reci�n nacido, m�ltiple"
"Trilobular, placenta "	745.8	"v�ase Placenta, anormal Trilocular, coraz�n"
"Trincheras, de las "		
� boca	101	
� fiebre	083.1	
� nefritis 		"v�ase Nefritis, aguda "
� pie	991.4	
"Tripanosoma, infestaci�n por "		v�ase Tripanosomiasis 
Tripanosomiasis	086.9	
� con meningoencefalitis	086.9[323.2]	
� africana	086.5	
�� debida a Trypanosoma	086.5	
��� gambiense	086.3	
��� rhodesiense	086.4	
� americana	086.2	
�� con 		
��� implicaci�n de otro �rgano	086.1	
��� implicaci�n del coraz�n	086.0	
�� sin menci�n de implicaci�n de �rganos 	086.2	
� brasile�a 		"v�ase Tripanosomiasis, americana "
"� Chagas, de "		"v�ase Tripanosomiasis, americana "
� debida a Trypanosoma 		
�� cruzi 		"v�ase Tripanosomiasis, americana "
�� gambiense	086.3	
�� rhodesiense	086.4	
"� gambiensis, de la Gambia"	086.3	
� norteamericana 		"v�ase Tripanosomiasis, americana "
"� rhodesiensis, rodesiana"	086.4	
� sudamericana 		"v�ase Tripanosomiasis, Americana"
"Tripartita, placenta "		"v�ase Placenta, anormal"
Triple 		v�ase Accesorio 
� cromosoma X femenino	758.8	
� ri��n	753.3	
� �tero	752.2	
Triplej�a	344.8	
� cong�nita o infantil	343.8	
Triplicaci�n 		v�ase Accesorio
Triquiasis	704.2	
� cicatricial	704.2	
"� p�rpado, palpebral"	374.05	
�� con entropi�n	374.00	v�ase adem�s Entropi�n
Triquineliasis	124	
Triquinelosis	124	
Triquiniasis	124	
Triquinosis	124	
Trismo	037	v�ase adem�s T�tanos 
Trismo	781.0	
� neonatorum	771.3	
� reci�n nacido	771.3	
Trisom�a (s�ndrome) NCOC	758.5	
� 13 (parcial)	758.1	
� 16		
� 18	758.2	
� 18 (parcial)	758.2	
� 21 (parcial)	758.0	
� 22	758.0	
� autos�mica NCOC	758.5	
� D1	758.1	
� E3	758.2	
� G (grupo)	758.0	
� grupo 		
�� D1	758.1	
�� E	758.2	
�� G	758.0	
Tritanomal�a	368.53	
Tritanop�a	368.53	
Trofedema (hereditario)	757.0	
� cong�nito	757.0	
"Trofobl�stica, enfermedad"	630	v�ase adem�s Mola hidatiforme
"� previa, cuando afecta la atenci�n del embarazo"	V23.1	
Trofolinfedema	757.0	
Trofoneurosis NCOC	356.9	
� brazo NCOC	354.9	
� diseminada	710.1	
� extremidad 		
�� inferior NCOC	355.8	
�� superior NCOC	354.9	
� facial	349.89	
� pierna NCOC	355.8	
"Troisier-Hanot-Chauffard, s�ndrome de (diabetes bronceada)"	275.0	
Trombastenia (de Glanzmann) (hemorr�gica) (hereditaria)	287.1	
Trombidosis	133.8	
Trombo 		v�ase Trombosis
Tromboangi�tis	443.1	
� obliterante (generalizada)	443.1	
�� cerebral	437.1	
�� vasos 		
��� cerebrales	437.1	
��� m�dula espinal	437.1	
Tromboarteritis 		v�ase Arteritis
Tromboastenia (de Glanzmann) (hemorr�gica) (hereditaria)	287.1	
Trombocitastenia (de Glanzmann)	287.1	
Trombocitemia (esencial) (hemorr�gica) (primaria) 	238.7(M9962/1)	
� idiop�tica 	238.7(M9962/1)	
Trombocitopat�a (distr�fica) (granulop�nica)	287.1	
"Trombocitopenia, trombocitop�nico"	287.5	
� con hemangioma gigante	287.3	
"� amegacarioc�tica, cong�nita"	287.3	
� asociada al sexo	287.3	
� c�clica	287.3	
� cong�nita	287.3	
� debida a 		
�� aloinmunizaci�n de plaquetas	287.4	
�� circulaci�n extracorp�rea de la sangre	287.4	
�� f�rmacos	287.4	
�� transfusi�n masiva de sangre	287.4	
� esencial	287.3	
� hereditaria	287.3	
� Kasabach-Merritt	287.3	
"� neonatal, transitoria"	776.1	
�� debida a 		
��� isoinmunizaci�n	776.1	
��� transfusi�n de recambio	776.1	
��� trombocitopenia materna idiop�tica	776.1	
� por hemodiluci�n	287.4	
� primaria	287.3	
"� puerperal, posparto"	666.3	
� p�rpura	287.3	"v�ase adem�s P�rpura, trombocitop�nica"
�� tromb�tica	446.6
� secundaria	287.4
