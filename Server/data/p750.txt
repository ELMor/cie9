Uretrorrea	788.7	
Uretrorrectal 		v�ase enfermedad espec�fica
"Uretrostom�a, estado de"	V44.6	
� con complicaci�n	997.5	
Uretrotrigonitis	595.3	
Uretrovaginal 		v�ase enfermedad espec�fica
"Uretrovesical paramediano, orificio"	753.8	
Uricacidemia	790.6	
Uricemia	790.6	
Uricosuria	791.9	
Uridrosis	705.89	
Urinemia 		v�ase Uremia
Urinoma NCOC	599.9	
� renal	593.89	
� rin�n	593.89	
� ur�ter	593.89	
� uretra	599.8
� vejiga	596.8
Uroartritis infecciosa	099.3
Urodi�lisis	788.5
Urolitiasis	592.9
Uronefrosis	593.89
Uropat�a	599.9
� obstructiva	599.6
Urosepsis	599.0
Urticaria	708.9
� con edema angioneur�tico	995.1
�� hereditaria	277.6
� al�rgica	708.0
� colin�rgica	708.5
� cr�nica	708.8
� debida a 	
�� alimento	708.0
�� drogas o f�rmacos	708.0
�� fr�o o calor	708.2
�� medicamento para inhalar	708.0
�� plantas	708.8
�� suero	999.5
� dermatogr�fica	708.3
� facticia	708.3
� fr�a familiar	708.2
� gigante	995.1
�� hereditaria	277.6
� idiop�tica	708.1
� laringe	995.1
�� hereditaria	277.6
� medicamentosa	693.0
�� debida a f�rmaco aplicado a la piel	692.3
� neonatorum	778.8	
� no al�rgica	708.1	
� papulosa (Hebra)	698.2	
� peri�dica recurrente	708.8	
� persistente hemorr�gica	757.39	
� pigmentosa	757.33	
� reci�n nacido	778.8	
� solar	692.71	
� suero	999.5	
� t�rmica (fr�o) (calor)	708.2	
� tipo especificado NCOC	708.8	
� vibratoria	708.4	
"Urticarioides, acarodermatitis"	133.9	
"Usher-Senear, enfermedad de (p�nfigo eritematoso)"	694.4	
Uso de 		
� espec�ficos o remedios de patente	305.9	"v�ase adem�s Abuso, drogas, sin dependencia"
� f�rmacos no recetados	305.9	"v�ase adem�s Abuso, drogas, sin dependencia"
Uso incorrecto de drogas NCOC	305.9	"v�ase adem�s Abuso, drogas, sin dependencia"
Uta	085.5	
Utero 		v�ase enfermedad espec�fica
Utero de forma anormal 		"v�ase Anomal�a, �tero"
Utero grav�dico 		v�ase enfermedad espec�fica
Uteromegalia	621.2	
Uterovaginal 		v�ase enfermedad espec�fica
Uterovesical 		v�ase enfermedad espec�fica
Utriculitis (utr�culo prost�tico)	597.89	
Uveal 		v�ase enfermedad espec�fica
Uve�tis (anterior)	364.3	v�ase adem�s lridociclitis
� aguda o subaguda	364.00	
�� debida a o asociada con  		
� herpes (simple)	054.44	
�� zoster	053.22	
��� infecci�n gonoc�cica	098.41	
�� primaria	364.01	
�� recurrente	364.02	
�� secundaria (no infecciosa)	364.04	
��� infecciosa	364.03	
� al�rgica	360.11	
� cr�nica	364.10	
�� debida a o asociada con 		
��� sarcoidosis	135[364.11]	
��� tuberculosis	017.3[364.11]	v�ase adem�s Tuberculosis
� debida a 		
�� operaci�n	360.11	
�� toxoplasmosis (adquirida)	130.2	
��� cong�nita (activa)	771.2	
� granulomatosa	364.10	
� heterocr�mica	364.21	
� inducida por lentes	364.23	
� no granulomatosa	364.00	
� posterior	363.20	
�� diseminada 		"v�ase Coriorretinitis, diseminada "
�� focal 		"v�ase Coriorretinitis, focal "
� recurrente	364.02	
� sifil�tica (secundaria)	091.50	
�� cong�nita	090.0[363.13]	
�� tard�a	095.8[363.13]	
� simp�tica	360.11	
� tuberculosa	017.3[364.11]	v�ase adem�s Tuberculosis  
Uveoencefalitis	363.22	
"Uveopar�tida, fiebre "	135	
Uveoparotiditis	135	
Uveoqueratitis	364.3	v�ase adem�s Iridociclitis   
Uvula 		v�ase enfermedad espec�fica
Uvulitis (aguda) (catarral) (cr�nica) (gangrenosa) (membranosa) (supurativa) (ulcerativa)	528.3	
" Vaciamiento r�pido del est�mago, s�ndrome de (posgastrectom�a)"	564.2	
� no quir�rgico	536.8	
Vac�o 		
� en senos nasales o paranasales	473.9	v�ase adem�s Sinusitis
� extracci�n del feto o reci�n nacido	763.3	
Vacuna (abortiva)	051.0	
� debida a vacunaci�n	999.0	
� p�rpado	051.0[373.5]	
�� posvacunaci�n	999.0[373.5]	
Vacuna o vaccinia (generalizada)	999.0	
� cong�nita	771.2	
� conjuntiva	999.3	
� localizada	999.3	
� nariz	999.3	
� no por vacunaci�n	051.0	
�� p�rpado	051.0[373.5]	
� p�rpados	999.0[373.5]	
� sin vacunaci�n	051.0	
Vacunaci�n 		
� complicaci�n o reacci�n 		"v�ase Complicaciones, vacunaci�n "
� no realizada (contraindicada)	V64.0	
�� por decisi�n del paciente	V64.2	
� profil�ctica (contra)	V05.9	
�� c�lera (s�lo)	V03.0	
��� con tifoidea-paratifoidea (c�lera + TAB)	V06.0	
�� difteria s�lo	V03.5	
��� con 		
���� pertussis-t�tanos combinada (DPT)	V06.1	
���� poliomielitis (DPT + polio)	V06.3	
���� tifoidea-paratifoidea (DPT + TAB)	V06.2	
�� encefalitis v�rica transmitida por artr�podos	V05.0	
�� enfermedad (una sola) NCOC	V05.9
��� bacteriana NCOC	V03.9
���� tipo especificado NCOC	V03.8
��� combinaciones NCOC	V06.9
���� tipo especificado NCOC	V06.8
��� tipo especificado NCOC	V05.8
�� enfermedad v�rica transmitida por artr�podos NCOC	V05.1
�� fiebre amarilla	V04.4
�� leishmaniasis	V05.2
�� parotiditis (paperas) s�lo	V04.6
��� con sarampi�n y rub�ola (SPR)	V06.4
�� pertussis s�lo	V03.6
�� peste	V03.3
�� poliomielitis	V04.0
��� con difteria-pertussis-t�tanos (DPT + polio) 	V06.3
�� rabia	V04.5
�� resfriado com�n	V04.7
�� rub�ola (s�lo)	V04.3
��� con sarampi�n y parotiditis (SPR)	V06.4
�� sarampi�n (s�lo)	V04.2
��� con parotiditis-rub�ola (SPR)	V06.4
�� tifoidea-paratifoidea (TAB) (solo)	V03.1
��� con difteria-pertussis-t�tanos (TAB + DPT)	V06.2
�� toxoide tet�nico (s�lo)	V03.7
��� con difteria y pertussis (DPT)	V06.1
���� con poliomielitis (DPT + polio)	V06.3
�� tuberculosis (BCG)	V03.2
�� tularemia	V03.4
�� viruela	V04.1
Vagabundeo	V60.0
Vagabundo	V60.0
Vagabundo(s)	V60.0
� enfermedad de los	132.1	
"Vagabundo, vagabundismo"	V60.0	
Vagancia	V60.0	
"Vagina, vaginal "		v�ase enfermedad espec�fica
Vaginalitis (t�nica)	608.4	
Vaginismo (reflejo)	625.1	
� funcional	306.51	
� hist�rico	300.11	
� psic�geno	306.51	
Vaginitis (aguda) (cr�nica) (circunscrita) (difusa) (enfisematosa) (Hemophilus vaginalis) (no espec�fica) (no ven�rea) (ulcerativa)	616.10	
� con 		
�� aborto 		"v�ase Aborto, por tipo, con septicemia "
�� embarazo ect�pico		v�ase adem�s categor�as 633.0-633.9639.0 
�� embarazo molar		v�ase adem�s categor�as 630-632639.0 
� adhesiva cong�nita	752.49	
� atr�fica posmenop�usica	627.3	
� blenorr�gica (aguda)	098.0
�� cr�nica o de duraci�n de 2 meses o m�s	098.2
� Candida 	112.1
� cong�nita (adhesiva)	752.49
� cuando complica el embarazo o puerperio   	646.6
�� que afecta al feto o al reci�n nacido	760.8
� debida a 	
�� C. albicans	112.1
�� Trichomonas (vaginalis)	131.01
� despu�s de 	
�� aborto	639.0
�� embarazo ect�pico o molar	639.0
� gonoc�cica (aguda)	098.0
�� cr�nica o de duraci�n de 2 meses o m�s     	098.2
� granuloma	099.2
� mic�tica	112.1
� Monilia	112.1	
� oxiur	127.4[616.11]	
� posirradiaci�n	616.10	
"� posmenop�usica, atr�fica"	627.3	
� senil (atr�fica)	627.3	
� sifil�tica (precoz)	091.0	
�� tard�a	095.8	
� tricomoni�sica	131.01	
� tuberculosa	016.7	v�ase adem�s Tuberculosis   
� ven�rea NCOC	099.8	
Vagoton�a	352.3	
Vaina (tend�n) 		v�ase enfermedad espec�fica
Vainillismo	692.89	
"Valle de San Joaqu�n, fiebre de"	114.0	
"Valle del Rift, fiebre del"	066.3	
"Valle, fiebre del"	114.0	
Vall�cula 		v�ase enfermedad espec�fica
"Valsuani, enfermedad de (anemia perniciosa progresiva puerperal)"	648.2	
"V�lvula, valvular (formaci�n de) "		v�ase adem�s enfermedad espec�fica 
� colon	751.5	
� cong�nita NCOC 		v�ase Atresia 
"� cuello uterino, orificio interno"	752.49	
� defecto del coraz�n 		"v�ase Anomal�a, coraz�n, v�lvula "
� ur�ter (uni�n pelviana) (orificio vesical)	753.2	
� uretra	753.6	
� ventr�culo cerebral (comunicante) in situ   	V45.2	
Valvulitis (cr�nica)	424.90	v�ase adem�s Endocarditis
� reum�tica (cr�nica) (inactiva) (con corea)   	397.9	
�� activa o aguda (a�rtica) (mitral) (pulmonar) (tric�spide)	391.1	
� sifil�tica NCOC	093.20	
�� a�rtica	093.22	
�� mitral	093.21	
�� pulmonar	093.24	
�� tric�spide	093.23	
Valvulopat�a 		v�ase Endocarditis
"van Bogaert, leucoencefalitis de (esclerosante) (subaguda)"	046.2	
"van Bogaert-Nijssen (-Peiffer), enfermedad de"	330.0	
"van Buchem, enfermedad de"	733.3	
"van Creveld-von Gierke, enfermedad de (glucogenosis 1)"	271.0	
"van de Hoeve-Waardenburg-Gualdi, s�ndrome de (ptosis epicanto)"	270.2	
"van den Bergh,  enfermedad de (cianosis enter�gena)"	 289.7	
"van der Hoeve, s�ndrome de (huesos fr�giles y escler�tica azul, sordera)"	756.51	
"van der Hoeve-Halbertsma-Waardenburg, s�ndrome de (ptosis-epicanto)"	270.2	
"van Neck (-Odelberg), enfermedad o s�ndrome de (osteocondrosis juvenil)"	732.1	
"Vapor, quemadura por "		"v�ase Quemadura, por sitio"
"Vapores, asfixia o sofocaci�n por NCOC"	987.9	
� agente especificado 		"v�ase Tabla de drogas, f�rmacos y productos qu�micos"
Variaciones en el color del pelo	704.3	
Variantes talasan�micas	282.4	
Variantes talasan�micas o talas�micas	282.4	
"Varianza, bola letal, v�lvula cardiaca prot�sica"	996.02	
Varicela	052.9	
� con 		
�� complicaci�n	052.8	
��� especificada NCOC	052.7	
�� neumon�a	052.1	
Varices 		v�ase Variz
Varices	991.5	
� lupus	991.5	
Varicocele (escroto) (trombosado)	456.4	
� cord�n esperm�tico (ulcerado)	456.4	
� ovario	456.5	
� perineo	456.6	
"Varicosis, varicosidad(es)"	454.9	v�ase adem�s Varice
Varicoso 		
� aneurisma (roto)	442.9	v�ase adem�s Aneurisma
� dermatitis (extremidad inferior) 		"v�ase Varicoso, vena, inflamada o infectada "
� eczema		"v�ase Varicoso, vena "
� flebitis 		"v�ase Varicoso, vena, inflamada o infectada "
� tumor		"v�ase Varicoso, vena "
"� �lcera (extremidad inferior, cualquier parte)"	454.0	
�� ano	455.8	
��� externa	455.5	
��� interna	455.2	
�� escroto	456.4	
�� es�fago	456.1	"v�ase adem�s V�rice, es�fago"
��� sangrante	456.0	"v�ase adem�s Varice, es�fago, sangrante"
�� inflamada o infectada	454.2	
�� perineo	456.6	
�� recto 		"v�ase Varicoso, ulcera, ano "
�� sitio especificado NCOC	456.8	
�� tabique nasal	456.8	
� vaso 		v�ase adem�s Variz 
�� placentario 		"v�ase Placenta, anormal "
� vaso placentario 		"v�ase Placenta, anormal "
� vena (extremidad inferior) (rota)	454.9	v�ase adem�s V�rice
� con 		
��� dermatitis por estasis	454.1	
���� con �lcera	454.2	
��� inflamaci�n o infecci�n	454.1	
���� ulcerada	454.2	
��� �lcera	454.0	
���� inflamada o infectada	454.2	
�� ano 		v�ase Hemorroides 
�� cong�nita (cualquier sitio)	747.6	
"�� cord�n umbilical, que afecta al feto o al reci�n nacido"	762.6	
��� uretra	456.8	
�� en el embarazo o puerperio	671.0	
��� vulva o perineo	671.1	
�� escroto (ulcerada)	456.4	
�� es�fago (ulcerada)	456.1	"v�ase adem�s V�rice, es�fago"
��� sangrante	456.0	"v�ase adem�s V�rice, es�fago, sangrante"
�� inflamada o infectada	454.1	
��� con ulcera	454.2	
�� ligamento ancho	456.5	
�� pelvis	456.5	
�� perineo	456.6	
"��� en el embarazo, parto o puerperio"	671.1	
�� recto		v�ase Hemorroides 
�� sitio especificado NCOC	456.8	
�� sublingual	456.3	
�� tabique nasal (con ulcera)	456.8	
�� ulcerada	454.0
��� inflamada o infectada	454.2
�� uretra	456.8
�� vulva	456.6
"��� en el embarazo, parto o puerperio"	671.1
Variola	050.9
� hemorr�gica (pustular)	050.0
� major	050.0
� minor	050.1
� modificada	050.2
Varioloide	050.2
"Variolosa, p�rpura"	050.0
"Variz, varice(s) (extremidad inferior) (ruptura)"	454.9
� con 	
�� dermatitis por estasis	454.1
��� con �lcera	454.2
�� inflamaci�n o infecci�n	454.1	
��� con �lcera	454.2	
�� �lcera	454.0	
��� con inflamaci�n o infecci�n	454.2	
� aneurismal	442.9	v�ase adem�s Aneurisma
� ano 		v�ase Hemorroides 
� arteriovenosa	747.6	
"� bazo, espl�nica (con flebolitos)"	456.8	
� colon sigmoideo	456.8	
� cong�nita (cualquier sitio)	747.6	
"� cord�n umbilical, que afecta al feto o al reci�n nacido"	762.6	
� cuerda vocal	456.8	
� en el embarazo o puerperio	671.0	
�� perineo	671.1	
�� vulva	671.1	
� escroto (ulcerada)	456.4	
� es�fago (ulcerada)	456.1
�� cong�nita	747.6
�� en 	
��� cirrosis del h�gado	571.5[456.21]
���� con hemorragia	571.5[456.20]
��� hipertensi�n portal	572.3[456.21]
���� con hemorragia	572.3[456.20]
�� sangrante	456.0
��� en 	
���� cirrosis del h�gado	571.5[456.20]
���� hipertensi�n portal	572.3[456.20]
� espinal (m�dula) (vasos)	456.8
� faringe	456.8
� inflamada o infectada	454.1
�� ulcerada	454.2
� labios vulvares (mayores)	456.6
� ligamento ancho	456.5	
� ligamento uterino	456.5	
� �rbita	456.8	
�� cong�nita	747.6	
� ovario	456.5	
� papilar	448.1	
� papilas renales	456.8	
� peIvis	456.5	
� perineo	456.6	
�� en el embarazo o puerperio	671.1	
� placenta 		"v�ase Placenta, anormal "
� recto 		v�ase Hemorroides 
� retina	362.17	
� sitio especificado NCOC	456.8	
� sublingual	456.3	
� ulcerada	454.0	
�� inflamada o infectada	454.2	
� vejiga	456.5	
� vulva	456.6	
"�� en el embarazo, parto o puerperio"	671.1	
Vasa previa	663.5	
"� hemorragia por, que afecta al feto o al reci�n nacido"	772.0	
� que afecta al feto o al reci�n nacido	762.6	
Vascular 		v�ase adem�s enfermedad espec�fica 
� ara�a	448.1	
� asa en la papila (�ptica)	743.51	
� espasmo	443.9	
"� revestimiento, retina"	362.13	
Vascularidad pulmonar cong�nita	747.3	
Vascularizaci�n 		
� c�rnea	370.60	
�� localizada	370.61	
�� profunda	370.63	
� coroides	362.16	
� retina	362.16	
� subretiniana	362.16	
Vasculitis	447.6	
� al�rgica	287.0	
� crioglobulin�mica	273.2	
� diseminada	447.6	
� nodular	695.2	
� retiniana	362.18	
� reum�tica 		"v�ase Fiebre, reum�tica "
� ri��n	447.8	
"Vasectomia, admisi�n para"	V25.2	
Vasitis	608.4	
� conducto deferente	608.4	
� cord�n esperm�tico	608.4	
� escroto	608.4	
� nudosa	608.4	
� test�culo	608.4	
� tuberculosa	016.5	v�ase adem�s Tuberculosis
� t�nica vaginal	608.4	
"Vaso de distribuci�n, at�pico"	747.6	
�� arteria coronaria	746.85	
�� espinal	747.6	
Vasodilaci�n	443.9	
Vasomotor 		v�ase enfermedad espec�fica
Vasoplastia despu�s de esterilizaci�n previa	V26.0	
Vasoplej�a espl�cnica	337.9	"v�ase adem�s Neuropat�a, perif�rica, aut�noma"
Vasospasmo	443.9	
� cerebral (arteria)	435.9	
�� con d�ficit neurol�gico transitorio	435.9	
� nervio 		
�� aut�nomo	337.9	
�� brazo NCOC	354.9	
�� espinal NCOC	355.9	
�� extremidad 		
��� inferior NCOC	355.8	
��� superior NCOC	354.9	
�� perif�rico NCOC	355.9	
�� pierna NCOC	355.8	
�� plexo 		
��� braquial	353.0	
��� cervical	353.2	
�� simp�tico	337.9	
� perif�rico NCOC	443.9	
� retina (arteria)	362.30	"v�ase adem�s Oclusi�n, retiniana, arteria"
Vasosp�stico 		v�ase enfermedad espec�fica
"Vasovagal, ataque (paroxismal)"	780.2	
� psic�geno	306.2	
"Vater, ampolla de "		v�ase enfermedad espec�fica
"Vater, s�ndrome"	759.89	
"V�zquez, enfermedad de "	238.4(M9950/1)	
"V�zquez-Osler, enfermedad de (policitemia vera) "	238.4(M9950/1)	
"Vegetaci�n, vegetativo "		
� adenoide (fosa nasal)	474.2	
� coraz�n (mic�tica) (v�lvula)	421.0	
� endocarditis (aguda) (cr�nica) (cualquier v�lvula) (subaguda)	421.0	
Vejez	797	
� demencia (de la)	290.0	
Vejiga 		v�ase enfermedad espec�fica
Vejiga no contr�ctil	344.61	
Vejiga no refleja	344.61	
"Veldt, ulcera del"	707.9	"v�ase adem�s ulcera, piel"
"Vello, epitelial (c�mara anterior)"	364.61	
"Vellosa, lengua negra"	529.3	
Velloso 		v�ase enfermedad espec�fica  
Velo		
� de Jackson	751.4	
� sobre la cara (cuando causa asfixia)	768.9	
"Velpeau, hernia de "		"v�ase Hernia, femoral"
"Vena, venosa "		v�ase enfermedad espec�fica 
"Veneno, venenoso "		
� envenenamiento	989.5	
� mordedura o picadura (animal o insecto)	989.5	
Ven�reo 		
� balanitis	099.8	
� bub�n	099.1	
� enfermedad	099.9	
�� naturaleza o tipo especificado NCOC	099.8	
� granuloma inguinal	099.2	
"� linfogranuloma (Durand-Nicolas-Favre), (cualquier sitio)"	099.1	
� salpingitis	098.37	
� uretritis	099.4	
� vaginitis	099.8	
� verrugas	078.1	
"Venganza, deseo de, en el ni�o"	312.0	"v�ase adem�s Perturbaci�n, conducta"
Venofibrosis	459.89	
Venoso 		v�ase enfermedad espec�fica
Ventana 		"v�ase adem�s Imperfecto, cierre "
� aorticopulmonar	745.0	
"Ventosa, parto por NCOC"	669.5	
� que afecta al feto o al reci�n nacido	763.3	
Ventral 		v�ase enfermedad espec�fica
Ventriculitis cerebral	322.9	v�ase adem�s Meningitis
"Ventr�culo, ventricular "		v�ase adem�s enfermedad espec�fica 
� escape	427.69	
� paro	427.5	"v�ase adem�s Paro, cardiaco"
"Ventriculostom�a, estado de"	V45.2	
"Verano, veraniego "		v�ase enfermedad espec�fica  
Verbal		
� ceguera (cong�nita) (del desarrollo)	315.01	
�� secundaria a lesi�n org�nica	784.61	
� sordera (secundaria a lesi�n org�nica)	784.69	
�� del desarrollo	315.31	
"Verbiest, s�ndrome de (claudicaci�n espinal intermitente)"	435.1	
Verde 		
"� cana, madera o tallo "		"v�ase Fractura, por sitio "
� enfermedad	280.9	
Verdug�n	709.8	
"Vernet, s�ndrome de"	352.6	
"Verneuil, enfermedad de (bursitis sifil�tica)"	095.7	
Verruca 		v�ase Verruga
Verruga (com�n) (digital) (filiforme) (infecciosa) (juvenil) (plana) (plana juvenil) (plantar) (v�rica) (vulgar)	078.1	
� acuminata (cualquier sitio)	078.1	
"� Hassle-Henle, de (de la c�rnea)"	371.41	
"� Henle, de (de la c�rnea)"	371.41	
� h�meda	078.1	
� necr�gena (primaria)	017.0	v�ase adem�s Tuberculosis
� �rganos genitales externos (ven�rea)	078.1	
� peruana	088.0	
� peruviana	088.0	
� prosectora 	017.0	v�ase adem�s Tuberculosis
� seborreica	702	
� senil	702	
� sifil�tica	091.3	
� tuberculosa (primaria)	017.0	v�ase adem�s Tuberculosis
� ven�rea (femenina) (masculina)	078.1	
� v�rica	078.1	
"Verrugosa, endocarditis (aguda) (cr�nica) (cualquier v�lvula) (subaguda)"	710.0[424.91]	
� no bacteriana	710.0[424.91]	
Verrugosidades	078.1	v�ase adem�s Verruga
"Verse, enfermedad de (calcinosis intervertebral)"	275.4[722.90]	
Versi�n 		
"� antes del parto, que afecta al feto o al reci�n nacido"	761.7	
� cef�lica (correcci�n de mala posici�n previa)	652.1	
�� que afecta al feto o al reci�n nacido	763.1	
� cuello uterino	621.6	"v�ase adem�s Mala posici�n, �tero"
"� �tero (posinfecciosa) (posparto, antigua)"	621.6	"v�ase adem�s Mala posici�n, �tero"
�� anterior		"v�ase Anteversi�n, �tero "
�� lateral 		"v�ase Lateroversi�n, �tero"
V�rtebra adelgazada	733.00	v�ase adem�s Osteoporosis
V�rtebra inclinada	737.9	
"V�rtebra, vertebral "		v�ase enfermedad espec�fica
V�rtigo	780.4	
� auditivo	386.19	
� aural	386.19	
� cerebral	386.2	
� Dix y Hallpike (epid�mico)	386.12	
� end�mico paral�tico	078.81	
� epid�mico	078.81	
�� Dix y Hallpike	386.12	
"�� Gerlier, de"	078.81	
�� neuronitis vestibular	386.12	
"�� Pedersen, de"	386.12	
� epil�ptico 		v�ase Epilepsia 
"� Gerlier, de (epid�mico)"	078.81	
� hist�rico	300.11	
� laber�ntico	386.10	
� lar�ngeo	786.2	
� maligno posicional	386.2	
"� M�ni�re, de"	386.00	"v�ase adem�s Enfermedad, M�ni�re, de"
� menop�usico	627.2	
� origen central	386.2	
� ot�geno	386.19	
� paral�tico	078.81	
"� parox�stico posicional, benigno"	386.11	
"� Pedersen, de (epid�mico)"	386.12	
� perif�rico	386.10	
�� tipo especificado NCOC	386.19	
� posicional 		
�� benigno parox�stico	386.11	
�� maligno	386.2	
� posicional parox�stico benigno	386.11	
Verumontanitis (cr�nica)	597.89	v�ase adem�s Uretritis
Vesania	298.9	v�ase adem�s Psicosis
Vesical 		v�ase enfermedad espec�fica
Vesic�lico 		v�ase enfermedad espec�fica
Vesicoperineal 		v�ase enfermedad espec�fica
Vesicorrectal 		v�ase enfermedad espec�fica
Vesicouretrorrectal 		v�ase enfermedad espec�fica
Vesicovaginal 		v�ase enfermedad espec�fica
Ves�cula 		
� cut�nea	709.8	
� piel	709.8	
� seminal 		v�ase enfermedad espec�fica
Ves�cula biliar 	575.0	"v�ase adem�s enfermedad espec�fica v�ase adem�s Enfermedad, ves�cula biliar"
Ves�cula biliar no visualizada	793.3	
Vesicular 		v�ase enfermedad espec�fica
Vesiculitis (seminal)	608.0	
� amebiana	006.8	
� gonorreica (aguda)	098.14	
�� cr�nica o de duraci�n de 2 meses o m�s  	098.34	
� tricomoni�sica	131.09	
� tuberculosa	016.5[608.81]	v�ase adem�s Tuberculosis
Vestibulitis (o�do) 	386.30	
� nariz (externa)	478.1	
" Vestibulopat�a, perif�rica aguda (recurrente)"	386.12	
"Vestigio, vestigial "		v�ase adem�s Persistencia 
� branquial	744.41	
� estructuras en el v�treo	743.51	
Vibriosis NCOC	027.9	
"Vidal, enfermedad de (liquen simple cr�nico)"	698.3	
"Vidriosa, lengua"	529.4	
"Vienna, encefalitis tipo de"	049.8	
Vientre en ciruela (di�stasis de los m�sculos rectos del abdomen) (s�ndrome de)	756.7	
"Vigilia, trastorno de"	780.54	v�ase adem�s Hipersomnia
� origen no org�nico	307.43	
"Villaret, s�ndrome de"	352.6	
"Vincent, de "		
� amigdalitis	101	
� angina	101	
� bronquitis	101	
� enfermedad	101	
� estomatitis	101	
� gingivitis	101	
� infecci�n (cualquier sitio)	101	
� Iaringitis 	101	
"Vino de oporto, nevus o mancha"	757.32	
"Vinson-Plummer, s�ndrome de (disfagia siderop�nica)"	280.8	
Violaci�n	959.9	v�ase adem�s naturaleza y sitio del traumatismo
"� presunta, observaci�n o examen"	V71.5	
"Viosterol, carencia de"	268.9	"v�ase adem�s Carencia, calciferol"
"Virchow, enfermedad de"	733.99	
Viremia	790.8	
"V�rgula, bacilo, portador (presunto) de"	V02.3	
Virilismo (suprarrenal) (femenino) NCOC	255.2	
� con 		
�� defecto de 		
��� 11 -hidroxilasa	255.2	
��� 21-hidroxilasa	255.2	
��� 3-beta-hidroxisteroide-deshidrogenasa   	255.2	
�� hiperfunci�n cortical	255.2	
�� hiperplasia suprarrenal	255.2	
�� insuficiencia (cong�nita) suprarrenal	255.2	
Virilizaci�n (femenina) (suprarrenal)	255.2	v�ase adem�s Virilismo
� isosexual	256.4	
Viruela	050.9	
� contacto	V01.3	
� exposici�n a	V01.3	
� hemorr�gica (pustular)	050.0	
� maligna	050.0	
� modificada	050.2	
� vacunaci�n 		
�� complicaciones 		"v�ase Complicaciones, vacunaci�n "
�� profil�ctica (contra)	V04.1	
"Virulento, bub�n"	099.0	
Virus de inmunodeficiencia humana 		
