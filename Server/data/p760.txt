virus, v�rico 		v�ase adem�s enfermedad. espec�fica 
� infecci�n NCOC	079.9	v�ase adem�s Infecci�n v�rica
� septicemia	079.9	
"V�scera, visceral "		v�ase enfermedad espec�fica 
V�sceroptosis	569.89	
"Visible, peristalsis"	787.4	
"Visi�n, visual "		
� alucinaciones	368.16	
� baja (ambos ojos)	369.20	
�� un ojo (otro ojo normal)	369.70	"v�ase adem�s  Deteriorado, visi�n"
��� ceguera del otro ojo	369.10	
"� binocular, supresi�n de"	368.31	
� borrosa	368.8	
�� hist�rica	300.11	
"� campo, limitaci�n de"	368.40	
"� defecto, defectuosa"	369.9	"v�ase adem�s Deteriorado, visi�n"
� desorientaci�n (s�ndrome de)	368.16	
� examen	V72.0	
"� fusi�n, estereopsis defectuosa"	368.33	
� halos	368.16	
"� percepci�n, simult�nea sin fusi�n"	368.32	
� p�rdida	369.9	
�� ambos ojos	369.3	"v�ase adem�s Ceguera, ambos ojos"
�� completa	369.00	"v�ase adem�s Ceguera, ambos ojos"
�� s�bita	368.16	
�� un ojo	369.8	
� perturbaci�n NCOC	368.9	"v�ase adem�s Perturbaci�n, visi�n"
�� hist�rica	300.11	
� t�nel (limitaci�n del campo)	368.45	
Vista cansada	368.13	
Vista cansada NCOC	368.13	
"Vitalidad, falta de"	780.7	
� reci�n nacido	779.8	
"Vitamina, carencia de NCOC"	269.2	"v�ase adem�s Carencia, vitamina"
Vit�ligo	709.0	
� debido a pinta (carate)	103.2	
� p�rpado	374.53	
� vulva	624.8	
Vitium cordis 		"v�ase Enfermedad, coraz�n "
V�treo 		v�ase adem�s enfermedad espec�fica 
"� toque, s�ndrome de"	997.9	
Vivir 		
� solo	V60.3	
�� con persona minusv�lida	V60.4	
Vogt (Cecile) enfermedad o s�ndrome de	333.7	
"Vogt-Koyanagi, s�ndrome de"	364.24	
"Vogt-Spielmeyer, enfermedad de (idiotez amaur�tica familiar)"	330.1	
"Volhard-Fahr, enfermedad de (nefrosclerosis maligna)"	403.00	
"Volhinia, fiebre de"	083.1	
"Volkmann, contractura o par�lisis isqu�mica de (cuando complica traumatismo)"	958.6	
"Voluble, diente"	520.2	
"Voluntaria, inanici�n"	307.1	
V�lvulo (colon) (intestino)	560.2	
� con 		
�� hernia 		"v�ase adem�s Hernia, por sitio, con obstrucci�n "
��� gangrenosa 		"v�ase Hernia, por sitio, con gangrena "
�� perforaci�n	560.2	
cong�nita	751.5	
� duodeno	537.3	
� est�mago (debido a ausencia del ligamento gastroc�lico)	537.89	
oviducto	620.5	
� trompa de Falopio	620.5	
V�mito(s)	787.0	
� al�rgico	535.4	
� asfixia	933.1	
� bilioso (cauda desconocida)	787.0	
�� despu�s de cirug�a gastrointestinal	564.3	
� c�clico	536.2	
� psic�geno	306.4	
"� cuando causa asfixia, atoramiento o sofocaci�n"	933.1	"v�ase adem�s Asfixia, alimento"
del o que complica el embarazo	643.9	
� debido a 		
��� causa especificada NCOC	643.8	
�� enfermedad org�nica	643.8	
� precoz 		"v�ase Hiperemesis, grav�dica "
� tard�o (despu�s de completarse 22 semanas de gestaci�n)	643.2	
� despu�s cirug�a gastrointestinal	564.3	
� epid�mico	078.82	
� estercoral	569.89	
� fisiol�gico	787.0	
� funcional	536.8	
�� psic�geno	306.4	
� h�bito	536.2	
� hist�rico	300.11	
� incontrolable	536.2	
�� psic�geno	306.4	
� invierno	078.82	
� materia fecal	569.89	
� negro	060.9	"v�ase adem�s Fiebre, amarilla"
� nervioso	306.4	
� neur�tico	306.4	
� pernicioso o persistente	536.2	
�� cuando complica el embarazo 		"v�ase Hiperemesis, grav�dica "
�� psic�gena	306.4	
� psic�geno	307.54	
� ps�quico	306.4	
� reci�n nacido	779.3	
� sangre	578.0	v�ase adem�s Hematemesis
"von Bechterew (-Strumpell), enfermedad o s�ndrome de (espondilitis anquilosante)"	720.0	
"von Bezold, absceso de"	383.01	
"von Economo, enfermedad de (encefalitis let�rgica)"	049.8	
"von Eulenburg, enfermedad de (paramioton�a cong�nita)"	359.2	
"von Gierke, enfermedad de (glucogenosis 1)"	271.0	
"von Gies, articulaci�n de"	095.8	
"von Graefe, enfermedad o s�ndrome de"	378.72	
"von Hippel (-Lindau), enfermedad o s�ndrome de (angiomatosis retinocerebral)"	759.6	
"von Jaksch, anemia o enfermedad de (seudoleucemia infantil)"	285.8	
"von Recklinghausen, de "		
� enfermedad o s�ndrome (nervios) (piel) 	237.71(M9540/1)	
�� huesos (oste�tis fibrosa qu�stica)	252.0	
� tumor 	237.71(M9540/1)	
"von Recklinghausen-Applebaum, enfermedad de (hemocromatosis)"	275.0	
"von Schroetter, s�ndrome de (claudicaci�n venosa intermitente)"	453.8	
"von Willebrand (-Jurgens) (-Minot), enfermedad o s�ndrome de (angiohemofilia)"	286.4	
"von Zambusch, enfermedad de (liquen escloroso y atr�fico)"	701.0	
"Voorhoeve, enfermedad o discondroplasia de"	756.4	
"Vossius, anillo de"	921.3	
� efecto tard�o	366.21	
Voyeurismo	302.82	
Voz 		
� cambio	784.49	v�ase adem�s Disfon�a
� p�rdida	784.41	v�ase adem�s Afon�a
Voz de los predicadores 	784.49	
"Vrolik, enfermedad de (osteog�nesis imperfecta)"	756.51	
Vulva 		v�ase enfermedad espec�fica
Vulvaginitis	616.10	v�ase adem�s Vulvitis
� amebiana	006.8	
� gonoc�cica (aguda)	098.0	
�� cr�nica o de duraci�n de 2 meses o m�s  	098.2	
� herp�tica	054.11	
� monili�sica	112.1	
� tricomoni�sica (Trichomonas vaginalis)	131.01	
Vulvismo	625.1	
Vulvitis (aguda) (al�rgica) (aftosa) (cr�nica) (gangrenosa) (hipertr�fica) (intertriginosa)	616.10	
� con 		
�� aborto		"v�ase Aborto, por tipo, con septicemia"
embarazo ect�pico	639.0	v�ase adem�s categor�as 633.0-633.9
�� embarazo molar	639.0	v�ase adem�s categor�as 630-632
� adhesiva cong�nita	752.49	
� blenorr�gica (aguda)	098.0	
�� cr�nica o de duraci�n de 2 meses o m�s    	098.2	
� cuando complica el parto o puerperio	646.6	
� debida al bacilo de Ducrey	099.0	
� despu�s de 		
�� aborto	639.0	
�� embarazo ect�pico o molar	639.0	
� gonoc�cica (aguda)	098.0	
�� cr�nica o de duraci�n de 2 meses o m�s	098.2	
� herp�tica	054.11	
� leucopl�sica	624.0	
� monili�sica	112.1	
"� puerperal, posparto, parto"	646.6	
� sifil�tica (precoz)	091.0	
�� tard�a	095.8	
� tricomoni�sica	131.01	
Vulvorrectal 		v�ase enfermedad espec�fica 
"Waardenburg, s�ndrome de"	756.89	
� cuando significa ptosis-epicanto	270.2	
"Waardenburg-Klein, s�ndrome de (ptosisepicanto)"	270.2	
"Wagner (-Unverricht), s�ndrome de (dermatomiositis)"	710.3	
"Wagner, enfermedad de (milio coloideo)"	709.3	
"Waldenstr�m, de "		
"� enfermedad de (osteocondrosis, cabeza del f�mur)"	732.1	
� hepatitis (hepatitis lupoide)	571.49	
� hipergammaglobulinemia	273.0	
� macroglobulinemia	273.3	
� p�rpura hipergammaglobulin�mica	273.0	
� s�ndrome (macroglobulinemia)	273.3	
"Waldenstr�m-Kjellberg, s�ndrome de (disfagia siderop�nica)"	280.8	
"Wallenberg, s�ndrome de (arteria cerebelosa inferior posterior)"	436	"v�ase adem�s Enfermedad, cerebrovascular. aguda "
"Wallgren, de "		
� enfermedad (obstrucci�n de la vena espl�nica con circulaci�n colateral)	459.89	
� meningitis	047.9	"v�ase adem�s Meningitis, as�ptica"
"Wardrop, enfermedad de (con linfangitis)"	681.9	
� dedo de 		
�� mano	681.02	
�� pie	681.11
"Warthin, tumor de (gl�ndula salival) "	210.2(M8561/0)
"Wassilieff, enfermedad de (ictericia leptospir�sica)"	100.0
"Waterhouse (-Friderichsen), enfermedad o s�ndrome de"	036.3
"Weber,"	
par�lisis o s�ndrome de	344.8
"Weber-Christian, enfermedad o s�ndrome de (paniculitis no supurativa nodular)"	729.30
"Weber-Cockayne, s�ndrome de (epiderm�lisis bullosa)"	757.39
"Weber-Dimitri, s�ndrome de"	759.6
"Weber-Gubler, s�ndrome de"	344.8
"Weber-Leyden, s�ndrome de"	344.8
"Weber-Osler, s�ndrome de (telangiectasia hemorr�gica familiar)"	448.0
"Wegener, granulomatosis o s�ndrome de"	446.4
"Wegner, enfermedad de (osteocondritis sifil�tica)"	090.0
"Weil, enfermedad de (ictericia leptospir�sica)"	100.0
"Weill-Marchesani, s�ndrome de (braquimorfismo y ectopia del cristalino)"	759.89
"Weingarten, s�ndrome de (eosinofilia tropical)"	 518.3	
"Weir Mitchell, enfermedad de (eritromelalgia)"	443.89	
"Weiss-Baker, s�ndrome de (s�ncope del seno carot�deo)"	337.0	
"Weissenbach-Thibierge, s�ndrome de (esclerosis cut�nea sist�mica o generalizada)"	710.1	
"Wenckebach, fen�meno de, bloqueo cardiaco (segundo grado)"	426.13	
"Werdnig-Hoffmann, s�ndrome de (atrofia muscular)"	335.0	
"Werlhof, enfermedad de"	287.3	"v�ase adem�s P�rpura, trombocitop�nica"
"Werlhof-Wichmann, s�ndrome de"	287.3	"v�ase adem�s P�rpura, trombocitop�nica"
"Wermer, s�ndrome o enfermedad de (adenomatosis poliendocrina)"	258.0	
"Werner, enfermedad o s�ndrome de (progeria adulta)"	259.8	
"Werner-His, enfermedad de (fiebre de las trincheras)"	083.1	
"Werner-Schultz, enfermedad de (agranulocitosis)"	288.0	
"Wernicke, encefalopat�a, enfermedad o s�ndrome de (polioencefalitis hemorr�gica superior)"	265.1	
"Wernicke-Korsakoff, s�ndrome o psicosis de (no alcoh�lico)"	294.0	
� alcoh�lico	291.1	
"Wernicke-Posadas, enfermedad de"	114.9	v�ase adem�s Coccidioidomicosis
"Wesselbron, fiebre de"	066.3	
"Westphal-Strumpell, s�ndrome de (degeneraci�n hepatolenticular)"	275.1	
"Wharton, conducto de "		v�ase enfermedad espec�fica
"Whipple, enfermedad o s�ndrome de (lipodistrofia intestinal)"	040.2	
"White, enfermedad de (cong�nita) (queratosis folicular)"	757.39	
"Whitehead, espinilla blanca"	706.2	
"Whitmore, enfermedad o fiebre de (melioidosis) "	025	
"Wichmann, asma de (laringismo estriduloso)"	478.75	
"Widal (-Abrami), s�ndrome de (ictericia hemol�tica)"	283.9	
"Wilkie, enfermedad o s�ndrome de"	557.1	
"Wilkinson-Sneddie, enfermedad o s�ndrome de (dermatosis pustular subcorneal)"	694.1	
"Willan, lepra de"	696.1	
" Willan-Plumbe, s�ndrome de (psoriasis)"	696.1	
"Willebrand (-Jurgens), s�ndrome o trombopat�a de (angiohemofilia)"	286.4	
"Willi-Prader, s�ndrome de (distrofia hipogenital con tendencia diab�tica)"	759.81	
"Willis, enfermedad de (diabetes mellitus)"	250.0	v�ase adem�s Diabetes
"Wilms, tumor o neoplasia de (nefroblastoma) "	189.0(M8960/3)
"Wilson, de "	
� degeneraci�n hepatolenticular	275.1
� enfermedad o s�ndrome (degeneraci�n hepatolenticular)	275.1
� liquen rojo o ruber	697.0
"Wilson-Brocq, enfermedad de (dermatitis exfoliativa)"	695.89
"Wilson-Mikity, s�ndrome de"	770.7
"Wise, enfermedad de"	696.2
"Wiskott-Aldrich, s�ndrome de (eczematrombocitopenia)"	279.12
"Witt, anemia de (anemia aclorh�drica)"	280.9
Witzelsucht	301.9
"Woakes, s�ndrome de (etmoiditis)"	471.1
"Wohlfart-Kugelberg-Welander, enfermedad de"	335.11
"Woillez, enfermedad de (congesti�n pulmonar idiop�tica aguda)"	518.5
"Wolff-Parkinson-White, s�ndrome de (excitaci�n atrioventricular an�mala)"	426.7
"Wolhinia, fiebre de"	083.1
"Wolman, enfermedad de (xantomatosis familiar primaria)"	272.7
"Wright, s�ndrome de (hiperabducci�n)"	447.8
� neumon�a	390[517.1]
"Wuchereria, infestaci�n por"	125.0
� bancrofti	125.0 
� Brugia malayi	125.1 
� malayi	125.1
Wuchereriasis	125.0
Wuchereriosis	125.0
Wuchernde-struma de Langhans 	193(M8332/3)
 Xantelasma	272.2
� palpebral	272.2[374.51]
� p�rpado	272.2[374.51]
Xantelasmatosis (esencial)	272.2
Xantelasmoide	757.33
"Xantina, piedras de"	277.2
Xantinuria	277.2	
Xantofibroma 	(M8831/0)	"v�ase Neoplasia, tejido conjuntivo, benigno"
"Xantoma(s), xantomatosis"	272.2	
� con 		
�� hiperlipoproteinemia 		
��� tipo 1	272.3	
��� tipo lll	272.2	
��� tipo IV	272.1	
��� tipo V	272.3	
� articulaci�n	272.7	
� craneohipofisario	277.8	
� cutaneotendinoso	272.7	
"� diab�ticos, de los"	250.8[272.2]	
� diseminado	272.7	
� eruptivo	272.2	
� familiar	272.7	
� hereditario	272.7
� hipercolesterin�mico	272.0
� hipercolesterol�mico	272.0
� hiperlip�mico	272.4
� hiperlipid�mico	272.4
� hueso	272.7
� infantil	272.7
� juvenil	272.7
� m�ltiple	272.7
� p�rpado	272.2[374.51]
� primario familiar	272.7
� tend�n (vaina)	272.7
� tuberoso	272.2
� tuboeruptivo	272.2
Xantosis	709.0
Xenofobia	300.29
Xeroderma (cong�nito)	757.39
� adquirido	701.1
�� p�rpado	373.33
� carencia de vitamina A	264.8
� p�rpado	373.33
� pigmentoso	757.33
Xeroftalmia	372.53
� carencia de vitamina A	264.7
Xerosis 	
� conjuntiva	372.53
�� con manchas de Bit�t	372.53
��� carencia de vitamina A	264.1
�� carencia de vitamina A	264.0
� c�rnea	371.40
�� con ulceraci�n corneal	370.00
��� carencia de vitamina A	264.3
�� carencia de vitamina A	264.2
� cutis	706.8
� piel	706.8
Xerostom�a	527.7
Xifodinia	733.90
Xifoidalgia	733.90
Xifoiditis	733.99
Xif�pago	759.4
Xiloquetosuria	271.8
Xiloquetosuria-L	271.8
Xilosuria	271.8
Xilulosuria	271.8
"XO, s�ndrome"	758.6
"XXX, s�ndrome"	758.8
"XXXXY, s�ndrome"	758.8
"XXY, s�ndrome"	758.7
"XYY, s�ndrome"	758.8	
Yellow jack	060.9	v�ase adem�s Fiebre amarilla
Yersinia s�ptica	027.8	
Yeyunitis	558.9	v�ase adem�s Enteritis
"Yeyuno, yeyunal "		v�ase enfermedad espec�fica
"Yeyunostom�a, estado de"	V44.4	
"Zagari, enfermedad de (xerostom�a)"	527.7	
"Zahorsky, enfermedad de (exantema s�bito)"	057.8	
� s�ndrome de (herpangina)	074.0	
"Zenker, divert�culo de (es�fago)"	530.6	
"Ziehen-Oppenheim, enfermedad de"	333.6	
"Zieve, s�ndrome de (ictericia, hiperlipemia, y anemia hemol�tica)"	571.1	
"Zike, fiebre"	066.3	
"Zollinger-Ellison, s�ndrome de (hipersecreci�n g�strica con tumor de c�lulas islotes del p�ncreas)"	251.5	
Zona	053.9	"v�ase adem�s Herpes, zoster"
� ojo NCOC	053.29	
Zona	053.9	"v�ase adem�s Herpes, zoster"
Zoofilia (er�tica)	302.1	
Zoofobia	300.29	
"Zopo, pie (cong�nito)"	754.70	
� adquirido	736.71	
� equinovaro	754.51	
� paral�tico	736.71	
Zoster (herpes)	053.9	"v�ase adem�s Herpes, zoster"
"Zuelzer (-Ogden), anemia o s�ndrome de (anemia megalobl�stica nutricional)"	281.2	
Zumbido en el o�do	388.30	v�ase adem�s Tinnitus   
"Zumbido o murmullo, venoso"	785.9	
