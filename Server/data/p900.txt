� escaldadura	E958.2	
� estrangulaci�n 		"v�ase Suicidio, sofocaci�n "
� explosivo(s) (clasificable(s) bajo E923)	 E955.5 	
� fr�o extremo	E958.3	
� fuego	E958.1 	
� herida NCOC	E958.9	
� instrumento cortante o punzante (clasificable bajo E920)	E956 	
� medios especificados NCOC	E958.8	
� punci�n (cualquier parte del cuerpo)	E956	
"� pu�alada, navajazo (cualquier parte del cuerpo)"	E956	
� quemadura(s)	E958.1	
� sallo 		
"�� delante de objeto, tren, veh�culo en movimiento"	E958.0	
�� desde lugar alto 		"v�ase Saltar, desde, lugar alto, declarado como, suicida "
� sofocaci�n	E953.9	
"�� por, en "		
��� ahorcamiento	E953.0
��� bolsa de pl�stico	E953.1
��� medios especificados NCOC	E953.8
� sumersi�n	E954
� sustancia c�ustica	E958.7
�� deglutida	E950.7
�� envenenamiento	E950.7
"� veh�culo de motor, choque de"	E958.5
Sumersi�n (accidental)	E910.8
"� al, durante "	
�� andar en el agua	E910.2
�� bucear con escafandra aut�noma (scuba)	E910.1
��� no en el recreo	E910.3
�� buceo con tubo de respiraci�n	E910.2
"�� cazar, no desde barca"	E910.2
�� colocaci�n de redes de pesca	E910.3
�� esqu� n�utico	E910.0 	
�� intentar rescatar a otra persona	E910.3	
�� jugar en el agua	E910.2	
�� nataci�n (piscina)	E910.2	
�� nataci�n submarina	E910.1	
�� patinaje sobre hielo	E910.2	
�� pesca de perlas	E910.3	
�� pesca submarina con harp�n	E910.1	
"�� pescar, no desde barca"	E910.2	
�� realizar 		
��� construcci�n o reparaci�n submarina	E910.3	
��� salvamento marino	E910.3	
�� surf (con tabla)	E910.2	
� autoinfligida (sin especificarse si fue accidental o intencionada)	E984	
"�� declarada como intencionada, deliberada"	E954	
�� en circunstancias accidentales 	E910	v�ase categor�a E910
"� buque, barco, embarcaci�n (cuando causa ahogamiento, sumersi�n)"	E830	
"�� cuando causa traumatismo, salvo ahogamiento, sumersi�n"	 E831  	
� corrimiento de tierra	E909	
"�� buque, barco o embarcaci�n que se hunde"	E909	
"�� cuando vuelca buque, barco, o embarcaci�n"	E909	
�� lluvias torrenciales	E908	
�� maremoto	E909	
��� causado por tormenta	E908	
� debida a 		
�� accidente de 		
"��� buque, barco, embarcaci�n"	E830	
��� maquinaria 		"v�ase Accidente, m�quina "
��� transporte 	E800-E848	v�ase categor�as E800-E848
"�� avalancha, alud"	E909	
�� ca�da		
��� desde 		
"���� buque, barco, embarcaci�n (no implicado en accidente)"	E832
"����� implicado en accidente, colisi�n"	E830
"����� incendiado, aplastado"	E830
���� plancha de embarque (al agua)	E832
��� por la borda NCOC	E832
�� cataclismo	
��  movimiento o erupci�n de la corteza terrestre	E909
��� tormenta	E908
�� chaparr�n	E908
�� cicl�n	 E908  
�� hurac�n	E908
�� inundaci�n	E908
�� salto al agua	E910.8
"��� desde buque, barco, embarcaci�n "	
"���� implicado en accidente, colisi�n"	E830
"���� incendiado, aplastado, que se hunde"	E830
"���� no implicado en accidente, para nadar"	E910.2
��� en actividad de recreo (sin equipo de buceo)	E910.2
���� con o con empleo de equipo de buceo	E910.1
��� para rescatar a otra persona	E910.3
� declarada como sin determinarse si fue accidental o intencionada	E984
� efecto tard�o de NCOC	E929.8
� en 	
�� actividad deportiva o de recreo (sin equipo de buceo)	E910.2
��� con o con empleo de equipo de buceo	E910.1
��� esqu� n�utico	E910.0
"�� actividad especificada, no deportiva, de transporte o de recreo"	E910.3
�� ba�era	 E910.4
�� operaciones de guerra	E995
�� piscina NCOC	E910.8
�� transporte acu�tico	 E832 
"��� debida a accidente de buque, barco, embarcaci�n"	E830
� homicidio (intento de)	E964	
� por otra persona 		
�� declarada como sin determinarse si fue accidental o intencionada	E984	
�� en circunstancias accidentales 		v�ase categor�a 910 
"�� intencionada, homicida"	E964	
� suicida (intento)	E954	
� tanque de templado	E910.8	
Sustancia s�lida en el ojo (cualquier parte)  	E914	
"T�rmica, fiebre"	E900.9	
Termoplej�a	E900.9	
Terremoto (cualquier traumatismo)	E909	
"Tierra, ca�da de (encima de) (con asfixia o sofocaci�n)"	E913.3	"v�ase adem�s Sofocaci�n, debida a, hundimiento de tierra"
"� en la forma de, o debida a, cataclismo (con implicaci�n de cualquier veh�culo de transporte) "		"v�ase categor�as E908, E909"
� no debida a acci�n catacl�smica	E913.3	
�� bicicleta de pedales	E826	
�� golpeado o aplastado por	E916	
��� con asfixia o sofocaci�n	E913.3	
"�� con traumatismo, salvo asfixia o sofocaci�n"	E916	
"� material rodante, tren o veh�culo ferroviario"	E806	
� tranv�a	E829	
� veh�culo de carretera sin motor NCOC	E829	
� veh�culo de motor (en movimiento) (en v�a p�blica)	E818	
�� no en la v�a p�blica	E825	
"Tirar de algo, traumatismo al"	E927	
"Tomar, toma de "		
� sobredosis de droga 		"v�ase Tabla de drogas, f�rmacos y productos qu�micos "
� veneno 		"v�ase Tabla de drogas, f�rmacos y productos qu�micos"
Topetazo de animal	E906.8	
"Torcerse, traumatismo al"	E927	
Tormenta (que causa inundaci�n)	E908	
Tornado (cualquier traumatismo)	E908	
"Trafico, accidente de NCOC"	E819	
Tragado 		
� por 		
�� motor de reacci�n (aeronave)	E844	
"Traumatismo, lesionado (accidental(mente)) NCOC"	E928.9	
� autoinfligido (sin especificarse si fue accidental o intencionado)	E988.9	
�� declarado como 		
��� accidental	E928.9	
"��� intencionado, deliberado"	E958.9	
� causa especificada NCOC	E928.8	
� de forma deliberada (infligido) por otra(s) persona(s) 		v�ase Ataque 
"� de ni�o, debido a aborto criminal"	E968.8	
� debido a 		
�� insurrecci�n civil 	E998	v�ase Operaciones de guerra cuando ocurre despu�s del cese de hostilidades
�� operaciones de guerra		v�ase Operaciones de guerra 
��� cuando ocurre despu�s del cese de hostilidades	E998	
� declarado como 		
�� sin determinarse si fue infligido de forma accidental o deliberada (por)	E988.9	
��� ahogamiento	E984	
��� ahorcamiento	E983.0	
��� caerse desde una gran altura	E987.9	
"���� estructura artificial, salvo residencial"	E987.1	
���� locales residenciales	E987.0	
���� sitio en estado natural	E987.2	
��� corte (cualquier parte del cuerpo)	E986	
��� cuchillo	E986	
��� disparos 		"v�ase Disparo, declarado como sin determinarse si fue accidental o intencionado "
��� efecto tard�o de	E989	
��� estrangulaci�n		"v�ase Sofocaci�n, declarada como sin determinarse si fue accidental o intencionada "
��� explosivo(s) (misil)	E985.5	
��� instrumento cortante o punzante (clasificable bajo 920)	E986 	
��� medios especificados NCOC	E988.8	
��� punci�n (cualquier parte del cuerpo)	E986	
��� pu�alada (cualquier parte del cuerpo)	E986	
��� sofocaci�n 		"v�ase Sofocaci�n, declarada como sin determinarse si fue accidental o intencionada "
��� sumersi�n	E984	
� efecto tard�o de	E9299	
� en 		
�� descenso en paraca�das (voluntario) (sin accidente de aeronave)	E844	
��� con accidente de aeronave 	E840-E842	v�ase categor�as E840-E842
�� insurrecci�n civil 		v�ase Operaciones de guerra 
�� operaciones de guerra		v�ase Operaciones de guerra 
�� pelea	E960.0	
�� servidumbre de paso en la v�a f�rrea	E807	
�� v�a p�blica	E819	
� homicida	E968.9	v�ase adem�s Ataque
� infligido (por) 		
�� agente de fuerzas del orden (de servicio)		v�ase Intervenci�n legal 
"�� durante el arresto (intento de), supresi�n de disturbio, manutenci�n del orden, por agentes de las fuerzas del orden "		v�ase Intervenci�n legal 
�� otra persona 		
��� declarado como 		
���� accidental	E928.9	
"���� homicida, intencionado "		v�ase Ataque 
��� sin determinarse si fue accidental o intencionado 		"v�ase Traumatismo, declarado como sin determinar "
�� polic�a (de servicio) 		v�ase Intervenci�n legal 
"� por, causado por "		
"�� agente de fuerzas de orden, polic�a, en el curso de una intervenci�n legal"		v�ase Intervenci�n legal 
�� animal (no montado) NCOC	E906.9	
��� montado (deporte o transporte)	E828	
�� astilla de madera	E920.8	
�� ataque	E968.9	v�ase adem�s Ataque
�� aterrizaje violento de veh�culo de motor tipo todo terreno (despu�s de despegarse del suelo o terreno accidentado)	E821	
��� veh�culo de nieve	E820	
�� avalancha o alud	E909	
�� bala 		v�ase Disparo 
�� bayoneta	E920.3	v�ase adem�s Herida por Bayoneta
"�� bengala, pistola Very"	E922.8	
�� carril electrificado 		v�ase Choque el�ctrico 
�� carril o hilo conductor 		v�ase Choque el�ctrico 
�� chaparr�n	E908	
�� cicl�n	E908	
�� clavo	E920.8	
�� corriente el�ctrica	E925.9	v�ase adem�s Choque el�ctrico
�� corrimiento de tierra	E909	
�� cristales rotos	E920.8	
�� cuerpo extra�o 		v�ase Cuerpo extra�o 
�� disparo 		v�ase Disparo 
�� encorvarse	E927	
�� erupci�n volc�nica	E909	
�� escopeta de aire (balines)	E917.9	
�� espada	E920.3	
�� explosi�n	E923.9	v�ase adem�s Explosi�n
�� fuego 		v�ase Fuego 
�� granizo	E904.3	
"�� h�lice en movimiento, aeronave"	E844	
�� hundimiento de tierra	E913.3	"v�ase adem�s Sofocaci�n, debida a, hundimiento de tierra"
��� movimiento o erupci�n de la corteza terrestre	E909	
��� sin asfixia o sofocaci�n	E916	
��� tormenta	E908	
�� hurac�n	E908	
"�� ingravidez (en nave espacial, verdadera o simulada)"	E9280	
�� instrumento cortante o punzante	E920.9	v�ase adem�s Corte
�� inundaci�n	E908	
�� lluvias torrenciales	E908	
"�� madera, astilla de"	E920.8	
�� maquinaria 	E844	"v�ase adem�s Accidente, m�quina aeronave, sin accidente de aeronave"
"��� buque, barco, embarcaci�n (carga) (cocina) (cubierta) (lavander�a) (sala de m�quinas)"	E836	
�� maremoto	E909	
��� causado por tormenta	E908	
"�� material rodante, tren, veh�culo ferroviario (parte de)"	E805	
��� puerta o ventana	E806	
"�� misil, proyectil "		
��� arma de fuego 		v�ase Disparo 
��� en operaciones de guerra 		"v�ase Operaciones de guerra, misil "
��� explosivo	E923.8	
�� objeto 		
��� ca�do encima de 		
���� veh�culo de motor (en movimiento) (en la v�a publica)	E818	
����� no en la v�a publica	E825	
��� que se cae 		"v�ase Golpe por, objeto, que se cae "
�� ondas sonoras	E928.1	
�� piedra de rayo	E907	
�� pieza m�vil de veh�culo de motor (en movimiento) (en la v�a publica)	E818	
"��� al subir, bajar, entrar o salir del mismo"		"v�ase Ca�da, desde, veh�culo de motor, al subir, bajar "
"��� no en la v�a publica, accidente no de tr�fico"	E825	
�� radiaci�n 		v�ase Radiaci�n 
"�� r�faga de arma, en operaciones de guerra"	E993	
�� rayo	E907	
�� ruido	E928.1	
�� sable	E920.3	"v�ase adem�s Herida, sable"
�� ser arrojado contra alguna parte de o alg�n objeto dentro de 		
��� tranv�a	E829	
��� tren ferroviario	E806	
��� veh�culo de carretera sin motor NCOC	E829	
��� veh�culo de motor (en movimiento) (en la v�a publica)	E818	
���� no en la v�a publica	E825	
��� veh�culo de motor todo terreno NCOC	E821	
"��� veh�culo de nieve, impulsado por motor"	E820	
�� suicidio (intento de)	E958.9	
�� terremoto	E909	
�� torcedura	E927	
�� tornado	E908	
�� torsi�n	E927	
�� tranv�a (puerta)	E829	
�� veh�culo NCOC 		"v�ase Accidente, veh�culo NCOC "
�� vibraciones	E928.2	
"Tropezar con aIfombra, aIfombrilla, animal, bordillo u objeto (peque�o) (con ca�da)"	E885	
� sin ca�da 		"v�ase Chocarse con, objeto"
"Tropez�n con alfombra, alfombrilla, animal, bordillo u objeto peque�o (con ca�da)"	E885	
� sin ca�da 		"v�ase Chocarse con, objeto"
Tumbado (accidentalmente) (por) NCOC	E928.9	
� animal (no montado)	E906.8	
�� montado (en el deporte o transporte)	E828	
� efecto tard�o de 		v�ase Efecto tard�o 
� en el boxeo	E917.0	
"� multitud, desbandada humana"	E917.1	
� onda explosiva por explosi�n	E923.9	v�ase adem�s Explosi�n
� persona (accidentalmente)	E917.9	
�� en el deporte	E917.0	
"�� en reyerta, pelea"	E960.0	
� veh�culo de transporte 		"v�ase Veh�culo implicado bajo golpe, por"
"Tumbarse delante de tren, veh�culo u otro objeto en movimiento (sin especificar si fue accidental o intencionado)"	E988.0	
"� declarado como intencionado, deliberado, suicidio (intento de)"	E958.0	
Ventisca	E908	
"Viaje, viajar (efectos de)"	E903	
� mareo por	E903	
V�ctima (no de guerra) NCOC	E928.9	
� de guerra (baja)	E995	v�ase adem�s Operaciones de guerra
Violaci�n	E960.1	
Violencia no accidental	E968.9	v�ase adem�s Ataque
Voladura	E923.9	v�ase adem�s Explosi�n
"V�mito en v�as respiratorias (con asfixia, obstrucci�n, o sofocaci�n)"	E911	
Vuelco (accidental) 		
� bicicleta de pedales	E826	
"� buque, barco, embarcaci�n "		
�� cuando causa 		
"��� ahogamiento, sumersi�n"	E830	
"��� traumatismo, salvo por ahogamiento, sumersi�n"	E831	
� maquinaria 		"v�ase Accidente, m�quina "
"� material rodante, tren, veh�culo ferroviario"	E802	"v�ase adem�s Descarrilamiento, ferroviario"
� tranv�a	E829	
� veh�culo de carretera sin motor NCOC	E829	
� veh�culo de motor	E816	"v�ase adem�s P�rdida de control, veh�culo de motor"
�� con colisi�n antecedente en la v�a publica		"v�ase Colisi�n, veh�culo de motor "
"�� fuera de la v�a p�blica, accidente no de tr�fico"	E825	
��� con colisi�n antecedente 		"v�ase Colisi�n, veh�culo de motor, fuera de la v�a publica "
� veh�culo de motor tipo todo terreno 		"v�ase P�rdida de control, veh�culo de motor tipo todo terreno "
� veh�culo NCOC 		"v�ase Accidente, veh�culo NCOC "
� veh�culo tirado por animal	E827	
