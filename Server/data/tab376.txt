Hipertensi�n, hipertensiva (arteria) (arteriolar) (degenerativa) (enfermedad) (esencial) (fluctuante) (idiop�tica) (intermitente) (Iabil) (ortost�tica) (parox�stica) (primaria) (renina baja) (sist�mica) (vascular)	401.0	401.1	401.9	
� con				
"�� complicaci�n card�aca (trastorno clasificable en 428, 329.0, 429.3, 429.8, 429.9 debido a hipertensi�n) "	402.00	402.10	402.90	"v�ase adem�s Hipertensi�n, coraz�n"
��� con complicaci�n renal				"v�ase Hipertensi�n, cardiorrenal"
"�� complicaci�n renal (trastornos clasificables en 585, 58� 587) "	403.00	403.10	403.90	v�ase adem�s Hipertensi�n renal 
��� con complicaci�n cardiaca				"v�ase Hipertensi�n, cardiorrenal"
��� insuficiencia (y esclerosis) 	403.01	403.11	403.91	"v�ase adem�s Hipertensi�n, renal"
��� esclerosis sin insuficiencia 	403.00	403.10	403.90	"v�ase adem�s Hipertensi�n, renal"
� cardiorrenal (enfermedad)	404.00	404.10	404.90	
�� con				
��� insuficiencia cardiaca (congestiva) 	404.01	404.11	404.91	
���� e insuficiencia renal.	404.03	404.13	404.93	
��� insuficiencia renal 	404.02	404.12	404.92	
���� e insuficiencia cardiaca (congestiva)	404.03	404.13	404.93	
� circulaci�n menor	_	_	416.0	
� complicaci�n del embarazo parto o puerperio	642.2	642.0	642.9
�� con			
��� albuminuria (y edema) poco severa	�	�	642.4
���� severa	�	�	642.5
��� edema (poco severa) 	�	�	642.4
��� severa	�	�	642.5
��� enfermedad cardiaca 	642.2	642.2	642.2
���� y enfermedad renal 	642.2	642.2	642.2
��� enfermedad renal	642.2	642.2	642.2
���� y enfermedad cardiaca	642.2	642.2	642.2
�� cr�nica	642.2	642.0	642.0
��� con pre-eclampsia o eclampsia	642.7	642.7	642.7
��� feto o reci�n nacido	760.0	760.0	760.0
�� esencial.	�	642.0	642.0
��� con pre-eclampsia o eclampsia	�	642.7	642.7
��� feto o reci�n nacido	760.0	760.0	760.0
�� feto o reci�n nacido	760.0	760.0	760.0	
�� gestacional	_	�	642.3	
�� pre-existente	642.2	642.0	642.0	
��� con pre-eclampsia o eclampsia	642.7	642.7	642.7	
��� feto o reci�n nacido	760.0	760.0	760.0	
�� secundaria a enfermedad renal 	642.1	642.1	642.1	
��� con pre-eclampsia o eclampsia	642.7	642.7	642.7	
��� feto o reci�n nacido	760.0	760.0	760.0	
�� transitoria	�	�	642.3	
"� coraz�n (enfermedad) (trastorno clasificable en 428, 429.0-429.3, 429.8, 429.9 debido a Hipertensi�n)"	402.00	402.10	402.90	
�� con				
��� enfermedad hipertensiva renal (trastorno clasificable en 403) 	404.00	404.10	404.90	"v�ase adem�s Hipertensi�n, cardiorrenal"
��� esclerosis renal 	404.00	404.10	404.90	"v�ase adem�s Hipertensi�n, cardiorrenal"
� insuficiencia cardiaca 	402.00	402.10	402.90	
���� congestiva 	402.01	402.11	402.01	
� crisis 	437.2	437.2	437.2	
� de Goldblatt 	440.1	440.1	440.1	
� debido a				
�� aldosteronismo primario	405.09	405.19	405.99	
�� c�lculos				
��� renales	405.09	405.19	405.99	
��� ureterales 	405.09	405.19	405.99	
�� coartaci�n aorta 	405.09	405.19	40599	
�� enfermedad de Cushing	405.09	405.19	405.99	
�� feocromocitoma 	405.09	405.19	40599	
�� glomeruloesclerosis 	403.00	403.10	403.90	"v�ase adem�s Hipertensi�n, renal"
�� periarteritis nodosa 	405.09	405.19	405.99	
�� pielonefritis	405.09	405.19	405.99	
�� policitemia	405.09	405.19	405.99	
�� poliomielitis bulbar 	405.09	405.19	40599	
�� porfiria 	405.09	405.19	405.99	
�� renal (arteria)				
��� aneurisma	405.01	405.11	405.91	
��� anomal�a 	405.01	405.11	405.91	
��� embolismo	405.01	405.11	405.99	
��� estenosis	405.01	405.11	405.99	
��� hiperplasia fibromuscular 	405.01	405.11	405.99	
��� oclusi�n 	405.01	405.11	405.99	
��� trombosis	405.01	405.11	405.99	
�� rin�n poliqu�stico 	405.09	409.19	405.99	
�� tumor cerebral 	405.09	405.19	405.99	
� encefalopat�a	437.2	437.2	437.2	
� enfermedad cardiovascular (arterioesclerosis) (esclerosis) 	402.00	402.10	402.90	
�� con				
��� complicaci�n renal (trastorno clasificable en 403) 	404.00	404.10	404.90	"v�ase adem�s Hipertensi�n, cardiorrenal "
�� insuficiencia cardiaca (congestiva) 	402.01	402.11	402.91	
� enfermedad cerebrovascular NEOM	437.2	437.2	437.2	
� gestacional (transitoria) NEOM 	�	�	642.3	
"� intracraneal, benigna "	�	348.2		
� intraocular	�	�	365.04	
� necrotizante	401.0			
� ocular	�	�	365.04	
� portal (debido a enfermedad cr�nica del h�gado)	_	�	572.3	
� psic�gena		�	306.2	
"� puerperal, postpartum"				"v�ase Hipertensi�n, complicaci�n del embarazo, parto o puerperio pulmonar"
� pulmonar (arteria) (idiop�tica) (primaria) (solitaria)	_	_	416.0	
�� con cor pulmonale (cr�nico) 	�	�	416.8	
��� agudo	�	�	415.0	
�� secundario	�	�	416.8	
� renal (enfermedad) 	403.00	403.10	403.90	"v�ase adem�s Hipertensi�n, ri��n "
� renal	403.00	403.10	403.90	
�� con				
"��� complicaci�n cardiaca (trastorno clasificable en 428, 429.0, 429.3, 429.8, 429.9 debido a Hipertensi�n)"	404.00	404.10	404.90	
��� hipertensi�n cardiaca (enfermedad) (condici�n clasificable en 402) 	404.00	404.10	404.90	"v�ase adem�s Hipertensi�n, cardiorrenal"
� renovascular NEOM	405.01	405.11	405.91	
� secundaria NEOM	405.09	405.19	40599	
�� debido a				
"��� aldosteronismo, primario"	405.09	405.19	405.99	
��� c�lculos				
���� renales	405.09	405.19	405.99	
���� ureterales	405.09	405.19	40599	
"��� coartaci�n, aorta"	405.09	405.19	405.99	
��� enfermedad de Cushing	405.09	405.19	405.99	
��� feocromocitoma	405.09	405.19	405.99	
��� glomeruloesclerosis 	403.00	403.10	403.90	"v�ase adem�s Hipertensi�n, ri��n"
��� periarteritis nodosa	405.09	405.19	405.99	
��� pielonefritis	405.09	405.19	405.99	
��� policitemia	405.09	405.19	405.99	
��� poliomielitis bulbar	405.09	405.19	405.99	
��� porfiria	405.09	405.19	405.99	
��� renal (arteria)			
���� aneurisma	405.01	405.11	405.91
���� an�mala 	405.01	405.11	405.91
���� embolismo	405.01	405.11	405.91
���� estenosis	405.01	405.11	405.91
���� hiperplasia fibromuscular 	405.01	405.11	405.91
���� oclusi�n	405.01	405.11	405.91
���� trombosis	405.01	405.11	405.91
��� rin�n poliqu�stico 	405.09	405.19	405.99
��� tumor cerebral	405.09	405.19	405.99
� transitoria	�	�	796.2
�� de embarazo	�	�	642.3
