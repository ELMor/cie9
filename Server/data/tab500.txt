� hoja de libro (boca en)	145.8	198.89	230.0	210.4	235.1	239.0	
� hombro NCOC*	195.4	198.89	232.6	229.8	238.8	239.8	
� horquilla vulvar	184.4	198.82	233.3	221.2	236.3	239.5	
� hoz (cerebelar) (cerebral)	192.1	198.4	-	225.2	237.6	239.7	
� hueso (periostio)	170.9	198.5	-	213.9	238.0	239.2	
�� acet�bulo	170.6	198.5	-	213.6	238.0	239.2	
�� acromi�n (ap�fisis)	170.4	198.5	-	213.4	238.0	239.2	
�� antebrazo	170.4	198.5	-	213.6	238.0	239.2	
�� ap�fisis xifoides	170.3	198.5	-	213.3	238.0	239.2	
�� articulaci�n costovertebral	170.3	198.5	-	213.3	238.0	239.2	
�� astr�galo	170.8	198.5	-	213.8	238.0	239.2	
�� atlas	170.2	198.5	-	213.2	238.0	239.3	
�� axis	170.2	198.5	-	213.2	238.0	239.3	
�� brazo NCOC	170.4	198.5	-	213.4	238.0	239.2	
�� cadera	170.6	198.5	-	213.6	238.0	239.2	
�� calc�neo	170.8	198.5	-	213.8	238.0	239.2
�� calvario	170.0	198.5	-	213.0	238.0	239.2
�� cara	170.0	198.5	-	213.0	238.0	239.2
��� maxilar inferior	170.1	198.5	-	213.1	238.0	239.2
�� carpio (cualquiera)	170.5	198.5	-	213.5	238.0	239.2
�� cart�lago (costal)	170.3	198.5	-	213.3	238.0	239.2
�� cart�lago NCOC	170.9	198.5	-	213.9	238.0	239.2
�� cart�lago o disco intervertebral	170.2	198.5	-	213.2	238.0	239.3
�� cigom�tico	170.0	198.5	-	213.0	238.0	239.2
�� clav�cula	170.3	198.5	-	213.3	238.0	239.2
�� clivus	170.0	198.5	-	213.0	238.0	239.2
�� coxis	170.6	198.5	-	213.6	238.0	239.2
�� codo	170.4	198.5	-	213.4	238.0	239.2
�� columna vertebral	170.2	198.5	-	213.2	238.0	239.3
��� c�ccix	170.6	198.5	-	213.6	238.0	239.2
��� sacro	170.6	198.5	-	213.6	238.0	239.2
�� corto	170.9	198.5	-	213.9	238.0	239.2
��� miembro inferior	170.8	198.5	-	213.8	238.0	239.2
��� miembro superior	170.5	198.5	-	213.5	238.0	239.2
�� costilla	170.3	198.5	-	213.3	238.0	239.2
�� craneal	170.0	198.5	-	213.0	238.0	239.2
�� cr�neo	170.0	198.5	-	213.0	238.0	239.2
�� c�bito (cualquier parte)	170.4	198.5	-	213.4	238.0	239.2
�� cuboides	170.8	198.5	-	213.8	238.0	239.2
�� cuneiforme	170.9	198.5	-	213.9	238.0	239.2
��� mu�eca	170.5	198.5	-	213.5	238.0	239.2
��� tobillo	170.8	198.5	-	213.8	238.0	239.2
�� dedo de mano (cualquiera)	170.5	198.5	-	213.5	238.0	239.2
�� dedo de pie (cualquiera)	170.8	198.5	-	213.8	238.0	239.2
�� digital	170.9	198.5	-	213.9	238.0	239.2
��� dedo de mano	170.5	198.5	-	213.5	238.0	239.2
��� dedo de pie	170.8	198.5	-	213.8	238.0	239.2
�� escafoides (mano)	170.5	198.5	-	213.5	238.0	239.2
��� del tobillo 	170.8	198.5	-	213.8	238.0	239.2
�� esc�pula (cualquier parte) 	170.4	198.5	-	213.6	238.0	239.2
�� esfenoides 	170.4	198.5	-	213.0	238.0	239.2
�� espalda NCOC	170.2	198.5	-	213.2	238.0	239.3
"�� esqueleto, esquel�tico NCOC"	170.9	198.5	-	213.9	238.0	239.2
�� estern�n 	170.3	198.5	-	213.9	238.0	239.2
�� etmoides (laber�ntico)	170.0	198.5	-	213.0	238.0	239.2
�� falanges	170.9	198.5	-	213.9	238.0	239.2
��� mano 	170.5	198.5	-	213.5	238.0	239.2
��� pie 	170.8	198.5	-	213.8	238.0	239.2
�� f�mur (cualquier parte)	170.7	198.5	-	213.7	238.0	239.2
�� frontal	170.0	198.5	-	213.0	238.0	239.2
�� hioides 	170.0	198.5	-	213.0	238.0	239.2
�� hombro	170.4	198.5	-	213.4	238.0	239.2
�� h�mero (cualquier parte)	170.4	198.5	-	213.4	238.0	239.2
�� ilion	170.6	198.5	-	213.6	238.0	239.2
�� innominado	170.6	198.5	-	213.6	238.0	239.2
�� isquion 	170.6	198.5	-	213.6	238.0	239.2
�� largo	170.9	198.5	-	213.9	238.0	239.2
��� miembro inferior NCOC	170.7	198.5	-	213.7	238.0	239.2
��� miembro superior NCOC	170.4	198.5	-	213.4	238.0	239.2
�� malar 	170.0	198.5	-	213.0	238.0	239.2
�� mand�bula 	170.1	198.5	-	213.1	238.0	239.2
�� mand�bula (inferior)	170.1	198.5	-	213.1	238.0	239.2
��� superior	170.0	198.5	-	213.0	238.0	239.2
�� mano 	170.5	198.5	-	213.5	238.0	239.2
�� mastoides 	170.0	198.5	-	213.0	238.0	239.2
"�� maxila, maxilar (superior) "	170.0	198.5	-	213.0	238.0	239.2
��� inferior	170.1	198.5	-	213.1	238.0	239.2
�� m�dula �sea 	202.9	198.5	-	238.7	238.0	239.2
�� metacarpo (cualquiera)	170.5	198.5	-	213.5	238.0	239.2
�� metatarso (cualquiera)	170.8	198.5	-	213.8	238.0	239.2
�� miembro NCOC	170.9	198.5	-	213.9	238.0	239.2
��� inferior (huesos largos)	170.7	198.5	-	213.7	238.0	239.2
���� huesos cortos	170.8	198.5	-	213.7	238.0	239.2
��� superior (huesos largos)	170.4	198.5	-	213.4	238.0	239.2
���� huesos cortos	170.8	198.5	-	213.5	238.0	239.2
�� mu�eca 	170.5	198.5	-	213.5	238.0	239.2
"�� nariz, nasal"	170.0	198.5	-	213.0	238.0	239.2
�� navicular (tobillo) 	170.8	198.5	-	213.8	238.0	239.2
�� mano 	170.5	198.5	-	213.5	238.0	239.2
�� occipital 	170.0	198.5	-	213.0	238.0	239.2
�� �rbita	170.0	198.5	-	213.0	238.0	239.2
�� parietal 	170.0	198.5	-	213.0	238.0	239.2
�� pelviano 	170.6	198.5	-	213.6	238.0	239.2
�� peron� (cualquier parte) 	170.7	198.5	-	213.7	238.0	239.2
�� pie	170.8	198.5	-	213.8	238.0	239.2
�� pierna NCOC	170.7	198.5	-	213.7	238.0	239.2
�� pubis	170.6	198.5	-	213.6	238.0	239.2
�� pulgar 	170.5	198.5	-	213.5	238.0	239.2
"�� radio (cualquier parte),"	170.4	198.5 	-	213.4	238.0	239.2
�� rodilla 	170.7	198.5	-	213.7	238.0	239.2
�� r�tula	170.8	198.5	-	213.8	238.0	239.2
�� sacro	170.6	198.5	-	213.6	238.0	239.2
�� silla turca	170.9	198.5	-	213.9	238.0	239.2
�� tal�n	170.8	198.5	-	213.8	238.0	239.2
�� tarso (cualquier parte)	170.8	198.5	-	213.8	238.0	239.2
�� temporal	170.0	198.5	-	213.0	238.0	239.2
�� tibia (cualquier parte)	170.7	198.5	-	213.7	238.0	239.2
�� tobillo	170.8	198.5	-	213.8	238.0	239.2
�� trapecio	170.5	198.5	-	213.5	238.0	239.2
�� trapezoide	170.5	198.5	-	213.5	238.0	239.2
�� turbinado	170.0	198.5	-	213.0	238.0	239.2
�� uniforme	170.5	198.5	-	213.5	238.0	239.2
�� v�rtebra cocc�gea	170.6	198.5	-	213.6	238.0	239.2
�� v�rtebra sacra	170.6	198.5	-	213.6	238.0	239.2
�� v�rtebra (columna)	170.2	198.5	-	213.2	238.0	239.3
��� c�ccix	170.6	198.5	-	213.6	238.0	239.2
��� sacro	170.6	198.5	-	213.6	238.0	239.2
�� v�mer	170.0	198.5	-	213.0	238.0	239.2
�� zigom�tico	145.8	198.89	230.0	210.4	235.1	239.0
� h�mero (cualquier parte)	170.4	198.5	-	213.6	238.0	239.2
� ileocecal (ap�ndice) (uni�n) (v�lvula)	153.4	197.5	230.3	211.3	235.2	239.0
� �leon	152.2	197.4	230.7	211.2	235.2	239.0
� ilion	170.6	198.5	-	213.6	238.0	239.2
� infraclavicular (regi�n)*	195.1	198.89	232.5	229.8	238.8	239.8
� ingle NCOC*	195.3	198.89	232.5	229.8	238.8	239.8
� inguinal (regi�n)*	195.3	198.89	232.5	229.8	238.8	239.8
� inmunoproliferativa NCOC	203.8	-	-	-	-	-
� �nsula	191.0	198.3	-	225.0	237.5	239.6
� interno						
�� c�psula	191.0	198.3	-	225.0	237.5	239.6
�� orificio (cuello uterino)	180.0	198.82	233.1	219.0	236.0	239.5
"� intestino, intestinal"	159.0	197.8	230.7	211.9	235.2	239.0
�� delgado	152.9	197.4	230.7	211.2	235.2	239.0
��� duodeno	152.0	197.4	230.7	211.2	235.2	239.0
��� �leon	152.2	197.4	230.7	211.2	235.2	239.0
��� sitios contiguos	152.8	-	-	-	-	-
��� yeyuno	152.1	197.4	230.7	211.2	235.2	239.0
�� grueso	159.0	197.8	230.7	211.3	235.2	239.0
��� ap�ndice	153.5	197.5	230.3	211.3	235.2	239.0
��� cabeza del colon	153.4	197.5	230.3	211.3	235.2	239.0
��� ciego	153.4	197.5	230.3	211.3	235.2	239.0
��� colon	153.9	197.5	230.3	211.3	235.2	239.0
���� ascendente	153.6	197.5	230.3	211.3	235.2	239.0
���� cabeza	153.4	197.5	230.3	211.3	235.2	239.0
���� derecho	153.6	197.5	230.3	211.3	235.2	239.0
���� descendente	153.2	197.5	230.3	211.3	235.2	239.0
���� distal	153.2	197.5	230.3	211.3	235.2	239.0
���� izquierdo	153.2	197.5	230.3	211.3	235.2	239.0
���� pelviano	153.3	197.5	230.3	211.3	235.2	239.0
���� sigmoideo (flexura)	153.3	197.5	230.3	211.3	235.2	239.0
���� sitios contiguos	153.8	-	-	-	-	-
���� transversal	153.1	197.5	230.3	211.3	235.2	239.0
���� y recto	154.0	197.5	230.4	211.4	235.2	239.0
��� flexura espl�nica	153.7	197.5	230.3	211.3	235.2	239.0
��� flexura hep�tica 	153.0	197.5	230.3	211.3	235.2	239.0
��� ileocecal (ap�ndice) (v�lvula)	153.4	197.5	230.3	211.3	235.2	239.0
��� sigmoideo (flexura)	153.3	197.5	230.3	211.3	235.2	239.0
��� sitios contiguos	153.8	-	-	-	-	-
�� tracto NCOC	159.0	197.8	230.7	211.9	235.2	239.0
� intraabdominal	195.2	198.89	234.8	229.8	238.8	239.8	
� intracraneal NCOC	191.9	198.3	-	225.0	237.5	239.6	
� intraocular	190.0	198.4	234.0	224.0	238.8	239.8	
� intraorbital	190.1	198.4	234.0	224.1	238.8	239.8	
� intraselar	194.3	198.89	234.8	227.3	237.0	239.7	
� intrator�cica (cavidad) (�rganos NCOC)	195.1	198.89	234.8	229.8	238.8	239.8	
�� sitios contiguos con �rganos respiratorios	165.8	-	-	-	-	-	
� iris	190.0	198.4	234.0	224.0	238.8	239.8	
� isla o �nsula de Reil	191.0	198.3	-	225.0	237.5	239.6	
� islas o islotes de Langerhans	157.4	197.8	230.0	211.7	235.5	239.0	
� isquion	170.6	198.5	-	213.6	238.0	239.2	
� isquiorrectal (fosa)	195.3	198.89	234.8	229.8	238.8	239.9	
� istmo del �tero	182.1	198.82	233.2	219.1	236.0	239.5	
� labial (boca)							"v�ase adem�s Neoplasia, labio"
�� surco (inferior) (superior)	145.1	198.89	230.0	210.4	235.1	239.0	
� labio (boca) (mucosa externa) (reborde bermelI�n)	140.9	198.89	230.0	210.0	235.1	239.0	
�� cara bucal							"v�ase Neoplasia, labio, interno"
�� cara interna							"v�ase Neoplasia, labio interno"
�� cara oral							"v�ase Neoplasia, labio"
�� comisura	140.6	198.89	230.0	210.4	235.1	239.0	
�� frenillo							"v�ase Neoplasia, labio, interno"
�� inferior	140.1	198.89	230.0	210.0	235.1	239.0	
��� interno (bucal) (frenillo) (mucosa) (oral)	140.4	198.89	230.0	210.0	235.1	239.0	
�� interno (bucal) (frenillo) (mucosa) (oral)	140.5	198.89	230.0	210.0	235.1	239.0	
��� inferior	140.4	198.89	230.0	210.0	235.1	239.0	
��� superior	140.3	198.89	230.0	210.0	235.1	239.0	
�� mucosa							"v�ase Neoplasia, labio, interna"
�� piel (comisura) (inferior) (superior)	173.0	198.2	232.0	216.0	238.2	239.2	
�� sitios contiguos	140.8	-	-	-	-	-	
��� con cavidad oral o faringe	149.8	-	-	-	-	-	
�� superior	140.0	198.89	230.0	210.0	235.1	239.0	
��� interno (bucal) (frenillo) (mucosa) (oral)	140.3	198.89	230.0	210.0	235.1	239.0	
� labio(s) vulvares (piel)	184.4	198.82	233.3	221.2	236.3	239.5
�� mayores	184.1	198.82	233.3	221.2	236.3	239.5
�� menores	184.2	198.82	233.3	221.2	236.3	239.5
� lagrimal(es)	-	-	-	-	-	-
�� canal�culos	190.7	198.4	234.0	224.7	238.8	239.8
�� conducto (nasal)	190.7	198.4	234.0	224.7	238.8	239.8
�� gl�ndula	190.2	198.4	234.0	224.2	238.8	239.8
�� punto	190.7	198.4	234.0	224.7	238.8	239.8
�� saco	190.7	198.4	234.0	224.7	238.8	239.8
"� Langerhans, islas o islotes"	157.4	197.8	230.9	211.7	235.6	239.1
"� laringe, lar�ngea NCOC"	161.9	197.3	231.0	212.1	235.6	239.1
�� banda ventricular	161.1	197.3	231.0	212.1	235.6	239.1
�� comisura (anterior) (posterior)	161.0	197.3	231.0	212.1	235.6	239.1
�� extr�nseca NCOC	161.1	197.3	231.0	212.1	235.6	239.1
��� cuando significa hipofaringe	148.9	198.89	230.0	210.8	235.1	239.0
�� intr�nseca	161.0	197.3	231.0	212.1	235.6	239.1
�� pliegue ariepigl�tico (cricoideo) (cuneiforme) (tiroideo)	161.3	197.3	231.0	212.1	235.6	239.1
�� pliegue interaritenoideo	161.1	197.3	231.0	212.1	235.6	239.1
�� sitios contiguos	161.8	-	-	-	-	-
� laringofaringe	148.9	198.89	230.0	210.8	235.1	239.0
� lengua	141.9	198.89	230.0	210.1	235.1	239.0
�� am�gdala	141.6	198.89	230.0	210.1	235.1	239.0
�� anterior (dos tercios) NCOC	141.4	198.89	230.0	210.1	235.1	239.0
��� superior dorsal	141.1	198.89	230.0	210.1	235.1	239.0
��� superior ventral	141.3	198.89	230.0	210.1	235.1	239.0
�� base (superficie dorsal)	141.0	198.89	230.0	210.1	235.1	239.0
"�� dorsal, superficie NCOC"	141.1	198.89	230.0	210.1	235.1	239.0
�� far�ngea	147.1	198.89	230.0	210.7	235.1	239.0
"�� fauces, faucial"	146.0	198.89	230.0	210.5	235.1	239.0
�� foramen ciego	141.1	198.89	230.0	210.1	235.1	239.0
�� frenillo	141.3	198.89	230.0	210.1	235.1	239.0
�� I�nea media NCOC	141.1	198.89	230.0	210.1	235.1	239.0
�� lingual	141.6	198.89	230.0	210.1	235.1	239.0	
�� m�rgen (lateral)	141.2	198.89	230.0	210.1	235.1	239.0	
�� palatina	146.0	198.89	230.0	210.5	235.1	239.0	
�� parte fija NCOC	141.0	198.89	230.0	210.1	235.1	239.0	
�� parte m�vil NCOC	141.4	198.89	230.0	210.1	235.1	239.0	
��� base	141.0	198.89	230.0	210.1	235.1	239.0	
��� ventral	141.3	198.89	230.0	210.1	235.1	239.0	
�� pilares (anterior)(posterior)	146.2	198.89	230.0	210.6	235.1	239.0	
�� punta	141.2	198.89	230.0	210.1	235.1	239.0	
�� sitios contiguos	141.8						
�� zona de uni�n	141.5	198.89	230.0	210.1	235.1	239.0	
� lente o cristalino	190.0	198.4	234.0	224.0	238.8	239.8	
� ligamento ancho	183.3	198.82	233.3	221.0	236.3	239.5	
� ligamento							"v�ase adem�s Neoplasia, tejido conjuntivo"
�� ancho	183.3	198.82	233.3	221.0	236.3	239.5	
"�� Mackkenrodt, de"	183.8	198.82	233.3	221.8	236.3	239.5	
�� miembro*	195.8	198.89	232.8	229.8	238.8	239.8	
��� inferior*	195.5	198.89	232.7	229.8	238.8	239.8	
��� superior*	195.4	198.89	232.6	229.8	238.8	239.8	
�� no uterino	183.5	198.82	-	221.0	236.3	239.5	"v�ase Neoplasia, tejido conjuntivo redondo"
�� sacrouterino	183.4	198.82	-	221.0	236.3	239.5	
�� uterino	183.4	198.82	-	221.0	236.3	239.5	
�� uteroov�rico	183.8	198.82	233.3	221.8	236.3	239.5	
�� uterosacro	183.4	198.82	-	221.0	236.3	239.5	
� limbo de la c�rnea	190.4	198.4	234.0	224.4	238.8	239.8	
� linf�tico							
�� canal NCOC	171.9	198.89	-	215.9	238.1	239.2	"v�ase adem�s, Neoplasia, tejido conjuntivo"
�� ganglio (secundaria)	-	196.9	-	229.0	238.8	239.8	
��� abdominal 	-	196.0	-	229.0	238.8	239.8	
��� a�rtico 	-	196.2	-	229.0	238.8	239.8	
��� auricular (anterior) (posterior) 	-	196.0	-	229.0	238.8	239.8	
"��� axila, axilar "	-	196.3	-	229.0	238.8	239.8	
��� branquial 	-	196.3	-	229.0	238.8	239.8
��� brazo 	-	196.3	-	229.0	238.8	239.8
��� broncopulmonar 	-	196.1	-	229.0	238.8	239.8
��� bronquial	-	196.1	-	229.0	238.8	239.8
��� cabeza	-	196.0	-	229.0	238.8	239.8
��� cara	-	196.0	-	229.0	238.8	239.8
��� cel�aco	-	196.2	-	229.0	238.8	239.8
��� cervical	-	196.0	-	229.0	238.8	239.8
��� cervicofacial	-	196.0	-	229.0	238.8	239.8
"��� Cloquet, de"	-	196.5	-	229.0	238.8	239.8
��� c�lico	-	196.2	-	229.0	238.8	239.8
��� conducto com�n	-	196.2	-	229.0	238.8	239.8
��� cubital	-	196.3	-	229.0	238.8	239.8
��� cuello	-	196.0	-	229.0	238.8	239.8
��� diafragm�tico	-	196.1	-	229.0	238.8	239.8
��� epig�strico inferior	-	196.6	-	229.0	238.8	239.8
��� epitroclear	-	196.3	-	229.0	238.8	239.8
��� escaleno	-	196.0	-	229.0	238.8	239.8
��� esof�gico	-	196.1	-	229.0	238.8	239.8
��� espl�nico	-	196.2	-	229.0	238.8	239.8
��� femoral	-	196.5	-	229.0	238.8	239.8
��� g�strico	-	196.2	-	229.0	238.8	239.8
��� hep�tico	-	196.2	-	229.0	238.8	239.8
��� hilar (pulmonar)	-	196.1	-	229.0	238.8	239.8
���� espl�nico	-	196.2	-	229.0	238.8	239.8
��� hipog�strico	-	196.6	-	229.0	238.8	239.8
��� ileoc�lico	-	196.2	-	229.0	238.8	239.8
��� il�aco	-	196.6	-	229.0	238.8	239.8
"��� inferior, miembro"	-	196.5	-	229.0	238.8	239.8
��� infraclavicular	-	196.3	-	229.0	238.8	239.8
��� inguinal	-	196.5	-	229.0	238.8	239.8
��� ingle	-	196.5	-	229.0	238.8	239.8
��� innominado	-	196.1	-	229.0	238.8	239.8
��� intercostal	-	196.1	-	229.0	238.8	239.8
��� intestinal	-	196.2	-	229.0	238.8	239.8
��� intraabdominal	-	196.2	-	229.0	238.8	239.8
��� intrapelviano	-	196.6	-	229.0	238.8	239.8
��� intrator�cico	-	196.1	-	229.0	238.8	239.8
��� lumbar	-	196.2	-	229.0	238.8	239.8
��� mediast�nico	-	196.1	-	229.0	238.8	239.8
��� mesent�rico (inferior) (superior)	-	196.2	-	229.0	238.8	239.8
��� mesoc�lico 	-	196.2	-	229.0	238.8	239.8
��� miembro						
���� inferior	-	196.5	-	229.0	238.8	239.8
���� superior	-	196.3	-	229.0	238.8	239.8
��� m�ltiples sitios en las categor�as 196.0-196.6	-	196.9	-	229.0	238.8	239.8
��� obturador		196.6	-	229.0	238.8	239.8
��� occipital	-	196.0	-	229.0	238.8	239.8
��� pancre�tico	-	196.2	-	229.0	238.8	239.8
��� paracervical	-	196.6	-	229.0	238.8	239.8
��� parametrial	-	196.6	-	229.0	238.8	239.8
��� para�rtico	-	196.2	-	229.0	238.8	239.8
��� parasternal	-	196.1	-	229.0	238.8	239.8
��� par�tido	-	196.0	-	229.0	238.8	239.8
��� pectoral	-	196.3	-	229.0	238.8	239.8
��� pelviano	-	196.6	-	229.0	238.8	239.8
��� peria�rtico	-	196.2	-	229.0	238.8	239.8
��� peripancre�tico 	-	196.2	-	229.0	238.8	239.8
��� pierna 	-	196.5	-	229.0	238.8	239.8
��� pil�rico 	-	196.2	-	229.0	238.8	239.8
��� popl�teo 	-	196.5	-	229.0	238.8	239.8
��� portahep�tico 	-	196.2	-	229.0	238.8	239.8
��� portal 	-	196.2	-	229.0	238.8	239.8
��� preauricular 	-	196.0	-	229.0	238.8	239.8
��� prelar�ngeo 	-	196.0	-	229.0	238.8	239.8
��� presinfisial 	-	196.6	-	229.0	238.8	239.8
��� pretraqueal 	-	196.0	-	229.0	238.8	239.8
��� primaria (cualquier sitio)	202.9	-	-	-	-	-
��� pulmonar (hilar)	-	196.1	-	229.0	238.8	239.8
��� retrofar�ngeo	-	196.0	-	229.0	238.8	239.8
��� retroperitoneal 	-	196.2	-	229.0	238.8	239.8
��� Rosenmuller de 	-	196.5	-	229.0	238.8	239.8
��� sacro 	-	196.6	-	229.0	238.8	239.8
��� sitio NCOC 	-	196.9	-	229.0	238.8	239.8
��� subclavicular 	-	196.3	-	229.0	238.8	239.8
��� subinguinal 	-	196.5	-	229.0	238.8	239.8
��� sublingual 	-	196.0	-	229.0	238.8	239.8
��� submandibular 	-	196.0	-	229.0	238.8	239.8
��� submaxilar 	-	196.0	-	229.0	238.8	239.8
��� submental 	-	196.0	-	229.0	238.8	239.8
��� subscapular 	-	196.3	-	229.0	238.8	239.8	
"��� superior, miembro "	-	196.3	-	229.0	238.8	239.8	
��� supraclavicular 	-	196.0	-	229.0	238.8	239.8	
��� tibial 	-	196.5	-	229.0	238.8	239.8	
��� tor�cico 	-	196.1	-	229.0	238.8	239.8	
��� traqueal 	-	196.1	-	229.0	238.8	239.8	
��� traqueobronquial 	-	196.1	-	229.0	238.8	239.8	
"��� Virchow, de "	-	196.0	-	229.0	238.8	239.8	
��� yugular 	-	196.0	-	229.0	238.8	239.8	
�� n�dulo							"v�ase adem�s Neoplasia, ganglio linf�tico"
��� primario NCOC	202.9	-	-	-	-	-	
�� vaso	171.9	198.89	-	215.9	238.1	239.2	"v�ase adem�s Neoplasia, tejido conjuntivo"
� lingual NCOC	141.9	198.89	230.0	210.1	235.1	239.0	"v�ase adem�s Neoplasia, lengua"
� I�ngula del pulm�n	162.3	197.0	231.2	212.3	235.7	239.1	
� I�bulo							
�� �cigos	162.3	197.0	231.2	212.3	235.7	239.1	
�� frontal	191.1	198.3	-	225.0	237.5	239.6	
�� inferior	162.5	197.0	231.2	212.3	235.7	239.1	
�� medio	162.4	197.0	231.2	212.3	235.7	239.1	
"�� occipital, cerebro"	191.4	198.3	-	225.0	237.5	239.6	
�� parietal	191.3	198.3	-	225.0	237.5	239.6	
�� superior	162.3	197.0	231.2	212.3	235.7	239.1	
�� temporal	191.2	198.3	-	225.0	237.5	239.6	
� I�bulo de la oreja	173.2	198.2	232.2	216.2	238.2	239.2	
"� Mackerondt, ligamento de"	183.8	198.82	233.3	221.8	236.3	239.5	
� malar	170.0	198.5	-	213.0	238.0	239.2	
�� regi�n							"v�ase Neoplasia, mejilla"
� mama (femenina) (partes blandas) (tejido conjuntivo) (tejido glandular)	174.9	198.81	233.0	217	238.3	239.3	
�� ar�ola	174.0	198.81	233.0	217	238.3	239.3	
��� masculina	175.0	198.81	233.0	217	238.3	239.3	
�� cuadrante							
��� externo							
���� inferior	174.5	198.81	233.0	217	238.3	239.3
���� superior	174.4	198.81	233.0	217	238.3	239.3
��� interno						
���� inferior	174.3	198.81	233.0	217	238.3	239.3
���� superior	174.2	198.81	233.0	217	238.3	239.3
�� externa	174.8	198.81	233.0	217	238.3	239.3
�� inferior	174.8	198.81	233.0	217	238.3	239.3
�� interna	174.8	198.81	233.0	217	238.3	239.3
�� I�nea media	174.8	198.81	233.0	217	238.3	239.3
�� masculina	175.9	198.81	233.0	217	238.3	239.3
��� ar�ola	175.0	198.81	233.0	217	238.3	239.3
��� pez�n	175.0	198.81	233.0	217	238.3	239.3
��� tejido ect�pico	175.9	198.81	233.0	217	238.3	239.3
�� pez�n	174.0	198.81	233.0	217	238.3	239.3
��� masculino	175.0	198.81	233.0	217	238.3	239.3
�� piel	173.5	198.81	233.0	217	238.3	239.3
�� porci�n central	174.6	198.81	233.0	217	238.3	239.3
�� prolongaci�n (axilar)	174.6	198.81	233.0	217	238.3	239.3
�� sitio de mastectom�a (piel) (secundaria) 	-	198.2	-	-	-	-
��� especificada como tejido mamario 	-	198.81	-	-	-	-
�� sitios contiguos	174.8	-	-	-	-	-
�� sitios ect�picos	174.8	198.81	233.0	217	238.2	239.3
�� superior	174.8	198.81	233.0	217	238.3	239.3
� mand�bula	170.1	198.5	-	213.1	238.0	239.2
�� alveolar						
��� mucosa	143.1	198.89	230.0	210.4	235.1	239.0
��� reborde o ap�fisis	170.1	198.5	-	213.1	238.0	239.2
���� carcinoma	143.1	-	-	-	-	-
�� carcinoma	143.1	-	-	-	-	-
� mandibular	195.0	198.89	234.8	229.8	238.8	239.8
�� hueso	170.1	198.5	-	213.1	238.0	239.2
��� carcinoma	143.9	-	-	-	-	-
���� inferior	143.1	-	-	-	-	-	
���� superior	143.0	-	-	-	-	-	
�� carcinoma (cualquier tipo) (inferior) (superior)	195.0	-	-	-	-	-	
� mano NCOC*	195.4	198.89	232.6	229.8	238.8	239.8	
� mastoideo (antro) (cavidad) (c�lulas de aire)	160.1	197.3	231.8	212.0	235.9	239.1	
�� hueso o ap�fisis	170.0	198.5	-	213.0	238.0	239.2	
� matriz							"v�ase Neoplasia, �tero"
"� maxila, maxilar (superior)"	170.0	198.5	-	213.0	238.0	239.2	
�� alveolar							
��� mucosa	143.0	198.89	230.0	210.4	235.1	239.0	
��� reborde o ap�fisis	170.0	198.5	-	213.0	238.0	239.2	
���� carcinoma	143.0	-	-	-	-	-	
�� antro	160.2	197.3	231.8	212.0	235.9	239.1	
�� carcinoma	143.0	-	-	-	-	-	
�� inferior							"v�ase Neoplasia, mand�bula"
�� seno	160.2	197.3	231.8	212.0	235.9	239.1	
� meato externo (o�do)	173.2	198.2	232.2	216.2	238.2	239.2
"� Meckel, divert�culo de"	152.3	197.4	230.7	211.2	235.2	239.0
"� mediastino, mediast�nico"	164.9	197.1	-	212.5	235.8	239.8
�� anterior	164.2	197.1	-	212.5	235.8	239.8
�� posterior	164.3	197.1	-	212.5	235.8	239.8
�� sitios contiguos con coraz�n y timo	164.8	-	-	-	-	-
� m�dula						
�� oblongada	191.7	198.3	-	225.0	237.5	239.6
�� suprarrenal	194.0	198.7	234.8	227.0	237.2	239.7
� m�dula espinal (cervical) (lumbar) (tor�cica)	192.2	198.3	-	225.3	237.5	239.7
� m�dula (�sea) NCOC	202.9	198.5	-	-	-	238.7
"� Meibomio, gl�ndula de"	173.1	198.2	232.1	216.1	238.2	239.2
� mejilla	195.0	198.89	234.8	229.8	238.8	239.8
�� cara interna	145.0	198.89	230.0	210.4	235.1	239.0
�� externa	173.3	198.2	232.3	216.3	238.2	239.2
�� interna	145.0	198.89	230.0	210.4	235.1	239.0
�� mucosa	145.0	198.89	230.0	210.4	235.1	239.0	
� membrana fetal	181	198.82	233.2	219.8	236.1	239.5	
� membrana sinovial							"v�ase Neoplasia, tejido conjuntivo"
� meninges (cerebro) (cerebral) (craneal) (intracraneal)	192.1	198.4	-	225.2	237.6	239.7	
�� espinal (m�dula)	192.3	198.4	-	225.4	237.6	239.7	
"� menisco, articulaci�n de la rodilla (lateral) (medio)"	170.7	198.5	-	213.7	238.0	239.2	
� ment�n	173.3	198.2	232.3	216.3	238.2	239.2	
� mesenc�falo	191.7	198.3	-	225.0	237.5	239.6	
"� mesenterio, mesent�rico"	158.8	197.6	-	211.8	235.4	239.0	
� mesoap�ndice	158.8	197.6	-	211.8	235.4	239.0	
� mesocolon	158.8	197.6	-	211.8	235.4	239.0	
� mesofaringe							"v�ase Neoplasia, orofaringe"
� mesos�lpinx	183.3	198.82	233.3	221.0	236.3	239.5	
� mesovario	183.3	198.82	233.3	221.0	236.3	239.5	
� metacarpo (cualquier hueso)	170.5	198.5	-	213.5	238.0	239.2	
�� secundario	-	199.1	-	-	-	-	
� metatarso (cualquier hueso)	170.8	198.5	-	213.8	238.0	239.2	
� miembro							
�� inferior	195.5	198.89	232.7	229.8	238.8	239.8	
�� superior	195.4	198.89	232.6	229.8	238.8	239.8	
� miocardio	164.1	198.89	-	212.7	238.8	239.8	
� miometrio	182.0	198.82	233.2	219.1	236.0	239.5	
� miopericardio	164.1	198.89	-	212.7	238.8	239.8	
� monte de Venus (pubis)	184.4	198.82	233.3	221.2	236.3	239.5	
� mucosa							
�� alveolar	143.9	198.89	230.0	210.4	235.1	239.0	
��� inferior	143.1	198.89	230.0	210.4	235.1	239.0	
��� superior	143.0	198.89	230.0	210.4	235.1	239.0	
�� bucal	145.0	198.89	230.0	210.4	235.1	239.0	
�� labio (boca)							"v�ase Neoplasia, labio, interno"
� mejilla	145.0	198.89	230.0	210.4	235.1	239.0	
�� nasal	160.0	197.3	231.8	212.0	235.9	239.1	
� m�ltiples sitios NCOC	199.0	199.0	234.9	229.9	238.9	199.0	
"� M�ller, conducto de femenino"	184.8	198.82	233.3	221.8	236.3	239.5	
�� masculino	187.8	198.82	233.6	222.8	236.6	239.5	
� mu�eca NCOC	195.4	198.89	232.6	229.8	238.8	239.8	
"� mu��n, cervical"	180.8	198.82	233.1	219.0	236.0	239.5	
� m�sculo extraocular	190.1	198.4	234.0	224.1	238.8	239.8	
� m�sculo							"v�ase adem�s Neoplasia, tejido conjuntivo"
�� extraocular	190.1	198.4	234.0	224.1	238.8	239.8	
� muslo NCOC*	195.5	198.89	234.8	229.8	238.8	239.8	
"� Naboth, gl�ndula de (fol�culo)"	180.0	198.82	233.1	219.0	236.0	239.5	
� nalga NCOC*	195.3	198.89	232.5	229.8	238.8	239.8	
� nalgas	173.5	198.2	232.5	216.5	238.2	239.2	
"� nariz, nasal"	195.0	198.89	234.8	229.8	238.8	239.8	
�� aleta (externa)	173.3	198.2	232.3	216.3	238.2	239.2	
�� cart�lago	160.0	197.3	231.8	212.0	235.9	239.1	
�� cavidad	160.0	197.3	231.8	212.0	235.9	239.1	
��� sitios contiguos con seno accesorio u o�do medio	160.8	-	-	-	-	-	
�� coana	147.3	198.89	230.0	210.7	235.1	239.0	
�� externa (piel)	173.3	198.2	232.2	216.3	238.2	239.2	
�� fosa	160.0	197.3	231.8	212.0	235.9	239.1	
�� hueso	170.0	198.5	-	213.0	238.0	239.2	
�� interna	160.0	197.3	231.8	212.0	235.9	239.1	
"�� I�bulo o polo, cerebro"	191.4	198.3	-	225.0	237.5	239.6	
�� piel	173.3	198.2	232.3	216.3	238.2	239.2	
�� seno							"v�ase Neoplasia, seno"
�� turbinado (mucosa)	160.0	197.3	231.8	212.0	235.9	239.1	
��� hueso	170.0	198.5	-	213.0	238.0	239.2	
�� vest�bulo	160.0	197.3	231.8	212.0	235.9	239.1	
� nasal							"v�ase Neoplasia, nariz"
"� nasofaringe, nasofar�ngea"	147.9	198.89	230.0	210.7	235.1	239.0	
� pared	147.9	198.89	230.0	210.7	235.1	239.0	
��� anterior	147.3	198.89	230.0	210.7	235.1	239.0	
��� posterior	147.1	198.89	230.0	210.7	235.1	239.0	
��� superior	147.0	198.89	230.0	210.7	235.1	239.0	
�� sitio especificado NCOC	147.8	198.89	230.0	210.7	235.1	239.0	
�� sitios contiguos	147.8	-	-	-	-	-	
�� suelo	147.3	198.89	230.0	210.7	235.1	239.0	
�� techo	147.0	198.89	230.0	210.7	235.1	239.0	
� nervio ac�stico	192.0	198.4	-	225.1	237.9	239.7	
� nervio aut�nomo o sistema nervioso NCOC	171.9	198.89	-	215.9	238.1	239.2	
� nervio o sistema nervioso simp�tico NCOC	171.9	198.89	-	215.9	238.1	239.2	
� nervio (aut�nomo) (ganglio) (parasimp�tico) (perif�rico) (simp�tico)							"v�ase adem�s Neoplasia, tejido conjuntivo"
�� abducens	192.0	198.4	-	225.1	237.9	239.7	
�� accesorio (espinal)	192.0	198.4	-	225.1	237.9	239.7	
�� ac�stico 	192.0	198.4	-	225.1	237.9	239.7	
�� auditivo	192.0	198.4	-	225.1	237.9	239.7	
�� branquial	171.2	198.89	-	215.2	238.1	239.2	
�� ci�tico	171.3	198.89	-	215.3	238.1	239.2	
�� craneal (cualquiera)	192.0	198.4	-	225.1	237.9	239.7
�� cubital	171.2	198.89	-	215.2	238.1	239.2
�� espinal NCOC	171.9	198.89	-	215.9	238.1	239.2
�� facial	192.0	198.4	-	225.1	237.9	239.7
�� femoral	171.3	198.89	-	215.3	238.1	239.2
