
//Maestros
Global Pal *Dic					 ;		
Global long TamDic		GlobalIni;
Global long TamDicSO	GlobalIni;
Global Ref *Cie					 ;		
Global long TamCie		GlobalIni;
Global long TamCieSO	GlobalIni;
Global char **PalCom			 ;	
Global long NumPCo		GlobalIni;

//Indices ordenados
Global Pal **IndDic_Pal			 ;
Global Ref **IndCie_Ref			 ;

//Archivo de Indice
Global char findciename[128]	 ;

//Puntero a zona de memoria global
Global long base		GlobalIni;

#ifdef UNIX
Global int  shmid		GlobalIni;	//En UNIX, segmento de memoria compartida.
#endif

#undef Global
#undef GlobalIni