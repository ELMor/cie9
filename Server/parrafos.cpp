#include "cie9.h"
#include "search.h"

#define  Global extern
#define  GlobalIni
#include "globals.h"

int FinFrase(char *beg, char *fin ){
	if( isdigit(*(fin-1)) && isdigit(*(fin+1)) )
		return 0;
	return 1;
}

long Parrafos( char *Texto, char **frases){
	//Se hace un parsing de los '.' ';' '.'
	char *stok=".,;:",*saux;
	long ntok=1;

	frases[0]=Texto;
	for(saux=Texto;*saux;saux++)
		if( strchr(stok,*saux) && FinFrase(frases[ntok],saux) ){
			frases[ntok]=saux+1;
			ntok++;
			*saux=0;
			saux++;
		}
	if(!stricmp(frases[ntok-1]," ") || *(frases[ntok-1])==0 )
	   ntok--; 
	return ntok;
}


