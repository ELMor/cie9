
#ifndef __SEARCH_H__
#define __SEARCH_H__

#include "tcp4u.h"

typedef struct _dummyMatch {
	long	Valor;
	long	Veces;
} Match;

void  ThreadProc		(void* lp);
int   SocketTextSearch  (SOCKET CSock);
long  TextSearch		(char *data, Match*, char*f[128], char **sout);
int   SearchRange		(char *s, Pal ***p1, Pal ***p2);
int   SearchSubClas     (char *s, Ref ***p1, Ref ***p2);
int   Search			(char *frase, Match*);
int   cmpMatch			(const void *p1, const void *p2);
long  RefTree			(char *ref, long *genea, long *tamgen);

#define PCTLONGSTRING	(0.8)	// Porcentaje minimo para evitar plurales y g�nero
#define TAMROOTSTRING	4		// Tama�o minimo de la raiz para suponer palabras iguales
#define RETNREF			8		// Numero de referencias a retornar
#define NMAXCOMMANDS	10		// N�mero m�ximo de comandos en la tabla.

#endif 