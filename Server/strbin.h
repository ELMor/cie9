
#ifndef __STRBIN_H__
#define __STRBIN_H__

char *Pack				(char **sout, ...);
long  UnPack			(char *sin, char **sout);
char *GetBody			(char *in, char **out);
long  UnPackAndGetBody	(char *sin, char **sout);

#endif