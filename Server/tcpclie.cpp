
extern "C" {
	#include "tcp4u.h"
	#include <string.h>
	#include <stdlib.h>
	#include <stdio.h>
	#ifdef UNIX
	#define stricmp strcasecmp
	#endif
}

#define PORT		7100
#define SZBUFFER	4*1024

void main(void){
	SOCKET sock;
	unsigned short Puerto=PORT;
	int ret;
	char *Buf=(char*)malloc(SZBUFFER);

	Tcp4uInit();							
	if(	(ret=TcpConnect(&sock,
						getenv("CIE9_HOST") ? getenv("CIE9_HOST") : "159.237.4.120",
						(char*)NULL,
						&Puerto))!=TCP4U_SUCCESS){
		perror("Texto del sistema ");
		printf("Error en la conexion: %s\n",Tcp4uErrorString(ret));
		exit(-1);
	}
	while( printf("\n-->Buscar:") && 
		   fgets(Buf,511,stdin)     	){
		if(!stricmp(Buf,"fin\n")) break;
		//Envio del diagnůstico
		ret=TcpSend(sock,Buf,1+strlen(Buf),0,HFILE_ERROR);
		//Espera de la respuesta
		ret=TcpRecv(sock,Buf,SZBUFFER,60,HFILE_ERROR);
		puts(Buf);
	}
	//Cerrar y salir
	TcpClose(&sock);
	Tcp4uCleanup();
	free(Buf);
}
